# Relatica Change Log

## Version 0.16.0 (beta)

* Changes
* Fixes
    * Fix error where sometimes profiles would display error screen from notifications panel
* New Features

## Version 0.15.0 (beta)

* Changes
    * Search Screen has been replaced with the Explorer Screen, of which Search is a tab
* Fixes
* New Features
    * Explorer Screen that contains tabs for: search, trending, and followed tags
    * Trending data endpoints supporting search for: global trending tags, local trending tags,
      trending links, trending statuses
    * Support for toggling if a tag is being followed or not
    * A tags search result screen that lists posts/comments that follow that tag and allows
      following/unfollowing tags
    * Clicking on a tag defaults to opening up the tags search screen in the app. There is a setting
      to toggle this back to opening up on the user's home server's search screen in the browser.
    * Add Suggested Users on the Explorer Screen
    * Adds User Media screen to the User's Profile viewer
    * Add User Posts and Comments screen to the User Profile viewer

## Version 0.14.0 (beta)

This was a major architectural refactoring behind the scenes. A lot of inconsistency type bugs
should be squashed by it. That will bear out as it is used by real users, however. The notes below
on fixes captures those identified during initial testing. This release will probably require a
little more shakeout than previous ones since almost every panel of the app is affected by the
refactoring change and all the data handling, syncing, etc. in the backend was affected.

* Changes
    * Reduce of the network "chattiness" that was happening to keep Connection statuses more up to
      date.
    * Thumbnail sizes when a post has multiple media attachments increased slightly
* Fixes
    * Timeline inconsistency issues (posts from the wrong timeline showing up) have been fixed
* New Features
    * User can now set the timeout for network requests in case their server regularly needs more
      time.
    * ALT text buttons now appear in timeline views not just in gallery views of images
    * Users can block someone directly from a post/comment's drop down menu

## Version 0.13.0 (beta)

* Changes
    * Removed post's borders to give a little extra room and added Dividers between posts in
      timelines
    * Removed unnecessary padding from Post/Comment Viewer screen
* Fixes
* New Features
    * HTML Embedded images honor low bandwidth settings

## Version 0.12.0 (beta)

* Changes
    * Use the network data in the friendica extensions rather than the application data if available
      since the app data
      is the app name. ([Feature #106](https://gitlab.com/mysocialportal/relatica/-/issues/106))
* Fixes
    * Fixes final column of network settings getting chopped off on smaller
      screens ([Issue #102](https://gitlab.com/mysocialportal/relatica/-/issues/102))
    * Only circles now appear in the Profile view for adding users to a circle. Previously groups
      and channels showed up
      there too which was
      nonsensical. ([Issue #103](https://gitlab.com/mysocialportal/relatica/-/issues/103))
    * Fix broken embedded images in message HTML when servers are running block post post-Friendica
      2024.08.([Issue #105](https://gitlab.com/mysocialportal/relatica/-/issues/105))
    * Fix broken Diaspora links when using "Copy URL" or "Open in
      Browser".([Issue #109](https://gitlab.com/mysocialportal/relatica/-/issues/109))
* New Features
    * Focus Mode with ability to set duration and difficulty for unlocking beforehand. Focus mode in
      this release shows
      only the "My Posts" timeline and no bottom navigation bar. So one can't get to search,
      notifications,
      contacts, etc. One can create new posts, comments, and edits to those things
      though. ([Feature #95](https://gitlab.com/mysocialportal/relatica/-/issues/95))
    * Add resharing of Bluesky posts if connected to a Friendica server running 2024.08 or
      later. ([Feature #107](https://gitlab.com/mysocialportal/relatica/-/issues/107))
    * Shows the Circles, Groups, and Channels timelines if connected to a Friendica server running
      2024.08 or
      later.([Feature #108](https://gitlab.com/mysocialportal/relatica/-/issues/108))

## Version 0.11.0 (beta)

* Changes
    * Connection request notifications won't have timestamps and can't be marked read. They go away
      when they are
      adjudicated. ([Issue #76](https://gitlab.com/mysocialportal/relatica/-/issues/76))
    * Add more explicit text and catch when a user tries to use an email address rather than
      username@servername if
      using username/password
      login. ([Issue #17](https://gitlab.com/mysocialportal/relatica/-/issues/17))
* Fixes
    * Fixes Unlisted posts are showing as and sharing as
      private ([Issue #78](https://gitlab.com/mysocialportal/relatica/-/issues/78))
    * Fixes trailing CW type ([Issue #90](https://gitlab.com/mysocialportal/relatica/-/issues/90))
    * Fix Threads open status in browser
      errors ([Issue #87](https://gitlab.com/mysocialportal/relatica/-/issues/87))
    * Fix Bluesky open status in browser
      errors ([Issue #79](https://gitlab.com/mysocialportal/relatica/-/issues/79))
    * Fix Threads profiles don't open in browser
      properly ([Issue #89](https://gitlab.com/mysocialportal/relatica/-/issues/89))
    * Fix Bluesky profiles don't open in browser
      properly ([Issue #98](https://gitlab.com/mysocialportal/relatica/-/issues/98))
    * Fixes being able to search Threads and Bluesky profiles and posts on the search
      page ([Issue #92](https://gitlab.com/mysocialportal/relatica/-/issues/92))
    * Multiple profiles from the same server now works again. Affected users have to use the new "
      Clear All" button to
      clear out existing credentials and re-add them all to fix
      though.  ([Feature #72](https://gitlab.com/mysocialportal/relatica/-/issues/72))
    * Fix empty profiles and/or sometimes lack of bidirectional contact data by always pulling
      profile data on refresh
      requests and adding explicit redraw of panel after user requests
      refresh ([Issue #36](https://gitlab.com/mysocialportal/relatica/-/issues/36), [Issue #62](https://gitlab.com/mysocialportal/relatica/-/issues/62),[Issue #70](https://gitlab.com/mysocialportal/relatica/-/issues/70))
    * Fix bug where read notifications would never load if there were pending connection requests or
      unread
      DMs ([Issue #101](https://gitlab.com/mysocialportal/relatica/-/issues/101))

* New Features
    * Shows the network of the
      post/comment ([Feature #82](https://gitlab.com/mysocialportal/relatica/-/issues/82))
    * User configurable ability to limit reacting to, commenting on, or resharing posts by network
      type([Feature #93](https://gitlab.com/mysocialportal/relatica/-/issues/93))
    * Notifications are grouped by type, starting with mentions, within the unread and read
      groupings of the
      notification list. Defaults to on by default but can be toggled off in
      settings.([Feature #65](https://gitlab.com/mysocialportal/relatica/-/issues/65))
    * Ability to turn off Spoiler Alert/CWs at the application
      level. Defaults to
      on.  ([Feature #42](https://gitlab.com/mysocialportal/relatica/-/issues/42))
    * Throws a confirm dialog box up if adding a comment to a post/comment over 30 days
      old. ([Feature #58](https://gitlab.com/mysocialportal/relatica/-/issues/58))
    * Autocomplete now lists hashtags and accounts that are used in a post or post above the rest of
      the
      results. ([Feature #28](https://gitlab.com/mysocialportal/relatica/-/issues/28))
    * Show delivery data for logged in user's posts and comments (not
      reshares) ([Feature #66](https://gitlab.com/mysocialportal/relatica/-/issues/66))
    * Add spellcheck highlighting in text fields (iOS and Android
      only) ([Feature #39](https://gitlab.com/mysocialportal/relatica/-/issues/39))
    * Add tie into suggestions from system password manager (confirmed works on iOS only so
      far) ([Feature #14](https://gitlab.com/mysocialportal/relatica/-/issues/14))

## Version 0.10.1 (beta)

* Changes
    * Adds Relatica User Agent string to API requests so that Friendica servers running >2023.06
      with blockbot enabled will allow requests.
    * Adds version string to the settings screen to help users identify version installed

## Version 0.10.0 (beta)

* Changes
    * Add user count in the Circles Management screen

## Version 0.9.0 (beta)

* Changes
    * Sign in screen has a better flow and layout to make it less confusing
    * Notifications screen has a cleaner look when no notifications exist
    * Timeline screens have a cleaner look when no posts exist
    * Clicking anywhere on a post/comment in the status search results will open post. The "Go To
      Post/Comment" menu
      option is still there.
    * Drag down to refresh timeline will clear and reload the timeline from scratch after a warning.
* Fixes
    * Search screen bug with initial search not updating screen correctly addressed
    * Multiple images showing up for the same post has been fixed for D* reshares and ActivityPub
      posts with link
      previews.
      that embedded the same image as an image attachment.
    * Capitalization in ALT Text field editors should be sentence now.
    * Maximum thread rendering depth is set to 5.
* New Features
    * When a user clicks on a tag in a post it opens the search onto their local server not the
      original post/comment's
      server
    * Log entry viewer screen with ability to filter, export individual entries, or the entire table
      to a JSON file for
      helping with debugging.

## Version 0.8.0 (beta)

* Changes
    * "Groups" have been renamed "Circles" to match the upcoming Friendica release nomenclature
      change.
    * Timeline selector has now been merged into one big list with a divider between the standard
      types and the circles
    * User Profile screen buttons have been rearranged so that (un)block and (un)follow are on their
      own line
    * Notifications processing has been streamlined again, especially for older notifications
    * Contacts information is updated as the data comes in (may need to pull this out since it can
      temporarily show
      incorrect connection information before the followers mapping occurs)
    * Link preview has a new more efficient layout with the photo on top and the text caption
      underneath
    * Sign-in screen has been changed to be more user-friendly
    * Start-up splash screen has more prompts to let users know what is happening and timeouts when
      trying to
      communicate with the servers. If nothing is logged in it drops to the sign in screen rather
      than staying on splash
      screen.
    * Streamlined the visibility selection in the post/comment editor
    * Interactions toolbar has been streamlined. Navigating to the screen with details on who
      liked/reshared etc. is now
      in menu and clicking in any free space in the post card area (except around images/videos
      since that is
      technically part of a media carousel control).
    * When saving images on mobile it writes to the photo gallery than the files area.
* Fixes
    * Adding/removing of users from Circles properly reflects on profile screen (before needed to
      navigate away and back
      to get it to appear)
    * Multiclicking on notifications will not cause multiple navigations to that post/comment
    * Multiclicking on post/comment creation will not cause multiple creation events
    * Loading newer notifications works.
    * Changing profiles when on the Contacts or Search screen now properly reflect the change
    * Fix privacy levels on response to Mastodon direct messages. Previously it would expand the
      privacy to include
      followers
    * Image/video size in post/comment viewer doesn't overflow the boundaries of the card
    * Unresharing a post doesn't cause it to disappear
    * Image viewer screen has better fill and image text appears after clicking ALT button rather
      than as a caption
    * Resharing of comments in Friendica is spotty through the UI and even more so through the API.
      It has been removed
      for the time being
    * PNG images always displayed very low res thumbnails that Friendica generates. PNGs now always
      load the full size
      image, for previews and full views
* New Features
    * Can click on the visibility icon on posts to get a dialog box listing the visibility of the
      post/comment

## Version 0.7.2 (beta)

* Changes
* Fixes
    * Fixes initial OAuth authorization failing against Friendica 2023.09-rc servers
* New Features

## Version 0.7.1 (beta)

* Changes
    * Updated versions of libraries to latest so could build on latest macOS version
* Fixes
* New Features

## Version 0.7.0 (beta)

* Changes
    * Added rate limiter on background contacts automatic updates
    * Refactored the reshared code to work with the updated server aspects that have been pushed out
    * Refactored the notifications service to drastically cut down on number of calls to server
* Fixes
    * Increase API call timeout to 60 seconds
    * Insertion error bug on hashtag processing fixed
* New Features

## Version 0.6.0 (beta), 10 Map 2023

* Changes
    * API calls now have timeouts of 30 seconds
    * Splash screen shows while app is loading the initial account then it brings up timelines/etc.
      rather than waiting
      for all accounts to login
* Fixes
    * Emojis are rendered at correct size
    * Performance improvements to fix jank and apparent freezes
    * False errors about "posts not existing" because they were still being fetched replaced with
      better notification
    * Deleting statuses in the post view now puts the user back on the timeline rather than a blank
      screen
* New Features
    * Managing user blocking
        * See list of blocked users
        * Add/remove blocked users from the list
        * See blocked status on user profile page and can toggle blocked/unblocked status there
    * Post/comment filtering
        * On the device only with no syncing between devices
        * Filters are unique to each account
        * Users decide whether to hide the post/comment completely or put it behind a warning card.
        * Filters are any combination of one or more of the following all ANDed together each of the
          categories and
          ORing within a category:
            * Authors
            * Keywords
            * Hashtags
            * Domains
        * Examples of possible filters:
            * Hide posts from User1 AND with keywords "linux" OR "RedHat"
            * Hide posts with keywords "linux" OR "RedHat" AND from domains "*phoronix.com"
        * Examples of not possible filters right new:
            * Hide posts with keywords "linux" AND "RedHat"
    * Image Galleries
        * Can rename galleries
        * Easy ALT text reading, delete button, and edit button in gallery view
        * Being able to edit ALT text of images and deleting images

## Version 0.5.0 (beta), 27 April 2023

* Changes
    * The "copy text" action now copies the plain text not HTML version. So things like hashtags
      show up as "#hashtag" not the HTML code around it with the link reference etc.
    * Clicking on the "Home" button if you already are on that screen scrolls to the top.
    * Auto-complete suggestions for hashtags and accounts render at the top of the edit text field
      not bottom.
    * Contacts screen shows user's handle and last post time (as far as your server knows about) for
      the user
    * Links now open in the system browser on iOS and Android like on the desktop rather than within
      the app as an
      embedded browser
    * Media Viewer Screen carousel function has been replaced with before/after arrows on the
      right/left edges in order
      to make the zooming in of images more predictable.
* Fixes
    * Seemingly disappearing contacts on Contacts screen have been corrected.
    * Workaround for a regresssion in Friendica 2023.04 for reshared posts
* New Features
    * Responsive design for allowing the image and video attachments to scale up for larger videos
      but limit timeline/list width on very large screens.
    * Videos are now viewable in the "Media Viewer Screen" (formerly "Image Viewer Screen")
    * Groups (Mastodon Lists) management including:
        * Creating new groups
        * Renaming groups
        * Deleting groups
        * Add users to groups
        * Remove users from groups

## Version 0.4.0 (beta), 04 April 2023

* Changes
    * Uses "Sentence Capitalization" keyboard to have shift on for first letter of beginning of each
      sentence.
    * Add version error check messages on DM sending and Resharing Diaspora posts for pre-2023.03
      Friendica servers
    * Using media_kit instead of video_player library on iOS
* Fixes
    * Keyboard on Search Screen now dismisses when user clicks outside of the search text field or
      hits "Done" button (which also initiates the search).
    * GIF images load correctly
* New Features
    * Adds video playing capability to desktop

## Version 0.3.0 (beta), 22 March 2023

* Changes
    * New post/comment display format
    * Comments only show up in post detailed view
    * Clicking on "Comments" stats loads the post comment view
    * Moved the utility buttons from the bottom of the post/comment to a popup menu on the upper
      right
    * Show visibility of images in the gallery
    * Notifications now show truncated post bodies not the full body
    * Sort the notifications by unread by category and then unread chronological so:
        * Unread Direct Messages (sorted newest first)
        * Unresolved Follow/Connection Requests (sorted newest first)
        * Unread notifications of all other kinds (sorted newest first)
        * Read notifications of all kinds (sorted newest first)
* Fixes
    * Toggle like status on first level of comments no longer generates error
    * Can properly load private images if the access control list is set correctly on the server
    * Proper paging of notifications on refresh
    * Drawer stability issues on Contacts Screen
    * Eliminated excessive cropping of top of Contacts Screen
    * Can go to account profiles of accounts never seen before from the Interactions Screen (list of
      which accounts,
      liked, reshared, etc)
    * Notifications refreshing more stable
* New Features
    * Post/Comment Editing
    * Being able to set the posts as private and a single group it is visible to (or "Followers",
      the default)
    * Link Previews
    * Use actual follow requests system not the "follow requests" in notifications if account is on
      server running
      Friendica 2023.03 or later
    * Server side search of: hashtags, statuses, and accounts
    * Direct loading of status or account URL within the app so it renders within the Friendica
      network to allow for
      interactions, commenting on, making follow requests of accounts, etc.

## Version 0.2.0 (beta), 15 March 2023

* Changes
    * Uses Google's Material 3 UI instead of Material 2
    * Drawer idiom replaces the menu screen idiom. Drawer contains list of logged in accounts at the
      top to quickly
      switch between, followed by the menu items previously on the menu screen.
    * "Sign In Screen" now also the "Manage Accounts" screen
        * Username/password settings split the username/servername field so can use the email
          address as the login like
          with
          the website
        * Have sections listing logged in and logged out accounts.
        * Can permanently delete previous credentials if no longer want them.
    * Added back swiping down for refresh on timelines, notifications, and contacts. Also changed
      the progress indicator
      to be a linear bar at the top
    * Unread notifications counts no longer appear.
    * "New Post" button is now a floating action button at the bottom right of the timeline
* Fixes
    * Toggle like status on nested comments no longer generates an error.
* New Features
    * Support for OAuth Login as well as username/password
    * Support for multiple concurrent logins and switching between them
    * Added paging to notifications
    * Ability to list who liked or reshared a post by clicking on the respective summary.
    * Unread direct messages indicators show up in the notifications panel
    * Data Pages manager allows for re-requesting data from server based on previous calls.
      Currently used only for
      notifications.

## Version 0.1.0b3 (beta), 30 January 2023

* Changes
    * The timeline names have been changed to:
        * My Posts (formerly "Yours")
        * My Network (formerly "Home")
        * Global Fediverse (formerly "Global")
        * Local Fediverse (formerly "Local")
        * "Groups (Lists)" (formerly "Group")
    * Group timeline names are sorted alphabetically rather than in creation order
* Fixes
    * Older Android Releases (5.0, 6.0, 7.0) LetsEncrypt certificate issue fixed by including
      certificate manually
    * In Gallery mode single-clicking on the image brings up the Image View Screen
    * Icon white boundary issues on iOS and Android
    * Keyboard type on login screen for username field is now "email" type
* New Features
    * A combined network status/refresh icon on timeline, notifications, and gallery panel
    * More detailed user profile screen for users which now includes things like their description,
      handle and common
      name, etc.
    * Paging in gallery list to incremental load of larger galleries (also sorted newest first)
    * Image View Screen has a Carousel feature for swiping through images of a post, gallery, etc.
    * Can copy post/comment Raw HTML text, image descriptions, and Direct Message text content
        * Copy icon on posts, comments, image descriptions
        * Long-press to copy on Direct messages
    * Dialog Box "Menu" on posts/comments to more easily decide to navigate to it internally (Posts
      only), open in the
      system's default browser, or copy the URL

## Version 0.1.0b2 (beta), 27 January 2023

* Fixes
    * Scrollwheel zooming now works correctly on desktop
    * Fix image overflow and apparent intermittent lack of zoom responsiveness in image preview pane
    * Fix inconsistent tap behavior on notifications cards
    * Search on the contacts screen now searches handles as well as names
    * All contacts now load properly from server
* New Features
    * Initial paging infrastructure for being able to incrementally load larger sets of things (like
      comments)
    * First usage of paging system: contacts
    * A "Clear Caches" button that clears all memory and disk data. Useful if you want to do a reset
      on things like tags, contacts, posts timelines, etc.
    * Initial implementation of direct messaging including:
        * List of conversation threads
        * Reading conversation threads
        * Marking conversations read by tapping on the message
        * Respond in a conversation
        * Start a new conversation by searching for a known contact "type @ and start typing like in
          posts/comments"

## Version 0.1.0 (beta), 20 January 2023

* Initial public release, as an early beta.
* Major working features (at least in initial implementation versions):
    * Logging in with username/password. These are stored using the OS specific key vaults
    * Writing posts
        * Typing @ brings up a list of all known fediverse accounts that the app has ever seen as
          you type (but not all
          that your server has seen)
        * Typing # brings up a list of all known hashtags that the app has ever seen as you type (
          but not all that your
          server has seen)
        * Very basic markdown (wrapping in stars for bold/italics)
        * Adding new images to posts
            * Image posting with easy means of adding ALT text
        * Attaching existing images to posts
    * Resharing posts
    * Browsing timelines including from groups you've created. In this app "Home" is the same as
      the "Network" (grid)
      button in Frio and "Yours" is the equivalent of the Home button in the app.
    * Browsing notifications and marking individual ones read or all read
    * Getting gallery list and browsing galleries
    * Browsing your contacts or searching contacts that are known by your app
    * Adjudicating follow requests
    * Going to individual profiles to see their posts, follow/unfollow, add/remove from groups
    * A "low bandwidth" mode that defers loading of images until you click on them, unless the app
      already has that
      image cached locally.
    * Light/dark mode selection
    * Opening posts, comments, and profiles in the "external" browser
    * Playing videos within the app on iOS and Android.
    * Pulling down to refresh timelines, galleries, and posts but loading newer or older posts has
      specific buttons (
      this will probably change in the near future)
    * Pulling down to refresh notifications and contacts gets updates to that respective data (this
      may change in the
      near future)

## Version 0.x.0 (beta)

* Fixed image viewer quirks with text overwriting, lack of mouse scrollwheel zoom, and
  responsiveness
  quirk (https://gitlab.com/mysocialportal/relatica/-/merge_requests/10)
