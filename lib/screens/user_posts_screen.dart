import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../controls/standard_appbar.dart';
import '../controls/timeline/timeline_panel.dart';
import '../models/auth/profile.dart';
import '../models/timeline_identifiers.dart';
import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/networking/network_status_services.dart';
import '../riverpod_controllers/timeline_services.dart';

class UserPostsScreen extends ConsumerStatefulWidget {
  final String userId;

  const UserPostsScreen({super.key, required this.userId});

  @override
  ConsumerState<UserPostsScreen> createState() => _UserPostsScreenState();
}

class _UserPostsScreenState extends ConsumerState<UserPostsScreen> {
  late final TimelineIdentifiers timeline;

  @override
  void initState() {
    super.initState();
    final profile = ref.read(activeProfileProvider);
    timeline = TimelineIdentifiers.profile(widget.userId);
    updateTimeline(profile);
  }

  void updateTimeline(Profile profile) {
    Future.delayed(const Duration(milliseconds: 100), () async {
      await ref
          .read(timelineManagerProvider(profile, timeline).notifier)
          .updateTimeline(TimelineRefreshType.refresh);
    });
  }

  @override
  Widget build(BuildContext context) {
    final profile = ref.watch(activeProfileProvider);
    ref.watch(timelineManagerProvider(profile, timeline));
    final loading = ref.watch(timelineLoadingStatusProvider(profile, timeline));

    return Scaffold(
      appBar: StandardAppBar.build(
        context,
        'User Posts',
        actions: [],
      ),
      body: Center(
        child: Column(
          children: [
            if (loading) const LinearProgressIndicator(),
            Expanded(
              child: TimelinePanel(
                timeline: timeline,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
