import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../globals.dart';
import '../models/auth/profile.dart';
import '../models/filters/timeline_entry_filter.dart';
import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/connection_manager_services.dart';
import '../riverpod_controllers/timeline_entry_filter_services.dart';

class FiltersScreen extends ConsumerWidget {
  const FiltersScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final profile = ref.watch(activeProfileProvider);
    final filters =
        ref.watch(timelineEntryFiltersProvider(profile)).values.toList();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Filters'),
        actions: [
          IconButton(
            onPressed: () {
              context.push('/filters/new');
            },
            icon: const Icon(Icons.add),
          ),
        ],
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: ListView.separated(
                  itemBuilder: (context, index) {
                    return buildFilterSummary(
                        context, filters[index], profile, ref);
                  },
                  separatorBuilder: (_, __) => const Divider(),
                  itemCount: filters.length),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildFilterSummary(BuildContext context, TimelineEntryFilter filter,
      Profile profile, WidgetRef ref) {
    final service = ref.read(timelineEntryFiltersProvider(profile).notifier);
    return ListTile(
      leading: Checkbox(
        onChanged: (value) => service.upsertFilter(filter.copy(enabled: value)),
        value: filter.enabled,
      ),
      title: Text('${filter.action.toVerb()} Filter: ${filter.name}'),
      subtitle: Text(
        filter.toSummaryText(ref, profile),
        maxLines: 10,
        softWrap: true,
      ),
      trailing: IconButton(
          onPressed: () async {
            final confirm =
                await showYesNoDialog(context, 'Delete filter ${filter.name}?');
            if (confirm == true) {
              service.removeById(filter.id);
            }
          },
          icon: const Icon(Icons.remove)),
      onTap: () => context.push('/filters/edit/${filter.id}'),
    );
  }
}

extension _TimelineEntryFilterSummary on TimelineEntryFilter {
  String toSummaryText(WidgetRef ref, Profile profile) {
    var authorsString = '';

    if (authorFilters.isNotEmpty) {
      authorsString = authorFilters
          .map((a) => ref
              .watch(connectionByIdProvider(profile, a.filterString))
              .transform((c) => '${c.name} (${c.handle})')
              .getValueOrElse(() => ''))
          .where((e) => e.isNotEmpty)
          .join('; ');
    }

    return [
      if (hashtagFilters.isNotEmpty)
        'Hashtags: ${hashtagFilters.map((f) => f.filterString).join(',')}',
      if (keywordFilters.isNotEmpty)
        'Keywords: ${keywordFilters.map((f) => f.filterString).join(',')}',
      if (domainFilters.isNotEmpty)
        'Domains: ${domainFilters.map((f) => f.filterString).join(', ')}',
      if (authorFilters.isNotEmpty) 'Authors: $authorsString',
    ].join('\n');
  }
}
