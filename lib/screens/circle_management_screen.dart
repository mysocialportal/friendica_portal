import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../controls/responsive_max_width.dart';
import '../controls/standard_appbar.dart';
import '../models/timeline_grouping_list_data.dart';
import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/circles_repo_services.dart';
import '../routes.dart';

class CircleManagementScreen extends ConsumerWidget {
  const CircleManagementScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final profile = ref.watch(activeProfileProvider);
    final circles =
        ref.watch(timelineGroupingListProvider(profile, GroupingType.circle));
    circles.sort((g1, g2) => g1.name.compareTo(g2.name));
    return Scaffold(
        appBar: StandardAppBar.build(
          context,
          'Circles Management',
          withHome: false,
          actions: [
            IconButton(
              onPressed: () => context.push(
                '${ScreenPaths.circleManagement}/new',
              ),
              icon: const Icon(Icons.add),
            ),
          ],
        ),
        body: Center(
          child: RefreshIndicator(
            onRefresh: () async {
              ref.read(circlesProvider(profile).notifier).refresh();
            },
            child: ResponsiveMaxWidth(
              child: ListView.separated(
                physics: const AlwaysScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  final circle = circles[index];
                  return ListTile(
                    title: Text(circle.name),
                    onTap: () => context.push(
                        '${ScreenPaths.circleManagement}/show/${circle.id}'),
                  );
                },
                separatorBuilder: (_, __) => const Divider(),
                itemCount: circles.length,
              ),
            ),
          ),
        ));
  }
}
