import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../controls/async_value_widget.dart';
import '../controls/error_message_widget.dart';
import '../controls/search_result_status_control.dart';
import '../controls/standard_appbar.dart';
import '../models/timeline_identifiers.dart';
import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/entry_tree_item_services.dart';
import '../riverpod_controllers/networking/network_status_services.dart';
import '../riverpod_controllers/timeline_services.dart';
import '../utils/snackbar_builder.dart';

class UserPostAndCommentsScreen extends ConsumerStatefulWidget {
  final String userId;

  const UserPostAndCommentsScreen({super.key, required this.userId});

  @override
  ConsumerState<UserPostAndCommentsScreen> createState() =>
      _UserPostAndCommentsScreenState();
}

class _UserPostAndCommentsScreenState
    extends ConsumerState<UserPostAndCommentsScreen> {
  @override
  Widget build(BuildContext context) {
    final profile = ref.watch(activeProfileProvider);
    final timeline = TimelineIdentifiers.profile(widget.userId);
    final loading = ref.watch(timelineLoadingStatusProvider(profile, timeline));

    return Scaffold(
      appBar: StandardAppBar.build(
        context,
        'User Posts & Comments',
        actions: [],
      ),
      body: Center(
        child: Column(
          children: [
            if (loading) const LinearProgressIndicator(),
            Expanded(
              child: AsyncValueWidget(
                  ref.watch(userPostsAndCommentsTimelineProvider(
                      profile, widget.userId)), valueBuilder: (_, __, result) {
                return result.fold(
                    onSuccess: (postsAndComments) {
                      if (postsAndComments.isEmpty) {
                        return const ErrorMessageWidget(
                            message: 'No posts or comments for this user');
                      }

                      return ListView.builder(
                          itemCount: postsAndComments.length,
                          itemBuilder: (_, index) {
                            final status = postsAndComments[index];
                            return SearchResultStatusControl(status, () async {
                              final result = await ref
                                  .read(
                                      timelineUpdaterProvider(profile).notifier)
                                  .refreshStatusChain(status.id);
                              if (context.mounted) {
                                result.match(
                                    onSuccess: (entry) => context.push(
                                        '/post/view/${entry.id}/${status.id}'),
                                    onError: (error) => buildSnackbar(
                                        context, 'Error getting post: $error'));
                              }
                            });
                          });
                    },
                    onError: (error) =>
                        ErrorMessageWidget(message: error.message));
              }),
            ),
          ],
        ),
      ),
    );
  }
}
