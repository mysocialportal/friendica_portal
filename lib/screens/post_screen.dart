import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../controls/responsive_max_width.dart' show ResponsiveMaxWidth;
import '../controls/standard_appbar.dart';
import '../controls/timeline/post_control.dart';
import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/entry_tree_item_services.dart';
import '../riverpod_controllers/networking/network_status_services.dart';

class PostScreen extends ConsumerStatefulWidget {
  final String id;

  final String goToId;

  const PostScreen({
    super.key,
    required this.id,
    required this.goToId,
  });

  @override
  ConsumerState<PostScreen> createState() => _PostScreenState();
}

class _PostScreenState extends ConsumerState<PostScreen> {
  @override
  void initState() {
    super.initState();
    final profile = ref.read(activeProfileProvider);
    Future.delayed(const Duration(milliseconds: 100), () async {
      await ref
          .read(timelineUpdaterProvider(profile).notifier)
          .refreshStatusChain(widget.id);
    });
  }

  @override
  Widget build(BuildContext context) {
    final profile = ref.watch(activeProfileProvider);
    final loading =
        ref.watch(postCommentLoadingStatusProvider(profile, widget.id));
    final entryResult =
        ref.watch(postTreeEntryByIdProvider(profile, widget.id));
    final body = entryResult.fold(
        onSuccess: (post) => RefreshIndicator(
              onRefresh: () async {
                await ref
                    .read(timelineUpdaterProvider(profile).notifier)
                    .refreshStatusChain(widget.id);
                return;
              },
              child: PostControl(
                id: post.id,
                scrollToId: widget.goToId,
                openRemote: true,
                showStatusOpenButton: false,
                isRoot: true,
              ),
            ),
        onError: (error) => Text(loading
            ? 'Attempting to load post'
            : 'Error getting post: $error'));
    return Scaffold(
        appBar: StandardAppBar.build(context, 'View Post', actions: []),
        body: Padding(
          padding: const EdgeInsets.all(0.0),
          child: Column(
            children: [
              if (loading) const LinearProgressIndicator(),
              Expanded(child: ResponsiveMaxWidth(child: body)),
            ],
          ),
        ));
  }
}
