import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../controls/async_value_widget.dart';
import '../controls/error_message_widget.dart';
import '../controls/image_control.dart';
import '../controls/responsive_max_width.dart';
import '../controls/standard_appbar.dart';
import '../controls/status_and_refresh_button.dart';
import '../models/auth/profile.dart';
import '../models/interaction_type_enum.dart';
import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/connection_manager_services.dart';
import '../riverpod_controllers/interactions_details_services.dart';
import '../routes.dart';

class InteractionsViewerScreen extends ConsumerWidget {
  final String statusId;
  final InteractionType type;

  const InteractionsViewerScreen({
    super.key,
    required this.statusId,
    required this.type,
  });

  void refreshInteractors(WidgetRef ref, Profile profile) async {
    switch (type) {
      case InteractionType.like:
        return ref.invalidate(likesForStatusProvider(profile, statusId));
      case InteractionType.reshare:
        return ref.invalidate(resharesForStatusProvider(profile, statusId));
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final profile = ref.watch(activeProfileProvider);
    final connectionsAsyncValue = switch (type) {
      InteractionType.like =>
        ref.watch(likesForStatusProvider(profile, statusId)),
      InteractionType.reshare =>
        ref.watch(resharesForStatusProvider(profile, statusId)),
    };

    final loading = switch (connectionsAsyncValue) {
      AsyncData() => false,
      _ => true,
    };

    return Scaffold(
      appBar: StandardAppBar.build(context, buildTitle(), actions: [
        StatusAndRefreshButton(
          executing: loading,
          refreshFunction: () async => refreshInteractors(ref, profile),
          busyColor: Theme.of(context).colorScheme.surface,
        )
      ]),
      body: Center(
        child: ResponsiveMaxWidth(
            child: AsyncValueWidget(connectionsAsyncValue,
                valueBuilder: (context, ref, connectionsResult) {
          if (connectionsResult.isFailure) {
            return ErrorMessageWidget(message: connectionsResult.error.message);
          }
          final connections = connectionsResult.value;
          return ListView.separated(
            itemCount: connections.length,
            itemBuilder: (context, index) {
              final connection = connections[index];
              return ListTile(
                onTap: () async {
                  await ref
                      .read(connectionByIdProvider(profile, connection.id))
                      .withErrorAsync((_) => ref
                          .read(connectionModifierProvider(profile, connection)
                              .notifier)
                          .fullRefresh());
                  if (context.mounted) {
                    context.pushNamed(ScreenPaths.userProfile,
                        pathParameters: {'id': connection.id});
                  }
                },
                leading: ImageControl(
                  imageUrl: connection.avatarUrl.toString(),
                  iconOverride: const Icon(Icons.person),
                  width: 32.0,
                  onTap: () => context.pushNamed(ScreenPaths.userProfile,
                      pathParameters: {'id': connection.id}),
                ),
                title: Text('${connection.name} (${connection.handle})'),
              );
            },
            separatorBuilder: (_, __) => const Divider(),
          );
        })),
      ),
    );
  }

  String buildTitle() {
    switch (type) {
      case InteractionType.like:
        return 'Likes';
      case InteractionType.reshare:
        return 'Reshares';
    }
  }
}
