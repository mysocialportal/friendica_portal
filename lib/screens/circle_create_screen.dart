import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../controls/padding.dart';
import '../controls/standard_appbar.dart';
import '../models/auth/profile.dart';
import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/circles_repo_services.dart';
import '../utils/snackbar_builder.dart';

class CircleCreateScreen extends ConsumerStatefulWidget {
  const CircleCreateScreen({super.key});

  @override
  ConsumerState<CircleCreateScreen> createState() => _CircleCreateScreenState();
}

class _CircleCreateScreenState extends ConsumerState<CircleCreateScreen> {
  final circleTextController = TextEditingController();

  Future<void> createCircle(Profile profile) async {
    if (circleTextController.text.isEmpty) {
      buildSnackbar(context, "Circle name can't be empty");
      return;
    }

    final result = await ref
        .read(circlesProvider(profile).notifier)
        .createCircle(circleTextController.text);
    if (context.mounted) {
      result.match(
          onSuccess: (_) => context.canPop() ? context.pop() : null,
          onError: (error) {
            buildSnackbar(context, 'Error trying to create new circle: $error');
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    final profile = ref.watch(activeProfileProvider);
    return Scaffold(
      appBar: StandardAppBar.build(
        context,
        'New circle',
        withHome: false,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            TextFormField(
              controller: circleTextController,
              textCapitalization: TextCapitalization.sentences,
              decoration: InputDecoration(
                labelText: 'Circle Name',
                border: OutlineInputBorder(
                  borderSide: const BorderSide(),
                  borderRadius: BorderRadius.circular(5.0),
                ),
              ),
            ),
            const VerticalPadding(),
            ElevatedButton(
              onPressed: () => createCircle(profile),
              child: const Text('Create'),
            ),
          ],
        ),
      ),
    );
  }
}
