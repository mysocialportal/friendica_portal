import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../controls/async_value_widget.dart';
import '../controls/image_control.dart';
import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/blocks_services.dart';
import '../routes.dart';

class BlocksScreen extends ConsumerWidget {
  const BlocksScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final profile = ref.watch(activeProfileProvider);
    final blocksValue = ref.watch(blocksManagerProvider(profile));
    return Scaffold(
      appBar: AppBar(
        title: const Text('Blocks'),
      ),
      body: SafeArea(
        child: AsyncValueWidget(
          blocksValue,
          valueBuilder: (context, ref, value) => ListView.builder(
            itemBuilder: (context, index) {
              final contact = value[index];
              return ListTile(
                onTap: () async {
                  context.pushNamed(ScreenPaths.userProfile,
                      pathParameters: {'id': contact.id});
                },
                leading: ImageControl(
                  imageUrl: contact.avatarUrl.toString(),
                  iconOverride: const Icon(Icons.person),
                  width: 32.0,
                ),
                title: Text(
                  '${contact.name} (${contact.handle})',
                  softWrap: true,
                ),
                subtitle: Text(
                  'Last Status: ${contact.lastStatus?.toIso8601String() ?? "Unknown"}',
                  softWrap: true,
                ),
                trailing: ElevatedButton(
                  onPressed: () async => await ref
                      .read(blocksManagerProvider(profile).notifier)
                      .unblockConnection(contact),
                  child: const Text('Unblock'),
                ),
              );
            },
            itemCount: value.length,
          ),
        ),
      ),
    );
  }
}
