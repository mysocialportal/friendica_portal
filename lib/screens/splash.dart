import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../controls/padding.dart';
import '../globals.dart';
import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/status_service.dart';
import '../routes.dart';

class SplashScreen extends ConsumerWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final initializing =
        !ref.watch(accountServicesInitializerProvider.notifier).initialized;
    final loggedIn = ref.watch(activeProfileProvider.notifier).hasActiveProfile;
    if (!initializing && !loggedIn) {
      context.pushNamed(ScreenPaths.signin);
    }

    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('icon/raster/macos/app_icon_1024.png', width: 256),
          const VerticalPadding(),
          Text(
            'Relatica $appVersion',
            style: Theme.of(context).textTheme.headlineLarge,
          ),
          const VerticalPadding(),
          if (initializing) ...[
            const CircularProgressIndicator(),
            const VerticalPadding(),
            const Text('Logging in accounts...'),
            const VerticalPadding(),
          ],
          Text(
            ref.watch(statusServiceProvider),
            softWrap: true,
            textAlign: TextAlign.center,
          ),
        ],
      )),
    );
  }
}
