import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../controls/search_panel.dart';
import '../controls/standard_appbar.dart';
import '../globals.dart';
import '../models/search_types.dart';
import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/hashtag_service.dart';
import '../riverpod_controllers/networking/friendica_tags_client_services.dart';

class TagsTimelineScreen extends ConsumerWidget {
  final String tagName;

  const TagsTimelineScreen({super.key, required this.tagName});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final profile = ref.watch(activeProfileProvider);
    final followingValue =
        ref.watch(hashtagFollowCheckProvider(profile, tagName));
    final loading = switch (followingValue) {
      AsyncData() => false,
      AsyncError() => false,
      _ => true,
    };
    final followingTag = switch (followingValue) {
      AsyncData(:final value) => value,
      _ => false
    };
    return Scaffold(
      appBar: StandardAppBar.build(context, '#$tagName', actions: [
        IconButton(
          onPressed: loading
              ? null
              : () async {
                  final prompt = followingTag ? 'Unfollow' : 'Follow';
                  final confirm =
                      await showYesNoDialog(context, '$prompt $tagName?');
                  if (confirm != true) {
                    return;
                  }
                  if (followingTag) {
                    await ref
                        .read(unfollowTagProvider(profile, tagName).future);
                  } else {
                    await ref.read(followTagProvider(profile, tagName).future);
                  }
                },
          icon: Icon(
            followingTag ? Icons.remove : Icons.add,
          ),
        )
      ]),
      body: SearchPanel(
        initialType: SearchTypes.statusesText,
        initialSearchText: '#$tagName',
        showSearchbar: false,
      ),
    );
  }
}
