import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../controls/async_value_widget.dart';
import '../controls/error_message_widget.dart';
import '../controls/media_attachment_viewer_control.dart';
import '../controls/standard_appbar.dart';
import '../models/timeline_identifiers.dart';
import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/networking/network_status_services.dart';
import '../riverpod_controllers/timeline_services.dart';

class UserMediaScreen extends ConsumerStatefulWidget {
  final String userId;

  const UserMediaScreen({super.key, required this.userId});

  @override
  ConsumerState<UserMediaScreen> createState() => _UserMediaScreenState();
}

class _UserMediaScreenState extends ConsumerState<UserMediaScreen> {
  static const thumbnailDimension = 350.0;

  @override
  Widget build(BuildContext context) {
    final profile = ref.watch(activeProfileProvider);
    final timeline = TimelineIdentifiers.profile(widget.userId);
    final loading = ref.watch(timelineLoadingStatusProvider(profile, timeline));

    return Scaffold(
      appBar: StandardAppBar.build(
        context,
        'User Media',
        actions: [],
      ),
      body: Center(
        child: Column(
          children: [
            if (loading) const LinearProgressIndicator(),
            Expanded(
              child: AsyncValueWidget(
                  ref.watch(userMediaTimelineProvider(profile, widget.userId)),
                  valueBuilder: (_, __, result) {
                return result.fold(
                    onSuccess: (media) {
                      if (media.isEmpty) {
                        return const ErrorMessageWidget(
                            message: 'No media for this user');
                      }

                      return GridView.builder(
                          itemCount: media.length,
                          padding: const EdgeInsets.all(5.0),
                          gridDelegate:
                              const SliverGridDelegateWithMaxCrossAxisExtent(
                                  maxCrossAxisExtent: thumbnailDimension),
                          itemBuilder: (context, index) {
                            if (index == media.length - 1) {
                              ref
                                  .read(userMediaTimelineProvider(
                                          profile, widget.userId)
                                      .notifier)
                                  .updateTimeline(
                                    reset: false,
                                    withNotification: true,
                                  );
                            }
                            return Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: MediaAttachmentViewerControl(
                                  attachments: [media[index]], index: 0),
                            );
                          });
                    },
                    onError: (error) =>
                        ErrorMessageWidget(message: error.message));
              }),
            ),
          ],
        ),
      ),
    );
  }
}
