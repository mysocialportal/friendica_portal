enum TimelineType {
  home,
  global,
  local,
  circle,
  profile,
  self;

  String toLabel() {
    switch (this) {
      case TimelineType.home:
        return 'My Network';
      case TimelineType.global:
        return 'Global Fediverse';
      case TimelineType.local:
        return 'Local Fediverse';
      case TimelineType.circle:
        return 'Circles';
      case TimelineType.profile:
        return 'Profile';
      case TimelineType.self:
        return 'My Posts';
    }
  }
}

class TimelineIdentifiers {
  final TimelineType timeline;
  final String auxData;
  final String label;

  const TimelineIdentifiers({
    required this.timeline,
    this.auxData = '',
    this.label = '',
  });

  String toHumanKey() {
    return auxData.isEmpty ? timeline.name : '${timeline.name}_$auxData';
  }

  String toLabel() {
    if (label.isNotEmpty) {
      return label;
    }

    if (timeline != TimelineType.circle) {
      return timeline.toLabel();
    }

    return '${timeline.toLabel()} $auxData';
  }

  factory TimelineIdentifiers.home() =>
      const TimelineIdentifiers(timeline: TimelineType.home);

  factory TimelineIdentifiers.myPosts() =>
      const TimelineIdentifiers(timeline: TimelineType.self);

  factory TimelineIdentifiers.profile(String profileId) => TimelineIdentifiers(
        timeline: TimelineType.profile,
        auxData: profileId,
      );

  @override
  String toString() {
    return auxData.isEmpty
        ? 'TimelineIdentifiers{timeline: $timeline)'
        : 'TimelineIdentifiers{timeline: $timeline, auxData: $auxData}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TimelineIdentifiers &&
          runtimeType == other.runtimeType &&
          timeline == other.timeline &&
          auxData == other.auxData &&
          label == other.label;

  @override
  int get hashCode => timeline.hashCode ^ auxData.hashCode ^ label.hashCode;
}
