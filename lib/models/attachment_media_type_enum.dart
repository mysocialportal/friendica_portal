enum AttachmentMediaType {
  unknown,
  image,
  video;

  static AttachmentMediaType parse(String? text) {
    if (text == null) {
      return AttachmentMediaType.unknown;
    }

    if (text == 'gif' || text == 'gifv') {
      return AttachmentMediaType.image;
    }

    return AttachmentMediaType.values.firstWhere(
      (e) => e.name == text,
      orElse: () => unknown,
    );
  }
}
