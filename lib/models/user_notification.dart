import 'package:logging/logging.dart';
import 'package:stack_trace/stack_trace.dart';

final _logger = Logger('NotificationType');

enum NotificationType {
  favourite,
  follow,
  follow_request,
  mention,
  reshare,
  reblog,
  status,
  direct_message,
  unknown;

  String toVerb() {
    switch (this) {
      case NotificationType.favourite:
        return 'favorited';
      case NotificationType.follow:
        return 'follows';
      case NotificationType.follow_request:
        return 'sent follow request to you';
      case NotificationType.mention:
        return 'mentioned you';
      case NotificationType.reshare:
      case NotificationType.reblog:
        return 'reshared';
      case NotificationType.status:
        return 'updated';
      case NotificationType.direct_message:
        return 'has sent you a new direct message';
      case NotificationType.unknown:
        return 'unknowned';
    }
  }

  static NotificationType parse(String? text) {
    if (text == null) {
      return unknown;
    }

    return NotificationType.values.firstWhere(
      (e) => e.name == text,
      orElse: () {
        _logger.severe(
            'Parsing error, unknown type string: $text', Trace.current());
        return unknown;
      },
    );
  }
}

class UserNotification implements Comparable<UserNotification> {
  final String id;
  final NotificationType type;
  final String fromId;
  final String fromName;
  final String fromUrl;
  final int timestamp;
  final String iid;
  final bool dismissed;
  final String content;
  final String link;

  UserNotification({
    required this.id,
    required this.type,
    required this.fromId,
    required this.fromName,
    required this.fromUrl,
    required this.timestamp,
    required this.iid,
    required this.dismissed,
    required this.content,
    required this.link,
  });

  UserNotification copy({
    bool? dismissed,
  }) =>
      UserNotification(
        id: id,
        type: type,
        fromId: fromId,
        fromName: fromName,
        fromUrl: fromUrl,
        timestamp: timestamp,
        iid: iid,
        dismissed: dismissed ?? this.dismissed,
        content: content,
        link: link,
      );

  @override
  String toString() {
    return 'UserNotification{id: $id, seen: $dismissed, fromName: $fromName, content: $content}';
  }

  @override
  int compareTo(UserNotification other) {
    if (dismissed == other.dismissed) {
      return -timestamp.compareTo(other.timestamp);
    }

    if (dismissed && !other.dismissed) {
      return 1;
    }

    return -1;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserNotification &&
          runtimeType == other.runtimeType &&
          id == other.id;

  @override
  int get hashCode => id.hashCode;
}
