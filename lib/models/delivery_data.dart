class DeliveryData {
  static const empty = DeliveryData(total: 0, done: 0, failed: 0);

  final int total;
  final int done;
  final int failed;

  const DeliveryData({
    required this.total,
    required this.done,
    required this.failed,
  });

  bool get hasDeliveryData => total > 0;

  int get leftForDelivery => total - done - failed;
}
