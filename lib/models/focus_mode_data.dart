const defaultMaxNumber = 1000;

class FocusModeData {
  final DateTime? disableTime;
  final int maxNumber;
  final bool enabled;

  const FocusModeData(this.enabled,
      {this.maxNumber = defaultMaxNumber, this.disableTime});

  factory FocusModeData.disabled() => const FocusModeData(false);

  factory FocusModeData.fromJson(Map<String, dynamic> json) => FocusModeData(
        json['enabled'],
        maxNumber: json['maxNumber'] ?? defaultMaxNumber,
        disableTime: DateTime.tryParse(json['disableTime'] ?? ''),
      );

  Map<String, dynamic> toJson() => {
        'enabled': enabled,
        'maxNumber': maxNumber,
        if (disableTime != null) 'disableTime': disableTime!.toIso8601String(),
      };
}
