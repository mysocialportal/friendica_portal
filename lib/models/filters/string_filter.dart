enum ComparisonType {
  contains,
  containsIgnoreCase,
  endsWithIgnoreCase,
  equals,
  equalsIgnoreCase,
  ;

  factory ComparisonType.parse(String? value) {
    return ComparisonType.values.firstWhere(
      (v) => v.name == value,
      orElse: () => equals,
    );
  }
}

class StringFilter {
  final String filterString;
  final ComparisonType type;

  const StringFilter({
    required this.filterString,
    required this.type,
  });

  Map<String, dynamic> toJson() => {
        'filterString': filterString,
        'type': type.name,
      };

  factory StringFilter.fromJson(Map<String, dynamic> json) => StringFilter(
        filterString: json['filterString'],
        type: ComparisonType.parse(json['type']),
      );
}
