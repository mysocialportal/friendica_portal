enum SearchTypes {
  account,
  statusesText,
  directLink,
  hashTag,
  ;

  String toLabel() {
    switch (this) {
      case SearchTypes.hashTag:
        return 'Hashtag';
      case SearchTypes.account:
        return 'Account';
      case SearchTypes.statusesText:
        return 'Statuses Text';
      case SearchTypes.directLink:
        return 'Direct Link';
    }
  }

  String toQueryParameters() {
    switch (this) {
      case SearchTypes.hashTag:
        return 'type=hashtags';
      case SearchTypes.account:
        return 'type=accounts';
      case SearchTypes.statusesText:
        return 'type=statuses';
      case SearchTypes.directLink:
        return 'resolve=true';
    }
  }
}
