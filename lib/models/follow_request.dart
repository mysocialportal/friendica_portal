import 'connection.dart';

class FollowRequest {
  final String id;
  final Connection connection;
  final int createdAtEpochSeconds;

  const FollowRequest({
    required this.id,
    required this.connection,
    required this.createdAtEpochSeconds,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FollowRequest &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          connection == other.connection &&
          createdAtEpochSeconds == other.createdAtEpochSeconds;

  @override
  int get hashCode =>
      id.hashCode ^ connection.hashCode ^ createdAtEpochSeconds.hashCode;
}
