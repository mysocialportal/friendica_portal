import 'connection.dart';
import 'timeline_entry.dart';

class SearchResults {
  final List<Connection> accounts;

  final List<TimelineEntry> statuses;

  final List<String> hashtags;

  const SearchResults({
    required this.accounts,
    required this.statuses,
    required this.hashtags,
  });

  factory SearchResults.empty() => const SearchResults(
        accounts: [],
        statuses: [],
        hashtags: [],
      );

  SearchResults merge(SearchResults newResults) => SearchResults(
        accounts: [...accounts, ...newResults.accounts],
        statuses: [...statuses, ...newResults.statuses],
        hashtags: [...hashtags, ...newResults.hashtags],
      );

  bool get isEmpty => accounts.isEmpty && statuses.isEmpty && hashtags.isEmpty;

  @override
  String toString() {
    return 'SearchResults{#accounts: ${accounts.length}, #statuses: ${statuses.length}, #hashtags: ${hashtags.length}}';
  }
}
