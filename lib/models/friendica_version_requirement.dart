import 'friendica_version.dart';

final FriendicaVersionRequirement unknownRequirement =
    FriendicaVersionRequirement(unknown);

class FriendicaVersionRequirement {
  final FriendicaVersion minimumVersion;
  final FriendicaVersion? maxVersion;

  const FriendicaVersionRequirement(this.minimumVersion, {this.maxVersion});

  bool versionMeetsRequirement(FriendicaVersion version) {
    if (version < minimumVersion) {
      return false;
    }

    if (maxVersion == null) {
      return true;
    }

    return version <= maxVersion!;
  }

  @override
  String toString() {
    if (maxVersion == null) {
      return 'requires at least Friendica $minimumVersion';
    }

    return 'works only on Friendica $minimumVersion to $maxVersion';
  }
}
