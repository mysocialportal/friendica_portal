class EngagementSummary {
  final int favoritesCount;
  final int rebloggedCount;
  final int repliesCount;

  const EngagementSummary(
      {this.favoritesCount = 0,
      this.rebloggedCount = 0,
      this.repliesCount = 0});

  @override
  String toString() {
    return 'EngagementSummary{#favs: $favoritesCount, #reshare: $rebloggedCount, #comments: $repliesCount}';
  }
}
