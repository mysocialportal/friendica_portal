import 'friendica_version.dart';

class InstanceInfo {
  final int maxStatusCharacters;
  final FriendicaVersion friendicaVersion;
  final String versionString;
  final int maxImageBytes;

  const InstanceInfo({
    required this.maxStatusCharacters,
    required this.friendicaVersion,
    required this.versionString,
    required this.maxImageBytes,
  });

  @override
  String toString() {
    return 'InstanceInfo{maxStatusCharacters: $maxStatusCharacters, version: $versionString, maxImageBytes: $maxImageBytes}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is InstanceInfo &&
          runtimeType == other.runtimeType &&
          maxStatusCharacters == other.maxStatusCharacters &&
          friendicaVersion == other.friendicaVersion &&
          versionString == other.versionString &&
          maxImageBytes == other.maxImageBytes;

  @override
  int get hashCode =>
      maxStatusCharacters.hashCode ^
      friendicaVersion.hashCode ^
      versionString.hashCode ^
      maxImageBytes.hashCode;
}
