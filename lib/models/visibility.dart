enum VisibilityType {
  public,
  private,
  unlisted,
  ;

  String toLabel() {
    switch (this) {
      case VisibilityType.public:
        return 'Public';
      case VisibilityType.private:
        return 'Private';
      case VisibilityType.unlisted:
        return 'Unlisted';
    }
  }
}

class Visibility {
  final VisibilityType type;

  final List<String> allowedUserIds;

  final List<String> excludedUserIds;

  final List<String> allowedCircleIds;

  final List<String> excludedCircleIds;

  bool get hasDetails =>
      allowedUserIds.isNotEmpty ||
      excludedUserIds.isNotEmpty ||
      allowedCircleIds.isNotEmpty ||
      excludedCircleIds.isNotEmpty;

  const Visibility({
    required this.type,
    this.allowedUserIds = const [],
    this.excludedUserIds = const [],
    this.allowedCircleIds = const [],
    this.excludedCircleIds = const [],
  });

  factory Visibility.public() => const Visibility(
        type: VisibilityType.public,
      );

  factory Visibility.private() => const Visibility(
        type: VisibilityType.private,
      );

  factory Visibility.unlisted() => const Visibility(
        type: VisibilityType.unlisted,
      );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Visibility &&
          runtimeType == other.runtimeType &&
          type == other.type &&
          allowedUserIds == other.allowedUserIds &&
          excludedUserIds == other.excludedUserIds &&
          allowedCircleIds == other.allowedCircleIds &&
          excludedCircleIds == other.excludedCircleIds;

  @override
  int get hashCode =>
      type.hashCode ^
      allowedUserIds.hashCode ^
      excludedUserIds.hashCode ^
      allowedCircleIds.hashCode ^
      excludedCircleIds.hashCode;
}
