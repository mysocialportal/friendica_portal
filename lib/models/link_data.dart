class LinkData {
  final String url;
  final String title;
  final String description;
  final String imageUrl;

  LinkData(
      {required this.url,
      required this.title,
      required this.description,
      required this.imageUrl});

  factory LinkData.fromMastodonJson(Map<String, dynamic> json) => LinkData(
        url: json['url'] ?? 'Unknown',
        title: json['title'] ?? 'Unknown',
        description: json['description'] ?? '',
        imageUrl: json['image'] ?? '',
      );
}
