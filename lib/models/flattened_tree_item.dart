import 'timeline_entry.dart';

class FlattenedTreeItem {
  final TimelineEntry timelineEntry;

  final bool isMine;

  final int level;

  FlattenedTreeItem({
    required this.timelineEntry,
    required this.isMine,
    required this.level,
  });
}
