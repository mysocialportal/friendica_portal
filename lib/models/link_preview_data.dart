import 'package:relatica/models/trend_history_data.dart';

class LinkPreviewData {
  final String link;
  final String title;
  final String description;
  final String siteName;
  final String selectedImageUrl;
  final List<String> availableImageUrls;
  final TrendHistoryData trendData;

  LinkPreviewData({
    required this.link,
    this.title = '',
    this.description = '',
    this.siteName = '',
    this.selectedImageUrl = '',
    this.availableImageUrls = const [],
  }) : trendData = TrendHistoryData.empty;

  LinkPreviewData copy({
    String? link,
    String? title,
    String? description,
    String? siteName,
    String? selectedImageUrl,
    List<String>? availableImageUrls,
  }) =>
      LinkPreviewData(
        link: link ?? this.link,
        title: title ?? this.title,
        description: description ?? this.description,
        siteName: siteName ?? this.siteName,
        selectedImageUrl: selectedImageUrl ?? this.selectedImageUrl,
        availableImageUrls: availableImageUrls ?? this.availableImageUrls,
      );

  @override
  String toString() {
    return 'LinkPreviewData{link: $link, title: $title, description: $description, siteName: $siteName, selectedImageUrl: $selectedImageUrl, availableImageUrls: $availableImageUrls}';
  }
}
