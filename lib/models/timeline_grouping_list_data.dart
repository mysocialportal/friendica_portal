enum GroupingType {
  channel,
  circle,
  group,
}

class TimelineGroupingListData {
  static const followersPseudoCircle =
      TimelineGroupingListData('~', 'Followers', GroupingType.circle);

  static const empty = TimelineGroupingListData('', '', GroupingType.circle);

  final String id;

  final String name;

  final GroupingType groupingType;

  const TimelineGroupingListData(this.id, this.name, this.groupingType);

  @override
  String toString() {
    return 'CircleData{id: $id, name: $name, type: ${groupingType.name}}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TimelineGroupingListData &&
          runtimeType == other.runtimeType &&
          id == other.id;

  @override
  int get hashCode => id.hashCode;
}
