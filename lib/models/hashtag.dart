import 'package:objectbox/objectbox.dart';

import 'trend_history_data.dart';

@Entity()
class Hashtag {
  @Id()
  int id;

  @Unique(onConflict: ConflictStrategy.replace)
  String tag;

  String url;

  @Property(type: PropertyType.date)
  DateTime lastUpdateTime;

  Hashtag({
    this.id = 0,
    required this.tag,
    required this.url,
    DateTime? updateTime,
  }) : lastUpdateTime = updateTime ?? DateTime.now();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Hashtag && runtimeType == other.runtimeType && tag == other.tag;

  @override
  int get hashCode => tag.hashCode;
}

class HashtagWithTrending {
  final String name;
  final String url;
  final bool following;
  final TrendHistoryData history;

  const HashtagWithTrending({
    required this.name,
    required this.url,
    required this.following,
    required this.history,
  });

  Hashtag toHashtag() => Hashtag(
        tag: name,
        url: url,
        updateTime: DateTime.now(),
      );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is HashtagWithTrending &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          url == other.url &&
          following == other.following &&
          history == other.history;

  @override
  int get hashCode =>
      name.hashCode ^ url.hashCode ^ following.hashCode ^ history.hashCode;
}
