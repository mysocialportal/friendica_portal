import 'package:path/path.dart' as p;

enum ImageTypes {
  gif,
  png,
  jpg;

  static ImageTypes fromExtension(String path) {
    final extension = p.extension(path).toLowerCase();

    if (extension == '.png') {
      return ImageTypes.png;
    }

    if (extension == '.jpg' || extension == '.jpeg') {
      return ImageTypes.jpg;
    }

    if (extension == '.gif') {
      return ImageTypes.gif;
    }

    throw ArgumentError('Unknown image extension: $extension');
  }
}
