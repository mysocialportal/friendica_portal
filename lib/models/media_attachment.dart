import 'package:path/path.dart' as p;

import '../globals.dart';
import 'attachment_media_type_enum.dart';
import 'image_entry.dart';
import 'visibility.dart';

class MediaAttachment {
  static final _graphicsExtensions = ['jpg', 'png', 'gif', 'tif'];
  static final _movieExtensions = ['avi', 'mp4', 'mpg', 'wmv'];

  final String id;

  final Uri uri;

  final int creationTimestamp;

  final Map<String, String> metadata;

  final AttachmentMediaType explicitType;

  final Uri thumbnailUri;

  final Uri fullFileUri;

  final String title;

  final String description;

  final Visibility visibility;

  late final String _extension;

  Uri get mainUri => _extension == '.png' ? fullFileUri : uri;

  Uri get usableThumbnailUri => _extension == '.png' ? fullFileUri : uri;

  MediaAttachment({
    required this.id,
    required this.uri,
    required this.creationTimestamp,
    required this.metadata,
    required this.thumbnailUri,
    required this.fullFileUri,
    required this.title,
    required this.explicitType,
    required this.description,
    required this.visibility,
  }) {
    _extension = p.extension(fullFileUri.path);
  }

  MediaAttachment.randomBuilt()
      : id = randomId(),
        uri = Uri.parse('http://localhost/${randomId()}'),
        creationTimestamp = DateTime.now().millisecondsSinceEpoch,
        fullFileUri = Uri.parse(''),
        title = 'Random title ${randomId()}',
        thumbnailUri = Uri.parse('${randomId()}.jpg'),
        description = 'Random description ${randomId()}',
        explicitType = AttachmentMediaType.image,
        metadata = {
          'value1': randomId(),
          'value2': randomId(),
        },
        visibility = Visibility.public();

  MediaAttachment.blank()
      : id = '',
        uri = Uri(),
        creationTimestamp = 0,
        thumbnailUri = Uri(),
        explicitType = AttachmentMediaType.unknown,
        title = '',
        fullFileUri = Uri(),
        description = '',
        metadata = {},
        visibility = Visibility.public();

  @override
  String toString() {
    return 'FriendicaMediaAttachment{id: $id, uri: $uri, creationTimestamp: $creationTimestamp, type: $explicitType, metadata: $metadata, title: $title, description: $description}';
  }

  ImageEntry toImageEntry() {
    return ImageEntry(
        id: id,
        album: '',
        filename: '',
        description: description,
        thumbnailUrl: thumbnailUri.toString(),
        created: DateTime.fromMillisecondsSinceEpoch(creationTimestamp),
        height: 0,
        width: 0,
        visibility: visibility,
        scales: []);
  }

  Map<String, dynamic> toJson() => {
        'uri': uri.toString(),
        'creationTimestamp': creationTimestamp,
        'metadata': metadata,
        'type': explicitType,
        'thumbnailUri': thumbnailUri.toString(),
        'title': title,
        'description': description,
      };

  static AttachmentMediaType mediaTypeFromString(String path) {
    final extension = p.extension(path);

    if (_graphicsExtensions.contains(extension)) {
      return AttachmentMediaType.image;
    }

    if (_movieExtensions.contains(extension)) {
      return AttachmentMediaType.video;
    }

    return AttachmentMediaType.unknown;
  }
}
