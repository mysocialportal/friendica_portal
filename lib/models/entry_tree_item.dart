import 'package:flutter/foundation.dart';

class EntryTreeItem {
  final String id;
  final bool isMine;
  bool isOrphaned;

  final _children = <String>{};

  EntryTreeItem(this.id,
      {this.isMine = true,
      this.isOrphaned = false,
      Iterable<String>? initialChildren}) {
    _children.addAll(initialChildren ?? {});
  }

  List<String> get children => List.unmodifiable(_children);

  factory EntryTreeItem.empty() => EntryTreeItem('');

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is EntryTreeItem &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          isMine == other.isMine &&
          isOrphaned == other.isOrphaned &&
          setEquals(_children, other._children);

  @override
  int get hashCode =>
      id.hashCode ^
      isMine.hashCode ^
      isOrphaned.hashCode ^
      Object.hashAll(_children);
}
