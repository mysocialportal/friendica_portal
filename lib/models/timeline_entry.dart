import '../globals.dart';
import 'connection.dart';
import 'delivery_data.dart';
import 'engagement_summary.dart';
import 'link_data.dart';
import 'link_preview_data.dart';
import 'location_data.dart';
import 'media_attachment.dart';
import 'timeline_network_info.dart';
import 'visibility.dart';

class TimelineEntry {
  final String id;

  final String parentId;

  final String parentAuthor;

  final String parentAuthorId;

  final int creationTimestamp;

  final int backdatedTimestamp;

  final int modificationTimestamp;

  final String body;

  final String title;

  final String spoilerText;

  final bool youReshared;

  final Visibility visibility;

  final String author;

  final String authorId;

  final String externalLink;

  final LocationData locationData;

  final bool isFavorited;

  final List<String> tags;

  final List<LinkData> links;

  final List<Connection> likes;

  final List<Connection> dislikes;

  final List<MediaAttachment> mediaAttachments;

  final TimelineNetworkInfo networkInfo;

  final LinkPreviewData? linkPreviewData;

  final EngagementSummary engagementSummary;

  final DeliveryData deliveryData;

  TimelineEntry(
      {this.id = '',
      this.parentId = '',
      this.creationTimestamp = 0,
      this.backdatedTimestamp = 0,
      this.modificationTimestamp = 0,
      this.youReshared = false,
      Visibility? visibility,
      this.body = '',
      this.title = '',
      this.spoilerText = '',
      this.author = '',
      this.authorId = '',
      this.parentAuthor = '',
      this.parentAuthorId = '',
      this.externalLink = '',
      this.locationData = const LocationData(),
      this.isFavorited = false,
      this.tags = const [],
      this.links = const [],
      this.likes = const [],
      this.dislikes = const [],
      this.mediaAttachments = const [],
      this.engagementSummary = const EngagementSummary(),
      this.networkInfo = TimelineNetworkInfo.empty,
      this.linkPreviewData,
      this.deliveryData = DeliveryData.empty})
      : visibility = visibility ?? Visibility.public();

  TimelineEntry.randomBuilt()
      : creationTimestamp = DateTime.now().millisecondsSinceEpoch,
        backdatedTimestamp = DateTime.now().millisecondsSinceEpoch,
        modificationTimestamp = DateTime.now().millisecondsSinceEpoch,
        id = randomId(),
        youReshared = DateTime.now().second ~/ 2 == 0 ? true : false,
        visibility = DateTime.now().second ~/ 2 == 0
            ? Visibility.public()
            : Visibility.private(),
        parentId = randomId(),
        externalLink = 'Random external link ${randomId()}',
        body = 'Random post text ${randomId()}',
        title = 'Random title ${randomId()}',
        spoilerText = 'Random spoiler text ${randomId()}',
        author = 'Random author ${randomId()}',
        authorId = 'Random authorId ${randomId()}',
        parentAuthor = 'Random parent author ${randomId()}',
        parentAuthorId = 'Random parent author id ${randomId()}',
        locationData = LocationData.randomBuilt(),
        isFavorited = DateTime.now().second ~/ 2 == 0 ? true : false,
        tags = [],
        links = [],
        likes = [],
        dislikes = [],
        mediaAttachments = [],
        networkInfo = TimelineNetworkInfo.empty,
        engagementSummary = const EngagementSummary(),
        linkPreviewData = LinkPreviewData(link: 'fake link'),
        deliveryData = DeliveryData.empty;

  TimelineEntry copy({
    int? creationTimestamp,
    int? backdatedTimestamp,
    int? modificationTimestamp,
    bool? isReshare,
    Visibility? visibility,
    String? id,
    String? reshareOriginalPostId,
    String? parentId,
    String? externalLink,
    String? body,
    String? title,
    String? spoilerText,
    String? author,
    String? authorId,
    String? parentAuthor,
    String? parentAuthorId,
    String? reshareAuthor,
    String? reshareAuthorId,
    LocationData? locationData,
    bool? isFavorited,
    List<String>? tags,
    List<LinkData>? links,
    List<Connection>? likes,
    List<Connection>? dislikes,
    List<MediaAttachment>? mediaAttachments,
    EngagementSummary? engagementSummary,
    TimelineNetworkInfo? networkInfo,
    LinkPreviewData? linkPreviewData,
    DeliveryData? deliveryData,
  }) {
    return TimelineEntry(
      creationTimestamp: creationTimestamp ?? this.creationTimestamp,
      backdatedTimestamp: backdatedTimestamp ?? this.backdatedTimestamp,
      modificationTimestamp:
          modificationTimestamp ?? this.modificationTimestamp,
      id: id ?? this.id,
      youReshared: isReshare ?? youReshared,
      visibility: visibility ?? this.visibility,
      parentId: parentId ?? this.parentId,
      externalLink: externalLink ?? this.externalLink,
      body: body ?? this.body,
      title: title ?? this.title,
      spoilerText: spoilerText ?? this.spoilerText,
      author: author ?? this.author,
      authorId: authorId ?? this.authorId,
      parentAuthor: parentAuthor ?? this.parentAuthor,
      parentAuthorId: parentAuthorId ?? this.parentAuthorId,
      locationData: locationData ?? this.locationData,
      isFavorited: isFavorited ?? this.isFavorited,
      tags: tags ?? this.tags,
      links: links ?? this.links,
      likes: likes ?? this.likes,
      dislikes: dislikes ?? this.dislikes,
      mediaAttachments: mediaAttachments ?? this.mediaAttachments,
      engagementSummary: engagementSummary ?? this.engagementSummary,
      networkInfo: networkInfo ?? this.networkInfo,
      linkPreviewData: linkPreviewData ?? this.linkPreviewData,
      deliveryData: deliveryData ?? this.deliveryData,
    );
  }

  @override
  String toString() {
    return 'TimelineEntry{id: $id, isReshare: $youReshared, isFavorited: $isFavorited, parentId: $parentId, creationTimestamp: $creationTimestamp, modificationTimestamp: $modificationTimestamp, backdatedTimeStamp: $backdatedTimestamp, post: $body, title: $title, author: $author, parentAuthor: $parentAuthor  externalLink:$externalLink}';
  }

  String toShortString() {
    return 'TimelineEntry{id: $id, isReshare: $youReshared, isFavorited: $isFavorited, parentId: $parentId, $engagementSummary}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TimelineEntry &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          parentId == other.parentId &&
          parentAuthor == other.parentAuthor &&
          parentAuthorId == other.parentAuthorId &&
          creationTimestamp == other.creationTimestamp &&
          backdatedTimestamp == other.backdatedTimestamp &&
          modificationTimestamp == other.modificationTimestamp &&
          body == other.body &&
          title == other.title &&
          spoilerText == other.spoilerText &&
          youReshared == other.youReshared &&
          visibility == other.visibility &&
          author == other.author &&
          authorId == other.authorId &&
          externalLink == other.externalLink &&
          locationData == other.locationData &&
          isFavorited == other.isFavorited &&
          tags == other.tags &&
          links == other.links &&
          likes == other.likes &&
          dislikes == other.dislikes &&
          mediaAttachments == other.mediaAttachments &&
          networkInfo == other.networkInfo &&
          engagementSummary == other.engagementSummary &&
          deliveryData == other.deliveryData;

  @override
  int get hashCode =>
      id.hashCode ^
      parentId.hashCode ^
      parentAuthor.hashCode ^
      parentAuthorId.hashCode ^
      creationTimestamp.hashCode ^
      backdatedTimestamp.hashCode ^
      modificationTimestamp.hashCode ^
      body.hashCode ^
      title.hashCode ^
      spoilerText.hashCode ^
      youReshared.hashCode ^
      visibility.hashCode ^
      author.hashCode ^
      authorId.hashCode ^
      externalLink.hashCode ^
      locationData.hashCode ^
      isFavorited.hashCode ^
      tags.hashCode ^
      links.hashCode ^
      likes.hashCode ^
      dislikes.hashCode ^
      mediaAttachments.hashCode ^
      networkInfo.hashCode ^
      engagementSummary.hashCode ^
      deliveryData.hashCode;
}
