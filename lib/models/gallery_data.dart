class GalleryData {
  final int count;
  final String name;
  final DateTime created;

  GalleryData({required this.count, required this.name, required this.created});

  GalleryData copy({String? name}) => GalleryData(
        count: count,
        name: name ?? this.name,
        created: created,
      );
}
