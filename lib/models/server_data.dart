class ServerData {
  final String domainName;
  final bool isFediverse;
  final String softwareName;
  final String softwareVersion;
  final List<String> protocols;

  ServerData(
      {required this.domainName,
      required this.isFediverse,
      this.softwareName = '',
      this.softwareVersion = '',
      List<String>? protocols})
      : protocols = protocols ?? <String>[];
}
