import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:result_monad/result_monad.dart';

import '../exec_error.dart';

abstract class ICredentials {
  String get authHeaderValue;

  String get serverName;

  String get id;

  FutureResult<ICredentials, ExecError> signIn(Ref ref);

  Map<String, dynamic> toJson();
}
