enum KnownNetworks {
  activityPub,
  bluesky,
  bookwyrm,
  calckey,
  diaspora,
  drupal,
  firefish,
  friendica,
  funkwhale,
  gnu_social,
  hometown,
  hubzilla,
  kbin,
  lemmy,
  mastodon,
  nextcloud,
  peertube,
  pixelfed,
  pleroma,
  plume,
  red,
  redmatrix,
  socialhome,
  threads,
  wordpress,
  unknown,
  ;

  static KnownNetworks parse(String? text) {
    if (text == null) {
      return unknown;
    }

    return values.firstWhere(
      (e) => e.name == text,
      orElse: () => unknown,
    );
  }
}

class TimelineNetworkInfo {
  static const empty = TimelineNetworkInfo(
    name: 'Unknown',
    vapidKey: '',
    network: KnownNetworks.unknown,
  );

  final String name;
  final String vapidKey;
  final KnownNetworks network;

  const TimelineNetworkInfo({
    required this.name,
    required this.vapidKey,
    required this.network,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TimelineNetworkInfo &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          vapidKey == other.vapidKey &&
          network == other.network;

  @override
  int get hashCode => name.hashCode ^ vapidKey.hashCode ^ network.hashCode;
}
