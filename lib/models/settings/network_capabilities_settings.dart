import '../timeline_network_info.dart';

class NetworkCapabilitiesSettings {
  late final List<NetworkCapabilitiesItem> _capabilities;
  late final Map<KnownNetworks, NetworkCapabilitiesItem> _capabilitiesKV;

  num get length => _capabilities.length;

  NetworkCapabilitiesSettings(
      {required List<NetworkCapabilitiesItem> capabilities}) {
    _capabilities = capabilities;
    _capabilitiesKV = {for (final c in capabilities) c.network: c};
  }

  NetworkCapabilitiesItem operator [](int i) => _capabilities[i];

  NetworkCapabilitiesItem getCapabilities(KnownNetworks network) =>
      _capabilitiesKV[network] ?? NetworkCapabilitiesItem.unknown();

  operator []=(int i, NetworkCapabilitiesItem item) {
    _capabilities[i] = item;
    _capabilitiesKV[item.network] = item;
  }

  factory NetworkCapabilitiesSettings.defaultSettings() {
    final networks = <KnownNetworks>{};
    networks.add(KnownNetworks.friendica);
    networks.add(KnownNetworks.mastodon);
    networks.add(KnownNetworks.threads);
    networks.add(KnownNetworks.bluesky);
    networks.add(KnownNetworks.diaspora);
    networks.add(KnownNetworks.pixelfed);
    networks.add(KnownNetworks.peertube);
    networks.add(KnownNetworks.unknown);
    networks.addAll(KnownNetworks.values);
    final capabilities = networks
        .map((e) => switch (e) {
              KnownNetworks.activityPub => NetworkCapabilitiesItem(
                  network: e,
                  react: true,
                  reshare: true,
                  comment: true,
                ),
              KnownNetworks.bluesky => NetworkCapabilitiesItem(
                  network: e,
                  react: true,
                  reshare: true,
                  comment: true,
                ),
              KnownNetworks.threads => NetworkCapabilitiesItem(
                  network: e,
                  react: true,
                  reshare: false,
                  comment: false,
                ),
              _ => NetworkCapabilitiesItem(
                  network: e,
                  react: true,
                  reshare: true,
                  comment: true,
                ),
            })
        .toList();
    return NetworkCapabilitiesSettings(capabilities: capabilities);
  }

  factory NetworkCapabilitiesSettings.fromJson(List<dynamic> json) {
    final capabilities =
        json.map((j) => NetworkCapabilitiesItem.fromJson(j)).toList();
    return NetworkCapabilitiesSettings(capabilities: capabilities);
  }

  List<Map<String, dynamic>> toJson() {
    return _capabilities.map((c) => c.toJson()).toList();
  }
}

class NetworkCapabilitiesItem {
  final KnownNetworks network;
  final bool react;
  final bool reshare;
  final bool comment;

  NetworkCapabilitiesItem({
    required this.network,
    required this.react,
    required this.reshare,
    required this.comment,
  });

  factory NetworkCapabilitiesItem.fromJson(Map<String, dynamic> json) =>
      NetworkCapabilitiesItem(
        network: KnownNetworks.parse(json['network']),
        react: json['react'] ?? true,
        reshare: json['reshare'] ?? true,
        comment: json['comment'] ?? true,
      );

  factory NetworkCapabilitiesItem.unknown() => NetworkCapabilitiesItem(
        network: KnownNetworks.unknown,
        react: true,
        reshare: true,
        comment: true,
      );

  NetworkCapabilitiesItem copyWith(
          {bool? react, bool? reshare, bool? comment}) =>
      NetworkCapabilitiesItem(
        network: network,
        react: react ?? this.react,
        reshare: reshare ?? this.reshare,
        comment: comment ?? this.comment,
      );

  Map<String, dynamic> toJson() => {
        'network': network.name,
        'react': react,
        'reshare': reshare,
        'comment': comment,
      };
}
