class TrendHistoryData {
  static final empty =
      TrendHistoryData(day: DateTime(1970), uses: 0, accounts: 0);
  final DateTime day;
  final int uses;
  final int accounts;

  const TrendHistoryData({
    required this.day,
    required this.uses,
    required this.accounts,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TrendHistoryData &&
          runtimeType == other.runtimeType &&
          day == other.day &&
          uses == other.uses &&
          accounts == other.accounts;

  @override
  int get hashCode => day.hashCode ^ uses.hashCode ^ accounts.hashCode;
}
