import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../routes.dart';
import 'current_profile_button.dart';

class StandardAppBar {
  static AppBar build(BuildContext context, String title,
      {bool withDrawer = false, bool withHome = true, List<Widget>? actions}) {
    return AppBar(
      leading: withDrawer ? const CurrentProfileButton() : null,
      title: Text(
        title,
        overflow: TextOverflow.ellipsis,
      ),
      actions: [
        if (actions != null) ...actions,
        if (withHome)
          IconButton(
            onPressed: () {
              context.goNamed(ScreenPaths.timelines);
            },
            icon: const Icon(Icons.home),
          ),
      ],
    );
  }
}
