import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class StatusAndRefreshButton extends ConsumerWidget {
  final bool executing;
  final Future<void> Function() refreshFunction;
  final Color? busyColor;
  final Color? buttonColor;

  const StatusAndRefreshButton({
    super.key,
    required this.executing,
    required this.refreshFunction,
    this.buttonColor,
    this.busyColor,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    if (executing) {
      final theme = Theme.of(context);
      final size = theme.appBarTheme.actionsIconTheme?.size ??
          theme.iconTheme.size ??
          24;
      return Center(
        child: SizedBox(
          width: size,
          height: size,
          child: CircularProgressIndicator(
            color: busyColor,
          ),
        ),
      );
    }

    return IconButton(
        onPressed: refreshFunction,
        icon: Icon(
          Icons.refresh,
          color: buttonColor,
        ));
  }
}
