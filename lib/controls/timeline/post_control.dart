import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

import '../../models/flattened_tree_item.dart';
import '../../riverpod_controllers/account_services.dart';
import '../../riverpod_controllers/entry_tree_item_services.dart';
import '../../riverpod_controllers/timeline_entry_services.dart';
import 'flattened_tree_entry_control.dart';

class PostControl extends ConsumerStatefulWidget {
  final String id;
  final String scrollToId;
  final bool openRemote;
  final bool showStatusOpenButton;
  final bool isRoot;

  const PostControl({
    super.key,
    required this.id,
    required this.scrollToId,
    required this.openRemote,
    required this.showStatusOpenButton,
    required this.isRoot,
  });

  @override
  ConsumerState<PostControl> createState() => _PostControlState();
}

class _PostControlState extends ConsumerState<PostControl> {
  static final _logger = Logger('$PostControl');

  final ItemScrollController itemScrollController = ItemScrollController();
  final ItemPositionsListener itemPositionsListener =
      ItemPositionsListener.create();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final profile = ref.watch(activeProfileProvider);
    ref.watch(timelineEntryManagerProvider(profile, widget.id));
    if (!widget.isRoot) {
      return ref
          .watch(entryTreeManagerProvider(profile, widget.id).notifier)
          .flattened(topLevelOnly: true)
          .fold(
              onSuccess: (flattenedItems) => FlattenedTreeEntryControl(
                    originalItem: flattenedItems.first,
                    openRemote: widget.openRemote,
                    showStatusOpenButton: widget.showStatusOpenButton,
                  ),
              onError: (_) => const SizedBox());
    }

    return ref
        .watch(entryTreeManagerProvider(profile, widget.id).notifier)
        .flattened()
        .fold(
            onSuccess: (items) => buildListView(context, items),
            onError: (_) => const SizedBox());
  }

  Widget buildListView(
    BuildContext context,
    List<FlattenedTreeItem> items,
  ) {
    final int count = items.length;
    final int scrollToIndex = _scrollToIndexCalc(items);

    // TODO Figure out why doesn't scroll to correct position on loading
    _logger.finest('Building view with initial position at $scrollToIndex');

    return ScrollablePositionedList.builder(
        physics: const AlwaysScrollableScrollPhysics(),
        itemCount: count,
        initialScrollIndex: scrollToIndex,
        itemScrollController: itemScrollController,
        itemPositionsListener: itemPositionsListener,
        itemBuilder: (context, index) {
          return FlattenedTreeEntryControl(
            originalItem: items[index],
            openRemote: widget.openRemote,
            showStatusOpenButton: widget.showStatusOpenButton,
          );
        });
  }

  int _scrollToIndexCalc(List<FlattenedTreeItem> items) {
    final scrollToIndexCalc = items.indexWhere(
      (e) => e.timelineEntry.id == widget.scrollToId,
    );
    if (scrollToIndexCalc < 0) {
      return 0;
    }

    return scrollToIndexCalc;
  }
}
