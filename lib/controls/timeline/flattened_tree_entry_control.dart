import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';

import '../../globals.dart';
import '../../models/auth/profile.dart';
import '../../models/filters/timeline_entry_filter.dart';
import '../../models/flattened_tree_item.dart';
import '../../models/timeline_entry.dart';
import '../../riverpod_controllers/account_services.dart';
import '../../riverpod_controllers/blocks_services.dart';
import '../../riverpod_controllers/connection_manager_services.dart';
import '../../riverpod_controllers/entry_tree_item_services.dart';
import '../../riverpod_controllers/settings_services.dart';
import '../../riverpod_controllers/timeline_entry_filter_services.dart';
import '../../riverpod_controllers/timeline_entry_services.dart';
import '../../riverpod_controllers/timeline_services.dart';
import '../../routes.dart';
import '../../utils/clipboard_utils.dart';
import '../../utils/filter_runner.dart';
import '../../utils/html_to_edit_text_helper.dart';
import '../../utils/network_utils.dart';
import '../../utils/responsive_sizes_calculator.dart';
import '../../utils/snackbar_builder.dart';
import '../../utils/url_opening_utils.dart';
import '../html_text_viewer_control.dart';
import '../media_attachment_viewer_control.dart';
import '../padding.dart';
import 'interactions_bar_control.dart';
import 'link_preview_control.dart';
import 'status_header_control.dart';

const maxDepth = 5.0;
const otherPadding = 8.0;

class FlattenedTreeEntryControl extends ConsumerStatefulWidget {
  final FlattenedTreeItem originalItem;
  final bool openRemote;
  final bool showStatusOpenButton;

  const FlattenedTreeEntryControl(
      {super.key,
      required this.originalItem,
      required this.openRemote,
      required this.showStatusOpenButton});

  @override
  ConsumerState<FlattenedTreeEntryControl> createState() =>
      _StatusControlState();
}

class _StatusControlState extends ConsumerState<FlattenedTreeEntryControl> {
  static final _logger = Logger('$FlattenedTreeEntryControl');

  var showSpoilerControl = true;
  var showContent = true;
  var showFilteredPost = false;
  var showComments = false;
  var isProcessing = false;
  var filteringInfo = FilterResult.show;
  var _processingTap = false;

  bool get isMine => widget.originalItem.isMine;

  @override
  void initState() {
    super.initState();
    showSpoilerControl = ref.read(spoilerHidingSettingProvider);
    showContent = !showSpoilerControl
        ? true
        : widget.originalItem.timelineEntry.spoilerText.isEmpty;
  }

  @override
  Widget build(BuildContext context) {
    final profile = ref.watch(activeProfileProvider);
    showSpoilerControl = ref.watch(spoilerHidingSettingProvider);
    final entry = ref
        .watch(timelineEntryManagerProvider(
            profile, widget.originalItem.timelineEntry.id))
        .getValueOrElse(() => widget.originalItem.timelineEntry);

    showComments = !entry.isPost;

    _logger.finest('Building ${entry.toShortString()}');

    filteringInfo = ref.watch(checkTimelineEntryProvider(profile, entry));

    final leftPadding =
        otherPadding + (min(maxDepth, widget.originalItem.level) * 15.0);
    final color = widget.originalItem.level.isOdd
        ? Theme.of(context).secondaryHeaderColor
        : Theme.of(context).dialogBackgroundColor;

    if (filteringInfo.isFiltered &&
        filteringInfo.action == TimelineEntryFilterAction.hide) {
      return kReleaseMode
          ? const SizedBox()
          : Container(
              height: 10,
              color: Colors.red,
            );
    }

    late final Widget body;
    if (filteringInfo.isFiltered && !showFilteredPost) {
      body = buildHiddenBody(context, filteringInfo);
    } else {
      body = buildMainWidgetBody(context, profile, entry);
    }

    final decoration = entry.isPost
        ? const BoxDecoration()
        : BoxDecoration(
            color: color,
            border: Border.all(width: 0.5),
            borderRadius: BorderRadius.circular(5.0),
            boxShadow: [
              BoxShadow(
                color: Theme.of(context).dividerColor,
                blurRadius: 2,
                offset: const Offset(4, 4),
                spreadRadius: 0.1,
                blurStyle: BlurStyle.normal,
              )
            ],
          );

    final bodyCard = Container(
      decoration: decoration,
      child: body,
    );
    return Padding(
      padding: EdgeInsets.only(
        left: leftPadding,
        right: otherPadding,
        top: otherPadding,
        bottom: otherPadding,
      ),
      child: GestureDetector(
          onTap: _processingTap
              ? null
              : () {
                  if (widget.showStatusOpenButton) {
                    if (_processingTap) {
                      return;
                    }
                    _tapProcessingStarted();
                    _goToPostView(entry);
                    _tapProcessingStop();
                  }
                },
          child: bodyCard),
    );
  }

  Widget buildMainWidgetBody(
    BuildContext context,
    Profile profile,
    TimelineEntry entry,
  ) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: StatusHeaderControl(
                  entry: entry,
                ),
              ),
              if (filteringInfo.isFiltered)
                IconButton(
                    onPressed: () => setState(() {
                          showFilteredPost = false;
                        }),
                    icon: const Icon(Icons.hide_source)),
              buildMenuControl(context, profile, entry),
            ],
          ),
          const VerticalPadding(
            height: 5,
          ),
          if (showSpoilerControl && entry.spoilerText.isNotEmpty)
            TextButton(
                onPressed: () {
                  setState(() {
                    _logger.info('Toggling show content');
                    showContent = !showContent;
                  });
                },
                child: Text(
                    'Content Summary: ${entry.spoilerText} (Click to ${showContent ? "Hide" : "Show"})')),
          if (showContent || !showSpoilerControl) ...[
            buildContentField(context, profile, entry),
            const VerticalPadding(
              height: 5,
            ),
            if (entry.linkPreviewData != null)
              LinkPreviewControl(preview: entry.linkPreviewData!),
            buildMediaBar(context, entry),
          ],
          const VerticalPadding(
            height: 5,
          ),
          InteractionsBarControl(
            entry: entry,
            isMine: isMine,
            showOpenControl: widget.showStatusOpenButton,
          ),
          const VerticalPadding(
            height: 5,
          ),
        ],
      ),
    );
  }

  Widget buildHiddenBody(BuildContext context, FilterResult result) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (result.isFiltered &&
              result.action == TimelineEntryFilterAction.warn)
            TextButton(
              onPressed: () {
                setState(() {
                  showFilteredPost = true;
                });
              },
              child: Text(
                  '${result.trippingFilterName} filtered post. Click to show'),
            ),
        ],
      ),
    );
  }

  Widget buildContentField(
      BuildContext context, Profile profile, TimelineEntry entry) {
    return HtmlTextViewerControl(
      content: entry.body,
      onTapUrl: (url) async {
        await processUrlStringForTag(url).withResultAsync((tagName) async {
          final openInApp = ref.read(openTagsInAppProvider);
          if (openInApp) {
            await context.push('${ScreenPaths.tagView}/$tagName');
            return;
          }

          final usersTagSearchPage =
              generateTagSearchFromProfile(profile, tagName);
          await openUrlStringInSystembrowser(
              context, usersTagSearchPage, 'link');
        }).withErrorAsync((_) async {
          await openUrlStringInSystembrowser(context, url, 'link');
        });

        return true;
      },
    );
  }

  Widget buildMediaBar(BuildContext context, TimelineEntry entry) {
    final items = entry.mediaAttachments;
    if (items.isEmpty) {
      return const SizedBox();
    }

    // A Link Preview with only one media attachment will have a duplicate image
    // even though it points to different resources server side. So we don't
    // want to render it twice.
    if (entry.linkPreviewData != null && items.length == 1) {
      return const SizedBox();
    }

    // A Diaspora reshare will have an HTML-built card with a link preview image
    // to the same image as what would be in the single attachment but at a
    // different link. So we don't want it to render twice.
    final linkPhotoBaseUrl = Uri.https(
      ref.read(activeProfileProvider).serverName,
      'photo/link',
    ).toString();
    if (entry.body.contains(linkPhotoBaseUrl) && items.length == 1) {
      return const SizedBox();
    }

    final width = items.length > 1
        ? ResponsiveSizesCalculator(context).maxThumbnailWidth
        : ResponsiveSizesCalculator(context).viewPortalWidth;
    return SizedBox(
        height: ResponsiveSizesCalculator(context).maxThumbnailHeight,
        child: ListView.separated(
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return MediaAttachmentViewerControl(
                attachments: items,
                index: index,
                width: width,
                height: ResponsiveSizesCalculator(context).maxThumbnailHeight,
              );
            },
            separatorBuilder: (context, index) {
              return const HorizontalPadding();
            },
            itemCount: items.length));
  }

  Widget buildMenuControl(
    BuildContext context,
    Profile profile,
    TimelineEntry entry,
  ) {
    const editStatus = 'Edit';
    const deleteStatus = 'Delete';
    const divider = 'Divider';
    const goToPost = 'Open Post';
    const copyText = 'Copy Post Text';
    const copyUrl = 'Copy URL';
    const openExternal = 'Open In Browser';
    const showLikers = 'Show Likers';
    const showResharers = 'Show Reshares';
    const blockUser = 'Block User';
    final options = [
      if (widget.showStatusOpenButton && !widget.openRemote) goToPost,
      if (isMine && !entry.youReshared) editStatus,
      if (isMine) deleteStatus,
      divider,
      showLikers,
      showResharers,
      divider,
      blockUser,
      divider,
      openExternal,
      copyUrl,
      copyText,
    ];

    if (options.first == divider) {
      options.removeAt(0);
    }

    return PopupMenuButton<String>(onSelected: (menuOption) async {
      if (!mounted) {
        return;
      }

      switch (menuOption) {
        case goToPost:
          _goToPostView(entry);
          break;
        case editStatus:
          if (entry.isPost) {
            context.push('/post/edit/${entry.id}');
          } else {
            context.push('/comment/edit/${entry.id}');
          }
          break;
        case deleteStatus:
          deleteEntry(profile, entry);
          break;
        case openExternal:
          await openUrlStringInSystembrowser(
            context,
            entry.externalLink,
            'Post',
          );
          break;
        case copyUrl:
          await copyToClipboard(
            context: context,
            text: entry.externalLink,
            message: 'Post link copied to clipboard',
          );
          break;
        case copyText:
          await copyToClipboard(
            context: context,
            text: htmlToSimpleText(entry.body),
            message: 'Post link copied to clipboard',
          );
          break;
        case showLikers:
          await context.push('/likes/${entry.id}');
          break;
        case showResharers:
          await context.push('/reshares/${entry.id}');
          break;
        case blockUser:
          await _blockUser(profile, entry);
          break;
        default:
        //do nothing
      }
    }, itemBuilder: (context) {
      return options
          .map(
            (o) => PopupMenuItem(
              value: o,
              enabled: switch (o) {
                divider => false,
                showResharers => entry.engagementSummary.rebloggedCount > 0,
                showLikers => entry.engagementSummary.favoritesCount > 0,
                _ => true,
              },
              child: o == divider ? const Divider() : Text(o),
            ),
          )
          .toList();
    });
  }

  Future<void> deleteEntry(Profile profile, TimelineEntry entry) async {
    setState(() {
      isProcessing = true;
    });
    final confirm = await showYesNoDialog(
      context,
      'Delete ${entry.isPost ? "Post" : "Comment"}',
    );
    if (confirm == true) {
      await ref
          .read(timelineUpdaterProvider(profile).notifier)
          .deleteEntryById(entry.id)
          .match(onSuccess: (_) {
        isProcessing = false;
        if (entry.isPost && context.canPop()) {
          context.pop();
        }
      }, onError: (e) {
        isProcessing = false;
        buildSnackbar(
          context,
          'Error deleting ${entry.isPost ? "Post" : "Comment"}: $e',
        );
      });
    }
    setState(() {});
  }

  Future<void> _blockUser(Profile profile, TimelineEntry entry) async {
    ref
        .read(connectionByIdProvider(profile, entry.authorId))
        .withResult((author) async {
      final confirmed =
          await showYesNoDialog(context, 'Block ${author.name}') ?? false;
      if (confirmed) {
        final status = await ref
            .read(blocksManagerProvider(profile).notifier)
            .blockConnection(author)
            .withResult((_) => ref
                .read(timelineMaintainerProvider(profile).notifier)
                .refreshAll())
            .fold(
                onSuccess: (_) => '${author.name} is block',
                onError: (error) => 'Error blocking ${author.name}: $error');
        if (mounted) {
          buildSnackbar(context, status);
        }
      }
    });
  }

  void _goToPostView(TimelineEntry entry) {
    context.push('/post/view/${entry.id}/${entry.id}');
  }

  void _tapProcessingStarted() {
    setState(() {
      _processingTap = true;
    });
    Future.delayed(const Duration(seconds: 20), () {
      if (mounted) {
        _tapProcessingStop();
      }
    });
  }

  void _tapProcessingStop() {
    setState(() {
      _processingTap = false;
    });
  }
}

extension _TimelineExtensions on TimelineEntry {
  bool get isPost => parentId.isEmpty;
}
