import 'package:flutter/material.dart';

import '../../globals.dart';
import '../../models/timeline_network_info.dart';
import '../../utils/known_network_extensions.dart';

class TimelineNetworkInfoControl extends StatelessWidget {
  final TimelineNetworkInfo info;

  const TimelineNetworkInfoControl({super.key, required this.info});

  @override
  Widget build(BuildContext context) {
    const genericNetworks = [KnownNetworks.unknown, KnownNetworks.activityPub];
    final networkText =
        genericNetworks.contains(info.network) ? info.name : info.labelName;
    return GestureDetector(
      onTap: () async => showConfirmDialog(context, networkText),
      child: Tooltip(
        message: networkText,
        child: Text(
          info.dSocFontUnicode,
          style: const TextStyle(fontFamily: 'DSoc'),
        ),
      ),
    );
  }
}
