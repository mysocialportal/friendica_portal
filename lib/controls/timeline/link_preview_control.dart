import 'package:flutter/material.dart';

import '../../models/link_preview_data.dart';
import '../../utils/string_utils.dart';
import '../../utils/url_opening_utils.dart';
import '../image_control.dart';

class LinkPreviewControl extends StatelessWidget {
  final LinkPreviewData preview;

  const LinkPreviewControl({super.key, required this.preview});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(width: 0.5),
            borderRadius: BorderRadius.circular(2.0)),
        child: GestureDetector(
          onTap: () async {
            await openUrlStringInSystembrowser(context, preview.link, 'link');
          },
          child: Column(
            children: [
              ImageControl(imageUrl: preview.selectedImageUrl),
              ListTile(
                  title: Text(preview.title,
                      style: const TextStyle(fontWeight: FontWeight.bold)),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        preview.siteName,
                        style: const TextStyle(fontStyle: FontStyle.italic),
                      ),
                      Text(preview.description.truncate(length: 128)),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
