import 'package:flutter/material.dart';

import '../models/attachment_media_type_enum.dart';
import '../models/media_attachment.dart';
import '../screens/media_viewer_screen.dart';
import 'audio_video/av_control.dart';
import 'image_control.dart';

class MediaAttachmentViewerControl extends StatefulWidget {
  final List<MediaAttachment> attachments;
  final int index;
  final double? width;
  final double? height;

  const MediaAttachmentViewerControl({
    super.key,
    required this.attachments,
    required this.index,
    this.width,
    this.height,
  });

  @override
  State<MediaAttachmentViewerControl> createState() =>
      _MediaAttachmentViewerControlState();
}

class _MediaAttachmentViewerControlState
    extends State<MediaAttachmentViewerControl> {
  @override
  Widget build(BuildContext context) {
    final item = widget.attachments[widget.index];
    final width = widget.width == null ? null : widget.width! * 0.9;
    final height = widget.height;
    openMediaScreenCallback() {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return MediaViewerScreen(
          attachments: widget.attachments,
          initialIndex: widget.index,
        );
      }));
    }

    if (item.explicitType == AttachmentMediaType.video) {
      return AVControl(
        videoUrl: item.mainUri.toString(),
        width: width,
        height: height,
        description: item.description,
        onGoFullScreen: openMediaScreenCallback,
      );
    }
    if (item.explicitType != AttachmentMediaType.image) {
      return Text('${item.explicitType}: ${item.mainUri}');
    }

    return ImageControl(
      width: width,
      height: height,
      imageUrl: item.usableThumbnailUri.toString(),
      altText: item.description,
      onTap: openMediaScreenCallback,
    );
  }
}
