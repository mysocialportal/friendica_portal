import 'package:flutter/material.dart';

import '../globals.dart';

class ResponsiveMaxWidth extends StatelessWidget {
  final Widget child;

  const ResponsiveMaxWidth({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    final border =
        BorderSide(color: Theme.of(context).highlightColor, width: 1);
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints.loose(
          const Size.fromWidth(maxViewPortalWidth),
        ),
        child: Container(
            decoration:
                BoxDecoration(border: Border(left: border, right: border)),
            child: child),
      ),
    );
  }
}
