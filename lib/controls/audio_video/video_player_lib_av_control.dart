import 'dart:math';

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerLibAvControl extends StatefulWidget {
  final String videoUrl;
  final double? width;
  final double? height;

  const VideoPlayerLibAvControl({
    super.key,
    required this.videoUrl,
    required this.width,
    required this.height,
  });

  @override
  State<VideoPlayerLibAvControl> createState() =>
      _VideoPlayerLibAvControlState();
}

class _VideoPlayerLibAvControlState extends State<VideoPlayerLibAvControl> {
  late final VideoPlayerController videoPlayerController;
  var needsToLoad = true;

  @override
  void initState() {
    super.initState();
    videoPlayerController =
        VideoPlayerController.networkUrl(Uri.parse(widget.videoUrl));
  }

  @override
  void dispose() {
    videoPlayerController.dispose();
    super.dispose();
  }

  @override
  void deactivate() {
    videoPlayerController.pause();
    super.deactivate();
  }

  void toggleVideoPlay() async {
    if (needsToLoad) {
      await videoPlayerController.initialize();
      needsToLoad = false;
    }
    videoPlayerController.value.isPlaying
        ? videoPlayerController.pause()
        : videoPlayerController.play();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final size = videoPlayerController.value.size;
    double? videoWidth;
    double? videoHeight;
    if (needsToLoad) {
      videoWidth = widget.width;
      videoHeight = widget.height;
    } else {
      final horizontalScale =
          widget.width == null ? 1 : widget.width! / size.width;
      final verticalScale =
          widget.height == null ? 1 : widget.height! / size.height;
      final scaling = min(horizontalScale, verticalScale);
      videoWidth = scaling * size.width;
      videoHeight = scaling * size.height;
    }

    return GestureDetector(
      onTap: toggleVideoPlay,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: videoWidth,
            height: videoHeight,
            child: needsToLoad
                ? Container(
                    color: Colors.black,
                  )
                : AspectRatio(
                    aspectRatio: videoPlayerController.value.aspectRatio,
                    child: VideoPlayer(videoPlayerController),
                  ),
          ),
          Row(
            children: [
              IconButton(
                icon: videoPlayerController.value.isPlaying
                    ? const Icon(Icons.pause)
                    : const Icon(Icons.play_arrow),
                onPressed: toggleVideoPlay,
              ),
              IconButton(
                  onPressed: () {
                    videoPlayerController.seekTo(Duration.zero);
                  },
                  icon: const Icon(Icons.replay)),
            ],
          )
        ],
      ),
    );
  }
}
