import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../globals.dart';
import '../../riverpod_controllers/settings_services.dart';
import '../../utils/snackbar_builder.dart';
import 'media_kit_av_control.dart';
import 'video_player_lib_av_control.dart';

final _shownVideos = <String>{};

const _useVideoPlayer = kIsWeb;
final _useMediaKit = Platform.isAndroid ||
    Platform.isIOS ||
    Platform.isWindows ||
    Platform.isMacOS ||
    (kReleaseMode && Platform.isLinux);

class AVControl extends ConsumerStatefulWidget {
  final String videoUrl;
  final String description;
  final double? width;
  final double? height;
  final bool alwaysShow;
  final Function()? onGoFullScreen;

  const AVControl({
    super.key,
    required this.videoUrl,
    this.width,
    this.height,
    this.alwaysShow = false,
    this.onGoFullScreen,
    required this.description,
  });

  @override
  ConsumerState<AVControl> createState() => _AVControlState();
}

class _AVControlState extends ConsumerState<AVControl> {
  void showVideo() {
    _shownVideos.add(widget.videoUrl);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final lowBandwidthMode = ref.watch(lowBandwidthModeSettingProvider);
    final shown = widget.alwaysShow ||
        !lowBandwidthMode ||
        _shownVideos.contains(widget.videoUrl);

    final placeHolderBox = SizedBox(
      width: widget.width,
      height: widget.height,
      child: const Card(
          color: Colors.black12,
          shape: RoundedRectangleBorder(),
          child: Icon(Icons.movie)),
    );

    if (!shown) {
      return GestureDetector(
        onTap: showVideo,
        child: placeHolderBox,
      );
    }

    _shownVideos.add(widget.videoUrl);

    if (_useVideoPlayer) {
      return VideoPlayerLibAvControl(
        videoUrl: widget.videoUrl,
        width: widget.width,
        height: widget.height,
      );
    }

    if (_useMediaKit) {
      return MediaKitAvControl(
        videoUrl: widget.videoUrl,
        width: widget.width,
        height: widget.height,
        onGoFullScreen: widget.onGoFullScreen,
      );
    }

    return buildStandinWidget(context);
  }

  Widget buildStandinWidget(BuildContext context) {
    return GestureDetector(
        onTap: () async {
          final confirm = await showYesNoDialog(
              context, 'Open Link in external app? ${widget.videoUrl}');
          if (confirm != true) {
            return;
          }
          if (await canLaunchUrl(Uri.parse(widget.videoUrl))) {
            if (context.mounted) {
              buildSnackbar(
                context,
                'Attempting to launch video: ${widget.videoUrl}',
              );
            }
            await launchUrl(Uri.parse(widget.videoUrl));
          } else {
            if (context.mounted) {
              buildSnackbar(
                  context, 'Unable to launch video: ${widget.videoUrl}');
            }
          }
        },
        child: SizedBox(
          height: widget.height,
          width: widget.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    widget.description.isNotEmpty
                        ? widget.description
                        : 'Video: ${widget.videoUrl}',
                    softWrap: true,
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
