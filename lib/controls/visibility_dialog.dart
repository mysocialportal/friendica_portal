import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../models/visibility.dart' as v;
import '../models/auth/profile.dart';
import '../models/timeline_grouping_list_data.dart';
import '../riverpod_controllers/circles_repo_services.dart';
import '../riverpod_controllers/connection_manager_services.dart';

Future<bool?> showVisibilityDialog(
  BuildContext context,
  WidgetRef ref,
  Profile profile,
  v.Visibility visibility,
) async {
  final circlesMap = {
    for (var item
        in ref.read(timelineGroupingListProvider(profile, GroupingType.circle)))
      item.id: item
  };

  final allowedCircles = visibility.allowedCircleIds.map((c) {
    if (c == '~') {
      return 'Followers';
    }

    return circlesMap[c]?.name ?? 'Circle #$c';
  }).toList();

  final excludedCircles = visibility.excludedCircleIds.map((c) {
    if (c == '~') {
      return 'Followers';
    }

    return circlesMap[c]?.name ?? 'Circle #$c';
  }).toList();

  final allowedUsers = visibility.allowedUserIds
      .map(
        (u) => ref.read(connectionByIdProvider(profile, u)).fold(
              onSuccess: (connection) => connection.handle,
              onError: (_) => 'User $u',
            ),
      )
      .toList();

  final excludedUsers = visibility.excludedUserIds
      .map(
        (u) => ref.read(connectionByIdProvider(profile, u)).fold(
              onSuccess: (connection) => connection.handle,
              onError: (_) => 'User $u',
            ),
      )
      .toList();

  return showDialog<bool>(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return AlertDialog(
        content: SizedBox(
          width: MediaQuery.sizeOf(context).width * 0.8,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Visibility Details',
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .bodyLarge
                    ?.copyWith(decoration: TextDecoration.underline),
              ),
              if (visibility.type == v.VisibilityType.public) ...[
                const Text('Public')
              ],
              if (visibility.type == v.VisibilityType.unlisted) ...[
                const Text('Unlisted')
              ],
              if (visibility.type != v.VisibilityType.public) ...[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Allowed Users: ',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Expanded(
                      child: Text(
                        allowedUsers.isEmpty
                            ? 'Empty'
                            : allowedUsers.join(', '),
                        softWrap: true,
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Text(
                      'Allowed Circles: ',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(
                      allowedCircles.isEmpty
                          ? 'Empty'
                          : allowedCircles.join(','),
                      softWrap: true,
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Text(
                      'Excluded Users: ',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(
                      excludedUsers.isEmpty ? 'Empty' : excludedUsers.join(','),
                      softWrap: true,
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Text(
                      'Excluded Circles: ',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(
                      excludedCircles.isEmpty
                          ? 'Empty'
                          : excludedCircles.join(','),
                      softWrap: true,
                    )
                  ],
                ),
              ],
            ],
          ),
        ),
        actions: <Widget>[
          ElevatedButton(
            child: const Text('Dismiss'),
            onPressed: () {
              Navigator.pop(context, true); // showDialog() returns true
            },
          ),
        ],
      );
    },
  );
}
