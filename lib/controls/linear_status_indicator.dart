import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class StandardLinearProgressIndicator extends StatelessWidget {
  final ValueListenable<bool> valueListenable;

  const StandardLinearProgressIndicator(
    this.valueListenable, {
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: valueListenable,
        builder: (context2, executing, _) {
          if (executing) {
            return const LinearProgressIndicator();
          }

          return const SizedBox();
        });
  }
}
