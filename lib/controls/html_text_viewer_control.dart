import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:html/dom.dart' as dom;

import 'image_control.dart';

const _emojiSize = {'width': '20px', 'height': '20px', 'margin-left': '-3px'};

class HtmlTextViewerControl extends StatelessWidget {
  final String content;
  final FutureOr<bool> Function(String)? onTapUrl;

  const HtmlTextViewerControl(
      {super.key, required this.content, this.onTapUrl});

  @override
  Widget build(BuildContext context) {
    return HtmlWidget(
      content,
      factoryBuilder: () => MyWidgetFactory(),
      customStylesBuilder: _defaultStylesBuilder,
      onTapUrl: onTapUrl,
    );
  }

  Map<String, String>? _defaultStylesBuilder(dom.Element element) {
    if (element.classes.contains('emoji')) {
      return _emojiSize;
    }

    return null;
  }
}

class MyWidgetFactory extends WidgetFactory {
  @override
  Widget? buildImageWidget(BuildTree tree, ImageSource src) {
    final url = src.url;
    if (!url.startsWith(RegExp('https?://'))) {
      return super.buildImageWidget(tree, src);
    }

    return ImageControl(imageUrl: url);
  }
}
