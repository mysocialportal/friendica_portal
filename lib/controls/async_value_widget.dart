import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

typedef LoadingWidgetBuilder = Widget Function(
  BuildContext context,
  WidgetRef ref,
);

typedef ValueWidgetBuilder<T> = Widget Function(
  BuildContext context,
  WidgetRef ref,
  T value,
);

class AsyncValueWidget<T> extends ConsumerWidget {
  final AsyncValue<T> asyncValue;
  final ValueWidgetBuilder<T> valueBuilder;
  final LoadingWidgetBuilder? loadingBuilder;
  final bool standaloneHolders;

  const AsyncValueWidget(
    this.asyncValue, {
    required this.valueBuilder,
    this.standaloneHolders = false,
    this.loadingBuilder,
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return switch (asyncValue) {
      AsyncError(:final error) => !standaloneHolders
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Text('Error getting data: $error')],
              ),
            )
          : Text('Error getting data: $error'),
      AsyncData<T>(:final value) => valueBuilder(context, ref, value),
      _ => loadingBuilder != null
          ? loadingBuilder!(context, ref)
          : !standaloneHolders
              ? const Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                    ],
                  ),
                )
              : const CircularProgressIndicator(),
    };
  }
}
