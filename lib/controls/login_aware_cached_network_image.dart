import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:stack_trace/stack_trace.dart';

import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/networking/friendica_client_services.dart';

class LoginAwareCachedNetworkImage extends ConsumerWidget {
  static final _logger = Logger('$LoginAwareCachedNetworkImage');
  final String imageUrl;
  final double? width;
  final double? height;

  const LoginAwareCachedNetworkImage({
    super.key,
    required this.imageUrl,
    this.width,
    this.height,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final profile = ref.watch(activeProfileProvider);

    try {
      final imageServer = Uri.parse(imageUrl).host;
      final isFromHomeServer = imageServer == profile.serverName;

      final headers = ref.read(friendicaClientHeadersProvider(
        profile,
        withContentString: false,
        withAuthorization: isFromHomeServer,
      ));

      return CachedNetworkImage(
        httpHeaders: headers,
        imageUrl: imageUrl,
        width: width,
        height: height,
      );
    } catch (e) {
      _logger.severe('Error Parsing ImageURL: $e', Trace.current());
    }

    return SizedBox(
      width: width,
      height: height,
    );
  }
}
