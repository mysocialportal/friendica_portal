import 'package:flutter/material.dart' hide Visibility;

import '../../globals.dart';
import '../../models/image_entry.dart';
import '../../models/visibility.dart';
import '../../screens/existing_image_selector_screen.dart';
import '../../screens/media_viewer_screen.dart';
import '../../serializers/friendica/image_entry_friendica_extensions.dart';
import '../login_aware_cached_network_image.dart';
import '../padding.dart';

class GallerySelectorControl extends StatefulWidget {
  final List<ImageEntry> entries;

  final Visibility? visibilityFilter;

  const GallerySelectorControl({
    super.key,
    required this.entries,
    this.visibilityFilter,
  });

  @override
  State<GallerySelectorControl> createState() => _GallerySelectorControlState();
}

class _GallerySelectorControlState extends State<GallerySelectorControl> {
  @override
  Widget build(BuildContext context) {
    // TODO Replace with scaling size
    var thumbnailSize = 100.0;
    return Column(children: [
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text(
          'Existing Images',
          style: Theme.of(context).textTheme.titleLarge,
        ),
        IconButton(
          onPressed: () async {
            final result = await Navigator.of(context).push<List<ImageEntry>>(
                MaterialPageRoute(
                    builder: (context) => ExistingImageSelectorScreen(
                        visibilityFilter: widget.visibilityFilter)));
            if (result != null) {
              setState(() {
                widget.entries.clear();
                widget.entries.addAll(result);
              });
            }
          },
          icon: const Icon(Icons.photo_library),
        ),
      ]),
      const VerticalDivider(),
      if (widget.entries.isNotEmpty)
        SizedBox(
          height: thumbnailSize,
          child: ListView.separated(
            scrollDirection: Axis.horizontal,
            separatorBuilder: (context, index) => const HorizontalPadding(
              width: 5,
            ),
            itemBuilder: (context, index) {
              final item = widget.entries[index];
              return GestureDetector(
                onDoubleTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => MediaViewerScreen(
                        attachments: widget.entries
                            .map((i) => i.toMediaAttachment())
                            .toList(),
                        initialIndex: index,
                      ),
                    ),
                  );
                },
                onTap: () async {
                  final confirm =
                      await showYesNoDialog(context, 'Remove image from post?');

                  if (confirm == true) {
                    setState(() {
                      widget.entries.remove(item);
                    });
                  }
                },
                child: LoginAwareCachedNetworkImage(
                  width: thumbnailSize,
                  height: thumbnailSize,
                  imageUrl: item.thumbnailUrl,
                ),
              );
            },
            itemCount: widget.entries.length,
          ),
        ),
    ]);
  }
}
