import 'package:flutter/material.dart';

import '../../models/media_attachment_uploads/media_upload_attachment.dart';
import '../padding.dart';

class MediaUploadEditorControl extends StatefulWidget {
  final MediaUploadAttachment media;
  final Function()? onDelete;

  const MediaUploadEditorControl(
      {super.key, required this.media, this.onDelete});

  @override
  State<MediaUploadEditorControl> createState() =>
      _MediaUploadEditorControlState();
}

class _MediaUploadEditorControlState extends State<MediaUploadEditorControl> {
  @override
  Widget build(BuildContext context) {
    // TODO Replace with scaling size
    const thumbnailsize = 100.0;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: thumbnailsize,
                height: thumbnailsize,
                child: widget.media.getPreviewImage(),
              ),
              const HorizontalPadding(),
              Expanded(
                  child: Column(
                children: [
                  TextFormField(
                    initialValue: widget.media.remoteFilename,
                    onChanged: (value) => widget.media.remoteFilename = value,
                    decoration: InputDecoration(
                      labelText: 'Filename (optional)',
                      alignLabelWithHint: true,
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Theme.of(context).colorScheme.surface,
                        ),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                  ),
                ],
              )),
              IconButton(
                onPressed: widget.onDelete,
                icon: const Icon(Icons.cancel),
              ),
            ],
          ),
          const VerticalPadding(),
          Column(
            children: [
              TextFormField(
                initialValue: widget.media.description,
                onChanged: (value) => widget.media.description = value,
                textCapitalization: TextCapitalization.sentences,
                spellCheckConfiguration: const SpellCheckConfiguration(),
                maxLines: 5,
                decoration: InputDecoration(
                  labelText: 'Description/ALT Text',
                  alignLabelWithHint: true,
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Theme.of(context).colorScheme.surface,
                    ),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
              ),
              const Divider(),
            ],
          ),
        ],
      ),
    );
  }
}
