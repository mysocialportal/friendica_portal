import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../riverpod_controllers/account_services.dart';
import '../../riverpod_controllers/autocomplete_services.dart';
import '../../riverpod_controllers/hashtag_service.dart';

class HashtagAutocompleteOptions extends ConsumerWidget {
  const HashtagAutocompleteOptions({
    super.key,
    required this.id,
    required this.query,
    required this.onHashtagTap,
  });

  final String id;
  final String query;
  final ValueSetter<String> onHashtagTap;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final profile = ref.watch(activeProfileProvider);
    final postTreeHashtags = ref
        .watch(postTreeHashtagsProvider(profile, id))
        .getValueOrElse(() => []);
    final hashtagsFromService =
        ref.watch(hashtagServiceProvider(profile, searchString: query));
    final hashtags = [...postTreeHashtags, ...hashtagsFromService];

    if (hashtags.isEmpty) return const SizedBox.shrink();

    return Card(
      margin: const EdgeInsets.all(8),
      elevation: 2,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      clipBehavior: Clip.hardEdge,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            color: const Color(0xFFF7F7F8),
            child: ListTile(
              dense: true,
              horizontalTitleGap: 0,
              title: Text("Hashtags matching '$query'"),
            ),
          ),
          LimitedBox(
            maxHeight: MediaQuery.sizeOf(context).height * 0.2,
            child: ListView.separated(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              itemCount: hashtags.length,
              separatorBuilder: (_, __) => const Divider(),
              itemBuilder: (context, i) {
                final hashtag = hashtags[i];
                return GestureDetector(
                  onTap: () => onHashtagTap(hashtag),
                  child: Text(hashtag),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
