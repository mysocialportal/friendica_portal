import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../models/connection.dart';
import '../../riverpod_controllers/account_services.dart';
import '../../riverpod_controllers/autocomplete_services.dart';
import '../../riverpod_controllers/connection_manager_services.dart';
import '../image_control.dart';

class MentionAutocompleteOptions extends ConsumerWidget {
  const MentionAutocompleteOptions({
    super.key,
    required this.id,
    required this.query,
    required this.onMentionUserTap,
  });

  final String id;
  final String query;
  final ValueSetter<Connection> onMentionUserTap;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final profile = ref.watch(activeProfileProvider);
    final postTreeUsers = ref
        .watch(postTreeConnectionIdsProvider(profile, id))
        .getValueOrElse(() => [])
        .map((id) => ref.watch(connectionByIdProvider(profile, id)))
        .where((result) => result.isSuccess)
        .map((result) => result.value)
        .toList()
      ..sort((u1, u2) => u1.name.compareTo(u2.name));

    final knownUsers = ref.watch(knownUsersByNameProvider(profile, query));

    final users = [...postTreeUsers, ...knownUsers];

    if (users.isEmpty) return const SizedBox.shrink();

    return Card(
      margin: const EdgeInsets.all(8),
      elevation: 2,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      clipBehavior: Clip.hardEdge,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            color: const Color(0xFFF7F7F8),
            child: ListTile(
              dense: true,
              horizontalTitleGap: 0,
              title: Text("Users matching '$query'"),
            ),
          ),
          LimitedBox(
            maxHeight: MediaQuery.sizeOf(context).height * 0.2,
            child: ListView.separated(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              itemCount: users.length,
              separatorBuilder: (_, __) => const Divider(height: 0),
              itemBuilder: (context, i) {
                final user = users.elementAt(i);
                return ListTile(
                    dense: true,
                    leading: ImageControl(
                      imageUrl: user.avatarUrl.toString(),
                      iconOverride: const Icon(Icons.person),
                      width: 25,
                      height: 25,
                      onTap: () => onMentionUserTap(user),
                    ),
                    title: Text(user.name),
                    subtitle: Text('@${user.handle}'),
                    onTap: () {
                      onMentionUserTap(user);
                    });
              },
            ),
          ),
        ],
      ),
    );
  }
}
