import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../riverpod_controllers/account_services.dart';

class CurrentProfileButton extends ConsumerWidget {
  const CurrentProfileButton({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    if (!ref.read(activeProfileProvider.notifier).hasActiveProfile) {
      return const SizedBox();
    }

    final profile = ref.watch(activeProfileProvider);
    return Builder(builder: (context) {
      return IconButton(
          onPressed: () {
            Scaffold.of(context).openDrawer();
          },
          icon: CachedNetworkImage(imageUrl: profile.avatar));
    });
  }
}
