import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../models/connection.dart';
import '../models/exec_error.dart';
import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/connection_manager_services.dart';
import '../routes.dart';
import 'image_control.dart';

class ConnectionListWidget extends ConsumerWidget {
  final String contactId;

  const ConnectionListWidget({super.key, required this.contactId});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final profile = ref.watch(activeProfileProvider);
    final contactResult = ref.watch(connectionByIdProvider(profile, contactId));

    return contactResult.fold(onSuccess: (contact) {
      return ListTile(
        onTap: () {
          context.pushNamed(ScreenPaths.userProfile,
              pathParameters: {'id': contact.id});
        },
        leading: ImageControl(
          imageUrl: contact.avatarUrl.toString(),
          iconOverride: const Icon(Icons.person),
          width: 32.0,
        ),
        title: Text(
          '${contact.name} (${contact.handle})',
          softWrap: true,
        ),
        subtitle: Text(
          'Last Status: ${contact.lastStatus?.toIso8601String() ?? "Unknown"}',
          softWrap: true,
        ),
        trailing: Text(contact.status.label()),
      );
    }, onError: (error) {
      final msg = error.type == ErrorType.notFound
          ? 'Connection not found'
          : error.message;
      return ListTile(title: Text(msg));
    });
  }
}
