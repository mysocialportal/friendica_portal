import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:wheel_chooser/wheel_chooser.dart';

import '../models/focus_mode_data.dart';
import '../riverpod_controllers/focus_mode.dart';
import '../routes.dart';
import 'padding.dart';

enum Difficulty {
  easy(10),
  medium(1000),
  difficult(1000000),
  extreme(1000000000);

  final int maxNumber;

  const Difficulty(this.maxNumber);

  String get label => switch (this) {
        easy => 'Easy',
        medium => 'Medium',
        difficult => 'Difficult',
        extreme => 'Extremely Difficult',
      };
}

class FocusModeMenuItem extends ConsumerWidget {
  const FocusModeMenuItem({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final focusMode = ref.watch(focusModeProvider);
    final title =
        focusMode.enabled ? 'Disable Focus Mode' : 'Enable Focus Mode';
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListTile(
        title: Text(title),
        onTap: () async {
          if (focusMode.enabled) {
            context.pop();
            context.push(ScreenPaths.focusModeDisable);
          } else {
            final focusMode = await _chooseFocusMode(context);
            if (focusMode == null) {
              return;
            }

            ref.read(focusModeProvider.notifier).setMode(focusMode);
            if (context.mounted) {
              context.pop();
              context.go(ScreenPaths.timelines);
            }
          }
        },
      ),
    );
  }
}

Future<FocusModeData?> _chooseFocusMode(
  BuildContext context,
) {
  int hours = 0;
  int minutes = 30;
  var difficulty = Difficulty.medium;

  return showDialog<FocusModeData?>(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) => StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Choose Focus Duration and Difficulty',
                  softWrap: true,
                  style: Theme.of(context)
                      .textTheme
                      .bodyLarge!
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                const VerticalPadding(),
                DropdownButton<Difficulty>(
                  value: difficulty,
                  onChanged: (v) => setState(() => difficulty = v!),
                  items: Difficulty.values
                      .map((e) => DropdownMenuItem(
                            value: e,
                            child: Text(e.label),
                          ))
                      .toList(),
                ),
                const VerticalPadding(),
                Wrap(
                  runSpacing: 10.0,
                  spacing: 10.0,
                  children: [
                    ElevatedButton(
                      child: const Text('15 minutes'),
                      onPressed: () {
                        Navigator.pop(
                          context,
                          FocusModeData(
                            true,
                            maxNumber: difficulty.maxNumber,
                            disableTime: DateTime.now().add(
                              const Duration(
                                minutes: 15,
                              ),
                            ),
                          ),
                        ); // showDialog() returns true
                      },
                    ),
                    ElevatedButton(
                      child: const Text('30 minutes'),
                      onPressed: () {
                        Navigator.pop(
                          context,
                          FocusModeData(
                            true,
                            maxNumber: difficulty.maxNumber,
                            disableTime: DateTime.now().add(
                              const Duration(
                                minutes: 30,
                              ),
                            ),
                          ),
                        ); // showDialog() returns true
                      },
                    ),
                    ElevatedButton(
                      child: const Text('1 hour'),
                      onPressed: () {
                        Navigator.pop(
                          context,
                          FocusModeData(
                            true,
                            maxNumber: difficulty.maxNumber,
                            disableTime: DateTime.now().add(
                              const Duration(
                                hours: 1,
                              ),
                            ),
                          ),
                        ); // showDialog() returns true
                      },
                    ),
                    ElevatedButton(
                      child: const Text('6 hours'),
                      onPressed: () {
                        Navigator.pop(
                          context,
                          FocusModeData(
                            true,
                            maxNumber: difficulty.maxNumber,
                            disableTime: DateTime.now().add(
                              const Duration(
                                hours: 6,
                              ),
                            ),
                          ),
                        ); // showDialog() returns true
                      },
                    ),
                    ElevatedButton(
                      child: const Text('12 hours'),
                      onPressed: () {
                        Navigator.pop(
                          context,
                          FocusModeData(
                            true,
                            maxNumber: difficulty.maxNumber,
                            disableTime: DateTime.now().add(
                              const Duration(
                                hours: 12,
                              ),
                            ),
                          ),
                        ); // showDialog() returns true
                      },
                    ),
                    ElevatedButton(
                      child: const Text('1 day'),
                      onPressed: () {
                        Navigator.pop(
                          context,
                          FocusModeData(
                            true,
                            maxNumber: difficulty.maxNumber,
                            disableTime: DateTime.now().add(
                              const Duration(
                                days: 1,
                              ),
                            ),
                          ),
                        ); // showDialog() returns true
                      },
                    ),
                    ElevatedButton(
                      child: const Text('Forever'),
                      onPressed: () {
                        Navigator.pop(
                          context,
                          FocusModeData(
                            true,
                            maxNumber: difficulty.maxNumber,
                          ),
                        ); // showDialog() returns true
                      },
                    ),
                  ],
                ),
                const VerticalPadding(),
                SizedBox(
                  height: 100,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Flexible(
                        child: WheelChooser.integer(
                          initValue: hours,
                          onValueChanged: (v) => hours = v,
                          maxValue: 24,
                          minValue: 0,
                          unSelectTextStyle:
                              const TextStyle(color: Colors.grey),
                        ),
                      ),
                      const Text('hours'),
                      Flexible(
                        child: WheelChooser.integer(
                          initValue: minutes,
                          onValueChanged: (v) => minutes = v,
                          maxValue: 59,
                          minValue: 0,
                          unSelectTextStyle:
                              const TextStyle(color: Colors.grey),
                        ),
                      ),
                      const Text('minutes'),
                    ],
                  ),
                )
              ],
            ),
            actions: [
              ElevatedButton(
                child: const Text('Select'),
                onPressed: () {
                  Navigator.pop(
                    context,
                    FocusModeData(
                      true,
                      maxNumber: difficulty.maxNumber,
                      disableTime: DateTime.now().add(
                        Duration(
                          hours: hours,
                          minutes: minutes,
                        ),
                      ),
                    ),
                  ); // showDialog() returns true
                },
              ),
            ]);
      },
    ),
  );
}
