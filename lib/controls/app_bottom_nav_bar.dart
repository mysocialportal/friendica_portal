import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../riverpod_controllers/account_services.dart';
import '../riverpod_controllers/notification_services.dart';
import '../routes.dart';

enum NavBarButtons {
  timelines,
  notifications,
  contacts,
  explore,
}

class AppBottomNavBar extends ConsumerWidget {
  final NavBarButtons currentButton;
  final Function()? onHomeButtonReclick;

  const AppBottomNavBar({
    super.key,
    required this.currentButton,
    this.onHomeButtonReclick,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final profile = ref.watch(activeProfileProvider);
    ref.watch(notificationsManagerProvider(profile));
    final hasNotifications = ref
        .read(notificationsManagerProvider(profile).notifier)
        .hasNotifications;

    return BottomNavigationBar(
      onTap: (index) {
        final newButton = _indexToButton(index);
        if (newButton == currentButton) {
          if (newButton == NavBarButtons.timelines &&
              onHomeButtonReclick != null) {
            onHomeButtonReclick!();
          }
          return;
        }

        switch (newButton) {
          case NavBarButtons.timelines:
            context.go(ScreenPaths.timelines);
            break;
          case NavBarButtons.notifications:
            context.pushNamed(ScreenPaths.notifications);
            break;
          case NavBarButtons.contacts:
            context.pushNamed(ScreenPaths.contacts);
            break;
          case NavBarButtons.explore:
            context.pushNamed(ScreenPaths.explore);
            break;
        }
      },
      type: BottomNavigationBarType.fixed,
      currentIndex: _buttonToIndex(currentButton),
      items: _menuItems(context, hasNotifications),
    );
  }

  int _buttonToIndex(NavBarButtons button) {
    switch (button) {
      case NavBarButtons.timelines:
        return 0;
      case NavBarButtons.notifications:
        return 1;
      case NavBarButtons.contacts:
        return 2;
      case NavBarButtons.explore:
        return 3;
    }
  }

  NavBarButtons _indexToButton(int index) {
    if (index == 0) {
      return NavBarButtons.timelines;
    }

    if (index == 1) {
      return NavBarButtons.notifications;
    }

    if (index == 2) {
      return NavBarButtons.contacts;
    }

    if (index == 3) {
      return NavBarButtons.explore;
    }

    throw ArgumentError('$index has no button type');
  }

  List<BottomNavigationBarItem> _menuItems(
      BuildContext context, bool hasNotifications) {
    return [
      const BottomNavigationBarItem(
        label: 'Timelines',
        icon: Icon(Icons.home_outlined),
        activeIcon: Icon(Icons.home_filled),
      ),
      BottomNavigationBarItem(
        label: 'Notifications',
        icon: Icon(hasNotifications
            ? Icons.notifications_active_outlined
            : Icons.notifications_none_outlined),
        activeIcon: Icon(hasNotifications
            ? Icons.notifications_active
            : Icons.notifications),
      ),
      const BottomNavigationBarItem(
        label: 'Contacts',
        icon: Icon(Icons.people_outline),
        activeIcon: Icon(Icons.people_sharp),
      ),
      const BottomNavigationBarItem(
        label: 'Explore',
        icon: Icon(Icons.explore_outlined),
        activeIcon: Icon(Icons.explore),
      ),
    ];
  }
}
