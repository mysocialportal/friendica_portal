import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:logging/logging.dart';
import 'package:stack_trace/stack_trace.dart';

import '../riverpod_controllers/account_services.dart';
import '../routes.dart';
import 'focus_mode_menu_item.dart';
import 'login_aware_cached_network_image.dart';

class StandardAppDrawer extends ConsumerWidget {
  final bool skipPopDismiss;

  static final _logger = Logger('$StandardAppDrawer');

  const StandardAppDrawer({
    super.key,
    this.skipPopDismiss = false,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final activeProfile = ref.watch(activeProfileProvider);
    return SafeArea(
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            ...ref.watch(loggedInProfilesProvider).map(
                  (p) => ListTile(
                    onTap: () async {
                      ref
                          .read(activeProfileProvider.notifier)
                          .setActiveProfile(p);
                      if (!skipPopDismiss &&
                          context.mounted &&
                          context.canPop()) {
                        try {
                          context.pop();
                        } catch (e) {
                          context.go(ScreenPaths.timelines);
                          _logger.severe(
                              'Error trying to close the drawer, going home: $e',
                              Trace.current());
                        }
                      }
                    },
                    leading: CircleAvatar(
                        child:
                            LoginAwareCachedNetworkImage(imageUrl: p.avatar)),
                    title: Text(
                      p.username,
                      style: p == activeProfile
                          ? const TextStyle(fontWeight: FontWeight.bold)
                          : null,
                    ),
                    subtitle: Text(
                      p.serverName,
                      style: p == activeProfile
                          ? const TextStyle(fontWeight: FontWeight.bold)
                          : null,
                    ),
                  ),
                ),
            buildMenuButton(
              context,
              'Manage Profiles',
              () => context.pushNamed(ScreenPaths.manageProfiles),
            ),
            const FocusModeMenuItem(),
            const Divider(),
            buildMenuButton(
              context,
              'Gallery',
              () => context.pushNamed(ScreenPaths.gallery),
            ),
            buildMenuButton(
              context,
              'Direct Messages',
              () => context.pushNamed(ScreenPaths.messages),
            ),
            const Divider(),
            buildMenuButton(
              context,
              'Blocks',
              () => context.pushNamed(ScreenPaths.blocks),
            ),
            buildMenuButton(
              context,
              'Filters',
              () => context.pushNamed(ScreenPaths.filters),
            ),
            buildMenuButton(
              context,
              'Circles Management',
              () => context.pushNamed(ScreenPaths.circleManagement),
            ),
            buildMenuButton(
              context,
              'Settings',
              () => context.pushNamed(ScreenPaths.settings),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildMenuButton(BuildContext context, String title, Function() onTap) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListTile(
        title: Text(title),
        onTap: () {
          context.pop();
          onTap();
        },
      ),
    );
  }
}
