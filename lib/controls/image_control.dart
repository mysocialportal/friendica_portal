import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../globals.dart';
import '../riverpod_controllers/settings_services.dart';
import 'login_aware_cached_network_image.dart';

final _shownImageUrls = <String>{};

class ImageControl extends ConsumerStatefulWidget {
  final String imageUrl;
  final double? width;
  final double? height;
  final Function()? onTap;
  final Function()? onDoubleTap;
  final Icon? iconOverride;
  final String altText;

  const ImageControl({
    super.key,
    required this.imageUrl,
    this.iconOverride,
    this.width,
    this.height,
    this.onTap,
    this.onDoubleTap,
    this.altText = '',
  });

  @override
  ConsumerState<ImageControl> createState() => _ImageControlState();
}

class _ImageControlState extends ConsumerState<ImageControl> {
  void showImage() {
    _shownImageUrls.add(widget.imageUrl);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final shown = !ref.watch(lowBandwidthModeSettingProvider) ||
        _shownImageUrls.contains(widget.imageUrl);
    late final Widget image;

    if (shown && widget.imageUrl.isNotEmpty) {
      _shownImageUrls.add(widget.imageUrl);
      image = SizedBox(
        height: widget.height,
        width: widget.width,
        child: Stack(
          children: [
            LoginAwareCachedNetworkImage(
              imageUrl: widget.imageUrl,
              width: widget.width,
              height: widget.height,
            ),
            if (widget.altText.isNotEmpty)
              Positioned(
                bottom: 5.0,
                left: 5.0,
                child: ElevatedButton(
                  onPressed: () async => await showInfoDialog(
                    context,
                    widget.altText,
                  ),
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Theme.of(context)
                          .scaffoldBackgroundColor
                          .withValues(alpha: 0.7)),
                  child: const Text('ALT'),
                ),
              ),
          ],
        ),
      );
    } else {
      final icon = widget.iconOverride ??
          const Icon(
            Icons.image,
          );
      image = SizedBox(
        width: widget.width,
        height: widget.height,
        child: Stack(
          children: [
            SizedBox(
              width: widget.width,
              height: widget.height,
              child: Card(
                  color: Colors.black12,
                  shape: const RoundedRectangleBorder(),
                  child: icon),
            ),
            Positioned(
              child: SizedBox(
                width: widget.width,
                height: widget.height,
                child: Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Text(
                    widget.altText,
                    textAlign: TextAlign.center,
                    maxLines: 5,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
    return GestureDetector(
      onTap: shown ? widget.onTap : showImage,
      child: image,
    );
  }
}
