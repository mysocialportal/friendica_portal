import 'package:flutter/material.dart';

class ErrorMessageWidget extends StatelessWidget {
  final String message;
  final bool standalone;

  const ErrorMessageWidget(
      {super.key, required this.message, this.standalone = true});

  @override
  Widget build(BuildContext context) {
    final errorWidget = Text(message);

    if (standalone) {
      return errorWidget;
    }

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [errorWidget],
      ),
    );
  }
}
