import 'dart:async';

import 'package:flutter/material.dart';

import '../utils/dateutils.dart';

class FocusModeStatusHeadline extends StatefulWidget {
  final DateTime? disableTime;

  const FocusModeStatusHeadline({super.key, required this.disableTime});

  @override
  State<FocusModeStatusHeadline> createState() =>
      _FocusModeStatusHeadlineState();
}

class _FocusModeStatusHeadlineState extends State<FocusModeStatusHeadline> {
  Timer? updateTimer;
  Duration? timeUntil;

  @override
  void initState() {
    super.initState();
    _updateTimeUntil();
    updateTimer = Timer.periodic(const Duration(seconds: 1), (_) {
      setState(() {
        _updateTimeUntil();
      });
    });
  }

  @override
  void dispose() {
    updateTimer?.cancel();
    super.dispose();
  }

  void _updateTimeUntil() {
    timeUntil = widget.disableTime?.difference(DateTime.now());
  }

  @override
  Widget build(BuildContext context) {
    final title = timeUntil == null
        ? 'Focus Mode for Forever'
        : 'Focus Mode for ${timeUntil!.simpleLabel}';
    return Text(
      title,
      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
            fontWeight: FontWeight.bold,
          ),
    );
  }
}
