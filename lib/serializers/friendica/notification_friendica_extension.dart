import '../../models/user_notification.dart';

extension NotificationFriendicaExtension on UserNotification {
  static UserNotification fromJson(Map<String, dynamic> json) =>
      UserNotification(
        id: json['id'].toString(),
        type: NotificationType.parse(json['type']),
        fromId: json['uid'],
        fromName: json['name'],
        fromUrl: json['url'],
        timestamp: int.tryParse(json['timestamp'] ?? '') ?? 0,
        iid: json['iid'].toString(),
        dismissed: json['seen'] ?? false,
        content: json['msg_html'],
        link: json['link'],
      );
}
