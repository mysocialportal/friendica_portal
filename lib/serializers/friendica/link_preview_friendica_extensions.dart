import '../../models/link_preview_data.dart';
import '../../utils/html_to_edit_text_helper.dart';
import '../../utils/string_utils.dart';

extension LinkPreviewExtension on LinkPreviewData {
  String toBodyAttachment() {
    if (selectedImageUrl.isEmpty) {
      return "[attachment type='link' url='$link' title='$title']$description[/attachment]";
    }

    final sanitizedTitle = htmlToSimpleText(title).stripHyperlinks();
    final sanitizedDescription =
        htmlToSimpleText(description).stripHyperlinks();

    return "[attachment type='link' url='$link' title='$sanitizedTitle' image='$selectedImageUrl']$sanitizedDescription[/attachment]";
  }
}
