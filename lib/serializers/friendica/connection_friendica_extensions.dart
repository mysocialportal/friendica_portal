import '../../models/connection.dart';

extension ConnectionFriendicaExtensions on Connection {
  static Connection fromJson(Map<String, dynamic> json) {
    final status = json['following']
        ? ConnectionStatus.youFollowThem
        : ConnectionStatus.none;
    final name = json['name'] ?? '';
    final id = json['id_str'] ?? '';
    final profileUrl = json['url'] ?? '';
    final network = json['network'] ?? 'unkn';
    final avatarUrl = json['profile_image_url_large'];

    return Connection(
        status: status,
        name: name,
        id: id,
        profileUrl: profileUrl,
        avatarUrl: avatarUrl,
        network: network);
  }
}
