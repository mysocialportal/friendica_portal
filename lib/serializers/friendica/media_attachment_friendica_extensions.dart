import '../../models/attachment_media_type_enum.dart';
import '../../models/media_attachment.dart';
import '../../models/visibility.dart';

extension MediaAttachmentFriendicaExtensions on MediaAttachment {
  static MediaAttachment fromJson(
    Map<String, dynamic> json,
    Visibility visibility,
  ) {
    final id = json['id'];
    final uri = Uri.parse(json['url']);
    const creationTimestamp = 0;
    final metadata = (json['metadata'] as Map<String, dynamic>? ?? {})
        .map((key, value) => MapEntry(key, value.toString()));
    final explicitType = (json['mimetype'] ?? '').startsWith('image')
        ? AttachmentMediaType.image
        : (json['mimetype'] ?? '').startsWith('video')
            ? AttachmentMediaType.video
            : AttachmentMediaType.unknown;
    final thumbnailUri = Uri();
    const title = '';
    const description = '';

    return MediaAttachment(
      id: id,
      uri: uri,
      fullFileUri: uri,
      creationTimestamp: creationTimestamp,
      metadata: metadata,
      thumbnailUri: thumbnailUri,
      title: title,
      explicitType: explicitType,
      description: description,
      visibility: visibility,
    );
  }
}
