import 'package:logging/logging.dart';
import 'package:stack_trace/stack_trace.dart';

import '../../models/direct_message.dart';
import '../../utils/dateutils.dart';

final _logger = Logger('DirectMessageFriendicaExtension');

extension DirectMessageFriendicaExtension on DirectMessage {
  static DirectMessage fromJson(Map<String, dynamic> json) {
    final id = json['id'].toString();

    final senderId = json['sender_id'].toString();

    final senderScreenName = json['sender_screen_name'];

    final recipientId = json['recipient_id'].toString();

    final recipientScreenName = json['recipient_screen_name'];

    final title = json['title'];

    final text = json['text'];

    final createdAt = json.containsKey('created_at')
        ? OffsetDateTimeUtils.epochSecTimeFromFriendicaString(
                json['created_at'])
            .fold(
                onSuccess: (value) => value,
                onError: (error) {
                  _logger.severe(
                    "Couldn't read date time string: $error",
                    Trace.current(),
                  );
                  return 0;
                })
        : 0;

    final bool seen = json['friendica_seen'];

    final String parentUri = json['friendica_parent_uri'];

    return DirectMessage(
      id: id,
      senderId: senderId,
      senderScreenName: senderScreenName,
      recipientId: recipientId,
      recipientScreenName: recipientScreenName,
      title: title,
      text: text,
      createdAt: createdAt,
      seen: seen,
      parentUri: parentUri,
    );
  }
}
