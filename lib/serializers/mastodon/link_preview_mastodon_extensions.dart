import '../../models/link_preview_data.dart';

extension LinkPreviewMastodonExtensions on LinkPreviewData {
  static LinkPreviewData? fromJson(Map<String, dynamic>? json) {
    if (json == null) {
      return null;
    }
    final link = json['url'];
    final title = json['title'];
    final description = json['description'];
    final image = json['image']?.toString() ?? '';
    final siteName = json['provider_name'];
    final images = image.isEmpty ? <String>[] : [image];
    return LinkPreviewData(
      link: link,
      title: title,
      description: description,
      siteName: siteName,
      selectedImageUrl: image,
      availableImageUrls: images,
    );
  }
}
