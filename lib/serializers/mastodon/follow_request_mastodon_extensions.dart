import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../../models/auth/profile.dart';
import '../../models/follow_request.dart';
import '../../models/user_notification.dart';
import '../../utils/dateutils.dart';
import 'connection_mastodon_extensions.dart';

extension FollowRequestMastodonExtension on FollowRequest {
  UserNotification toUserNotification() {
    return UserNotification(
      id: id,
      type: NotificationType.follow_request,
      fromId: connection.id,
      fromName: connection.name,
      fromUrl: connection.profileUrl,
      timestamp: createdAtEpochSeconds,
      iid: '',
      dismissed: false,
      content:
          '${connection.name}(${connection.handle}) submitted a follow request ',
      link: '',
    );
  }
}

FollowRequest followRequestFromJson(
  Ref ref,
  Profile profile,
  Map<String, dynamic> json,
) {
  final connection = connectionFromJson(ref, profile, json);
  final id = json['id'] ?? const Uuid().v4();
  final int timestamp = json.containsKey('created_at')
      ? OffsetDateTimeUtils.epochSecTimeFromTimeZoneString(json['created_at'])
          .fold(
          onSuccess: (value) => value,
          onError: (error) => 0,
        )
      : 0;
  return FollowRequest(
    id: id,
    connection: connection,
    createdAtEpochSeconds: timestamp,
  );
}
