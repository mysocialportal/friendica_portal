import '../../models/hashtag.dart';
import '../../models/trend_history_data.dart';

extension HashtagMastodonExtensions on Hashtag {
  static Hashtag fromJson(Map<String, dynamic> json) {
    final tag = json['name'];
    final url = json['url'];
    return Hashtag(tag: tag, url: url);
  }
}

extension HashtagWithTrendingMastodonExtensions on Hashtag {
  static HashtagWithTrending fromJson(Map<String, dynamic> json) {
    final name = json['name'];
    final url = json['url'];
    final following = json['following'];
    final List<dynamic> historyList = json['history'];
    late final TrendHistoryData history;
    if (historyList.isNotEmpty) {
      final Map<String, dynamic> h = historyList.first;
      final dayEpSec = int.parse(h['day']);
      final day = DateTime.fromMillisecondsSinceEpoch(dayEpSec * 1000);
      final uses = int.parse(h['uses']);
      final accounts = int.parse(h['accounts']);
      history = TrendHistoryData(day: day, uses: uses, accounts: accounts);
    } else {
      history = TrendHistoryData.empty;
    }

    return HashtagWithTrending(
      name: name,
      url: url,
      following: following,
      history: history,
    );
  }
}
