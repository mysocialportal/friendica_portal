import '../../models/timeline_network_info.dart';

extension TimelineNetworkInfoMastodonExtensions on TimelineNetworkInfo {
  static TimelineNetworkInfo fromJson(Map<String, dynamic> json) {
    final String? applicationName = json['application']?['name'];
    final String? name = json['friendica']?['platform'] ?? applicationName;
    if (name == null) {
      return TimelineNetworkInfo.empty;
    }

    final vapidKey = json['vapid_key'] ?? '';
    final nameMainPart = name.split('(').first.trim();
    final KnownNetworks network = switch (nameMainPart.toLowerCase()) {
      'activitypub' => KnownNetworks.activityPub,
      'akkoma' => KnownNetworks.pleroma,
      'bluesky' => KnownNetworks.bluesky,
      'bookwyrm' => KnownNetworks.bookwyrm,
      'diaspora' => KnownNetworks.diaspora,
      'friendica' => KnownNetworks.friendica,
      'friendika' => KnownNetworks.friendica,
      'gnu social' => KnownNetworks.gnu_social,
      'gnusocial' => KnownNetworks.gnu_social,
      'hubzilla' => KnownNetworks.hubzilla,
      'hometown' => KnownNetworks.hometown,
      'mastodon' => KnownNetworks.mastodon,
      'peertube' => KnownNetworks.peertube,
      'pixelfed' => KnownNetworks.pixelfed,
      'pleroma' => KnownNetworks.pleroma,
      'red' => KnownNetworks.hubzilla,
      'redmatrix' => KnownNetworks.hubzilla,
      'socialhome' => KnownNetworks.socialhome,
      'wordpress' => KnownNetworks.wordpress,
      'lemmy' => KnownNetworks.lemmy,
      'plume' => KnownNetworks.plume,
      'funkwhale' => KnownNetworks.funkwhale,
      'nextcloud' => KnownNetworks.nextcloud,
      'drupal' => KnownNetworks.drupal,
      'firefish' => KnownNetworks.firefish,
      'calckey' => KnownNetworks.calckey,
      'kbin' => KnownNetworks.kbin,
      'threads' => KnownNetworks.threads,
      _ => name.contains('(AP)')
          ? KnownNetworks.activityPub
          : KnownNetworks.unknown,
    };

    return TimelineNetworkInfo(
      name: name,
      vapidKey: vapidKey,
      network: network,
    );
  }
}
