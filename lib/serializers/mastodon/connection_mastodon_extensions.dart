import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../models/auth/profile.dart';
import '../../models/connection.dart';

Connection connectionFromJson(
  Ref ref,
  Profile profile,
  Map<String, dynamic> json, {
  String defaultServerName = '',
}) {
  final name = json['display_name'] ?? '';
  final id = json['id']?.toString() ?? '';
  var profileUrl = Uri.parse(json['url'] ?? '');
  const network = 'Unknown';
  final avatar = Uri.tryParse(json['avatar_static'] ?? '') ?? Uri();
  final String handleFromJson = json['acct'];
  final String note = json['note'] ?? '';
  final int followersCount = json['followers_count'];
  final int followingCount = json['following_count'];
  final int statusesCount = json['statuses_count'];
  final lastStatus = DateTime.tryParse(json['last_status_at'] ?? '');

  late final String handle;
  if (handleFromJson.contains('@')) {
    handle = handleFromJson;
    final handleElements = handleFromJson.split('@');
    if (handleElements.last == 'threads.net') {
      profileUrl =
          Uri.parse('https://www.threads.net/@${handleElements.first}');
    }
  } else {
    final server = profile.serverName;
    handle = '$handleFromJson@$server';
    if (server == 'threads.net') {
      profileUrl = Uri.parse('https://www.threads.net/@$handleFromJson');
    }
  }

  if (profileUrl.scheme == 'did') {
    profileUrl = Uri.parse('https://bsky.app/profile/$handleFromJson');
  }

  return Connection(
    name: name,
    id: id,
    handle: handle,
    profileUrl: profileUrl.toString(),
    network: network,
    avatarUrl: avatar.toString(),
    note: note,
    followerCount: followersCount,
    followingCount: followingCount,
    statusesCount: statusesCount,
    lastStatus: lastStatus,
  );
}
