import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../models/auth/profile.dart';
import '../../models/search_results.dart';
import 'connection_mastodon_extensions.dart';
import 'timeline_entry_mastodon_extensions.dart';

SearchResults searchResultFromJson(
  Ref ref,
  Profile profile,
  Map<String, dynamic> json,
) {
  final accounts = (json['accounts'] as List<dynamic>? ?? [])
      .map((j) => connectionFromJson(ref, profile, j))
      .toList();

  final statuses = (json['statuses'] as List<dynamic>? ?? [])
      .map((j) => timelineEntryFromJson(ref, profile, j))
      .toList();

  final hashtags = (json['hashtags'] as List<dynamic>? ?? [])
      .map((j) => j.toString())
      .toList();

  return SearchResults(
    accounts: accounts,
    statuses: statuses,
    hashtags: hashtags,
  );
}
