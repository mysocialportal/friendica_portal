import '../../models/timeline_grouping_list_data.dart';

extension TimelineGroupingListDataMastodonExtensions
    on TimelineGroupingListData {
  static TimelineGroupingListData fromJson(Map<String, dynamic> json) {
    final id = json['id']?.toString() ?? '';
    final typeString = json['replies_policy']?.toString() ?? '';

    late final GroupingType type;
    if (typeString == 'followed') {
      if (id.startsWith('channel')) {
        type = GroupingType.channel;
      } else if (id.startsWith('group')) {
        type = GroupingType.group;
      } else {
        type = GroupingType.circle;
      }
    } else {
      type = GroupingType.circle;
    }
    return TimelineGroupingListData(
      id,
      json['title'],
      type,
    );
  }
}
