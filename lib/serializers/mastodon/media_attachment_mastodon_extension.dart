import '../../models/attachment_media_type_enum.dart';
import '../../models/media_attachment.dart';
import '../../models/visibility.dart';

extension MediaAttachmentMastodonExtension on MediaAttachment {
  static MediaAttachment fromJson(
      Map<String, dynamic> json, Visibility visibility) {
    return MediaAttachment(
        id: json['id'] ?? '',
        uri: Uri.parse(json['url'] ?? 'http://localhost'),
        creationTimestamp: 0,
        metadata: {},
        thumbnailUri: Uri.parse(json['url'] ?? 'http://localhost'),
        title: '',
        fullFileUri: Uri.parse(json['remote_url'] ?? 'http://localhost'),
        explicitType: AttachmentMediaType.parse(json['type']),
        description: json['description'] ?? '',
        visibility: visibility);
  }
}
