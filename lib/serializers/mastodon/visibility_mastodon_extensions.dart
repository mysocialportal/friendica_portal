import '../../models/timeline_grouping_list_data.dart';
import '../../models/visibility.dart';

extension VisibilityMastodonExtensions on Visibility {
  String toCreateStatusValue(bool onComment) {
    if (type == VisibilityType.public) {
      return 'public';
    }

    if (type == VisibilityType.unlisted) {
      return 'unlisted';
    }

    if (!onComment && hasDetails) {
      final circleId =
          allowedCircleIds.firstOrNull ?? allowedUserIds.firstOrNull;
      if (circleId == TimelineGroupingListData.followersPseudoCircle.id) {
        return 'private';
      }

      return circleId ?? 'private';
    }

    if (onComment && !hasDetails && type == VisibilityType.private) {
      return 'direct';
    }

    return 'private';
  }

  Map<String, dynamic> friendicaExtensionVisibilityJson() {
    return {
      "allow_cid": allowedUserIds,
      "deny_cid": excludedUserIds,
      "allow_gid": allowedCircleIds,
      "deny_gid": excludedCircleIds,
    };
  }
}
