import 'package:result_monad/result_monad.dart';

import '../../models/connection.dart';
import '../../models/exec_error.dart';
import '../../models/timeline_grouping_list_data.dart';
import '../interfaces/circles_repo_intf.dart';

class MemoryCirclesRepo implements ICirclesRepo {
  final _circlesForConnection = <String, List<TimelineGroupingListData>>{};
  final _connectionsForCircle = <String, Set<Connection>>{};
  final _myCircles = <TimelineGroupingListData>{};

  @override
  void clear() {
    _circlesForConnection.clear();
    _connectionsForCircle.clear();
    _myCircles.clear();
  }

  @override
  Result<List<TimelineGroupingListData>, ExecError> getCirclesForUser(
      String id) {
    if (!_circlesForConnection.containsKey(id)) {
      return Result.error(ExecError(
        type: ErrorType.notFound,
        message: '$id not a known user ID',
      ));
    }

    return Result.ok(_circlesForConnection[id]!);
  }

  @override
  List<TimelineGroupingListData> getMyCircles() {
    return _myCircles.toList();
  }

  @override
  Result<List<Connection>, ExecError> getCircleMembers(
      TimelineGroupingListData circle) {
    if (_connectionsForCircle.containsKey(circle.id)) {
      return Result.ok(_connectionsForCircle[circle.id]!.toList());
    }

    return buildErrorResult(
      type: ErrorType.notFound,
      message: 'Circle ${circle.id} not found',
    );
  }

  @override
  void clearMyCircles() {
    _myCircles.clear();
  }

  @override
  void addConnectionToCircle(
      TimelineGroupingListData circle, Connection connection) {
    _connectionsForCircle.putIfAbsent(circle.id, () => {}).add(connection);
    _circlesForConnection[connection.id]?.add(circle);
  }

  @override
  void addAllCircles(List<TimelineGroupingListData> circle) {
    _myCircles.addAll(circle);
  }

  @override
  bool updateConnectionCircleData(
      String id, List<TimelineGroupingListData> currentCircles) {
    _circlesForConnection[id] = currentCircles;
    return true;
  }

  @override
  void upsertCircle(TimelineGroupingListData circle) {
    _connectionsForCircle.putIfAbsent(circle.id, () => {});
    _myCircles.remove(circle);
    _myCircles.add(circle);
  }

  @override
  void deleteCircle(TimelineGroupingListData circle) {
    for (final conCircles in _circlesForConnection.values) {
      conCircles.remove(circle);
    }
    _connectionsForCircle.remove(circle.id);
    _myCircles.remove(circle);
  }
}
