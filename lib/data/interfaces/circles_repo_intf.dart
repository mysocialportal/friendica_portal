import 'package:result_monad/result_monad.dart';

import '../../models/connection.dart';
import '../../models/exec_error.dart';
import '../../models/timeline_grouping_list_data.dart';

abstract class ICirclesRepo {
  void clear();

  void addAllCircles(List<TimelineGroupingListData> circles);

  void addConnectionToCircle(
      TimelineGroupingListData circle, Connection connection);

  void clearMyCircles();

  void upsertCircle(TimelineGroupingListData circle);

  void deleteCircle(TimelineGroupingListData circle);

  List<TimelineGroupingListData> getMyCircles();

  Result<List<Connection>, ExecError> getCircleMembers(
      TimelineGroupingListData circle);

  Result<List<TimelineGroupingListData>, ExecError> getCirclesForUser(
      String id);

  bool updateConnectionCircleData(
      String id, List<TimelineGroupingListData> currentCircles);
}
