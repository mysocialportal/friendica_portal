import 'package:result_monad/result_monad.dart';

import '../../models/connection.dart';
import '../../models/exec_error.dart';

abstract class IConnectionsRepo {
  void clear();

  bool upsertConnection(Connection connection);

  Result<Connection, ExecError> getById(String id);

  Result<Connection, ExecError> getByName(String name);

  Result<Connection, ExecError> getByHandle(String handle);

  List<Connection> getMyContacts();

  List<Connection> getKnownUsersByName(String name);
}
