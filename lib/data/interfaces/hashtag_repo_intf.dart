import 'package:result_monad/result_monad.dart';

import '../../models/exec_error.dart';
import '../../models/hashtag.dart';

abstract class IHashtagRepo {
  Future<void> clear();

  void add(Hashtag tag);

  Result<Hashtag, ExecError> get(String tagName);

  List<String> getMatchingHashTags(String text);
}
