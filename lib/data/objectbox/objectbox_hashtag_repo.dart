import 'package:result_monad/result_monad.dart';

import '../../models/exec_error.dart';
import '../../models/hashtag.dart';
import '../../objectbox.g.dart';
import '../interfaces/hashtag_repo_intf.dart';
import 'objectbox_cache.dart';

class ObjectBoxHashtagRepo implements IHashtagRepo {
  late final Box<Hashtag> box;

  ObjectBoxHashtagRepo(ObjectBoxCache cache) {
    box = cache.store.box<Hashtag>();
  }

  @override
  Future<void> clear() async {
    await box.removeAllAsync();
  }

  @override
  void add(Hashtag tag) {
    box.putQueued(tag);
  }

  Result<Hashtag, ExecError> get(String tagName) {
    final resultTags = box.query(Hashtag_.tag.equals(tagName)).build().find();

    if (resultTags.isEmpty) {
      return buildErrorResult(
          type: ErrorType.notFound, message: 'Tag $tagName not found');
    }

    return Result.ok(resultTags.first);
  }

  @override
  List<String> getMatchingHashTags(String text) {
    return (box
            .query(
              text.length <= 2
                  ? Hashtag_.tag.startsWith(text, caseSensitive: false)
                  : Hashtag_.tag.contains(text, caseSensitive: false),
            )
            .order(Hashtag_.tag)
            .build()
          ..limit = 100)
        .find()
        .map((h) => h.tag)
        .toList();
  }
}
