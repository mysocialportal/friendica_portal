// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'interactions_details_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$likesForStatusHash() => r'54bda3abfabbfd001396003a5429e3451b547f49';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [likesForStatus].
@ProviderFor(likesForStatus)
const likesForStatusProvider = LikesForStatusFamily();

/// See also [likesForStatus].
class LikesForStatusFamily
    extends Family<AsyncValue<Result<List<Connection>, ExecError>>> {
  /// See also [likesForStatus].
  const LikesForStatusFamily();

  /// See also [likesForStatus].
  LikesForStatusProvider call(
    Profile profile,
    String statusId,
  ) {
    return LikesForStatusProvider(
      profile,
      statusId,
    );
  }

  @override
  LikesForStatusProvider getProviderOverride(
    covariant LikesForStatusProvider provider,
  ) {
    return call(
      provider.profile,
      provider.statusId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'likesForStatusProvider';
}

/// See also [likesForStatus].
class LikesForStatusProvider
    extends AutoDisposeFutureProvider<Result<List<Connection>, ExecError>> {
  /// See also [likesForStatus].
  LikesForStatusProvider(
    Profile profile,
    String statusId,
  ) : this._internal(
          (ref) => likesForStatus(
            ref as LikesForStatusRef,
            profile,
            statusId,
          ),
          from: likesForStatusProvider,
          name: r'likesForStatusProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$likesForStatusHash,
          dependencies: LikesForStatusFamily._dependencies,
          allTransitiveDependencies:
              LikesForStatusFamily._allTransitiveDependencies,
          profile: profile,
          statusId: statusId,
        );

  LikesForStatusProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.statusId,
  }) : super.internal();

  final Profile profile;
  final String statusId;

  @override
  Override overrideWith(
    FutureOr<Result<List<Connection>, ExecError>> Function(
            LikesForStatusRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: LikesForStatusProvider._internal(
        (ref) => create(ref as LikesForStatusRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        statusId: statusId,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<Connection>, ExecError>>
      createElement() {
    return _LikesForStatusProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is LikesForStatusProvider &&
        other.profile == profile &&
        other.statusId == statusId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, statusId.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin LikesForStatusRef
    on AutoDisposeFutureProviderRef<Result<List<Connection>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `statusId` of this provider.
  String get statusId;
}

class _LikesForStatusProviderElement extends AutoDisposeFutureProviderElement<
    Result<List<Connection>, ExecError>> with LikesForStatusRef {
  _LikesForStatusProviderElement(super.provider);

  @override
  Profile get profile => (origin as LikesForStatusProvider).profile;
  @override
  String get statusId => (origin as LikesForStatusProvider).statusId;
}

String _$resharesForStatusHash() => r'39eb847ff4b54c7a949c67af1ab3c13a858259fb';

/// See also [resharesForStatus].
@ProviderFor(resharesForStatus)
const resharesForStatusProvider = ResharesForStatusFamily();

/// See also [resharesForStatus].
class ResharesForStatusFamily
    extends Family<AsyncValue<Result<List<Connection>, ExecError>>> {
  /// See also [resharesForStatus].
  const ResharesForStatusFamily();

  /// See also [resharesForStatus].
  ResharesForStatusProvider call(
    Profile profile,
    String statusId,
  ) {
    return ResharesForStatusProvider(
      profile,
      statusId,
    );
  }

  @override
  ResharesForStatusProvider getProviderOverride(
    covariant ResharesForStatusProvider provider,
  ) {
    return call(
      provider.profile,
      provider.statusId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'resharesForStatusProvider';
}

/// See also [resharesForStatus].
class ResharesForStatusProvider
    extends AutoDisposeFutureProvider<Result<List<Connection>, ExecError>> {
  /// See also [resharesForStatus].
  ResharesForStatusProvider(
    Profile profile,
    String statusId,
  ) : this._internal(
          (ref) => resharesForStatus(
            ref as ResharesForStatusRef,
            profile,
            statusId,
          ),
          from: resharesForStatusProvider,
          name: r'resharesForStatusProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$resharesForStatusHash,
          dependencies: ResharesForStatusFamily._dependencies,
          allTransitiveDependencies:
              ResharesForStatusFamily._allTransitiveDependencies,
          profile: profile,
          statusId: statusId,
        );

  ResharesForStatusProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.statusId,
  }) : super.internal();

  final Profile profile;
  final String statusId;

  @override
  Override overrideWith(
    FutureOr<Result<List<Connection>, ExecError>> Function(
            ResharesForStatusRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ResharesForStatusProvider._internal(
        (ref) => create(ref as ResharesForStatusRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        statusId: statusId,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<Connection>, ExecError>>
      createElement() {
    return _ResharesForStatusProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ResharesForStatusProvider &&
        other.profile == profile &&
        other.statusId == statusId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, statusId.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ResharesForStatusRef
    on AutoDisposeFutureProviderRef<Result<List<Connection>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `statusId` of this provider.
  String get statusId;
}

class _ResharesForStatusProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<List<Connection>, ExecError>> with ResharesForStatusRef {
  _ResharesForStatusProviderElement(super.provider);

  @override
  Profile get profile => (origin as ResharesForStatusProvider).profile;
  @override
  String get statusId => (origin as ResharesForStatusProvider).statusId;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
