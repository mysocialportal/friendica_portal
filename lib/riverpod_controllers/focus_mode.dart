import 'dart:async';

import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../models/focus_mode_data.dart';
import 'settings_services.dart';

part 'focus_mode.g.dart';

@Riverpod(keepAlive: true)
class FocusMode extends _$FocusMode {
  Timer? _disableTimer;

  void setMode(FocusModeData newMode, {bool updateSettingsService = true}) {
    _disableTimer?.cancel();
    var updatedState = newMode;
    if (newMode.enabled && newMode.disableTime != null) {
      final timeDifference = newMode.disableTime!.difference(DateTime.now());
      if (timeDifference.isNegative || timeDifference.inMicroseconds == 0) {
        updatedState = FocusModeData.disabled();
      }
      _disableTimer = Timer(timeDifference, () {
        state = FocusModeData.disabled();
      });
    }

    if (updateSettingsService) {
      ref.read(focusModeSettingProvider.notifier).value = updatedState;
    }
    state = updatedState;
  }

  @override
  FocusModeData build() {
    final storedFocusMode = ref.watch(focusModeSettingProvider);
    setMode(storedFocusMode, updateSettingsService: false);
    return state;
  }
}
