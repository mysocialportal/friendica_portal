// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reshared_via_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$resharedViaHash() => r'6297129d715ee55c710f0d39ad997f7b599f905a';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$ResharedVia extends BuildlessNotifier<ResharedViaData> {
  late final Profile profile;
  late final String postId;

  ResharedViaData build(
    Profile profile,
    String postId,
  );
}

/// See also [ResharedVia].
@ProviderFor(ResharedVia)
const resharedViaProvider = ResharedViaFamily();

/// See also [ResharedVia].
class ResharedViaFamily extends Family<ResharedViaData> {
  /// See also [ResharedVia].
  const ResharedViaFamily();

  /// See also [ResharedVia].
  ResharedViaProvider call(
    Profile profile,
    String postId,
  ) {
    return ResharedViaProvider(
      profile,
      postId,
    );
  }

  @override
  ResharedViaProvider getProviderOverride(
    covariant ResharedViaProvider provider,
  ) {
    return call(
      provider.profile,
      provider.postId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'resharedViaProvider';
}

/// See also [ResharedVia].
class ResharedViaProvider
    extends NotifierProviderImpl<ResharedVia, ResharedViaData> {
  /// See also [ResharedVia].
  ResharedViaProvider(
    Profile profile,
    String postId,
  ) : this._internal(
          () => ResharedVia()
            ..profile = profile
            ..postId = postId,
          from: resharedViaProvider,
          name: r'resharedViaProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$resharedViaHash,
          dependencies: ResharedViaFamily._dependencies,
          allTransitiveDependencies:
              ResharedViaFamily._allTransitiveDependencies,
          profile: profile,
          postId: postId,
        );

  ResharedViaProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.postId,
  }) : super.internal();

  final Profile profile;
  final String postId;

  @override
  ResharedViaData runNotifierBuild(
    covariant ResharedVia notifier,
  ) {
    return notifier.build(
      profile,
      postId,
    );
  }

  @override
  Override overrideWith(ResharedVia Function() create) {
    return ProviderOverride(
      origin: this,
      override: ResharedViaProvider._internal(
        () => create()
          ..profile = profile
          ..postId = postId,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        postId: postId,
      ),
    );
  }

  @override
  NotifierProviderElement<ResharedVia, ResharedViaData> createElement() {
    return _ResharedViaProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ResharedViaProvider &&
        other.profile == profile &&
        other.postId == postId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, postId.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ResharedViaRef on NotifierProviderRef<ResharedViaData> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `postId` of this provider.
  String get postId;
}

class _ResharedViaProviderElement
    extends NotifierProviderElement<ResharedVia, ResharedViaData>
    with ResharedViaRef {
  _ResharedViaProviderElement(super.provider);

  @override
  Profile get profile => (origin as ResharedViaProvider).profile;
  @override
  String get postId => (origin as ResharedViaProvider).postId;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
