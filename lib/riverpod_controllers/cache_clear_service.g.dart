// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cache_clear_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$clearCachesHash() => r'3b3c4ad976679b13fe5b92311e22757548ec81a8';

/// See also [ClearCaches].
@ProviderFor(ClearCaches)
final clearCachesProvider =
    AutoDisposeNotifierProvider<ClearCaches, bool>.internal(
  ClearCaches.new,
  name: r'clearCachesProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$clearCachesHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ClearCaches = AutoDisposeNotifier<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
