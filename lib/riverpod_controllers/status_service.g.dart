// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'status_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$statusServiceHash() => r'6956f3bb44f10ed960681ee61cb814675081db4e';

/// See also [StatusService].
@ProviderFor(StatusService)
final statusServiceProvider =
    AutoDisposeNotifierProvider<StatusService, String>.internal(
  StatusService.new,
  name: r'statusServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$statusServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$StatusService = AutoDisposeNotifier<String>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
