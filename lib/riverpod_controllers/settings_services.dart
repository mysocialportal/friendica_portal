import 'dart:convert';

import 'package:color_blindness/color_blindness.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../models/focus_mode_data.dart';
import '../models/settings/network_capabilities_settings.dart';
import '../utils/theme_mode_extensions.dart';
import 'globals_services.dart';

part 'settings_services.g.dart';

@Riverpod(keepAlive: true)
class FocusModeSetting extends _$FocusModeSetting {
  static const _focusModeKey = 'FocusMode';

  @override
  FocusModeData build() {
    ref.watch(sharedPreferencesProvider);
    return _focusModeDataFromPrefs();
  }

  set value(FocusModeData updatedData) {
    state = updatedData;
    final jsonData = state.toJson();
    final jsonString = jsonEncode(jsonData);
    ref.read(sharedPreferencesProvider).setString(_focusModeKey, jsonString);
  }

  FocusModeData _focusModeDataFromPrefs() {
    final fmString =
        ref.watch(sharedPreferencesProvider).getString(_focusModeKey);
    if (fmString?.isEmpty ?? true) {
      return FocusModeData.disabled();
    }

    final Map<String, dynamic> json = jsonDecode(fmString!);
    final fm = FocusModeData.fromJson(json);
    return fm;
  }
}

@Riverpod(keepAlive: true)
class LowBandwidthModeSetting extends _$LowBandwidthModeSetting {
  static const _lowBandwidthModeKey = 'LowBandwidthMode';

  @override
  bool build() {
    return ref.watch(sharedPreferencesProvider).getBool(_lowBandwidthModeKey) ??
        false;
  }

  set value(bool value) {
    ref.read(sharedPreferencesProvider).setBool(_lowBandwidthModeKey, value);
    state = value;
  }
}

@Riverpod(keepAlive: true)
class NotificationGroupingSetting extends _$NotificationGroupingSetting {
  static const _notificationGroupingKey = 'NotificationGrouping';

  @override
  bool build() {
    return ref
            .watch(sharedPreferencesProvider)
            .getBool(_notificationGroupingKey) ??
        true;
  }

  set value(bool value) {
    ref
        .read(sharedPreferencesProvider)
        .setBool(_notificationGroupingKey, value);
    state = value;
  }
}

@Riverpod(keepAlive: true)
class SpoilerHidingSetting extends _$SpoilerHidingSetting {
  static const _spoilerHidingEnabledKey = 'SpoilerHidingEnabled';

  @override
  bool build() {
    return ref
            .watch(sharedPreferencesProvider)
            .getBool(_spoilerHidingEnabledKey) ??
        true;
  }

  set value(bool value) {
    ref
        .read(sharedPreferencesProvider)
        .setBool(_spoilerHidingEnabledKey, value);
    state = value;
  }
}

@Riverpod(keepAlive: true)
class ThemeModeSetting extends _$ThemeModeSetting {
  static const _themeModeKey = 'ThemeMode';

  @override
  ThemeMode build() {
    return ThemeModeExtensions.parse(
        ref.watch(sharedPreferencesProvider).getString(_themeModeKey));
  }

  set value(ThemeMode value) {
    ref.read(sharedPreferencesProvider).setString(_themeModeKey, value.name);
    state = value;
  }
}

@Riverpod(keepAlive: true)
class LogLevelSetting extends _$LogLevelSetting {
  static const _logLevelKey = 'LogLevel';

  @override
  Level build() {
    ref.watch(sharedPreferencesProvider);
    return _levelFromPrefs();
  }

  set value(Level level) {
    ref.read(sharedPreferencesProvider).setString(_logLevelKey, level.name);
    Logger.root.level = level;
    state = level;
  }

  Level _levelFromPrefs() {
    final levelString =
        ref.read(sharedPreferencesProvider).getString(_logLevelKey);
    return switch (levelString) {
      'ALL' => Level.ALL,
      'FINEST' => Level.FINEST,
      'FINER' => Level.FINER,
      'FINE' => Level.FINE,
      'CONFIG' => Level.CONFIG,
      'INFO' => Level.INFO,
      'WARNING' => Level.WARNING,
      'SEVERE' => Level.SEVERE,
      'SHOUT' => Level.SHOUT,
      'OFF' => Level.OFF,
      _ => Level.OFF,
    };
  }
}

@Riverpod(keepAlive: true)
class NetworkCapabilitiesSetting extends _$NetworkCapabilitiesSetting {
  static const _networkCapabilitiesKey = 'NetworkCapabilities';

  @override
  NetworkCapabilitiesSettings build() {
    ref.watch(sharedPreferencesProvider);
    return _networkCapabilitiesFromPrefs();
  }

  set value(NetworkCapabilitiesSettings updatedCapabilities) {
    final jsonString = jsonEncode(updatedCapabilities.toJson());
    ref
        .read(sharedPreferencesProvider)
        .setString(_networkCapabilitiesKey, jsonString);
    state = updatedCapabilities;
  }

  NetworkCapabilitiesSettings _networkCapabilitiesFromPrefs() {
    final ncString =
        ref.read(sharedPreferencesProvider).getString(_networkCapabilitiesKey);
    if (ncString?.isEmpty ?? true) {
      return NetworkCapabilitiesSettings.defaultSettings();
    }

    final List<dynamic> json = jsonDecode(ncString!);
    final nc = NetworkCapabilitiesSettings.fromJson(json);
    return nc;
  }
}

@Riverpod(keepAlive: true)
class FriendicaApiTimeoutSetting extends _$FriendicaApiTimeoutSetting {
  static const int defaultTimeoutSec = 60;
  static const _friendicaApiTimeoutKey = 'FriendicaApiTimeoutSetting';

  @override
  Duration build() {
    final seconds =
        ref.watch(sharedPreferencesProvider).getInt(_friendicaApiTimeoutKey) ??
            defaultTimeoutSec;
    return Duration(seconds: seconds);
  }

  set value(Duration value) {
    ref
        .read(sharedPreferencesProvider)
        .setInt(_friendicaApiTimeoutKey, value.inSeconds);
    state = value;
  }
}

@Riverpod(keepAlive: true)
class ColorBlindnessTestingModeSetting
    extends _$ColorBlindnessTestingModeSetting {
  static const _colorBlindnessTestingModeKey = 'ColorBlindnessTestingMode';

  @override
  ColorBlindnessType build() {
    ref.watch(sharedPreferencesProvider);
    return _colorBlindnessTypeFromPrefs();
  }

  set value(ColorBlindnessType value) {
    ref
        .read(sharedPreferencesProvider)
        .setString(_colorBlindnessTestingModeKey, value.name);
    state = value;
  }

  ColorBlindnessType _colorBlindnessTypeFromPrefs() {
    final cbString = ref
        .read(sharedPreferencesProvider)
        .getString(_colorBlindnessTestingModeKey);
    if (cbString?.isEmpty ?? true) {
      return ColorBlindnessType.none;
    }
    return ColorBlindnessType.values.firstWhere(
      (c) => c.name == cbString,
      orElse: () => ColorBlindnessType.none,
    );
  }
}

@Riverpod(keepAlive: true)
class OpenTagsInApp extends _$SpoilerHidingSetting {
  static const _openTagsInAppKey = 'OpenTagsInApp';

  @override
  bool build() {
    return ref.watch(sharedPreferencesProvider).getBool(_openTagsInAppKey) ??
        true;
  }

  set value(bool value) {
    ref.read(sharedPreferencesProvider).setBool(_openTagsInAppKey, value);
    state = value;
  }
}
