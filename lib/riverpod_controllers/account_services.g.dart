// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$loggedInHash() => r'0049eeccc873622b8d5d5709af2d2c5001a0c791';

/// See also [loggedIn].
@ProviderFor(loggedIn)
final loggedInProvider = AutoDisposeProvider<bool>.internal(
  loggedIn,
  name: r'loggedInProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$loggedInHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef LoggedInRef = AutoDisposeProviderRef<bool>;
String _$loggedInProfilesHash() => r'9a2ed6600f3922753c81162c127f6df452484ec1';

/// See also [LoggedInProfiles].
@ProviderFor(LoggedInProfiles)
final loggedInProfilesProvider =
    NotifierProvider<LoggedInProfiles, List<Profile>>.internal(
  LoggedInProfiles.new,
  name: r'loggedInProfilesProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$loggedInProfilesHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LoggedInProfiles = Notifier<List<Profile>>;
String _$loggedOutProfilesHash() => r'2b4094507a9f7e4fadcb8c70be7128a9eb6e968a';

/// See also [LoggedOutProfiles].
@ProviderFor(LoggedOutProfiles)
final loggedOutProfilesProvider =
    NotifierProvider<LoggedOutProfiles, List<Profile>>.internal(
  LoggedOutProfiles.new,
  name: r'loggedOutProfilesProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$loggedOutProfilesHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LoggedOutProfiles = Notifier<List<Profile>>;
String _$activeProfileHash() => r'79e274762a2fa2c8f68ee33e500ddaf24426b00c';

/// See also [ActiveProfile].
@ProviderFor(ActiveProfile)
final activeProfileProvider = NotifierProvider<ActiveProfile, Profile>.internal(
  ActiveProfile.new,
  name: r'activeProfileProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$activeProfileHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ActiveProfile = Notifier<Profile>;
String _$credentialSigninHash() => r'e9731ad5b916838122a2a86961af4cfe4bef57b5';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$CredentialSignin extends BuildlessAutoDisposeNotifier<bool> {
  late final ICredentials credentials;

  bool build(
    ICredentials credentials,
  );
}

/// See also [CredentialSignin].
@ProviderFor(CredentialSignin)
const credentialSigninProvider = CredentialSigninFamily();

/// See also [CredentialSignin].
class CredentialSigninFamily extends Family<bool> {
  /// See also [CredentialSignin].
  const CredentialSigninFamily();

  /// See also [CredentialSignin].
  CredentialSigninProvider call(
    ICredentials credentials,
  ) {
    return CredentialSigninProvider(
      credentials,
    );
  }

  @override
  CredentialSigninProvider getProviderOverride(
    covariant CredentialSigninProvider provider,
  ) {
    return call(
      provider.credentials,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'credentialSigninProvider';
}

/// See also [CredentialSignin].
class CredentialSigninProvider
    extends AutoDisposeNotifierProviderImpl<CredentialSignin, bool> {
  /// See also [CredentialSignin].
  CredentialSigninProvider(
    ICredentials credentials,
  ) : this._internal(
          () => CredentialSignin()..credentials = credentials,
          from: credentialSigninProvider,
          name: r'credentialSigninProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$credentialSigninHash,
          dependencies: CredentialSigninFamily._dependencies,
          allTransitiveDependencies:
              CredentialSigninFamily._allTransitiveDependencies,
          credentials: credentials,
        );

  CredentialSigninProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.credentials,
  }) : super.internal();

  final ICredentials credentials;

  @override
  bool runNotifierBuild(
    covariant CredentialSignin notifier,
  ) {
    return notifier.build(
      credentials,
    );
  }

  @override
  Override overrideWith(CredentialSignin Function() create) {
    return ProviderOverride(
      origin: this,
      override: CredentialSigninProvider._internal(
        () => create()..credentials = credentials,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        credentials: credentials,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<CredentialSignin, bool> createElement() {
    return _CredentialSigninProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CredentialSigninProvider &&
        other.credentials == credentials;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, credentials.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin CredentialSigninRef on AutoDisposeNotifierProviderRef<bool> {
  /// The parameter `credentials` of this provider.
  ICredentials get credentials;
}

class _CredentialSigninProviderElement
    extends AutoDisposeNotifierProviderElement<CredentialSignin, bool>
    with CredentialSigninRef {
  _CredentialSigninProviderElement(super.provider);

  @override
  ICredentials get credentials =>
      (origin as CredentialSigninProvider).credentials;
}

String _$profileManagerHash() => r'5f37162a9b7b19f3cd9e804d83dbb37cda96568b';

abstract class _$ProfileManager extends BuildlessNotifier<bool> {
  late final Profile profile;

  bool build(
    Profile profile,
  );
}

/// See also [ProfileManager].
@ProviderFor(ProfileManager)
const profileManagerProvider = ProfileManagerFamily();

/// See also [ProfileManager].
class ProfileManagerFamily extends Family<bool> {
  /// See also [ProfileManager].
  const ProfileManagerFamily();

  /// See also [ProfileManager].
  ProfileManagerProvider call(
    Profile profile,
  ) {
    return ProfileManagerProvider(
      profile,
    );
  }

  @override
  ProfileManagerProvider getProviderOverride(
    covariant ProfileManagerProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'profileManagerProvider';
}

/// See also [ProfileManager].
class ProfileManagerProvider
    extends NotifierProviderImpl<ProfileManager, bool> {
  /// See also [ProfileManager].
  ProfileManagerProvider(
    Profile profile,
  ) : this._internal(
          () => ProfileManager()..profile = profile,
          from: profileManagerProvider,
          name: r'profileManagerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$profileManagerHash,
          dependencies: ProfileManagerFamily._dependencies,
          allTransitiveDependencies:
              ProfileManagerFamily._allTransitiveDependencies,
          profile: profile,
        );

  ProfileManagerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  bool runNotifierBuild(
    covariant ProfileManager notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(ProfileManager Function() create) {
    return ProviderOverride(
      origin: this,
      override: ProfileManagerProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<ProfileManager, bool> createElement() {
    return _ProfileManagerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ProfileManagerProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ProfileManagerRef on NotifierProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _ProfileManagerProviderElement
    extends NotifierProviderElement<ProfileManager, bool>
    with ProfileManagerRef {
  _ProfileManagerProviderElement(super.provider);

  @override
  Profile get profile => (origin as ProfileManagerProvider).profile;
}

String _$accountServicesInitializerHash() =>
    r'386b364d2fe41581ad17d5823a7f47dc12bc4760';

/// See also [AccountServicesInitializer].
@ProviderFor(AccountServicesInitializer)
final accountServicesInitializerProvider =
    AutoDisposeNotifierProvider<AccountServicesInitializer, bool>.internal(
  AccountServicesInitializer.new,
  name: r'accountServicesInitializerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$accountServicesInitializerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AccountServicesInitializer = AutoDisposeNotifier<bool>;
String _$accountClearerHash() => r'153fd7330da38b5c9c30b13adbd387bbdf096663';

/// See also [AccountClearer].
@ProviderFor(AccountClearer)
final accountClearerProvider =
    AutoDisposeNotifierProvider<AccountClearer, bool>.internal(
  AccountClearer.new,
  name: r'accountClearerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$accountClearerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AccountClearer = AutoDisposeNotifier<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
