// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'globals_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$sharedPreferencesHash() => r'6bbc55d4dc38d5979ff916112845bcc4a4a3a1ea';

/// See also [sharedPreferences].
@ProviderFor(sharedPreferences)
final sharedPreferencesProvider = Provider<SharedPreferences>.internal(
  sharedPreferences,
  name: r'sharedPreferencesProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$sharedPreferencesHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef SharedPreferencesRef = ProviderRef<SharedPreferences>;
String _$applicationSupportDirectoryHash() =>
    r'c45ac5a9d1cc638b03fbe714335d11ccde2694a6';

/// See also [applicationSupportDirectory].
@ProviderFor(applicationSupportDirectory)
final applicationSupportDirectoryProvider = Provider<Directory>.internal(
  applicationSupportDirectory,
  name: r'applicationSupportDirectoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$applicationSupportDirectoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef ApplicationSupportDirectoryRef = ProviderRef<Directory>;
String _$secretsServiceHash() => r'e71a3e6f5d7736ef7e30f0582aae2a75d6ceec6b';

/// See also [secretsService].
@ProviderFor(secretsService)
final secretsServiceProvider = Provider<SecretsService>.internal(
  secretsService,
  name: r'secretsServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$secretsServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef SecretsServiceRef = ProviderRef<SecretsService>;
String _$userAgentHash() => r'4f31d38a291097489e47164b49e0a23f9735e299';

/// See also [userAgent].
@ProviderFor(userAgent)
final userAgentProvider = Provider<String>.internal(
  userAgent,
  name: r'userAgentProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$userAgentHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef UserAgentRef = ProviderRef<String>;
String _$objectBoxCacheHash() => r'67e4a50a78a4acb59169794fde57d6dcc7fd5586';

/// See also [objectBoxCache].
@ProviderFor(objectBoxCache)
final objectBoxCacheProvider = Provider<ObjectBoxCache>.internal(
  objectBoxCache,
  name: r'objectBoxCacheProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$objectBoxCacheHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef ObjectBoxCacheRef = ProviderRef<ObjectBoxCache>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
