import 'dart:collection';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../models/auth/profile.dart';
import '../models/exec_error.dart';
import '../models/follow_request.dart';
import '../models/networking/paged_response.dart';
import '../models/networking/paging_data.dart';
import 'networking/friendica_relationship_client_services.dart';
import 'notification_services.dart';

part 'follow_requests_services.g.dart';

const _maxIterations = 20;

@Riverpod(keepAlive: true)
class FollowRequests extends _$FollowRequests {
  @override
  Map<String, FollowRequest> build(Profile profile) {
    return {};
  }

  Future<void> update() async {
    var result = await _processPage(PagingData());
    var count = 0;
    final updatedRequests = <FollowRequest>{};
    while (result.isSuccess &&
        result.value.hasMorePages &&
        count < _maxIterations) {
      result
          .andThenSuccess((requests) => updatedRequests.addAll(requests.data));
      result = await _processPage(result.value.next);
      count++;
    }

    state = {for (final r in updatedRequests) r.connection.id: r};
  }

  FutureResult<PagedResponse<List<FollowRequest>>, ExecError> _processPage(
      PagingData? page) async {
    if (page == null) {
      return buildErrorResult(type: ErrorType.rangeError);
    }
    final result =
        await ref.read(followRequestsClientProvider(profile, page).future);

    return result;
  }
}

@Riverpod(keepAlive: true)
class FollowRequestRefresher extends _$FollowRequestRefresher {
  @override
  bool build(Profile profile) {
    return true;
  }

  Future<void> update() async {
    await ref
        .read(notificationsManagerProvider(profile).notifier)
        .clearConnectionRequestNotifications();
    await ref.read(followRequestsProvider(profile).notifier).update();
    await ref
        .read(notificationsManagerProvider(profile).notifier)
        .refreshConnectionRequestNotifications();
  }
}

@riverpod
List<FollowRequest> followRequestList(Ref ref, Profile profile) {
  return UnmodifiableListView(
    ref.watch(followRequestsProvider(profile)).values,
  );
}

@riverpod
Result<FollowRequest, ExecError> followRequestById(
    Ref ref, Profile profile, String id) {
  final request = ref.watch(followRequestsProvider(profile))[id];
  return request != null
      ? Result.ok(request)
      : buildErrorResult(
          type: ErrorType.notFound,
          message: 'Request for $id not found',
        );
}
