// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'log_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$logHistoryHash() => r'7f1865f051b54ad783f50c9bf8ae781f5125c56d';

/// See also [logHistory].
@ProviderFor(logHistory)
final logHistoryProvider = Provider<List<LogRecord>>.internal(
  logHistory,
  name: r'logHistoryProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$logHistoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef LogHistoryRef = ProviderRef<List<LogRecord>>;
String _$logServiceHash() => r'3eed95f8843f5d5a7d23a0a81cfa265323a9b819';

/// See also [LogService].
@ProviderFor(LogService)
final logServiceProvider =
    NotifierProvider<LogService, UnmodifiableListView<LogRecord>>.internal(
  LogService.new,
  name: r'logServiceProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$logServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LogService = Notifier<UnmodifiableListView<LogRecord>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
