import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'log_service.g.dart';

const _defaultMaxItems = 1000;
const _maxItems = _defaultMaxItems;
final _events = Queue<LogRecord>();

@Riverpod(keepAlive: true)
List<LogRecord> logHistory(Ref ref) {
  return UnmodifiableListView(_events);
}

@Riverpod(keepAlive: true)
class LogService extends _$LogService {
  @override
  UnmodifiableListView<LogRecord> build() {
    Logger.root.clearListeners();
    Logger.root.onRecord.listen((event) {
      add(event);
    });
    ref.onDispose(Logger.root.clearListeners);
    return UnmodifiableListView(_events);
  }

  void add(LogRecord event) {
    final logName = event.loggerName.isEmpty ? 'ROOT' : event.loggerName;
    final msg =
        '${event.level.name} - $logName @ ${event.time}: ${event.message}';
    debugPrint(msg);
    _events.add(event);
    if (_events.length > _maxItems) {
      _events.removeFirst();
    }
    ref.invalidate(logHistoryProvider);
  }
}
