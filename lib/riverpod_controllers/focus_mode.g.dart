// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'focus_mode.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$focusModeHash() => r'60722a1c70c59d1e3c2cddd8013896ef0b5da466';

/// See also [FocusMode].
@ProviderFor(FocusMode)
final focusModeProvider = NotifierProvider<FocusMode, FocusModeData>.internal(
  FocusMode.new,
  name: r'focusModeProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$focusModeHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$FocusMode = Notifier<FocusModeData>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
