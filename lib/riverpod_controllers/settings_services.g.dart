// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'settings_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$focusModeSettingHash() => r'36a6d815ad1568165fe8fc4ec65cb91ea9e7c784';

/// See also [FocusModeSetting].
@ProviderFor(FocusModeSetting)
final focusModeSettingProvider =
    NotifierProvider<FocusModeSetting, FocusModeData>.internal(
  FocusModeSetting.new,
  name: r'focusModeSettingProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$focusModeSettingHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$FocusModeSetting = Notifier<FocusModeData>;
String _$lowBandwidthModeSettingHash() =>
    r'21e0f9ddf19fe8e700c07584eecef4cd4183fa0e';

/// See also [LowBandwidthModeSetting].
@ProviderFor(LowBandwidthModeSetting)
final lowBandwidthModeSettingProvider =
    NotifierProvider<LowBandwidthModeSetting, bool>.internal(
  LowBandwidthModeSetting.new,
  name: r'lowBandwidthModeSettingProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$lowBandwidthModeSettingHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LowBandwidthModeSetting = Notifier<bool>;
String _$notificationGroupingSettingHash() =>
    r'ff546e61777eec2f655e368463c06ab82097a3a6';

/// See also [NotificationGroupingSetting].
@ProviderFor(NotificationGroupingSetting)
final notificationGroupingSettingProvider =
    NotifierProvider<NotificationGroupingSetting, bool>.internal(
  NotificationGroupingSetting.new,
  name: r'notificationGroupingSettingProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$notificationGroupingSettingHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$NotificationGroupingSetting = Notifier<bool>;
String _$spoilerHidingSettingHash() =>
    r'24568352d2d8bb7ffa1595274de86592996a1da8';

/// See also [SpoilerHidingSetting].
@ProviderFor(SpoilerHidingSetting)
final spoilerHidingSettingProvider =
    NotifierProvider<SpoilerHidingSetting, bool>.internal(
  SpoilerHidingSetting.new,
  name: r'spoilerHidingSettingProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$spoilerHidingSettingHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$SpoilerHidingSetting = Notifier<bool>;
String _$themeModeSettingHash() => r'f98477dd01d94e0097b78c006393276a7bbc0970';

/// See also [ThemeModeSetting].
@ProviderFor(ThemeModeSetting)
final themeModeSettingProvider =
    NotifierProvider<ThemeModeSetting, ThemeMode>.internal(
  ThemeModeSetting.new,
  name: r'themeModeSettingProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$themeModeSettingHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ThemeModeSetting = Notifier<ThemeMode>;
String _$logLevelSettingHash() => r'26722dbfc2e5dcdcdc25532ad5b6acbd44c293b3';

/// See also [LogLevelSetting].
@ProviderFor(LogLevelSetting)
final logLevelSettingProvider =
    NotifierProvider<LogLevelSetting, Level>.internal(
  LogLevelSetting.new,
  name: r'logLevelSettingProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$logLevelSettingHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LogLevelSetting = Notifier<Level>;
String _$networkCapabilitiesSettingHash() =>
    r'daf609c614433e4e3152a45223f6b710c50f163a';

/// See also [NetworkCapabilitiesSetting].
@ProviderFor(NetworkCapabilitiesSetting)
final networkCapabilitiesSettingProvider = NotifierProvider<
    NetworkCapabilitiesSetting, NetworkCapabilitiesSettings>.internal(
  NetworkCapabilitiesSetting.new,
  name: r'networkCapabilitiesSettingProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$networkCapabilitiesSettingHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$NetworkCapabilitiesSetting = Notifier<NetworkCapabilitiesSettings>;
String _$friendicaApiTimeoutSettingHash() =>
    r'53994dc37e3da3e19c443a763c4c6a715749f7b4';

/// See also [FriendicaApiTimeoutSetting].
@ProviderFor(FriendicaApiTimeoutSetting)
final friendicaApiTimeoutSettingProvider =
    NotifierProvider<FriendicaApiTimeoutSetting, Duration>.internal(
  FriendicaApiTimeoutSetting.new,
  name: r'friendicaApiTimeoutSettingProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$friendicaApiTimeoutSettingHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$FriendicaApiTimeoutSetting = Notifier<Duration>;
String _$colorBlindnessTestingModeSettingHash() =>
    r'8e1d45036fdf1ce7408dcded71c591ee3d7a1e26';

/// See also [ColorBlindnessTestingModeSetting].
@ProviderFor(ColorBlindnessTestingModeSetting)
final colorBlindnessTestingModeSettingProvider = NotifierProvider<
    ColorBlindnessTestingModeSetting, ColorBlindnessType>.internal(
  ColorBlindnessTestingModeSetting.new,
  name: r'colorBlindnessTestingModeSettingProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$colorBlindnessTestingModeSettingHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ColorBlindnessTestingModeSetting = Notifier<ColorBlindnessType>;
String _$openTagsInAppHash() => r'4aeee38bd8de61c8fdf84cd6cb30bc61fbeeca1b';

/// See also [OpenTagsInApp].
@ProviderFor(OpenTagsInApp)
final openTagsInAppProvider = NotifierProvider<OpenTagsInApp, bool>.internal(
  OpenTagsInApp.new,
  name: r'openTagsInAppProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$openTagsInAppHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$OpenTagsInApp = Notifier<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
