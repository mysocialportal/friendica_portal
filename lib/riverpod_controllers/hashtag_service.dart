import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../data/interfaces/hashtag_repo_intf.dart';
import '../data/objectbox/objectbox_hashtag_repo.dart';
import '../models/auth/profile.dart';
import '../models/exec_error.dart';
import '../models/hashtag.dart';
import 'globals_services.dart';
import 'networking/friendica_tags_client_services.dart';

part 'hashtag_service.g.dart';

@Riverpod(keepAlive: true)
IHashtagRepo _hashtagRepo(Ref ref) {
  final box = ref.watch(objectBoxCacheProvider);
  return ObjectBoxHashtagRepo(box);
}

@riverpod
class HashtagService extends _$HashtagService {
  @override
  List<String> build(Profile profile, {String searchString = ''}) {
    final repo = ref.watch(_hashtagRepoProvider);
    return repo.getMatchingHashTags(searchString);
  }

  Future<void> clear() async {
    await ref.read(_hashtagRepoProvider).clear();
    ref.notifyListeners();
  }

  void add(Hashtag tag) {
    ref.read(_hashtagRepoProvider).add(tag);
    ref.invalidate(hashtagFollowCheckProvider(profile, tag.tag));
    ref.notifyListeners();
  }

  Result<Hashtag, ExecError> getExact() {
    return ref.read(_hashtagRepoProvider).get(searchString);
  }
}

@Riverpod(keepAlive: true)
Future<Result<Map<String, HashtagWithTrending>, ExecError>> followedTagsMap(
    Ref ref, Profile profile) async {
  return await ref
      .read(followedTagsProvider(profile).future)
      .transform((tagsList) => {for (final t in tagsList) t.name: t})
      .execErrorCastAsync();
}

@riverpod
Future<bool> hashtagFollowCheck(
    Ref ref, Profile profile, String tagName) async {
  final tagsValue = ref.watch(followedTagsMapProvider(profile));
  return switch (tagsValue) {
    AsyncData(:final value) => value.fold(
        onSuccess: (tags) => tags[tagName]?.following ?? false,
        onError: (_) => false),
    _ => false
  };
}
