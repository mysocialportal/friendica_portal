// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hashtag_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$hashtagRepoHash() => r'eca10268e2668da4d44ec5e2f012dce22202aa60';

/// See also [_hashtagRepo].
@ProviderFor(_hashtagRepo)
final _hashtagRepoProvider = Provider<IHashtagRepo>.internal(
  _hashtagRepo,
  name: r'_hashtagRepoProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$hashtagRepoHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef _HashtagRepoRef = ProviderRef<IHashtagRepo>;
String _$followedTagsMapHash() => r'459c939eea0c5dc8f3381db5adf1ef702f0972cf';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [followedTagsMap].
@ProviderFor(followedTagsMap)
const followedTagsMapProvider = FollowedTagsMapFamily();

/// See also [followedTagsMap].
class FollowedTagsMapFamily extends Family<
    AsyncValue<Result<Map<String, HashtagWithTrending>, ExecError>>> {
  /// See also [followedTagsMap].
  const FollowedTagsMapFamily();

  /// See also [followedTagsMap].
  FollowedTagsMapProvider call(
    Profile profile,
  ) {
    return FollowedTagsMapProvider(
      profile,
    );
  }

  @override
  FollowedTagsMapProvider getProviderOverride(
    covariant FollowedTagsMapProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'followedTagsMapProvider';
}

/// See also [followedTagsMap].
class FollowedTagsMapProvider extends FutureProvider<
    Result<Map<String, HashtagWithTrending>, ExecError>> {
  /// See also [followedTagsMap].
  FollowedTagsMapProvider(
    Profile profile,
  ) : this._internal(
          (ref) => followedTagsMap(
            ref as FollowedTagsMapRef,
            profile,
          ),
          from: followedTagsMapProvider,
          name: r'followedTagsMapProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$followedTagsMapHash,
          dependencies: FollowedTagsMapFamily._dependencies,
          allTransitiveDependencies:
              FollowedTagsMapFamily._allTransitiveDependencies,
          profile: profile,
        );

  FollowedTagsMapProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<Result<Map<String, HashtagWithTrending>, ExecError>> Function(
            FollowedTagsMapRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FollowedTagsMapProvider._internal(
        (ref) => create(ref as FollowedTagsMapRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  FutureProviderElement<Result<Map<String, HashtagWithTrending>, ExecError>>
      createElement() {
    return _FollowedTagsMapProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FollowedTagsMapProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FollowedTagsMapRef
    on FutureProviderRef<Result<Map<String, HashtagWithTrending>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _FollowedTagsMapProviderElement extends FutureProviderElement<
        Result<Map<String, HashtagWithTrending>, ExecError>>
    with FollowedTagsMapRef {
  _FollowedTagsMapProviderElement(super.provider);

  @override
  Profile get profile => (origin as FollowedTagsMapProvider).profile;
}

String _$hashtagFollowCheckHash() =>
    r'0e29ab06a4aee63a42236c44cf1c3b1045d5ba9e';

/// See also [hashtagFollowCheck].
@ProviderFor(hashtagFollowCheck)
const hashtagFollowCheckProvider = HashtagFollowCheckFamily();

/// See also [hashtagFollowCheck].
class HashtagFollowCheckFamily extends Family<AsyncValue<bool>> {
  /// See also [hashtagFollowCheck].
  const HashtagFollowCheckFamily();

  /// See also [hashtagFollowCheck].
  HashtagFollowCheckProvider call(
    Profile profile,
    String tagName,
  ) {
    return HashtagFollowCheckProvider(
      profile,
      tagName,
    );
  }

  @override
  HashtagFollowCheckProvider getProviderOverride(
    covariant HashtagFollowCheckProvider provider,
  ) {
    return call(
      provider.profile,
      provider.tagName,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'hashtagFollowCheckProvider';
}

/// See also [hashtagFollowCheck].
class HashtagFollowCheckProvider extends AutoDisposeFutureProvider<bool> {
  /// See also [hashtagFollowCheck].
  HashtagFollowCheckProvider(
    Profile profile,
    String tagName,
  ) : this._internal(
          (ref) => hashtagFollowCheck(
            ref as HashtagFollowCheckRef,
            profile,
            tagName,
          ),
          from: hashtagFollowCheckProvider,
          name: r'hashtagFollowCheckProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$hashtagFollowCheckHash,
          dependencies: HashtagFollowCheckFamily._dependencies,
          allTransitiveDependencies:
              HashtagFollowCheckFamily._allTransitiveDependencies,
          profile: profile,
          tagName: tagName,
        );

  HashtagFollowCheckProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.tagName,
  }) : super.internal();

  final Profile profile;
  final String tagName;

  @override
  Override overrideWith(
    FutureOr<bool> Function(HashtagFollowCheckRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: HashtagFollowCheckProvider._internal(
        (ref) => create(ref as HashtagFollowCheckRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        tagName: tagName,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<bool> createElement() {
    return _HashtagFollowCheckProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is HashtagFollowCheckProvider &&
        other.profile == profile &&
        other.tagName == tagName;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, tagName.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin HashtagFollowCheckRef on AutoDisposeFutureProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `tagName` of this provider.
  String get tagName;
}

class _HashtagFollowCheckProviderElement
    extends AutoDisposeFutureProviderElement<bool> with HashtagFollowCheckRef {
  _HashtagFollowCheckProviderElement(super.provider);

  @override
  Profile get profile => (origin as HashtagFollowCheckProvider).profile;
  @override
  String get tagName => (origin as HashtagFollowCheckProvider).tagName;
}

String _$hashtagServiceHash() => r'7953d7146cb53d9ec1a2cecdd9c24502e4cf7717';

abstract class _$HashtagService
    extends BuildlessAutoDisposeNotifier<List<String>> {
  late final Profile profile;
  late final String searchString;

  List<String> build(
    Profile profile, {
    String searchString = '',
  });
}

/// See also [HashtagService].
@ProviderFor(HashtagService)
const hashtagServiceProvider = HashtagServiceFamily();

/// See also [HashtagService].
class HashtagServiceFamily extends Family<List<String>> {
  /// See also [HashtagService].
  const HashtagServiceFamily();

  /// See also [HashtagService].
  HashtagServiceProvider call(
    Profile profile, {
    String searchString = '',
  }) {
    return HashtagServiceProvider(
      profile,
      searchString: searchString,
    );
  }

  @override
  HashtagServiceProvider getProviderOverride(
    covariant HashtagServiceProvider provider,
  ) {
    return call(
      provider.profile,
      searchString: provider.searchString,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'hashtagServiceProvider';
}

/// See also [HashtagService].
class HashtagServiceProvider
    extends AutoDisposeNotifierProviderImpl<HashtagService, List<String>> {
  /// See also [HashtagService].
  HashtagServiceProvider(
    Profile profile, {
    String searchString = '',
  }) : this._internal(
          () => HashtagService()
            ..profile = profile
            ..searchString = searchString,
          from: hashtagServiceProvider,
          name: r'hashtagServiceProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$hashtagServiceHash,
          dependencies: HashtagServiceFamily._dependencies,
          allTransitiveDependencies:
              HashtagServiceFamily._allTransitiveDependencies,
          profile: profile,
          searchString: searchString,
        );

  HashtagServiceProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.searchString,
  }) : super.internal();

  final Profile profile;
  final String searchString;

  @override
  List<String> runNotifierBuild(
    covariant HashtagService notifier,
  ) {
    return notifier.build(
      profile,
      searchString: searchString,
    );
  }

  @override
  Override overrideWith(HashtagService Function() create) {
    return ProviderOverride(
      origin: this,
      override: HashtagServiceProvider._internal(
        () => create()
          ..profile = profile
          ..searchString = searchString,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        searchString: searchString,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<HashtagService, List<String>>
      createElement() {
    return _HashtagServiceProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is HashtagServiceProvider &&
        other.profile == profile &&
        other.searchString == searchString;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, searchString.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin HashtagServiceRef on AutoDisposeNotifierProviderRef<List<String>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `searchString` of this provider.
  String get searchString;
}

class _HashtagServiceProviderElement
    extends AutoDisposeNotifierProviderElement<HashtagService, List<String>>
    with HashtagServiceRef {
  _HashtagServiceProviderElement(super.provider);

  @override
  Profile get profile => (origin as HashtagServiceProvider).profile;
  @override
  String get searchString => (origin as HashtagServiceProvider).searchString;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
