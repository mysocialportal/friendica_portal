// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$notificationsManagerHash() =>
    r'c92bf43255b0c2b6b4a94c4239e545ee1c9bf90f';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$NotificationsManager
    extends BuildlessAsyncNotifier<Result<List<UserNotification>, ExecError>> {
  late final Profile profile;

  FutureOr<Result<List<UserNotification>, ExecError>> build(
    Profile profile,
  );
}

/// See also [NotificationsManager].
@ProviderFor(NotificationsManager)
const notificationsManagerProvider = NotificationsManagerFamily();

/// See also [NotificationsManager].
class NotificationsManagerFamily
    extends Family<AsyncValue<Result<List<UserNotification>, ExecError>>> {
  /// See also [NotificationsManager].
  const NotificationsManagerFamily();

  /// See also [NotificationsManager].
  NotificationsManagerProvider call(
    Profile profile,
  ) {
    return NotificationsManagerProvider(
      profile,
    );
  }

  @override
  NotificationsManagerProvider getProviderOverride(
    covariant NotificationsManagerProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'notificationsManagerProvider';
}

/// See also [NotificationsManager].
class NotificationsManagerProvider extends AsyncNotifierProviderImpl<
    NotificationsManager, Result<List<UserNotification>, ExecError>> {
  /// See also [NotificationsManager].
  NotificationsManagerProvider(
    Profile profile,
  ) : this._internal(
          () => NotificationsManager()..profile = profile,
          from: notificationsManagerProvider,
          name: r'notificationsManagerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$notificationsManagerHash,
          dependencies: NotificationsManagerFamily._dependencies,
          allTransitiveDependencies:
              NotificationsManagerFamily._allTransitiveDependencies,
          profile: profile,
        );

  NotificationsManagerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  FutureOr<Result<List<UserNotification>, ExecError>> runNotifierBuild(
    covariant NotificationsManager notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(NotificationsManager Function() create) {
    return ProviderOverride(
      origin: this,
      override: NotificationsManagerProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AsyncNotifierProviderElement<NotificationsManager,
      Result<List<UserNotification>, ExecError>> createElement() {
    return _NotificationsManagerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is NotificationsManagerProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin NotificationsManagerRef
    on AsyncNotifierProviderRef<Result<List<UserNotification>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _NotificationsManagerProviderElement extends AsyncNotifierProviderElement<
    NotificationsManager,
    Result<List<UserNotification>, ExecError>> with NotificationsManagerRef {
  _NotificationsManagerProviderElement(super.provider);

  @override
  Profile get profile => (origin as NotificationsManagerProvider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
