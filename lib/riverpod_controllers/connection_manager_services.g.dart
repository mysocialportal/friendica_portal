// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'connection_manager_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$connectionsRepoHash() => r'd174f361fd94df316da6e525dff1b3578fc67a0f';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [_connectionsRepo].
@ProviderFor(_connectionsRepo)
const _connectionsRepoProvider = _ConnectionsRepoFamily();

/// See also [_connectionsRepo].
class _ConnectionsRepoFamily extends Family<AsyncValue<IConnectionsRepo>> {
  /// See also [_connectionsRepo].
  const _ConnectionsRepoFamily();

  /// See also [_connectionsRepo].
  _ConnectionsRepoProvider call(
    Profile profile,
  ) {
    return _ConnectionsRepoProvider(
      profile,
    );
  }

  @override
  _ConnectionsRepoProvider getProviderOverride(
    covariant _ConnectionsRepoProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'_connectionsRepoProvider';
}

/// See also [_connectionsRepo].
class _ConnectionsRepoProvider extends FutureProvider<IConnectionsRepo> {
  /// See also [_connectionsRepo].
  _ConnectionsRepoProvider(
    Profile profile,
  ) : this._internal(
          (ref) => _connectionsRepo(
            ref as _ConnectionsRepoRef,
            profile,
          ),
          from: _connectionsRepoProvider,
          name: r'_connectionsRepoProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$connectionsRepoHash,
          dependencies: _ConnectionsRepoFamily._dependencies,
          allTransitiveDependencies:
              _ConnectionsRepoFamily._allTransitiveDependencies,
          profile: profile,
        );

  _ConnectionsRepoProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<IConnectionsRepo> Function(_ConnectionsRepoRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: _ConnectionsRepoProvider._internal(
        (ref) => create(ref as _ConnectionsRepoRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  FutureProviderElement<IConnectionsRepo> createElement() {
    return _ConnectionsRepoProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is _ConnectionsRepoProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin _ConnectionsRepoRef on FutureProviderRef<IConnectionsRepo> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _ConnectionsRepoProviderElement
    extends FutureProviderElement<IConnectionsRepo> with _ConnectionsRepoRef {
  _ConnectionsRepoProviderElement(super.provider);

  @override
  Profile get profile => (origin as _ConnectionsRepoProvider).profile;
}

String _$connectionRepoInitHash() =>
    r'b529e48cf0c9231f77de413f301dd32f563efd6c';

/// See also [connectionRepoInit].
@ProviderFor(connectionRepoInit)
const connectionRepoInitProvider = ConnectionRepoInitFamily();

/// See also [connectionRepoInit].
class ConnectionRepoInitFamily extends Family<AsyncValue<bool>> {
  /// See also [connectionRepoInit].
  const ConnectionRepoInitFamily();

  /// See also [connectionRepoInit].
  ConnectionRepoInitProvider call(
    Profile profile,
  ) {
    return ConnectionRepoInitProvider(
      profile,
    );
  }

  @override
  ConnectionRepoInitProvider getProviderOverride(
    covariant ConnectionRepoInitProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'connectionRepoInitProvider';
}

/// See also [connectionRepoInit].
class ConnectionRepoInitProvider extends AutoDisposeFutureProvider<bool> {
  /// See also [connectionRepoInit].
  ConnectionRepoInitProvider(
    Profile profile,
  ) : this._internal(
          (ref) => connectionRepoInit(
            ref as ConnectionRepoInitRef,
            profile,
          ),
          from: connectionRepoInitProvider,
          name: r'connectionRepoInitProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$connectionRepoInitHash,
          dependencies: ConnectionRepoInitFamily._dependencies,
          allTransitiveDependencies:
              ConnectionRepoInitFamily._allTransitiveDependencies,
          profile: profile,
        );

  ConnectionRepoInitProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<bool> Function(ConnectionRepoInitRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ConnectionRepoInitProvider._internal(
        (ref) => create(ref as ConnectionRepoInitRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<bool> createElement() {
    return _ConnectionRepoInitProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ConnectionRepoInitProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ConnectionRepoInitRef on AutoDisposeFutureProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _ConnectionRepoInitProviderElement
    extends AutoDisposeFutureProviderElement<bool> with ConnectionRepoInitRef {
  _ConnectionRepoInitProviderElement(super.provider);

  @override
  Profile get profile => (origin as ConnectionRepoInitProvider).profile;
}

String _$knownUsersByNameHash() => r'564fbbf69fcc62f0c18c8ec98e8a0382df95f8a3';

/// See also [knownUsersByName].
@ProviderFor(knownUsersByName)
const knownUsersByNameProvider = KnownUsersByNameFamily();

/// See also [knownUsersByName].
class KnownUsersByNameFamily extends Family<List<Connection>> {
  /// See also [knownUsersByName].
  const KnownUsersByNameFamily();

  /// See also [knownUsersByName].
  KnownUsersByNameProvider call(
    Profile profile,
    String userName,
  ) {
    return KnownUsersByNameProvider(
      profile,
      userName,
    );
  }

  @override
  KnownUsersByNameProvider getProviderOverride(
    covariant KnownUsersByNameProvider provider,
  ) {
    return call(
      provider.profile,
      provider.userName,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'knownUsersByNameProvider';
}

/// See also [knownUsersByName].
class KnownUsersByNameProvider extends AutoDisposeProvider<List<Connection>> {
  /// See also [knownUsersByName].
  KnownUsersByNameProvider(
    Profile profile,
    String userName,
  ) : this._internal(
          (ref) => knownUsersByName(
            ref as KnownUsersByNameRef,
            profile,
            userName,
          ),
          from: knownUsersByNameProvider,
          name: r'knownUsersByNameProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$knownUsersByNameHash,
          dependencies: KnownUsersByNameFamily._dependencies,
          allTransitiveDependencies:
              KnownUsersByNameFamily._allTransitiveDependencies,
          profile: profile,
          userName: userName,
        );

  KnownUsersByNameProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.userName,
  }) : super.internal();

  final Profile profile;
  final String userName;

  @override
  Override overrideWith(
    List<Connection> Function(KnownUsersByNameRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: KnownUsersByNameProvider._internal(
        (ref) => create(ref as KnownUsersByNameRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        userName: userName,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<List<Connection>> createElement() {
    return _KnownUsersByNameProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is KnownUsersByNameProvider &&
        other.profile == profile &&
        other.userName == userName;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, userName.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin KnownUsersByNameRef on AutoDisposeProviderRef<List<Connection>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `userName` of this provider.
  String get userName;
}

class _KnownUsersByNameProviderElement
    extends AutoDisposeProviderElement<List<Connection>>
    with KnownUsersByNameRef {
  _KnownUsersByNameProviderElement(super.provider);

  @override
  Profile get profile => (origin as KnownUsersByNameProvider).profile;
  @override
  String get userName => (origin as KnownUsersByNameProvider).userName;
}

String _$myContactsHash() => r'd627cbc98c66e636fdf057ef7d946b5b0248d2a9';

/// See also [myContacts].
@ProviderFor(myContacts)
const myContactsProvider = MyContactsFamily();

/// See also [myContacts].
class MyContactsFamily extends Family<AsyncValue<List<Connection>>> {
  /// See also [myContacts].
  const MyContactsFamily();

  /// See also [myContacts].
  MyContactsProvider call(
    Profile profile,
  ) {
    return MyContactsProvider(
      profile,
    );
  }

  @override
  MyContactsProvider getProviderOverride(
    covariant MyContactsProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'myContactsProvider';
}

/// See also [myContacts].
class MyContactsProvider extends AutoDisposeFutureProvider<List<Connection>> {
  /// See also [myContacts].
  MyContactsProvider(
    Profile profile,
  ) : this._internal(
          (ref) => myContacts(
            ref as MyContactsRef,
            profile,
          ),
          from: myContactsProvider,
          name: r'myContactsProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$myContactsHash,
          dependencies: MyContactsFamily._dependencies,
          allTransitiveDependencies:
              MyContactsFamily._allTransitiveDependencies,
          profile: profile,
        );

  MyContactsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<List<Connection>> Function(MyContactsRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: MyContactsProvider._internal(
        (ref) => create(ref as MyContactsRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<List<Connection>> createElement() {
    return _MyContactsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is MyContactsProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin MyContactsRef on AutoDisposeFutureProviderRef<List<Connection>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _MyContactsProviderElement
    extends AutoDisposeFutureProviderElement<List<Connection>>
    with MyContactsRef {
  _MyContactsProviderElement(super.provider);

  @override
  Profile get profile => (origin as MyContactsProvider).profile;
}

String _$connectionByIdHash() => r'28721c7b361e72b0c5f5ecf16194f08eb4005d52';

/// See also [connectionById].
@ProviderFor(connectionById)
const connectionByIdProvider = ConnectionByIdFamily();

/// See also [connectionById].
class ConnectionByIdFamily extends Family<Result<Connection, ExecError>> {
  /// See also [connectionById].
  const ConnectionByIdFamily();

  /// See also [connectionById].
  ConnectionByIdProvider call(
    Profile profile,
    String id, {
    bool forceUpdate = false,
  }) {
    return ConnectionByIdProvider(
      profile,
      id,
      forceUpdate: forceUpdate,
    );
  }

  @override
  ConnectionByIdProvider getProviderOverride(
    covariant ConnectionByIdProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
      forceUpdate: provider.forceUpdate,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'connectionByIdProvider';
}

/// See also [connectionById].
class ConnectionByIdProvider
    extends AutoDisposeProvider<Result<Connection, ExecError>> {
  /// See also [connectionById].
  ConnectionByIdProvider(
    Profile profile,
    String id, {
    bool forceUpdate = false,
  }) : this._internal(
          (ref) => connectionById(
            ref as ConnectionByIdRef,
            profile,
            id,
            forceUpdate: forceUpdate,
          ),
          from: connectionByIdProvider,
          name: r'connectionByIdProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$connectionByIdHash,
          dependencies: ConnectionByIdFamily._dependencies,
          allTransitiveDependencies:
              ConnectionByIdFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
          forceUpdate: forceUpdate,
        );

  ConnectionByIdProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
    required this.forceUpdate,
  }) : super.internal();

  final Profile profile;
  final String id;
  final bool forceUpdate;

  @override
  Override overrideWith(
    Result<Connection, ExecError> Function(ConnectionByIdRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ConnectionByIdProvider._internal(
        (ref) => create(ref as ConnectionByIdRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
        forceUpdate: forceUpdate,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<Result<Connection, ExecError>> createElement() {
    return _ConnectionByIdProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ConnectionByIdProvider &&
        other.profile == profile &&
        other.id == id &&
        other.forceUpdate == forceUpdate;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);
    hash = _SystemHash.combine(hash, forceUpdate.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ConnectionByIdRef
    on AutoDisposeProviderRef<Result<Connection, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;

  /// The parameter `forceUpdate` of this provider.
  bool get forceUpdate;
}

class _ConnectionByIdProviderElement
    extends AutoDisposeProviderElement<Result<Connection, ExecError>>
    with ConnectionByIdRef {
  _ConnectionByIdProviderElement(super.provider);

  @override
  Profile get profile => (origin as ConnectionByIdProvider).profile;
  @override
  String get id => (origin as ConnectionByIdProvider).id;
  @override
  bool get forceUpdate => (origin as ConnectionByIdProvider).forceUpdate;
}

String _$connectionByHandleHash() =>
    r'00667a128bd3bb6fd8071bbc6ba20b37940c9aad';

/// See also [connectionByHandle].
@ProviderFor(connectionByHandle)
const connectionByHandleProvider = ConnectionByHandleFamily();

/// See also [connectionByHandle].
class ConnectionByHandleFamily extends Family<Result<Connection, ExecError>> {
  /// See also [connectionByHandle].
  const ConnectionByHandleFamily();

  /// See also [connectionByHandle].
  ConnectionByHandleProvider call(
    Profile profile,
    String handle,
  ) {
    return ConnectionByHandleProvider(
      profile,
      handle,
    );
  }

  @override
  ConnectionByHandleProvider getProviderOverride(
    covariant ConnectionByHandleProvider provider,
  ) {
    return call(
      provider.profile,
      provider.handle,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'connectionByHandleProvider';
}

/// See also [connectionByHandle].
class ConnectionByHandleProvider
    extends AutoDisposeProvider<Result<Connection, ExecError>> {
  /// See also [connectionByHandle].
  ConnectionByHandleProvider(
    Profile profile,
    String handle,
  ) : this._internal(
          (ref) => connectionByHandle(
            ref as ConnectionByHandleRef,
            profile,
            handle,
          ),
          from: connectionByHandleProvider,
          name: r'connectionByHandleProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$connectionByHandleHash,
          dependencies: ConnectionByHandleFamily._dependencies,
          allTransitiveDependencies:
              ConnectionByHandleFamily._allTransitiveDependencies,
          profile: profile,
          handle: handle,
        );

  ConnectionByHandleProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.handle,
  }) : super.internal();

  final Profile profile;
  final String handle;

  @override
  Override overrideWith(
    Result<Connection, ExecError> Function(ConnectionByHandleRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ConnectionByHandleProvider._internal(
        (ref) => create(ref as ConnectionByHandleRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        handle: handle,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<Result<Connection, ExecError>> createElement() {
    return _ConnectionByHandleProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ConnectionByHandleProvider &&
        other.profile == profile &&
        other.handle == handle;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, handle.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ConnectionByHandleRef
    on AutoDisposeProviderRef<Result<Connection, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `handle` of this provider.
  String get handle;
}

class _ConnectionByHandleProviderElement
    extends AutoDisposeProviderElement<Result<Connection, ExecError>>
    with ConnectionByHandleRef {
  _ConnectionByHandleProviderElement(super.provider);

  @override
  Profile get profile => (origin as ConnectionByHandleProvider).profile;
  @override
  String get handle => (origin as ConnectionByHandleProvider).handle;
}

String _$updateStatusHash() => r'88c6908dfb87a74ed9c0ffacefec7508072898ff';

/// See also [updateStatus].
@ProviderFor(updateStatus)
const updateStatusProvider = UpdateStatusFamily();

/// See also [updateStatus].
class UpdateStatusFamily extends Family<String> {
  /// See also [updateStatus].
  const UpdateStatusFamily();

  /// See also [updateStatus].
  UpdateStatusProvider call(
    Profile profile,
  ) {
    return UpdateStatusProvider(
      profile,
    );
  }

  @override
  UpdateStatusProvider getProviderOverride(
    covariant UpdateStatusProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'updateStatusProvider';
}

/// See also [updateStatus].
class UpdateStatusProvider extends AutoDisposeProvider<String> {
  /// See also [updateStatus].
  UpdateStatusProvider(
    Profile profile,
  ) : this._internal(
          (ref) => updateStatus(
            ref as UpdateStatusRef,
            profile,
          ),
          from: updateStatusProvider,
          name: r'updateStatusProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$updateStatusHash,
          dependencies: UpdateStatusFamily._dependencies,
          allTransitiveDependencies:
              UpdateStatusFamily._allTransitiveDependencies,
          profile: profile,
        );

  UpdateStatusProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    String Function(UpdateStatusRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: UpdateStatusProvider._internal(
        (ref) => create(ref as UpdateStatusRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<String> createElement() {
    return _UpdateStatusProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is UpdateStatusProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin UpdateStatusRef on AutoDisposeProviderRef<String> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _UpdateStatusProviderElement extends AutoDisposeProviderElement<String>
    with UpdateStatusRef {
  _UpdateStatusProviderElement(super.provider);

  @override
  Profile get profile => (origin as UpdateStatusProvider).profile;
}

String _$suggestedConnectionsHash() =>
    r'5b9997d6815db3666b5e99f7d7d39b3f7f35fd8c';

/// See also [suggestedConnections].
@ProviderFor(suggestedConnections)
const suggestedConnectionsProvider = SuggestedConnectionsFamily();

/// See also [suggestedConnections].
class SuggestedConnectionsFamily
    extends Family<AsyncValue<Result<List<Connection>, ExecError>>> {
  /// See also [suggestedConnections].
  const SuggestedConnectionsFamily();

  /// See also [suggestedConnections].
  SuggestedConnectionsProvider call(
    Profile profile,
  ) {
    return SuggestedConnectionsProvider(
      profile,
    );
  }

  @override
  SuggestedConnectionsProvider getProviderOverride(
    covariant SuggestedConnectionsProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'suggestedConnectionsProvider';
}

/// See also [suggestedConnections].
class SuggestedConnectionsProvider
    extends AutoDisposeFutureProvider<Result<List<Connection>, ExecError>> {
  /// See also [suggestedConnections].
  SuggestedConnectionsProvider(
    Profile profile,
  ) : this._internal(
          (ref) => suggestedConnections(
            ref as SuggestedConnectionsRef,
            profile,
          ),
          from: suggestedConnectionsProvider,
          name: r'suggestedConnectionsProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$suggestedConnectionsHash,
          dependencies: SuggestedConnectionsFamily._dependencies,
          allTransitiveDependencies:
              SuggestedConnectionsFamily._allTransitiveDependencies,
          profile: profile,
        );

  SuggestedConnectionsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<Result<List<Connection>, ExecError>> Function(
            SuggestedConnectionsRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: SuggestedConnectionsProvider._internal(
        (ref) => create(ref as SuggestedConnectionsRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<Connection>, ExecError>>
      createElement() {
    return _SuggestedConnectionsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SuggestedConnectionsProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin SuggestedConnectionsRef
    on AutoDisposeFutureProviderRef<Result<List<Connection>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _SuggestedConnectionsProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<List<Connection>, ExecError>> with SuggestedConnectionsRef {
  _SuggestedConnectionsProviderElement(super.provider);

  @override
  Profile get profile => (origin as SuggestedConnectionsProvider).profile;
}

String _$connectionsRepoSyncHash() =>
    r'ba59d1737cecf479c187598352a11ad9264a3a67';

abstract class _$ConnectionsRepoSync
    extends BuildlessNotifier<Result<IConnectionsRepo, ExecError>> {
  late final Profile profile;

  Result<IConnectionsRepo, ExecError> build(
    Profile profile,
  );
}

/// See also [_ConnectionsRepoSync].
@ProviderFor(_ConnectionsRepoSync)
const _connectionsRepoSyncProvider = _ConnectionsRepoSyncFamily();

/// See also [_ConnectionsRepoSync].
class _ConnectionsRepoSyncFamily
    extends Family<Result<IConnectionsRepo, ExecError>> {
  /// See also [_ConnectionsRepoSync].
  const _ConnectionsRepoSyncFamily();

  /// See also [_ConnectionsRepoSync].
  _ConnectionsRepoSyncProvider call(
    Profile profile,
  ) {
    return _ConnectionsRepoSyncProvider(
      profile,
    );
  }

  @override
  _ConnectionsRepoSyncProvider getProviderOverride(
    covariant _ConnectionsRepoSyncProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'_connectionsRepoSyncProvider';
}

/// See also [_ConnectionsRepoSync].
class _ConnectionsRepoSyncProvider extends NotifierProviderImpl<
    _ConnectionsRepoSync, Result<IConnectionsRepo, ExecError>> {
  /// See also [_ConnectionsRepoSync].
  _ConnectionsRepoSyncProvider(
    Profile profile,
  ) : this._internal(
          () => _ConnectionsRepoSync()..profile = profile,
          from: _connectionsRepoSyncProvider,
          name: r'_connectionsRepoSyncProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$connectionsRepoSyncHash,
          dependencies: _ConnectionsRepoSyncFamily._dependencies,
          allTransitiveDependencies:
              _ConnectionsRepoSyncFamily._allTransitiveDependencies,
          profile: profile,
        );

  _ConnectionsRepoSyncProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Result<IConnectionsRepo, ExecError> runNotifierBuild(
    covariant _ConnectionsRepoSync notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(_ConnectionsRepoSync Function() create) {
    return ProviderOverride(
      origin: this,
      override: _ConnectionsRepoSyncProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<_ConnectionsRepoSync,
      Result<IConnectionsRepo, ExecError>> createElement() {
    return _ConnectionsRepoSyncProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is _ConnectionsRepoSyncProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin _ConnectionsRepoSyncRef
    on NotifierProviderRef<Result<IConnectionsRepo, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _ConnectionsRepoSyncProviderElement extends NotifierProviderElement<
    _ConnectionsRepoSync,
    Result<IConnectionsRepo, ExecError>> with _ConnectionsRepoSyncRef {
  _ConnectionsRepoSyncProviderElement(super.provider);

  @override
  Profile get profile => (origin as _ConnectionsRepoSyncProvider).profile;
}

String _$connectionsRepoClearerHash() =>
    r'f28ccdba4bc4170a39b3d99f992538e40c1bf15e';

abstract class _$ConnectionsRepoClearer
    extends BuildlessAutoDisposeNotifier<bool> {
  late final Profile profile;

  bool build(
    Profile profile,
  );
}

/// See also [ConnectionsRepoClearer].
@ProviderFor(ConnectionsRepoClearer)
const connectionsRepoClearerProvider = ConnectionsRepoClearerFamily();

/// See also [ConnectionsRepoClearer].
class ConnectionsRepoClearerFamily extends Family<bool> {
  /// See also [ConnectionsRepoClearer].
  const ConnectionsRepoClearerFamily();

  /// See also [ConnectionsRepoClearer].
  ConnectionsRepoClearerProvider call(
    Profile profile,
  ) {
    return ConnectionsRepoClearerProvider(
      profile,
    );
  }

  @override
  ConnectionsRepoClearerProvider getProviderOverride(
    covariant ConnectionsRepoClearerProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'connectionsRepoClearerProvider';
}

/// See also [ConnectionsRepoClearer].
class ConnectionsRepoClearerProvider
    extends AutoDisposeNotifierProviderImpl<ConnectionsRepoClearer, bool> {
  /// See also [ConnectionsRepoClearer].
  ConnectionsRepoClearerProvider(
    Profile profile,
  ) : this._internal(
          () => ConnectionsRepoClearer()..profile = profile,
          from: connectionsRepoClearerProvider,
          name: r'connectionsRepoClearerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$connectionsRepoClearerHash,
          dependencies: ConnectionsRepoClearerFamily._dependencies,
          allTransitiveDependencies:
              ConnectionsRepoClearerFamily._allTransitiveDependencies,
          profile: profile,
        );

  ConnectionsRepoClearerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  bool runNotifierBuild(
    covariant ConnectionsRepoClearer notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(ConnectionsRepoClearer Function() create) {
    return ProviderOverride(
      origin: this,
      override: ConnectionsRepoClearerProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<ConnectionsRepoClearer, bool>
      createElement() {
    return _ConnectionsRepoClearerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ConnectionsRepoClearerProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ConnectionsRepoClearerRef on AutoDisposeNotifierProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _ConnectionsRepoClearerProviderElement
    extends AutoDisposeNotifierProviderElement<ConnectionsRepoClearer, bool>
    with ConnectionsRepoClearerRef {
  _ConnectionsRepoClearerProviderElement(super.provider);

  @override
  Profile get profile => (origin as ConnectionsRepoClearerProvider).profile;
}

String _$connectionModifierHash() =>
    r'f91de86b36d5e3d804a872747f005b44667a8b93';

abstract class _$ConnectionModifier
    extends BuildlessAutoDisposeNotifier<Connection> {
  late final Profile profile;
  late final Connection connection;

  Connection build(
    Profile profile,
    Connection connection,
  );
}

/// See also [ConnectionModifier].
@ProviderFor(ConnectionModifier)
const connectionModifierProvider = ConnectionModifierFamily();

/// See also [ConnectionModifier].
class ConnectionModifierFamily extends Family<Connection> {
  /// See also [ConnectionModifier].
  const ConnectionModifierFamily();

  /// See also [ConnectionModifier].
  ConnectionModifierProvider call(
    Profile profile,
    Connection connection,
  ) {
    return ConnectionModifierProvider(
      profile,
      connection,
    );
  }

  @override
  ConnectionModifierProvider getProviderOverride(
    covariant ConnectionModifierProvider provider,
  ) {
    return call(
      provider.profile,
      provider.connection,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'connectionModifierProvider';
}

/// See also [ConnectionModifier].
class ConnectionModifierProvider
    extends AutoDisposeNotifierProviderImpl<ConnectionModifier, Connection> {
  /// See also [ConnectionModifier].
  ConnectionModifierProvider(
    Profile profile,
    Connection connection,
  ) : this._internal(
          () => ConnectionModifier()
            ..profile = profile
            ..connection = connection,
          from: connectionModifierProvider,
          name: r'connectionModifierProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$connectionModifierHash,
          dependencies: ConnectionModifierFamily._dependencies,
          allTransitiveDependencies:
              ConnectionModifierFamily._allTransitiveDependencies,
          profile: profile,
          connection: connection,
        );

  ConnectionModifierProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.connection,
  }) : super.internal();

  final Profile profile;
  final Connection connection;

  @override
  Connection runNotifierBuild(
    covariant ConnectionModifier notifier,
  ) {
    return notifier.build(
      profile,
      connection,
    );
  }

  @override
  Override overrideWith(ConnectionModifier Function() create) {
    return ProviderOverride(
      origin: this,
      override: ConnectionModifierProvider._internal(
        () => create()
          ..profile = profile
          ..connection = connection,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        connection: connection,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<ConnectionModifier, Connection>
      createElement() {
    return _ConnectionModifierProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ConnectionModifierProvider &&
        other.profile == profile &&
        other.connection == connection;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, connection.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ConnectionModifierRef on AutoDisposeNotifierProviderRef<Connection> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `connection` of this provider.
  Connection get connection;
}

class _ConnectionModifierProviderElement
    extends AutoDisposeNotifierProviderElement<ConnectionModifier, Connection>
    with ConnectionModifierRef {
  _ConnectionModifierProviderElement(super.provider);

  @override
  Profile get profile => (origin as ConnectionModifierProvider).profile;
  @override
  Connection get connection =>
      (origin as ConnectionModifierProvider).connection;
}

String _$updateStatusInternalHash() =>
    r'fad078a5cd12ff8d85b59d010afde7470167c1b2';

abstract class _$UpdateStatusInternal extends BuildlessNotifier<String> {
  late final Profile profile;

  String build(
    Profile profile,
  );
}

/// See also [_UpdateStatusInternal].
@ProviderFor(_UpdateStatusInternal)
const _updateStatusInternalProvider = _UpdateStatusInternalFamily();

/// See also [_UpdateStatusInternal].
class _UpdateStatusInternalFamily extends Family<String> {
  /// See also [_UpdateStatusInternal].
  const _UpdateStatusInternalFamily();

  /// See also [_UpdateStatusInternal].
  _UpdateStatusInternalProvider call(
    Profile profile,
  ) {
    return _UpdateStatusInternalProvider(
      profile,
    );
  }

  @override
  _UpdateStatusInternalProvider getProviderOverride(
    covariant _UpdateStatusInternalProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'_updateStatusInternalProvider';
}

/// See also [_UpdateStatusInternal].
class _UpdateStatusInternalProvider
    extends NotifierProviderImpl<_UpdateStatusInternal, String> {
  /// See also [_UpdateStatusInternal].
  _UpdateStatusInternalProvider(
    Profile profile,
  ) : this._internal(
          () => _UpdateStatusInternal()..profile = profile,
          from: _updateStatusInternalProvider,
          name: r'_updateStatusInternalProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$updateStatusInternalHash,
          dependencies: _UpdateStatusInternalFamily._dependencies,
          allTransitiveDependencies:
              _UpdateStatusInternalFamily._allTransitiveDependencies,
          profile: profile,
        );

  _UpdateStatusInternalProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  String runNotifierBuild(
    covariant _UpdateStatusInternal notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(_UpdateStatusInternal Function() create) {
    return ProviderOverride(
      origin: this,
      override: _UpdateStatusInternalProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<_UpdateStatusInternal, String> createElement() {
    return _UpdateStatusInternalProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is _UpdateStatusInternalProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin _UpdateStatusInternalRef on NotifierProviderRef<String> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _UpdateStatusInternalProviderElement
    extends NotifierProviderElement<_UpdateStatusInternal, String>
    with _UpdateStatusInternalRef {
  _UpdateStatusInternalProviderElement(super.provider);

  @override
  Profile get profile => (origin as _UpdateStatusInternalProvider).profile;
}

String _$allContactsUpdaterHash() =>
    r'77b82a2382d19d202ce5dc025528a27ef58d8a81';

abstract class _$AllContactsUpdater extends BuildlessNotifier<bool> {
  late final Profile profile;

  bool build(
    Profile profile,
  );
}

/// See also [AllContactsUpdater].
@ProviderFor(AllContactsUpdater)
const allContactsUpdaterProvider = AllContactsUpdaterFamily();

/// See also [AllContactsUpdater].
class AllContactsUpdaterFamily extends Family<bool> {
  /// See also [AllContactsUpdater].
  const AllContactsUpdaterFamily();

  /// See also [AllContactsUpdater].
  AllContactsUpdaterProvider call(
    Profile profile,
  ) {
    return AllContactsUpdaterProvider(
      profile,
    );
  }

  @override
  AllContactsUpdaterProvider getProviderOverride(
    covariant AllContactsUpdaterProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'allContactsUpdaterProvider';
}

/// See also [AllContactsUpdater].
class AllContactsUpdaterProvider
    extends NotifierProviderImpl<AllContactsUpdater, bool> {
  /// See also [AllContactsUpdater].
  AllContactsUpdaterProvider(
    Profile profile,
  ) : this._internal(
          () => AllContactsUpdater()..profile = profile,
          from: allContactsUpdaterProvider,
          name: r'allContactsUpdaterProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$allContactsUpdaterHash,
          dependencies: AllContactsUpdaterFamily._dependencies,
          allTransitiveDependencies:
              AllContactsUpdaterFamily._allTransitiveDependencies,
          profile: profile,
        );

  AllContactsUpdaterProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  bool runNotifierBuild(
    covariant AllContactsUpdater notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(AllContactsUpdater Function() create) {
    return ProviderOverride(
      origin: this,
      override: AllContactsUpdaterProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<AllContactsUpdater, bool> createElement() {
    return _AllContactsUpdaterProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is AllContactsUpdaterProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin AllContactsUpdaterRef on NotifierProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _AllContactsUpdaterProviderElement
    extends NotifierProviderElement<AllContactsUpdater, bool>
    with AllContactsUpdaterRef {
  _AllContactsUpdaterProviderElement(super.provider);

  @override
  Profile get profile => (origin as AllContactsUpdaterProvider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
