import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:stack_trace/stack_trace.dart';

import '../models/auth/profile.dart';
import '../models/friendica_version.dart';
import '../models/instance_info.dart';
import 'networking/fediverse_instance_client_services.dart';

part 'instance_info_services.g.dart';

final _logger = Logger('InstanceInfoManager');

@Riverpod(keepAlive: true)
class InstanceInfoManager extends _$InstanceInfoManager {
  @override
  InstanceInfo build(Profile profile) {
    _logger.info('Building instance info for $profile');
    update();
    return InstanceInfo(
      maxStatusCharacters: 1000,
      maxImageBytes: 800000,
      friendicaVersion: v2024_08,
      versionString: v2024_08.toVersionString(),
    );
  }

  Future<void> update() async {
    await ref.read(instanceDataProvider(profile).future).match(
        onSuccess: (info) {
      _logger.info('Got server info for ${profile.handle}: $info');
      state = info;
    }, onError: (error) {
      _logger.severe('Error getting server info for ${profile.handle}: $error',
          Trace.current());
    });
  }
}
