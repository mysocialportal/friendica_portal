// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feature_checker_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$versionRequirementHash() =>
    r'cfc9da3d211eaaad027e46f0549ca5c4be72142e';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [versionRequirement].
@ProviderFor(versionRequirement)
const versionRequirementProvider = VersionRequirementFamily();

/// See also [versionRequirement].
class VersionRequirementFamily extends Family<FriendicaVersionRequirement> {
  /// See also [versionRequirement].
  const VersionRequirementFamily();

  /// See also [versionRequirement].
  VersionRequirementProvider call(
    RelaticaFeatures feature,
  ) {
    return VersionRequirementProvider(
      feature,
    );
  }

  @override
  VersionRequirementProvider getProviderOverride(
    covariant VersionRequirementProvider provider,
  ) {
    return call(
      provider.feature,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'versionRequirementProvider';
}

/// See also [versionRequirement].
class VersionRequirementProvider
    extends AutoDisposeProvider<FriendicaVersionRequirement> {
  /// See also [versionRequirement].
  VersionRequirementProvider(
    RelaticaFeatures feature,
  ) : this._internal(
          (ref) => versionRequirement(
            ref as VersionRequirementRef,
            feature,
          ),
          from: versionRequirementProvider,
          name: r'versionRequirementProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$versionRequirementHash,
          dependencies: VersionRequirementFamily._dependencies,
          allTransitiveDependencies:
              VersionRequirementFamily._allTransitiveDependencies,
          feature: feature,
        );

  VersionRequirementProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.feature,
  }) : super.internal();

  final RelaticaFeatures feature;

  @override
  Override overrideWith(
    FriendicaVersionRequirement Function(VersionRequirementRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: VersionRequirementProvider._internal(
        (ref) => create(ref as VersionRequirementRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        feature: feature,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<FriendicaVersionRequirement> createElement() {
    return _VersionRequirementProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is VersionRequirementProvider && other.feature == feature;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, feature.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin VersionRequirementRef
    on AutoDisposeProviderRef<FriendicaVersionRequirement> {
  /// The parameter `feature` of this provider.
  RelaticaFeatures get feature;
}

class _VersionRequirementProviderElement
    extends AutoDisposeProviderElement<FriendicaVersionRequirement>
    with VersionRequirementRef {
  _VersionRequirementProviderElement(super.provider);

  @override
  RelaticaFeatures get feature =>
      (origin as VersionRequirementProvider).feature;
}

String _$versionErrorStringHash() =>
    r'f1021879716c31e5272736a96e126e334c061e9d';

/// See also [versionErrorString].
@ProviderFor(versionErrorString)
const versionErrorStringProvider = VersionErrorStringFamily();

/// See also [versionErrorString].
class VersionErrorStringFamily extends Family<String> {
  /// See also [versionErrorString].
  const VersionErrorStringFamily();

  /// See also [versionErrorString].
  VersionErrorStringProvider call(
    RelaticaFeatures feature,
  ) {
    return VersionErrorStringProvider(
      feature,
    );
  }

  @override
  VersionErrorStringProvider getProviderOverride(
    covariant VersionErrorStringProvider provider,
  ) {
    return call(
      provider.feature,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'versionErrorStringProvider';
}

/// See also [versionErrorString].
class VersionErrorStringProvider extends AutoDisposeProvider<String> {
  /// See also [versionErrorString].
  VersionErrorStringProvider(
    RelaticaFeatures feature,
  ) : this._internal(
          (ref) => versionErrorString(
            ref as VersionErrorStringRef,
            feature,
          ),
          from: versionErrorStringProvider,
          name: r'versionErrorStringProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$versionErrorStringHash,
          dependencies: VersionErrorStringFamily._dependencies,
          allTransitiveDependencies:
              VersionErrorStringFamily._allTransitiveDependencies,
          feature: feature,
        );

  VersionErrorStringProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.feature,
  }) : super.internal();

  final RelaticaFeatures feature;

  @override
  Override overrideWith(
    String Function(VersionErrorStringRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: VersionErrorStringProvider._internal(
        (ref) => create(ref as VersionErrorStringRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        feature: feature,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<String> createElement() {
    return _VersionErrorStringProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is VersionErrorStringProvider && other.feature == feature;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, feature.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin VersionErrorStringRef on AutoDisposeProviderRef<String> {
  /// The parameter `feature` of this provider.
  RelaticaFeatures get feature;
}

class _VersionErrorStringProviderElement
    extends AutoDisposeProviderElement<String> with VersionErrorStringRef {
  _VersionErrorStringProviderElement(super.provider);

  @override
  RelaticaFeatures get feature =>
      (origin as VersionErrorStringProvider).feature;
}

String _$featureCheckHash() => r'8e60d2e4278f205aac2e0e434b595cb7aa5b192b';

/// See also [featureCheck].
@ProviderFor(featureCheck)
const featureCheckProvider = FeatureCheckFamily();

/// See also [featureCheck].
class FeatureCheckFamily extends Family<bool> {
  /// See also [featureCheck].
  const FeatureCheckFamily();

  /// See also [featureCheck].
  FeatureCheckProvider call(
    Profile profile,
    RelaticaFeatures feature,
  ) {
    return FeatureCheckProvider(
      profile,
      feature,
    );
  }

  @override
  FeatureCheckProvider getProviderOverride(
    covariant FeatureCheckProvider provider,
  ) {
    return call(
      provider.profile,
      provider.feature,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'featureCheckProvider';
}

/// See also [featureCheck].
class FeatureCheckProvider extends AutoDisposeProvider<bool> {
  /// See also [featureCheck].
  FeatureCheckProvider(
    Profile profile,
    RelaticaFeatures feature,
  ) : this._internal(
          (ref) => featureCheck(
            ref as FeatureCheckRef,
            profile,
            feature,
          ),
          from: featureCheckProvider,
          name: r'featureCheckProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$featureCheckHash,
          dependencies: FeatureCheckFamily._dependencies,
          allTransitiveDependencies:
              FeatureCheckFamily._allTransitiveDependencies,
          profile: profile,
          feature: feature,
        );

  FeatureCheckProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.feature,
  }) : super.internal();

  final Profile profile;
  final RelaticaFeatures feature;

  @override
  Override overrideWith(
    bool Function(FeatureCheckRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FeatureCheckProvider._internal(
        (ref) => create(ref as FeatureCheckRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        feature: feature,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<bool> createElement() {
    return _FeatureCheckProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FeatureCheckProvider &&
        other.profile == profile &&
        other.feature == feature;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, feature.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FeatureCheckRef on AutoDisposeProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `feature` of this provider.
  RelaticaFeatures get feature;
}

class _FeatureCheckProviderElement extends AutoDisposeProviderElement<bool>
    with FeatureCheckRef {
  _FeatureCheckProviderElement(super.provider);

  @override
  Profile get profile => (origin as FeatureCheckProvider).profile;
  @override
  RelaticaFeatures get feature => (origin as FeatureCheckProvider).feature;
}

String _$featureCheckResultHash() =>
    r'bd6f18680f32fd0b261c9b850dd1b26de490d1e3';

/// See also [featureCheckResult].
@ProviderFor(featureCheckResult)
const featureCheckResultProvider = FeatureCheckResultFamily();

/// See also [featureCheckResult].
class FeatureCheckResultFamily extends Family<Result<bool, ExecError>> {
  /// See also [featureCheckResult].
  const FeatureCheckResultFamily();

  /// See also [featureCheckResult].
  FeatureCheckResultProvider call(
    Profile profile,
    RelaticaFeatures feature,
  ) {
    return FeatureCheckResultProvider(
      profile,
      feature,
    );
  }

  @override
  FeatureCheckResultProvider getProviderOverride(
    covariant FeatureCheckResultProvider provider,
  ) {
    return call(
      provider.profile,
      provider.feature,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'featureCheckResultProvider';
}

/// See also [featureCheckResult].
class FeatureCheckResultProvider
    extends AutoDisposeProvider<Result<bool, ExecError>> {
  /// See also [featureCheckResult].
  FeatureCheckResultProvider(
    Profile profile,
    RelaticaFeatures feature,
  ) : this._internal(
          (ref) => featureCheckResult(
            ref as FeatureCheckResultRef,
            profile,
            feature,
          ),
          from: featureCheckResultProvider,
          name: r'featureCheckResultProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$featureCheckResultHash,
          dependencies: FeatureCheckResultFamily._dependencies,
          allTransitiveDependencies:
              FeatureCheckResultFamily._allTransitiveDependencies,
          profile: profile,
          feature: feature,
        );

  FeatureCheckResultProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.feature,
  }) : super.internal();

  final Profile profile;
  final RelaticaFeatures feature;

  @override
  Override overrideWith(
    Result<bool, ExecError> Function(FeatureCheckResultRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FeatureCheckResultProvider._internal(
        (ref) => create(ref as FeatureCheckResultRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        feature: feature,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<Result<bool, ExecError>> createElement() {
    return _FeatureCheckResultProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FeatureCheckResultProvider &&
        other.profile == profile &&
        other.feature == feature;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, feature.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FeatureCheckResultRef on AutoDisposeProviderRef<Result<bool, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `feature` of this provider.
  RelaticaFeatures get feature;
}

class _FeatureCheckResultProviderElement
    extends AutoDisposeProviderElement<Result<bool, ExecError>>
    with FeatureCheckResultRef {
  _FeatureCheckResultProviderElement(super.provider);

  @override
  Profile get profile => (origin as FeatureCheckResultProvider).profile;
  @override
  RelaticaFeatures get feature =>
      (origin as FeatureCheckResultProvider).feature;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
