import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../models/auth/profile.dart';
import 'connection_manager_services.dart';
import 'direct_message_services.dart';
import 'follow_requests_services.dart';
import 'hashtag_service.dart';
import 'notification_services.dart';

part 'cache_clear_service.g.dart';

@riverpod
class ClearCaches extends _$ClearCaches {
  @override
  bool build() {
    return true;
  }

  bool cacheClearService() {
    ref.invalidate(notificationsManagerProvider);
    return true;
  }

  bool profileCacheClearService(Profile profile) {
    ref.invalidate(followRequestsProvider(profile));
    ref.invalidate(directMessageThreadIdsProvider(profile));
    ref.read(connectionsRepoClearerProvider(profile).notifier).clear();
    ref.read(hashtagServiceProvider(profile).notifier).clear();
    // TODO see what else needs to be cleared out here
    return true;
  }
}
