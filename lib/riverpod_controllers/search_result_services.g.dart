// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_result_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$sharedPreferencesHash() => r'6bbc55d4dc38d5979ff916112845bcc4a4a3a1ea';

/// See also [sharedPreferences].
@ProviderFor(sharedPreferences)
final sharedPreferencesProvider = Provider<SharedPreferences>.internal(
  sharedPreferences,
  name: r'sharedPreferencesProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$sharedPreferencesHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef SharedPreferencesRef = ProviderRef<SharedPreferences>;
String _$searchResultsManagerHash() =>
    r'ab855cebf742ac26f5b35b667bba0314d1da702e';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$SearchResultsManager extends BuildlessAutoDisposeAsyncNotifier<
    Result<SearchResults, ExecError>> {
  late final Profile profile;
  late final SearchTypes searchType;
  late final String searchText;
  late final int limit;

  FutureOr<Result<SearchResults, ExecError>> build(
    Profile profile,
    SearchTypes searchType,
    String searchText, {
    int limit = 50,
  });
}

/// See also [SearchResultsManager].
@ProviderFor(SearchResultsManager)
const searchResultsManagerProvider = SearchResultsManagerFamily();

/// See also [SearchResultsManager].
class SearchResultsManagerFamily
    extends Family<AsyncValue<Result<SearchResults, ExecError>>> {
  /// See also [SearchResultsManager].
  const SearchResultsManagerFamily();

  /// See also [SearchResultsManager].
  SearchResultsManagerProvider call(
    Profile profile,
    SearchTypes searchType,
    String searchText, {
    int limit = 50,
  }) {
    return SearchResultsManagerProvider(
      profile,
      searchType,
      searchText,
      limit: limit,
    );
  }

  @override
  SearchResultsManagerProvider getProviderOverride(
    covariant SearchResultsManagerProvider provider,
  ) {
    return call(
      provider.profile,
      provider.searchType,
      provider.searchText,
      limit: provider.limit,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'searchResultsManagerProvider';
}

/// See also [SearchResultsManager].
class SearchResultsManagerProvider extends AutoDisposeAsyncNotifierProviderImpl<
    SearchResultsManager, Result<SearchResults, ExecError>> {
  /// See also [SearchResultsManager].
  SearchResultsManagerProvider(
    Profile profile,
    SearchTypes searchType,
    String searchText, {
    int limit = 50,
  }) : this._internal(
          () => SearchResultsManager()
            ..profile = profile
            ..searchType = searchType
            ..searchText = searchText
            ..limit = limit,
          from: searchResultsManagerProvider,
          name: r'searchResultsManagerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$searchResultsManagerHash,
          dependencies: SearchResultsManagerFamily._dependencies,
          allTransitiveDependencies:
              SearchResultsManagerFamily._allTransitiveDependencies,
          profile: profile,
          searchType: searchType,
          searchText: searchText,
          limit: limit,
        );

  SearchResultsManagerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.searchType,
    required this.searchText,
    required this.limit,
  }) : super.internal();

  final Profile profile;
  final SearchTypes searchType;
  final String searchText;
  final int limit;

  @override
  FutureOr<Result<SearchResults, ExecError>> runNotifierBuild(
    covariant SearchResultsManager notifier,
  ) {
    return notifier.build(
      profile,
      searchType,
      searchText,
      limit: limit,
    );
  }

  @override
  Override overrideWith(SearchResultsManager Function() create) {
    return ProviderOverride(
      origin: this,
      override: SearchResultsManagerProvider._internal(
        () => create()
          ..profile = profile
          ..searchType = searchType
          ..searchText = searchText
          ..limit = limit,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        searchType: searchType,
        searchText: searchText,
        limit: limit,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<SearchResultsManager,
      Result<SearchResults, ExecError>> createElement() {
    return _SearchResultsManagerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SearchResultsManagerProvider &&
        other.profile == profile &&
        other.searchType == searchType &&
        other.searchText == searchText &&
        other.limit == limit;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, searchType.hashCode);
    hash = _SystemHash.combine(hash, searchText.hashCode);
    hash = _SystemHash.combine(hash, limit.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin SearchResultsManagerRef
    on AutoDisposeAsyncNotifierProviderRef<Result<SearchResults, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `searchType` of this provider.
  SearchTypes get searchType;

  /// The parameter `searchText` of this provider.
  String get searchText;

  /// The parameter `limit` of this provider.
  int get limit;
}

class _SearchResultsManagerProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<SearchResultsManager,
        Result<SearchResults, ExecError>> with SearchResultsManagerRef {
  _SearchResultsManagerProviderElement(super.provider);

  @override
  Profile get profile => (origin as SearchResultsManagerProvider).profile;
  @override
  SearchTypes get searchType =>
      (origin as SearchResultsManagerProvider).searchType;
  @override
  String get searchText => (origin as SearchResultsManagerProvider).searchText;
  @override
  int get limit => (origin as SearchResultsManagerProvider).limit;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
