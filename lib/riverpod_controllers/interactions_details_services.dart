import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../models/auth/profile.dart';
import '../models/connection.dart';
import '../models/exec_error.dart';
import 'networking/friendica_interactions_client_services.dart';
import 'rp_provider_extension.dart';

part 'interactions_details_services.g.dart';

const _cacheDuration = Duration(minutes: 5);
final _likesLogger = Logger('LikesForStatusProvider');
final _resharesLogger = Logger('ResharesForStatusProvider');

@riverpod
Future<Result<List<Connection>, ExecError>> likesForStatus(
    Ref ref, Profile profile, String statusId) async {
  _likesLogger.fine('Creating provider for $statusId for Profile $profile');
  ref.cacheFor(_cacheDuration);
  final likesResult =
      await ref.read(likesClientProvider(profile, statusId).future);
  _likesLogger.fine('Values received for $statusId for Profile $profile');

  return likesResult;
}

@riverpod
Future<Result<List<Connection>, ExecError>> resharesForStatus(
    Ref ref, Profile profile, String statusId) async {
  ref.cacheFor(_cacheDuration);
  _resharesLogger.fine('Creating provider for $statusId for Profile $profile');

  final resharesResult =
      await ref.read(resharesClientProvider(profile, statusId).future);
  _resharesLogger.fine('Values received for $statusId for Profile $profile');

  return resharesResult;
}
