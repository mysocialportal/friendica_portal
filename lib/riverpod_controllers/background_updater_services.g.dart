// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'background_updater_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$backgroundUpdatersHash() =>
    r'6deeb64abc934750e4122d65193d04b9f2760231';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$BackgroundUpdaters extends BuildlessNotifier<Timer> {
  late final Profile profile;

  Timer build(
    Profile profile,
  );
}

/// See also [BackgroundUpdaters].
@ProviderFor(BackgroundUpdaters)
const backgroundUpdatersProvider = BackgroundUpdatersFamily();

/// See also [BackgroundUpdaters].
class BackgroundUpdatersFamily extends Family<Timer> {
  /// See also [BackgroundUpdaters].
  const BackgroundUpdatersFamily();

  /// See also [BackgroundUpdaters].
  BackgroundUpdatersProvider call(
    Profile profile,
  ) {
    return BackgroundUpdatersProvider(
      profile,
    );
  }

  @override
  BackgroundUpdatersProvider getProviderOverride(
    covariant BackgroundUpdatersProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'backgroundUpdatersProvider';
}

/// See also [BackgroundUpdaters].
class BackgroundUpdatersProvider
    extends NotifierProviderImpl<BackgroundUpdaters, Timer> {
  /// See also [BackgroundUpdaters].
  BackgroundUpdatersProvider(
    Profile profile,
  ) : this._internal(
          () => BackgroundUpdaters()..profile = profile,
          from: backgroundUpdatersProvider,
          name: r'backgroundUpdatersProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$backgroundUpdatersHash,
          dependencies: BackgroundUpdatersFamily._dependencies,
          allTransitiveDependencies:
              BackgroundUpdatersFamily._allTransitiveDependencies,
          profile: profile,
        );

  BackgroundUpdatersProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Timer runNotifierBuild(
    covariant BackgroundUpdaters notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(BackgroundUpdaters Function() create) {
    return ProviderOverride(
      origin: this,
      override: BackgroundUpdatersProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<BackgroundUpdaters, Timer> createElement() {
    return _BackgroundUpdatersProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is BackgroundUpdatersProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin BackgroundUpdatersRef on NotifierProviderRef<Timer> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _BackgroundUpdatersProviderElement
    extends NotifierProviderElement<BackgroundUpdaters, Timer>
    with BackgroundUpdatersRef {
  _BackgroundUpdatersProviderElement(super.provider);

  @override
  Profile get profile => (origin as BackgroundUpdatersProvider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
