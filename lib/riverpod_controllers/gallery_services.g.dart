// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gallery_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$galleryImageHash() => r'd4f8ac40d9832f84c80850f5ff7cbf80e735b561';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [galleryImage].
@ProviderFor(galleryImage)
const galleryImageProvider = GalleryImageFamily();

/// See also [galleryImage].
class GalleryImageFamily
    extends Family<AsyncValue<Result<ImageEntry, ExecError>>> {
  /// See also [galleryImage].
  const GalleryImageFamily();

  /// See also [galleryImage].
  GalleryImageProvider call(
    Profile profile,
    String galleryName,
    String id,
  ) {
    return GalleryImageProvider(
      profile,
      galleryName,
      id,
    );
  }

  @override
  GalleryImageProvider getProviderOverride(
    covariant GalleryImageProvider provider,
  ) {
    return call(
      provider.profile,
      provider.galleryName,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'galleryImageProvider';
}

/// See also [galleryImage].
class GalleryImageProvider
    extends AutoDisposeFutureProvider<Result<ImageEntry, ExecError>> {
  /// See also [galleryImage].
  GalleryImageProvider(
    Profile profile,
    String galleryName,
    String id,
  ) : this._internal(
          (ref) => galleryImage(
            ref as GalleryImageRef,
            profile,
            galleryName,
            id,
          ),
          from: galleryImageProvider,
          name: r'galleryImageProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$galleryImageHash,
          dependencies: GalleryImageFamily._dependencies,
          allTransitiveDependencies:
              GalleryImageFamily._allTransitiveDependencies,
          profile: profile,
          galleryName: galleryName,
          id: id,
        );

  GalleryImageProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.galleryName,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String galleryName;
  final String id;

  @override
  Override overrideWith(
    FutureOr<Result<ImageEntry, ExecError>> Function(GalleryImageRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: GalleryImageProvider._internal(
        (ref) => create(ref as GalleryImageRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        galleryName: galleryName,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<ImageEntry, ExecError>>
      createElement() {
    return _GalleryImageProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GalleryImageProvider &&
        other.profile == profile &&
        other.galleryName == galleryName &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, galleryName.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin GalleryImageRef
    on AutoDisposeFutureProviderRef<Result<ImageEntry, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `galleryName` of this provider.
  String get galleryName;

  /// The parameter `id` of this provider.
  String get id;
}

class _GalleryImageProviderElement
    extends AutoDisposeFutureProviderElement<Result<ImageEntry, ExecError>>
    with GalleryImageRef {
  _GalleryImageProviderElement(super.provider);

  @override
  Profile get profile => (origin as GalleryImageProvider).profile;
  @override
  String get galleryName => (origin as GalleryImageProvider).galleryName;
  @override
  String get id => (origin as GalleryImageProvider).id;
}

String _$galleriesHash() => r'3d80f6e9d99fd9cd6fe5d4f72ba91849783ac7e1';

abstract class _$Galleries extends BuildlessAutoDisposeAsyncNotifier<
    Result<Map<String, GalleryData>, ExecError>> {
  late final Profile profile;

  FutureOr<Result<Map<String, GalleryData>, ExecError>> build(
    Profile profile,
  );
}

/// See also [_Galleries].
@ProviderFor(_Galleries)
const _galleriesProvider = _GalleriesFamily();

/// See also [_Galleries].
class _GalleriesFamily
    extends Family<AsyncValue<Result<Map<String, GalleryData>, ExecError>>> {
  /// See also [_Galleries].
  const _GalleriesFamily();

  /// See also [_Galleries].
  _GalleriesProvider call(
    Profile profile,
  ) {
    return _GalleriesProvider(
      profile,
    );
  }

  @override
  _GalleriesProvider getProviderOverride(
    covariant _GalleriesProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'_galleriesProvider';
}

/// See also [_Galleries].
class _GalleriesProvider extends AutoDisposeAsyncNotifierProviderImpl<
    _Galleries, Result<Map<String, GalleryData>, ExecError>> {
  /// See also [_Galleries].
  _GalleriesProvider(
    Profile profile,
  ) : this._internal(
          () => _Galleries()..profile = profile,
          from: _galleriesProvider,
          name: r'_galleriesProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$galleriesHash,
          dependencies: _GalleriesFamily._dependencies,
          allTransitiveDependencies:
              _GalleriesFamily._allTransitiveDependencies,
          profile: profile,
        );

  _GalleriesProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  FutureOr<Result<Map<String, GalleryData>, ExecError>> runNotifierBuild(
    covariant _Galleries notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(_Galleries Function() create) {
    return ProviderOverride(
      origin: this,
      override: _GalleriesProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<_Galleries,
      Result<Map<String, GalleryData>, ExecError>> createElement() {
    return _GalleriesProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is _GalleriesProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin _GalleriesRef on AutoDisposeAsyncNotifierProviderRef<
    Result<Map<String, GalleryData>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _GalleriesProviderElement extends AutoDisposeAsyncNotifierProviderElement<
    _Galleries,
    Result<Map<String, GalleryData>, ExecError>> with _GalleriesRef {
  _GalleriesProviderElement(super.provider);

  @override
  Profile get profile => (origin as _GalleriesProvider).profile;
}

String _$galleryListHash() => r'f396b7d4b7ffc6b06247d6ccf460f41fcf3fbceb';

abstract class _$GalleryList extends BuildlessAutoDisposeAsyncNotifier<
    Result<List<GalleryData>, ExecError>> {
  late final Profile profile;

  FutureOr<Result<List<GalleryData>, ExecError>> build(
    Profile profile,
  );
}

/// See also [GalleryList].
@ProviderFor(GalleryList)
const galleryListProvider = GalleryListFamily();

/// See also [GalleryList].
class GalleryListFamily
    extends Family<AsyncValue<Result<List<GalleryData>, ExecError>>> {
  /// See also [GalleryList].
  const GalleryListFamily();

  /// See also [GalleryList].
  GalleryListProvider call(
    Profile profile,
  ) {
    return GalleryListProvider(
      profile,
    );
  }

  @override
  GalleryListProvider getProviderOverride(
    covariant GalleryListProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'galleryListProvider';
}

/// See also [GalleryList].
class GalleryListProvider extends AutoDisposeAsyncNotifierProviderImpl<
    GalleryList, Result<List<GalleryData>, ExecError>> {
  /// See also [GalleryList].
  GalleryListProvider(
    Profile profile,
  ) : this._internal(
          () => GalleryList()..profile = profile,
          from: galleryListProvider,
          name: r'galleryListProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$galleryListHash,
          dependencies: GalleryListFamily._dependencies,
          allTransitiveDependencies:
              GalleryListFamily._allTransitiveDependencies,
          profile: profile,
        );

  GalleryListProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  FutureOr<Result<List<GalleryData>, ExecError>> runNotifierBuild(
    covariant GalleryList notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(GalleryList Function() create) {
    return ProviderOverride(
      origin: this,
      override: GalleryListProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<GalleryList,
      Result<List<GalleryData>, ExecError>> createElement() {
    return _GalleryListProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GalleryListProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin GalleryListRef on AutoDisposeAsyncNotifierProviderRef<
    Result<List<GalleryData>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _GalleryListProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<GalleryList,
        Result<List<GalleryData>, ExecError>> with GalleryListRef {
  _GalleryListProviderElement(super.provider);

  @override
  Profile get profile => (origin as GalleryListProvider).profile;
}

String _$galleryHash() => r'4d576f91d7b9d85a14abe4eab8a15d2acde8b0fc';

abstract class _$Gallery
    extends BuildlessAutoDisposeAsyncNotifier<Result<GalleryData, ExecError>> {
  late final Profile profile;
  late final String galleryName;

  FutureOr<Result<GalleryData, ExecError>> build(
    Profile profile,
    String galleryName,
  );
}

/// See also [Gallery].
@ProviderFor(Gallery)
const galleryProvider = GalleryFamily();

/// See also [Gallery].
class GalleryFamily extends Family<AsyncValue<Result<GalleryData, ExecError>>> {
  /// See also [Gallery].
  const GalleryFamily();

  /// See also [Gallery].
  GalleryProvider call(
    Profile profile,
    String galleryName,
  ) {
    return GalleryProvider(
      profile,
      galleryName,
    );
  }

  @override
  GalleryProvider getProviderOverride(
    covariant GalleryProvider provider,
  ) {
    return call(
      provider.profile,
      provider.galleryName,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'galleryProvider';
}

/// See also [Gallery].
class GalleryProvider extends AutoDisposeAsyncNotifierProviderImpl<Gallery,
    Result<GalleryData, ExecError>> {
  /// See also [Gallery].
  GalleryProvider(
    Profile profile,
    String galleryName,
  ) : this._internal(
          () => Gallery()
            ..profile = profile
            ..galleryName = galleryName,
          from: galleryProvider,
          name: r'galleryProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$galleryHash,
          dependencies: GalleryFamily._dependencies,
          allTransitiveDependencies: GalleryFamily._allTransitiveDependencies,
          profile: profile,
          galleryName: galleryName,
        );

  GalleryProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.galleryName,
  }) : super.internal();

  final Profile profile;
  final String galleryName;

  @override
  FutureOr<Result<GalleryData, ExecError>> runNotifierBuild(
    covariant Gallery notifier,
  ) {
    return notifier.build(
      profile,
      galleryName,
    );
  }

  @override
  Override overrideWith(Gallery Function() create) {
    return ProviderOverride(
      origin: this,
      override: GalleryProvider._internal(
        () => create()
          ..profile = profile
          ..galleryName = galleryName,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        galleryName: galleryName,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<Gallery,
      Result<GalleryData, ExecError>> createElement() {
    return _GalleryProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GalleryProvider &&
        other.profile == profile &&
        other.galleryName == galleryName;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, galleryName.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin GalleryRef
    on AutoDisposeAsyncNotifierProviderRef<Result<GalleryData, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `galleryName` of this provider.
  String get galleryName;
}

class _GalleryProviderElement extends AutoDisposeAsyncNotifierProviderElement<
    Gallery, Result<GalleryData, ExecError>> with GalleryRef {
  _GalleryProviderElement(super.provider);

  @override
  Profile get profile => (origin as GalleryProvider).profile;
  @override
  String get galleryName => (origin as GalleryProvider).galleryName;
}

String _$galleryImagesHash() => r'1614e0528f38368fc8fe4b2e4a095570653e0f08';

abstract class _$GalleryImages extends BuildlessAutoDisposeAsyncNotifier<
    Result<List<ImageEntry>, ExecError>> {
  late final Profile profile;
  late final String galleryName;

  FutureOr<Result<List<ImageEntry>, ExecError>> build(
    Profile profile,
    String galleryName,
  );
}

/// See also [GalleryImages].
@ProviderFor(GalleryImages)
const galleryImagesProvider = GalleryImagesFamily();

/// See also [GalleryImages].
class GalleryImagesFamily
    extends Family<AsyncValue<Result<List<ImageEntry>, ExecError>>> {
  /// See also [GalleryImages].
  const GalleryImagesFamily();

  /// See also [GalleryImages].
  GalleryImagesProvider call(
    Profile profile,
    String galleryName,
  ) {
    return GalleryImagesProvider(
      profile,
      galleryName,
    );
  }

  @override
  GalleryImagesProvider getProviderOverride(
    covariant GalleryImagesProvider provider,
  ) {
    return call(
      provider.profile,
      provider.galleryName,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'galleryImagesProvider';
}

/// See also [GalleryImages].
class GalleryImagesProvider extends AutoDisposeAsyncNotifierProviderImpl<
    GalleryImages, Result<List<ImageEntry>, ExecError>> {
  /// See also [GalleryImages].
  GalleryImagesProvider(
    Profile profile,
    String galleryName,
  ) : this._internal(
          () => GalleryImages()
            ..profile = profile
            ..galleryName = galleryName,
          from: galleryImagesProvider,
          name: r'galleryImagesProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$galleryImagesHash,
          dependencies: GalleryImagesFamily._dependencies,
          allTransitiveDependencies:
              GalleryImagesFamily._allTransitiveDependencies,
          profile: profile,
          galleryName: galleryName,
        );

  GalleryImagesProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.galleryName,
  }) : super.internal();

  final Profile profile;
  final String galleryName;

  @override
  FutureOr<Result<List<ImageEntry>, ExecError>> runNotifierBuild(
    covariant GalleryImages notifier,
  ) {
    return notifier.build(
      profile,
      galleryName,
    );
  }

  @override
  Override overrideWith(GalleryImages Function() create) {
    return ProviderOverride(
      origin: this,
      override: GalleryImagesProvider._internal(
        () => create()
          ..profile = profile
          ..galleryName = galleryName,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        galleryName: galleryName,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<GalleryImages,
      Result<List<ImageEntry>, ExecError>> createElement() {
    return _GalleryImagesProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GalleryImagesProvider &&
        other.profile == profile &&
        other.galleryName == galleryName;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, galleryName.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin GalleryImagesRef on AutoDisposeAsyncNotifierProviderRef<
    Result<List<ImageEntry>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `galleryName` of this provider.
  String get galleryName;
}

class _GalleryImagesProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<GalleryImages,
        Result<List<ImageEntry>, ExecError>> with GalleryImagesRef {
  _GalleryImagesProviderElement(super.provider);

  @override
  Profile get profile => (origin as GalleryImagesProvider).profile;
  @override
  String get galleryName => (origin as GalleryImagesProvider).galleryName;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
