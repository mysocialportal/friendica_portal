import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:stack_trace/stack_trace.dart';

import '../models/auth/profile.dart';
import '../models/exec_error.dart';
import '../models/friendica_version.dart';
import '../models/friendica_version_requirement.dart';
import 'instance_info_services.dart';

part 'feature_checker_services.g.dart';

enum RelaticaFeatures {
  blueskyReshare('Resharing Bluesky Posts'),
  diasporaReshare('Resharing Diaspora Posts'),
  directMessageCreation('Direct message creation with OAuth login'),
  postSpoilerText('Spoiler Text on Posts'),
  reshareIdFix('Reshare ID fix'),
  statusEditing('Post/Comment Editing'),
  usingActualFollowRequests(
      'Using Follow Request System not Friend Request Notifications'),
  ;

  final String errorLabel;

  const RelaticaFeatures(this.errorLabel);

  String get label => errorLabel;
}

final _featureVersionRequirement =
    <RelaticaFeatures, FriendicaVersionRequirement>{
  RelaticaFeatures.blueskyReshare: FriendicaVersionRequirement(v2024_08),
  RelaticaFeatures.diasporaReshare: FriendicaVersionRequirement(v2023_04),
  RelaticaFeatures.directMessageCreation: FriendicaVersionRequirement(
    v2023_04,
  ),
  RelaticaFeatures.postSpoilerText: FriendicaVersionRequirement(v2023_04),
  RelaticaFeatures.statusEditing: FriendicaVersionRequirement(v2023_04),
  RelaticaFeatures.usingActualFollowRequests: FriendicaVersionRequirement(
    v2023_04,
  ),
};

final _logger = Logger('FriendicaVersionChecker');

@riverpod
FriendicaVersionRequirement versionRequirement(
    Ref ref, RelaticaFeatures feature) {
  return _featureVersionRequirement[feature] ?? unknownRequirement;
}

@riverpod
String versionErrorString(Ref ref, RelaticaFeatures feature) {
  final requirement = ref.watch(versionRequirementProvider(feature));
  return "${feature.label} $requirement";
}

@riverpod
bool featureCheck(Ref ref, Profile profile, RelaticaFeatures feature) {
  final requirement = _featureVersionRequirement[feature];
  if (requirement == null) {
    _logger.severe(
      'Return false since no minimum version data in table for: $feature',
      Trace.current(),
    );
    return false;
  }

  final info = ref.read(instanceInfoManagerProvider(profile));
  return requirement.versionMeetsRequirement(info.friendicaVersion);
}

@riverpod
Result<bool, ExecError> featureCheckResult(
    Ref ref, Profile profile, RelaticaFeatures feature) {
  if (ref.watch(featureCheckProvider(profile, feature))) {
    return Result.ok(true);
  }

  return buildErrorResult(
    type: ErrorType.minVersionError,
    message: ref.read(versionErrorStringProvider(feature)),
  );
}
