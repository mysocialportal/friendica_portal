import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../models/auth/profile.dart';
import '../../models/timeline_identifiers.dart';

part 'network_status_services.g.dart';

//TODO Confirm logic works, where there is always a corresponding network call so that even if the UI creates it first it will probably turn false after a network attempt is completed
@riverpod
class PostCommentLoadingStatus extends _$PostCommentLoadingStatus {
  @override
  bool build(Profile profile, String id) {
    return false;
  }

  void begin() => state = true;

  void end() => state = false;
}

@riverpod
class TimelineLoadingStatus extends _$TimelineLoadingStatus {
  @override
  bool build(Profile profile, TimelineIdentifiers type) {
    return false;
  }

  void begin() => state = true;

  void end() => state = false;
}

@riverpod
class SearchLoadingStatus extends _$SearchLoadingStatus {
  @override
  bool build(Profile profile) {
    return false;
  }

  void begin() => state = true;

  void end() => state = false;
}

@riverpod
class DirectMessageLoading extends _$DirectMessageLoading {
  @override
  bool build(Profile profile) {
    return false;
  }

  void begin() => state = true;

  void end() => state = false;
}

@riverpod
class NotificationsLoading extends _$NotificationsLoading {
  @override
  bool build(Profile profile) {
    return false;
  }

  void begin() => state = true;

  void end() => state = false;
}

@riverpod
class ConnectionsLoading extends _$ConnectionsLoading {
  @override
  bool build(Profile profile) {
    return false;
  }

  void begin() => state = true;

  void end() => state = false;
}

@riverpod
class FollowRequestsLoading extends _$FollowRequestsLoading {
  @override
  bool build(Profile profile) {
    return false;
  }

  void begin() => state = true;

  void end() => state = false;
}
