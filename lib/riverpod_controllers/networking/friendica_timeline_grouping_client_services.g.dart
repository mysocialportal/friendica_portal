// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_timeline_grouping_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$timelineGroupingListDataClientHash() =>
    r'aa27329df4c5ed2fe0b390ccd2a643c5180113a7';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [timelineGroupingListDataClient].
@ProviderFor(timelineGroupingListDataClient)
const timelineGroupingListDataClientProvider =
    TimelineGroupingListDataClientFamily();

/// See also [timelineGroupingListDataClient].
class TimelineGroupingListDataClientFamily extends Family<
    AsyncValue<Result<List<TimelineGroupingListData>, ExecError>>> {
  /// See also [timelineGroupingListDataClient].
  const TimelineGroupingListDataClientFamily();

  /// See also [timelineGroupingListDataClient].
  TimelineGroupingListDataClientProvider call(
    Profile profile,
  ) {
    return TimelineGroupingListDataClientProvider(
      profile,
    );
  }

  @override
  TimelineGroupingListDataClientProvider getProviderOverride(
    covariant TimelineGroupingListDataClientProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'timelineGroupingListDataClientProvider';
}

/// See also [timelineGroupingListDataClient].
class TimelineGroupingListDataClientProvider extends AutoDisposeFutureProvider<
    Result<List<TimelineGroupingListData>, ExecError>> {
  /// See also [timelineGroupingListDataClient].
  TimelineGroupingListDataClientProvider(
    Profile profile,
  ) : this._internal(
          (ref) => timelineGroupingListDataClient(
            ref as TimelineGroupingListDataClientRef,
            profile,
          ),
          from: timelineGroupingListDataClientProvider,
          name: r'timelineGroupingListDataClientProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$timelineGroupingListDataClientHash,
          dependencies: TimelineGroupingListDataClientFamily._dependencies,
          allTransitiveDependencies:
              TimelineGroupingListDataClientFamily._allTransitiveDependencies,
          profile: profile,
        );

  TimelineGroupingListDataClientProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<Result<List<TimelineGroupingListData>, ExecError>> Function(
            TimelineGroupingListDataClientRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: TimelineGroupingListDataClientProvider._internal(
        (ref) => create(ref as TimelineGroupingListDataClientRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
      Result<List<TimelineGroupingListData>, ExecError>> createElement() {
    return _TimelineGroupingListDataClientProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TimelineGroupingListDataClientProvider &&
        other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin TimelineGroupingListDataClientRef on AutoDisposeFutureProviderRef<
    Result<List<TimelineGroupingListData>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _TimelineGroupingListDataClientProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<List<TimelineGroupingListData>, ExecError>>
    with TimelineGroupingListDataClientRef {
  _TimelineGroupingListDataClientProviderElement(super.provider);

  @override
  Profile get profile =>
      (origin as TimelineGroupingListDataClientProvider).profile;
}

String _$circleMembersHash() => r'37d8fa5e94fa11081ed72598203b071d74706c2d';

/// See also [circleMembers].
@ProviderFor(circleMembers)
const circleMembersProvider = CircleMembersFamily();

/// See also [circleMembers].
class CircleMembersFamily extends Family<
    AsyncValue<Result<PagedResponse<List<Connection>>, ExecError>>> {
  /// See also [circleMembers].
  const CircleMembersFamily();

  /// See also [circleMembers].
  CircleMembersProvider call(
    Profile profile,
    TimelineGroupingListData circleData,
    PagingData page,
  ) {
    return CircleMembersProvider(
      profile,
      circleData,
      page,
    );
  }

  @override
  CircleMembersProvider getProviderOverride(
    covariant CircleMembersProvider provider,
  ) {
    return call(
      provider.profile,
      provider.circleData,
      provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'circleMembersProvider';
}

/// See also [circleMembers].
class CircleMembersProvider extends AutoDisposeFutureProvider<
    Result<PagedResponse<List<Connection>>, ExecError>> {
  /// See also [circleMembers].
  CircleMembersProvider(
    Profile profile,
    TimelineGroupingListData circleData,
    PagingData page,
  ) : this._internal(
          (ref) => circleMembers(
            ref as CircleMembersRef,
            profile,
            circleData,
            page,
          ),
          from: circleMembersProvider,
          name: r'circleMembersProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$circleMembersHash,
          dependencies: CircleMembersFamily._dependencies,
          allTransitiveDependencies:
              CircleMembersFamily._allTransitiveDependencies,
          profile: profile,
          circleData: circleData,
          page: page,
        );

  CircleMembersProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.circleData,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final TimelineGroupingListData circleData;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<PagedResponse<List<Connection>>, ExecError>> Function(
            CircleMembersRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: CircleMembersProvider._internal(
        (ref) => create(ref as CircleMembersRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        circleData: circleData,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
      Result<PagedResponse<List<Connection>>, ExecError>> createElement() {
    return _CircleMembersProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CircleMembersProvider &&
        other.profile == profile &&
        other.circleData == circleData &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, circleData.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin CircleMembersRef on AutoDisposeFutureProviderRef<
    Result<PagedResponse<List<Connection>>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `circleData` of this provider.
  TimelineGroupingListData get circleData;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _CircleMembersProviderElement extends AutoDisposeFutureProviderElement<
    Result<PagedResponse<List<Connection>>, ExecError>> with CircleMembersRef {
  _CircleMembersProviderElement(super.provider);

  @override
  Profile get profile => (origin as CircleMembersProvider).profile;
  @override
  TimelineGroupingListData get circleData =>
      (origin as CircleMembersProvider).circleData;
  @override
  PagingData get page => (origin as CircleMembersProvider).page;
}

String _$createCircleHash() => r'110c10b52856b1f856e675c96d5c66ff0e8ff000';

/// See also [createCircle].
@ProviderFor(createCircle)
const createCircleProvider = CreateCircleFamily();

/// See also [createCircle].
class CreateCircleFamily
    extends Family<AsyncValue<Result<TimelineGroupingListData, ExecError>>> {
  /// See also [createCircle].
  const CreateCircleFamily();

  /// See also [createCircle].
  CreateCircleProvider call(
    Profile profile,
    String title,
  ) {
    return CreateCircleProvider(
      profile,
      title,
    );
  }

  @override
  CreateCircleProvider getProviderOverride(
    covariant CreateCircleProvider provider,
  ) {
    return call(
      provider.profile,
      provider.title,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'createCircleProvider';
}

/// See also [createCircle].
class CreateCircleProvider extends AutoDisposeFutureProvider<
    Result<TimelineGroupingListData, ExecError>> {
  /// See also [createCircle].
  CreateCircleProvider(
    Profile profile,
    String title,
  ) : this._internal(
          (ref) => createCircle(
            ref as CreateCircleRef,
            profile,
            title,
          ),
          from: createCircleProvider,
          name: r'createCircleProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$createCircleHash,
          dependencies: CreateCircleFamily._dependencies,
          allTransitiveDependencies:
              CreateCircleFamily._allTransitiveDependencies,
          profile: profile,
          title: title,
        );

  CreateCircleProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.title,
  }) : super.internal();

  final Profile profile;
  final String title;

  @override
  Override overrideWith(
    FutureOr<Result<TimelineGroupingListData, ExecError>> Function(
            CreateCircleRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: CreateCircleProvider._internal(
        (ref) => create(ref as CreateCircleRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        title: title,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<TimelineGroupingListData, ExecError>>
      createElement() {
    return _CreateCircleProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CreateCircleProvider &&
        other.profile == profile &&
        other.title == title;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, title.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin CreateCircleRef on AutoDisposeFutureProviderRef<
    Result<TimelineGroupingListData, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `title` of this provider.
  String get title;
}

class _CreateCircleProviderElement extends AutoDisposeFutureProviderElement<
    Result<TimelineGroupingListData, ExecError>> with CreateCircleRef {
  _CreateCircleProviderElement(super.provider);

  @override
  Profile get profile => (origin as CreateCircleProvider).profile;
  @override
  String get title => (origin as CreateCircleProvider).title;
}

String _$renameCircleHash() => r'4bb3b90fc0dc47b59a1154e55857ec21b98fa622';

/// See also [renameCircle].
@ProviderFor(renameCircle)
const renameCircleProvider = RenameCircleFamily();

/// See also [renameCircle].
class RenameCircleFamily
    extends Family<AsyncValue<Result<TimelineGroupingListData, ExecError>>> {
  /// See also [renameCircle].
  const RenameCircleFamily();

  /// See also [renameCircle].
  RenameCircleProvider call(
    Profile profile,
    String id,
    String title,
  ) {
    return RenameCircleProvider(
      profile,
      id,
      title,
    );
  }

  @override
  RenameCircleProvider getProviderOverride(
    covariant RenameCircleProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
      provider.title,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'renameCircleProvider';
}

/// See also [renameCircle].
class RenameCircleProvider extends AutoDisposeFutureProvider<
    Result<TimelineGroupingListData, ExecError>> {
  /// See also [renameCircle].
  RenameCircleProvider(
    Profile profile,
    String id,
    String title,
  ) : this._internal(
          (ref) => renameCircle(
            ref as RenameCircleRef,
            profile,
            id,
            title,
          ),
          from: renameCircleProvider,
          name: r'renameCircleProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$renameCircleHash,
          dependencies: RenameCircleFamily._dependencies,
          allTransitiveDependencies:
              RenameCircleFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
          title: title,
        );

  RenameCircleProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
    required this.title,
  }) : super.internal();

  final Profile profile;
  final String id;
  final String title;

  @override
  Override overrideWith(
    FutureOr<Result<TimelineGroupingListData, ExecError>> Function(
            RenameCircleRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: RenameCircleProvider._internal(
        (ref) => create(ref as RenameCircleRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
        title: title,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<TimelineGroupingListData, ExecError>>
      createElement() {
    return _RenameCircleProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is RenameCircleProvider &&
        other.profile == profile &&
        other.id == id &&
        other.title == title;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);
    hash = _SystemHash.combine(hash, title.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin RenameCircleRef on AutoDisposeFutureProviderRef<
    Result<TimelineGroupingListData, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;

  /// The parameter `title` of this provider.
  String get title;
}

class _RenameCircleProviderElement extends AutoDisposeFutureProviderElement<
    Result<TimelineGroupingListData, ExecError>> with RenameCircleRef {
  _RenameCircleProviderElement(super.provider);

  @override
  Profile get profile => (origin as RenameCircleProvider).profile;
  @override
  String get id => (origin as RenameCircleProvider).id;
  @override
  String get title => (origin as RenameCircleProvider).title;
}

String _$deleteCircleHash() => r'025301a7c086968584382489b1dfde42420cc206';

/// See also [deleteCircle].
@ProviderFor(deleteCircle)
const deleteCircleProvider = DeleteCircleFamily();

/// See also [deleteCircle].
class DeleteCircleFamily extends Family<AsyncValue<Result<bool, ExecError>>> {
  /// See also [deleteCircle].
  const DeleteCircleFamily();

  /// See also [deleteCircle].
  DeleteCircleProvider call(
    Profile profile,
    TimelineGroupingListData circleData,
  ) {
    return DeleteCircleProvider(
      profile,
      circleData,
    );
  }

  @override
  DeleteCircleProvider getProviderOverride(
    covariant DeleteCircleProvider provider,
  ) {
    return call(
      provider.profile,
      provider.circleData,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'deleteCircleProvider';
}

/// See also [deleteCircle].
class DeleteCircleProvider
    extends AutoDisposeFutureProvider<Result<bool, ExecError>> {
  /// See also [deleteCircle].
  DeleteCircleProvider(
    Profile profile,
    TimelineGroupingListData circleData,
  ) : this._internal(
          (ref) => deleteCircle(
            ref as DeleteCircleRef,
            profile,
            circleData,
          ),
          from: deleteCircleProvider,
          name: r'deleteCircleProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$deleteCircleHash,
          dependencies: DeleteCircleFamily._dependencies,
          allTransitiveDependencies:
              DeleteCircleFamily._allTransitiveDependencies,
          profile: profile,
          circleData: circleData,
        );

  DeleteCircleProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.circleData,
  }) : super.internal();

  final Profile profile;
  final TimelineGroupingListData circleData;

  @override
  Override overrideWith(
    FutureOr<Result<bool, ExecError>> Function(DeleteCircleRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: DeleteCircleProvider._internal(
        (ref) => create(ref as DeleteCircleRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        circleData: circleData,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<bool, ExecError>> createElement() {
    return _DeleteCircleProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is DeleteCircleProvider &&
        other.profile == profile &&
        other.circleData == circleData;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, circleData.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin DeleteCircleRef on AutoDisposeFutureProviderRef<Result<bool, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `circleData` of this provider.
  TimelineGroupingListData get circleData;
}

class _DeleteCircleProviderElement
    extends AutoDisposeFutureProviderElement<Result<bool, ExecError>>
    with DeleteCircleRef {
  _DeleteCircleProviderElement(super.provider);

  @override
  Profile get profile => (origin as DeleteCircleProvider).profile;
  @override
  TimelineGroupingListData get circleData =>
      (origin as DeleteCircleProvider).circleData;
}

String _$memberCirclesForConnectionHash() =>
    r'cd3bec39fa82d9fabf640808fe38070e0138fc4f';

/// See also [memberCirclesForConnection].
@ProviderFor(memberCirclesForConnection)
const memberCirclesForConnectionProvider = MemberCirclesForConnectionFamily();

/// See also [memberCirclesForConnection].
class MemberCirclesForConnectionFamily extends Family<
    AsyncValue<Result<List<TimelineGroupingListData>, ExecError>>> {
  /// See also [memberCirclesForConnection].
  const MemberCirclesForConnectionFamily();

  /// See also [memberCirclesForConnection].
  MemberCirclesForConnectionProvider call(
    Profile profile,
    String connectionId,
  ) {
    return MemberCirclesForConnectionProvider(
      profile,
      connectionId,
    );
  }

  @override
  MemberCirclesForConnectionProvider getProviderOverride(
    covariant MemberCirclesForConnectionProvider provider,
  ) {
    return call(
      provider.profile,
      provider.connectionId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'memberCirclesForConnectionProvider';
}

/// See also [memberCirclesForConnection].
class MemberCirclesForConnectionProvider extends AutoDisposeFutureProvider<
    Result<List<TimelineGroupingListData>, ExecError>> {
  /// See also [memberCirclesForConnection].
  MemberCirclesForConnectionProvider(
    Profile profile,
    String connectionId,
  ) : this._internal(
          (ref) => memberCirclesForConnection(
            ref as MemberCirclesForConnectionRef,
            profile,
            connectionId,
          ),
          from: memberCirclesForConnectionProvider,
          name: r'memberCirclesForConnectionProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$memberCirclesForConnectionHash,
          dependencies: MemberCirclesForConnectionFamily._dependencies,
          allTransitiveDependencies:
              MemberCirclesForConnectionFamily._allTransitiveDependencies,
          profile: profile,
          connectionId: connectionId,
        );

  MemberCirclesForConnectionProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.connectionId,
  }) : super.internal();

  final Profile profile;
  final String connectionId;

  @override
  Override overrideWith(
    FutureOr<Result<List<TimelineGroupingListData>, ExecError>> Function(
            MemberCirclesForConnectionRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: MemberCirclesForConnectionProvider._internal(
        (ref) => create(ref as MemberCirclesForConnectionRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        connectionId: connectionId,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
      Result<List<TimelineGroupingListData>, ExecError>> createElement() {
    return _MemberCirclesForConnectionProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is MemberCirclesForConnectionProvider &&
        other.profile == profile &&
        other.connectionId == connectionId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, connectionId.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin MemberCirclesForConnectionRef on AutoDisposeFutureProviderRef<
    Result<List<TimelineGroupingListData>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `connectionId` of this provider.
  String get connectionId;
}

class _MemberCirclesForConnectionProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<List<TimelineGroupingListData>, ExecError>>
    with MemberCirclesForConnectionRef {
  _MemberCirclesForConnectionProviderElement(super.provider);

  @override
  Profile get profile => (origin as MemberCirclesForConnectionProvider).profile;
  @override
  String get connectionId =>
      (origin as MemberCirclesForConnectionProvider).connectionId;
}

String _$addConnectionToCircleHash() =>
    r'82d9b0a10c90a727c6a4f29f498a62baabf2171f';

/// See also [addConnectionToCircle].
@ProviderFor(addConnectionToCircle)
const addConnectionToCircleProvider = AddConnectionToCircleFamily();

/// See also [addConnectionToCircle].
class AddConnectionToCircleFamily
    extends Family<AsyncValue<Result<bool, ExecError>>> {
  /// See also [addConnectionToCircle].
  const AddConnectionToCircleFamily();

  /// See also [addConnectionToCircle].
  AddConnectionToCircleProvider call(
    Profile profile,
    TimelineGroupingListData circle,
    Connection connection,
  ) {
    return AddConnectionToCircleProvider(
      profile,
      circle,
      connection,
    );
  }

  @override
  AddConnectionToCircleProvider getProviderOverride(
    covariant AddConnectionToCircleProvider provider,
  ) {
    return call(
      provider.profile,
      provider.circle,
      provider.connection,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'addConnectionToCircleProvider';
}

/// See also [addConnectionToCircle].
class AddConnectionToCircleProvider
    extends AutoDisposeFutureProvider<Result<bool, ExecError>> {
  /// See also [addConnectionToCircle].
  AddConnectionToCircleProvider(
    Profile profile,
    TimelineGroupingListData circle,
    Connection connection,
  ) : this._internal(
          (ref) => addConnectionToCircle(
            ref as AddConnectionToCircleRef,
            profile,
            circle,
            connection,
          ),
          from: addConnectionToCircleProvider,
          name: r'addConnectionToCircleProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$addConnectionToCircleHash,
          dependencies: AddConnectionToCircleFamily._dependencies,
          allTransitiveDependencies:
              AddConnectionToCircleFamily._allTransitiveDependencies,
          profile: profile,
          circle: circle,
          connection: connection,
        );

  AddConnectionToCircleProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.circle,
    required this.connection,
  }) : super.internal();

  final Profile profile;
  final TimelineGroupingListData circle;
  final Connection connection;

  @override
  Override overrideWith(
    FutureOr<Result<bool, ExecError>> Function(
            AddConnectionToCircleRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: AddConnectionToCircleProvider._internal(
        (ref) => create(ref as AddConnectionToCircleRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        circle: circle,
        connection: connection,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<bool, ExecError>> createElement() {
    return _AddConnectionToCircleProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is AddConnectionToCircleProvider &&
        other.profile == profile &&
        other.circle == circle &&
        other.connection == connection;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, circle.hashCode);
    hash = _SystemHash.combine(hash, connection.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin AddConnectionToCircleRef
    on AutoDisposeFutureProviderRef<Result<bool, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `circle` of this provider.
  TimelineGroupingListData get circle;

  /// The parameter `connection` of this provider.
  Connection get connection;
}

class _AddConnectionToCircleProviderElement
    extends AutoDisposeFutureProviderElement<Result<bool, ExecError>>
    with AddConnectionToCircleRef {
  _AddConnectionToCircleProviderElement(super.provider);

  @override
  Profile get profile => (origin as AddConnectionToCircleProvider).profile;
  @override
  TimelineGroupingListData get circle =>
      (origin as AddConnectionToCircleProvider).circle;
  @override
  Connection get connection =>
      (origin as AddConnectionToCircleProvider).connection;
}

String _$removeConnectionFromCircleHash() =>
    r'b47d07b08a00651e9f151f32001544e90223232a';

/// See also [removeConnectionFromCircle].
@ProviderFor(removeConnectionFromCircle)
const removeConnectionFromCircleProvider = RemoveConnectionFromCircleFamily();

/// See also [removeConnectionFromCircle].
class RemoveConnectionFromCircleFamily
    extends Family<AsyncValue<Result<bool, ExecError>>> {
  /// See also [removeConnectionFromCircle].
  const RemoveConnectionFromCircleFamily();

  /// See also [removeConnectionFromCircle].
  RemoveConnectionFromCircleProvider call(
    Profile profile,
    TimelineGroupingListData circle,
    Connection connection,
  ) {
    return RemoveConnectionFromCircleProvider(
      profile,
      circle,
      connection,
    );
  }

  @override
  RemoveConnectionFromCircleProvider getProviderOverride(
    covariant RemoveConnectionFromCircleProvider provider,
  ) {
    return call(
      provider.profile,
      provider.circle,
      provider.connection,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'removeConnectionFromCircleProvider';
}

/// See also [removeConnectionFromCircle].
class RemoveConnectionFromCircleProvider
    extends AutoDisposeFutureProvider<Result<bool, ExecError>> {
  /// See also [removeConnectionFromCircle].
  RemoveConnectionFromCircleProvider(
    Profile profile,
    TimelineGroupingListData circle,
    Connection connection,
  ) : this._internal(
          (ref) => removeConnectionFromCircle(
            ref as RemoveConnectionFromCircleRef,
            profile,
            circle,
            connection,
          ),
          from: removeConnectionFromCircleProvider,
          name: r'removeConnectionFromCircleProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$removeConnectionFromCircleHash,
          dependencies: RemoveConnectionFromCircleFamily._dependencies,
          allTransitiveDependencies:
              RemoveConnectionFromCircleFamily._allTransitiveDependencies,
          profile: profile,
          circle: circle,
          connection: connection,
        );

  RemoveConnectionFromCircleProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.circle,
    required this.connection,
  }) : super.internal();

  final Profile profile;
  final TimelineGroupingListData circle;
  final Connection connection;

  @override
  Override overrideWith(
    FutureOr<Result<bool, ExecError>> Function(
            RemoveConnectionFromCircleRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: RemoveConnectionFromCircleProvider._internal(
        (ref) => create(ref as RemoveConnectionFromCircleRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        circle: circle,
        connection: connection,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<bool, ExecError>> createElement() {
    return _RemoveConnectionFromCircleProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is RemoveConnectionFromCircleProvider &&
        other.profile == profile &&
        other.circle == circle &&
        other.connection == connection;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, circle.hashCode);
    hash = _SystemHash.combine(hash, connection.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin RemoveConnectionFromCircleRef
    on AutoDisposeFutureProviderRef<Result<bool, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `circle` of this provider.
  TimelineGroupingListData get circle;

  /// The parameter `connection` of this provider.
  Connection get connection;
}

class _RemoveConnectionFromCircleProviderElement
    extends AutoDisposeFutureProviderElement<Result<bool, ExecError>>
    with RemoveConnectionFromCircleRef {
  _RemoveConnectionFromCircleProviderElement(super.provider);

  @override
  Profile get profile => (origin as RemoveConnectionFromCircleProvider).profile;
  @override
  TimelineGroupingListData get circle =>
      (origin as RemoveConnectionFromCircleProvider).circle;
  @override
  Connection get connection =>
      (origin as RemoveConnectionFromCircleProvider).connection;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
