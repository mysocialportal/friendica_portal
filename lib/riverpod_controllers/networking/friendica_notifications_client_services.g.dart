// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_notifications_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$notificationsClientHash() =>
    r'258b9209dfdb02928adf9c346fea78d725881361';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [notificationsClient].
@ProviderFor(notificationsClient)
const notificationsClientProvider = NotificationsClientFamily();

/// See also [notificationsClient].
class NotificationsClientFamily extends Family<
    AsyncValue<Result<PagedResponse<List<UserNotification>>, ExecError>>> {
  /// See also [notificationsClient].
  const NotificationsClientFamily();

  /// See also [notificationsClient].
  NotificationsClientProvider call(
    Profile profile,
    PagingData page,
    bool includeAll,
  ) {
    return NotificationsClientProvider(
      profile,
      page,
      includeAll,
    );
  }

  @override
  NotificationsClientProvider getProviderOverride(
    covariant NotificationsClientProvider provider,
  ) {
    return call(
      provider.profile,
      provider.page,
      provider.includeAll,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'notificationsClientProvider';
}

/// See also [notificationsClient].
class NotificationsClientProvider extends AutoDisposeFutureProvider<
    Result<PagedResponse<List<UserNotification>>, ExecError>> {
  /// See also [notificationsClient].
  NotificationsClientProvider(
    Profile profile,
    PagingData page,
    bool includeAll,
  ) : this._internal(
          (ref) => notificationsClient(
            ref as NotificationsClientRef,
            profile,
            page,
            includeAll,
          ),
          from: notificationsClientProvider,
          name: r'notificationsClientProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$notificationsClientHash,
          dependencies: NotificationsClientFamily._dependencies,
          allTransitiveDependencies:
              NotificationsClientFamily._allTransitiveDependencies,
          profile: profile,
          page: page,
          includeAll: includeAll,
        );

  NotificationsClientProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.page,
    required this.includeAll,
  }) : super.internal();

  final Profile profile;
  final PagingData page;
  final bool includeAll;

  @override
  Override overrideWith(
    FutureOr<Result<PagedResponse<List<UserNotification>>, ExecError>> Function(
            NotificationsClientRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: NotificationsClientProvider._internal(
        (ref) => create(ref as NotificationsClientRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        page: page,
        includeAll: includeAll,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
          Result<PagedResponse<List<UserNotification>>, ExecError>>
      createElement() {
    return _NotificationsClientProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is NotificationsClientProvider &&
        other.profile == profile &&
        other.page == page &&
        other.includeAll == includeAll;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);
    hash = _SystemHash.combine(hash, includeAll.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin NotificationsClientRef on AutoDisposeFutureProviderRef<
    Result<PagedResponse<List<UserNotification>>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `page` of this provider.
  PagingData get page;

  /// The parameter `includeAll` of this provider.
  bool get includeAll;
}

class _NotificationsClientProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<PagedResponse<List<UserNotification>>, ExecError>>
    with NotificationsClientRef {
  _NotificationsClientProviderElement(super.provider);

  @override
  Profile get profile => (origin as NotificationsClientProvider).profile;
  @override
  PagingData get page => (origin as NotificationsClientProvider).page;
  @override
  bool get includeAll => (origin as NotificationsClientProvider).includeAll;
}

String _$clearNotificationsHash() =>
    r'733ebc6d9ffc940a6da41db9002794da7fb56d98';

/// See also [clearNotifications].
@ProviderFor(clearNotifications)
const clearNotificationsProvider = ClearNotificationsFamily();

/// See also [clearNotifications].
class ClearNotificationsFamily
    extends Family<AsyncValue<Result<bool, ExecError>>> {
  /// See also [clearNotifications].
  const ClearNotificationsFamily();

  /// See also [clearNotifications].
  ClearNotificationsProvider call(
    Profile profile,
  ) {
    return ClearNotificationsProvider(
      profile,
    );
  }

  @override
  ClearNotificationsProvider getProviderOverride(
    covariant ClearNotificationsProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'clearNotificationsProvider';
}

/// See also [clearNotifications].
class ClearNotificationsProvider
    extends AutoDisposeFutureProvider<Result<bool, ExecError>> {
  /// See also [clearNotifications].
  ClearNotificationsProvider(
    Profile profile,
  ) : this._internal(
          (ref) => clearNotifications(
            ref as ClearNotificationsRef,
            profile,
          ),
          from: clearNotificationsProvider,
          name: r'clearNotificationsProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$clearNotificationsHash,
          dependencies: ClearNotificationsFamily._dependencies,
          allTransitiveDependencies:
              ClearNotificationsFamily._allTransitiveDependencies,
          profile: profile,
        );

  ClearNotificationsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<Result<bool, ExecError>> Function(ClearNotificationsRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ClearNotificationsProvider._internal(
        (ref) => create(ref as ClearNotificationsRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<bool, ExecError>> createElement() {
    return _ClearNotificationsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ClearNotificationsProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ClearNotificationsRef
    on AutoDisposeFutureProviderRef<Result<bool, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _ClearNotificationsProviderElement
    extends AutoDisposeFutureProviderElement<Result<bool, ExecError>>
    with ClearNotificationsRef {
  _ClearNotificationsProviderElement(super.provider);

  @override
  Profile get profile => (origin as ClearNotificationsProvider).profile;
}

String _$clearNotificationHash() => r'171d9ecddc12418e920d55c82c0c5b06f35e62e0';

/// See also [clearNotification].
@ProviderFor(clearNotification)
const clearNotificationProvider = ClearNotificationFamily();

/// See also [clearNotification].
class ClearNotificationFamily
    extends Family<AsyncValue<Result<bool, ExecError>>> {
  /// See also [clearNotification].
  const ClearNotificationFamily();

  /// See also [clearNotification].
  ClearNotificationProvider call(
    Profile profile,
    UserNotification notification,
  ) {
    return ClearNotificationProvider(
      profile,
      notification,
    );
  }

  @override
  ClearNotificationProvider getProviderOverride(
    covariant ClearNotificationProvider provider,
  ) {
    return call(
      provider.profile,
      provider.notification,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'clearNotificationProvider';
}

/// See also [clearNotification].
class ClearNotificationProvider
    extends AutoDisposeFutureProvider<Result<bool, ExecError>> {
  /// See also [clearNotification].
  ClearNotificationProvider(
    Profile profile,
    UserNotification notification,
  ) : this._internal(
          (ref) => clearNotification(
            ref as ClearNotificationRef,
            profile,
            notification,
          ),
          from: clearNotificationProvider,
          name: r'clearNotificationProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$clearNotificationHash,
          dependencies: ClearNotificationFamily._dependencies,
          allTransitiveDependencies:
              ClearNotificationFamily._allTransitiveDependencies,
          profile: profile,
          notification: notification,
        );

  ClearNotificationProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.notification,
  }) : super.internal();

  final Profile profile;
  final UserNotification notification;

  @override
  Override overrideWith(
    FutureOr<Result<bool, ExecError>> Function(ClearNotificationRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ClearNotificationProvider._internal(
        (ref) => create(ref as ClearNotificationRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        notification: notification,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<bool, ExecError>> createElement() {
    return _ClearNotificationProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ClearNotificationProvider &&
        other.profile == profile &&
        other.notification == notification;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, notification.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ClearNotificationRef
    on AutoDisposeFutureProviderRef<Result<bool, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `notification` of this provider.
  UserNotification get notification;
}

class _ClearNotificationProviderElement
    extends AutoDisposeFutureProviderElement<Result<bool, ExecError>>
    with ClearNotificationRef {
  _ClearNotificationProviderElement(super.provider);

  @override
  Profile get profile => (origin as ClearNotificationProvider).profile;
  @override
  UserNotification get notification =>
      (origin as ClearNotificationProvider).notification;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
