import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../globals.dart';
import '../../models/auth/profile.dart';
import '../../models/exec_error.dart';
import '../../models/networking/paged_response.dart';
import '../globals_services.dart';
import 'network_services.dart';

part 'friendica_client_services.g.dart';

@riverpod
Map<String, String> friendicaClientHeaders(
  Ref ref,
  Profile profile, {
  bool withContentString = true,
  bool withAuthorization = true,
  bool withClientString = true,
}) {
  final userAgent = ref.watch(userAgentProvider);
  return {
    if (withAuthorization) 'Authorization': profile.credentials.authHeaderValue,
    if (withContentString) 'Content-Type': 'application/json; charset=UTF-8',
    if (usePhpDebugging) 'Cookie': 'XDEBUG_SESSION=PHPSTORM;path=/',
    if (withClientString) 'user-agent': userAgent,
  };
}

@riverpod
Future<Result<dynamic, ExecError>> getApiRequest(
    Ref ref, Profile profile, Uri url,
    {Duration? timeout}) async {
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  return await ref
      .read(httpGetProvider(
              NetworkRequest(url, headers: headers, timeout: timeout))
          .future)
      .transform((response) => jsonDecode(response.data))
      .execErrorCastAsync();
}

@riverpod
Future<Result<PagedResponse<dynamic>, ExecError>> getApiPagedRequest(
    Ref ref, Profile profile, Uri url,
    {Duration? timeout}) async {
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  return await ref
      .read(httpGetProvider(
              NetworkRequest(url, headers: headers, timeout: timeout))
          .future)
      .transform((response) => response.map((data) => jsonDecode(data)))
      .execErrorCastAsync();
}

@riverpod
Future<Result<PagedResponse<List<dynamic>>, ExecError>> getApiListRequest(
  Ref ref,
  Profile profile,
  Uri url, {
  Duration? timeout,
}) async {
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  return await ref
      .read(httpGetProvider(
              NetworkRequest(url, headers: headers, timeout: timeout))
          .future)
      .transform((response) =>
          response.map((data) => jsonDecode(data) as List<dynamic>))
      .execErrorCastAsync();
}
