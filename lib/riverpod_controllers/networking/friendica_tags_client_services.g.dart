// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_tags_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$followedTagsHash() => r'27bdaa57ccf606df04708a2fe9905907e9b771af';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [followedTags].
@ProviderFor(followedTags)
const followedTagsProvider = FollowedTagsFamily();

/// See also [followedTags].
class FollowedTagsFamily
    extends Family<AsyncValue<Result<List<HashtagWithTrending>, ExecError>>> {
  /// See also [followedTags].
  const FollowedTagsFamily();

  /// See also [followedTags].
  FollowedTagsProvider call(
    Profile profile,
  ) {
    return FollowedTagsProvider(
      profile,
    );
  }

  @override
  FollowedTagsProvider getProviderOverride(
    covariant FollowedTagsProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'followedTagsProvider';
}

/// See also [followedTags].
class FollowedTagsProvider extends AutoDisposeFutureProvider<
    Result<List<HashtagWithTrending>, ExecError>> {
  /// See also [followedTags].
  FollowedTagsProvider(
    Profile profile,
  ) : this._internal(
          (ref) => followedTags(
            ref as FollowedTagsRef,
            profile,
          ),
          from: followedTagsProvider,
          name: r'followedTagsProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$followedTagsHash,
          dependencies: FollowedTagsFamily._dependencies,
          allTransitiveDependencies:
              FollowedTagsFamily._allTransitiveDependencies,
          profile: profile,
        );

  FollowedTagsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<Result<List<HashtagWithTrending>, ExecError>> Function(
            FollowedTagsRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FollowedTagsProvider._internal(
        (ref) => create(ref as FollowedTagsRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<HashtagWithTrending>, ExecError>>
      createElement() {
    return _FollowedTagsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FollowedTagsProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FollowedTagsRef on AutoDisposeFutureProviderRef<
    Result<List<HashtagWithTrending>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _FollowedTagsProviderElement extends AutoDisposeFutureProviderElement<
    Result<List<HashtagWithTrending>, ExecError>> with FollowedTagsRef {
  _FollowedTagsProviderElement(super.provider);

  @override
  Profile get profile => (origin as FollowedTagsProvider).profile;
}

String _$followTagHash() => r'56ba93de8c98a579f5717caef26423ec135e58b3';

/// See also [followTag].
@ProviderFor(followTag)
const followTagProvider = FollowTagFamily();

/// See also [followTag].
class FollowTagFamily
    extends Family<AsyncValue<Result<HashtagWithTrending, ExecError>>> {
  /// See also [followTag].
  const FollowTagFamily();

  /// See also [followTag].
  FollowTagProvider call(
    Profile profile,
    String tagName,
  ) {
    return FollowTagProvider(
      profile,
      tagName,
    );
  }

  @override
  FollowTagProvider getProviderOverride(
    covariant FollowTagProvider provider,
  ) {
    return call(
      provider.profile,
      provider.tagName,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'followTagProvider';
}

/// See also [followTag].
class FollowTagProvider
    extends AutoDisposeFutureProvider<Result<HashtagWithTrending, ExecError>> {
  /// See also [followTag].
  FollowTagProvider(
    Profile profile,
    String tagName,
  ) : this._internal(
          (ref) => followTag(
            ref as FollowTagRef,
            profile,
            tagName,
          ),
          from: followTagProvider,
          name: r'followTagProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$followTagHash,
          dependencies: FollowTagFamily._dependencies,
          allTransitiveDependencies: FollowTagFamily._allTransitiveDependencies,
          profile: profile,
          tagName: tagName,
        );

  FollowTagProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.tagName,
  }) : super.internal();

  final Profile profile;
  final String tagName;

  @override
  Override overrideWith(
    FutureOr<Result<HashtagWithTrending, ExecError>> Function(
            FollowTagRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FollowTagProvider._internal(
        (ref) => create(ref as FollowTagRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        tagName: tagName,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<HashtagWithTrending, ExecError>>
      createElement() {
    return _FollowTagProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FollowTagProvider &&
        other.profile == profile &&
        other.tagName == tagName;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, tagName.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FollowTagRef
    on AutoDisposeFutureProviderRef<Result<HashtagWithTrending, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `tagName` of this provider.
  String get tagName;
}

class _FollowTagProviderElement extends AutoDisposeFutureProviderElement<
    Result<HashtagWithTrending, ExecError>> with FollowTagRef {
  _FollowTagProviderElement(super.provider);

  @override
  Profile get profile => (origin as FollowTagProvider).profile;
  @override
  String get tagName => (origin as FollowTagProvider).tagName;
}

String _$unfollowTagHash() => r'8075ecca678529ce07f7017139a6d2b766727417';

/// See also [unfollowTag].
@ProviderFor(unfollowTag)
const unfollowTagProvider = UnfollowTagFamily();

/// See also [unfollowTag].
class UnfollowTagFamily
    extends Family<AsyncValue<Result<HashtagWithTrending, ExecError>>> {
  /// See also [unfollowTag].
  const UnfollowTagFamily();

  /// See also [unfollowTag].
  UnfollowTagProvider call(
    Profile profile,
    String tagName,
  ) {
    return UnfollowTagProvider(
      profile,
      tagName,
    );
  }

  @override
  UnfollowTagProvider getProviderOverride(
    covariant UnfollowTagProvider provider,
  ) {
    return call(
      provider.profile,
      provider.tagName,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'unfollowTagProvider';
}

/// See also [unfollowTag].
class UnfollowTagProvider
    extends AutoDisposeFutureProvider<Result<HashtagWithTrending, ExecError>> {
  /// See also [unfollowTag].
  UnfollowTagProvider(
    Profile profile,
    String tagName,
  ) : this._internal(
          (ref) => unfollowTag(
            ref as UnfollowTagRef,
            profile,
            tagName,
          ),
          from: unfollowTagProvider,
          name: r'unfollowTagProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$unfollowTagHash,
          dependencies: UnfollowTagFamily._dependencies,
          allTransitiveDependencies:
              UnfollowTagFamily._allTransitiveDependencies,
          profile: profile,
          tagName: tagName,
        );

  UnfollowTagProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.tagName,
  }) : super.internal();

  final Profile profile;
  final String tagName;

  @override
  Override overrideWith(
    FutureOr<Result<HashtagWithTrending, ExecError>> Function(
            UnfollowTagRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: UnfollowTagProvider._internal(
        (ref) => create(ref as UnfollowTagRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        tagName: tagName,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<HashtagWithTrending, ExecError>>
      createElement() {
    return _UnfollowTagProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is UnfollowTagProvider &&
        other.profile == profile &&
        other.tagName == tagName;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, tagName.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin UnfollowTagRef
    on AutoDisposeFutureProviderRef<Result<HashtagWithTrending, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `tagName` of this provider.
  String get tagName;
}

class _UnfollowTagProviderElement extends AutoDisposeFutureProviderElement<
    Result<HashtagWithTrending, ExecError>> with UnfollowTagRef {
  _UnfollowTagProviderElement(super.provider);

  @override
  Profile get profile => (origin as UnfollowTagProvider).profile;
  @override
  String get tagName => (origin as UnfollowTagProvider).tagName;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
