// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_profile_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$myProfileHash() => r'd9ec22713ccb0d3938e0f55123d7d3b097dc19f9';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [myProfile].
@ProviderFor(myProfile)
const myProfileProvider = MyProfileFamily();

/// See also [myProfile].
class MyProfileFamily
    extends Family<AsyncValue<Result<(Connection, Profile), ExecError>>> {
  /// See also [myProfile].
  const MyProfileFamily();

  /// See also [myProfile].
  MyProfileProvider call(
    Profile profile,
  ) {
    return MyProfileProvider(
      profile,
    );
  }

  @override
  MyProfileProvider getProviderOverride(
    covariant MyProfileProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'myProfileProvider';
}

/// See also [myProfile].
class MyProfileProvider extends AutoDisposeFutureProvider<
    Result<(Connection, Profile), ExecError>> {
  /// See also [myProfile].
  MyProfileProvider(
    Profile profile,
  ) : this._internal(
          (ref) => myProfile(
            ref as MyProfileRef,
            profile,
          ),
          from: myProfileProvider,
          name: r'myProfileProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$myProfileHash,
          dependencies: MyProfileFamily._dependencies,
          allTransitiveDependencies: MyProfileFamily._allTransitiveDependencies,
          profile: profile,
        );

  MyProfileProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<Result<(Connection, Profile), ExecError>> Function(
            MyProfileRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: MyProfileProvider._internal(
        (ref) => create(ref as MyProfileRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<(Connection, Profile), ExecError>>
      createElement() {
    return _MyProfileProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is MyProfileProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin MyProfileRef
    on AutoDisposeFutureProviderRef<Result<(Connection, Profile), ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _MyProfileProviderElement extends AutoDisposeFutureProviderElement<
    Result<(Connection, Profile), ExecError>> with MyProfileRef {
  _MyProfileProviderElement(super.provider);

  @override
  Profile get profile => (origin as MyProfileProvider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
