// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_trending_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$trendingHashtagsHash() => r'f886761a30744f1189db0f0b82fade8a89bcae38';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [trendingHashtags].
@ProviderFor(trendingHashtags)
const trendingHashtagsProvider = TrendingHashtagsFamily();

/// See also [trendingHashtags].
class TrendingHashtagsFamily
    extends Family<AsyncValue<Result<List<HashtagWithTrending>, ExecError>>> {
  /// See also [trendingHashtags].
  const TrendingHashtagsFamily();

  /// See also [trendingHashtags].
  TrendingHashtagsProvider call(
    Profile profile, {
    bool local = false,
    PagingData page = const PagingData(),
  }) {
    return TrendingHashtagsProvider(
      profile,
      local: local,
      page: page,
    );
  }

  @override
  TrendingHashtagsProvider getProviderOverride(
    covariant TrendingHashtagsProvider provider,
  ) {
    return call(
      provider.profile,
      local: provider.local,
      page: provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'trendingHashtagsProvider';
}

/// See also [trendingHashtags].
class TrendingHashtagsProvider extends AutoDisposeFutureProvider<
    Result<List<HashtagWithTrending>, ExecError>> {
  /// See also [trendingHashtags].
  TrendingHashtagsProvider(
    Profile profile, {
    bool local = false,
    PagingData page = const PagingData(),
  }) : this._internal(
          (ref) => trendingHashtags(
            ref as TrendingHashtagsRef,
            profile,
            local: local,
            page: page,
          ),
          from: trendingHashtagsProvider,
          name: r'trendingHashtagsProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$trendingHashtagsHash,
          dependencies: TrendingHashtagsFamily._dependencies,
          allTransitiveDependencies:
              TrendingHashtagsFamily._allTransitiveDependencies,
          profile: profile,
          local: local,
          page: page,
        );

  TrendingHashtagsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.local,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final bool local;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<List<HashtagWithTrending>, ExecError>> Function(
            TrendingHashtagsRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: TrendingHashtagsProvider._internal(
        (ref) => create(ref as TrendingHashtagsRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        local: local,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<HashtagWithTrending>, ExecError>>
      createElement() {
    return _TrendingHashtagsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TrendingHashtagsProvider &&
        other.profile == profile &&
        other.local == local &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, local.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin TrendingHashtagsRef on AutoDisposeFutureProviderRef<
    Result<List<HashtagWithTrending>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `local` of this provider.
  bool get local;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _TrendingHashtagsProviderElement extends AutoDisposeFutureProviderElement<
    Result<List<HashtagWithTrending>, ExecError>> with TrendingHashtagsRef {
  _TrendingHashtagsProviderElement(super.provider);

  @override
  Profile get profile => (origin as TrendingHashtagsProvider).profile;
  @override
  bool get local => (origin as TrendingHashtagsProvider).local;
  @override
  PagingData get page => (origin as TrendingHashtagsProvider).page;
}

String _$trendingStatusesHash() => r'1e96003d5daa61283babc66dd3603957500a9b4d';

/// See also [trendingStatuses].
@ProviderFor(trendingStatuses)
const trendingStatusesProvider = TrendingStatusesFamily();

/// See also [trendingStatuses].
class TrendingStatusesFamily
    extends Family<AsyncValue<Result<List<TimelineEntry>, ExecError>>> {
  /// See also [trendingStatuses].
  const TrendingStatusesFamily();

  /// See also [trendingStatuses].
  TrendingStatusesProvider call(
    Profile profile, {
    PagingData page = const PagingData(),
  }) {
    return TrendingStatusesProvider(
      profile,
      page: page,
    );
  }

  @override
  TrendingStatusesProvider getProviderOverride(
    covariant TrendingStatusesProvider provider,
  ) {
    return call(
      provider.profile,
      page: provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'trendingStatusesProvider';
}

/// See also [trendingStatuses].
class TrendingStatusesProvider
    extends AutoDisposeFutureProvider<Result<List<TimelineEntry>, ExecError>> {
  /// See also [trendingStatuses].
  TrendingStatusesProvider(
    Profile profile, {
    PagingData page = const PagingData(),
  }) : this._internal(
          (ref) => trendingStatuses(
            ref as TrendingStatusesRef,
            profile,
            page: page,
          ),
          from: trendingStatusesProvider,
          name: r'trendingStatusesProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$trendingStatusesHash,
          dependencies: TrendingStatusesFamily._dependencies,
          allTransitiveDependencies:
              TrendingStatusesFamily._allTransitiveDependencies,
          profile: profile,
          page: page,
        );

  TrendingStatusesProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<List<TimelineEntry>, ExecError>> Function(
            TrendingStatusesRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: TrendingStatusesProvider._internal(
        (ref) => create(ref as TrendingStatusesRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<TimelineEntry>, ExecError>>
      createElement() {
    return _TrendingStatusesProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TrendingStatusesProvider &&
        other.profile == profile &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin TrendingStatusesRef
    on AutoDisposeFutureProviderRef<Result<List<TimelineEntry>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _TrendingStatusesProviderElement extends AutoDisposeFutureProviderElement<
    Result<List<TimelineEntry>, ExecError>> with TrendingStatusesRef {
  _TrendingStatusesProviderElement(super.provider);

  @override
  Profile get profile => (origin as TrendingStatusesProvider).profile;
  @override
  PagingData get page => (origin as TrendingStatusesProvider).page;
}

String _$trendingLinksHash() => r'0208811a95ff643c201e918a82b3bdb619e3cee2';

/// See also [trendingLinks].
@ProviderFor(trendingLinks)
const trendingLinksProvider = TrendingLinksFamily();

/// See also [trendingLinks].
class TrendingLinksFamily
    extends Family<AsyncValue<Result<List<LinkPreviewData>, ExecError>>> {
  /// See also [trendingLinks].
  const TrendingLinksFamily();

  /// See also [trendingLinks].
  TrendingLinksProvider call(
    Profile profile, {
    PagingData page = const PagingData(),
  }) {
    return TrendingLinksProvider(
      profile,
      page: page,
    );
  }

  @override
  TrendingLinksProvider getProviderOverride(
    covariant TrendingLinksProvider provider,
  ) {
    return call(
      provider.profile,
      page: provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'trendingLinksProvider';
}

/// See also [trendingLinks].
class TrendingLinksProvider extends AutoDisposeFutureProvider<
    Result<List<LinkPreviewData>, ExecError>> {
  /// See also [trendingLinks].
  TrendingLinksProvider(
    Profile profile, {
    PagingData page = const PagingData(),
  }) : this._internal(
          (ref) => trendingLinks(
            ref as TrendingLinksRef,
            profile,
            page: page,
          ),
          from: trendingLinksProvider,
          name: r'trendingLinksProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$trendingLinksHash,
          dependencies: TrendingLinksFamily._dependencies,
          allTransitiveDependencies:
              TrendingLinksFamily._allTransitiveDependencies,
          profile: profile,
          page: page,
        );

  TrendingLinksProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<List<LinkPreviewData>, ExecError>> Function(
            TrendingLinksRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: TrendingLinksProvider._internal(
        (ref) => create(ref as TrendingLinksRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<LinkPreviewData>, ExecError>>
      createElement() {
    return _TrendingLinksProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TrendingLinksProvider &&
        other.profile == profile &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin TrendingLinksRef
    on AutoDisposeFutureProviderRef<Result<List<LinkPreviewData>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _TrendingLinksProviderElement extends AutoDisposeFutureProviderElement<
    Result<List<LinkPreviewData>, ExecError>> with TrendingLinksRef {
  _TrendingLinksProviderElement(super.provider);

  @override
  Profile get profile => (origin as TrendingLinksProvider).profile;
  @override
  PagingData get page => (origin as TrendingLinksProvider).page;
}

String _$suggestedConnectionsClientHash() =>
    r'7f593123b01add5c75894697c03c57dc8024b2d2';

/// See also [suggestedConnectionsClient].
@ProviderFor(suggestedConnectionsClient)
const suggestedConnectionsClientProvider = SuggestedConnectionsClientFamily();

/// See also [suggestedConnectionsClient].
class SuggestedConnectionsClientFamily
    extends Family<AsyncValue<Result<List<Connection>, ExecError>>> {
  /// See also [suggestedConnectionsClient].
  const SuggestedConnectionsClientFamily();

  /// See also [suggestedConnectionsClient].
  SuggestedConnectionsClientProvider call(
    Profile profile,
  ) {
    return SuggestedConnectionsClientProvider(
      profile,
    );
  }

  @override
  SuggestedConnectionsClientProvider getProviderOverride(
    covariant SuggestedConnectionsClientProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'suggestedConnectionsClientProvider';
}

/// See also [suggestedConnectionsClient].
class SuggestedConnectionsClientProvider
    extends AutoDisposeFutureProvider<Result<List<Connection>, ExecError>> {
  /// See also [suggestedConnectionsClient].
  SuggestedConnectionsClientProvider(
    Profile profile,
  ) : this._internal(
          (ref) => suggestedConnectionsClient(
            ref as SuggestedConnectionsClientRef,
            profile,
          ),
          from: suggestedConnectionsClientProvider,
          name: r'suggestedConnectionsClientProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$suggestedConnectionsClientHash,
          dependencies: SuggestedConnectionsClientFamily._dependencies,
          allTransitiveDependencies:
              SuggestedConnectionsClientFamily._allTransitiveDependencies,
          profile: profile,
        );

  SuggestedConnectionsClientProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<Result<List<Connection>, ExecError>> Function(
            SuggestedConnectionsClientRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: SuggestedConnectionsClientProvider._internal(
        (ref) => create(ref as SuggestedConnectionsClientRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<Connection>, ExecError>>
      createElement() {
    return _SuggestedConnectionsClientProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SuggestedConnectionsClientProvider &&
        other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin SuggestedConnectionsClientRef
    on AutoDisposeFutureProviderRef<Result<List<Connection>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _SuggestedConnectionsClientProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<List<Connection>, ExecError>>
    with SuggestedConnectionsClientRef {
  _SuggestedConnectionsClientProviderElement(super.provider);

  @override
  Profile get profile => (origin as SuggestedConnectionsClientProvider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
