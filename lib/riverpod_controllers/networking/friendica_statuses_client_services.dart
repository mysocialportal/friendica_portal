import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../models/auth/profile.dart';
import '../../models/exec_error.dart';
import '../../models/timeline_entry.dart';
import '../../models/visibility.dart';
import '../../serializers/mastodon/timeline_entry_mastodon_extensions.dart';
import '../../serializers/mastodon/visibility_mastodon_extensions.dart';
import '../rp_provider_extension.dart';
import 'friendica_client_services.dart';
import 'network_services.dart';
import 'network_status_services.dart';

part 'friendica_statuses_client_services.g.dart';

final _logger = Logger('FriendicaStatusesClient');

// TODO Convert getPostOrComment to using paging for real
@riverpod
Future<Result<List<TimelineEntry>, ExecError>> postOrComment(
    Ref ref, Profile profile, String id,
    {bool fullContext = false}) async {
  ref.read(postCommentLoadingStatusProvider(profile, id));
  Future.microtask(
    () async => ref
        .read(postCommentLoadingStatusProvider(profile, id).notifier)
        .begin(),
  );
  final result = (await runCatchingAsync(() async {
    final baseUrl = 'https://${profile.serverName}/api/v1/statuses/$id';
    final url = fullContext ? '$baseUrl/context' : baseUrl;
    final request = Uri.parse('$url?limit=1000');
    _logger.finest(() =>
        'Getting entry for status $id, full context? $fullContext : $url');
    return (await ref
        .read(getApiRequestProvider(profile, request).future)
        .andThenSuccessAsync((json) async {
      if (fullContext) {
        final ancestors = json['ancestors'] as List<dynamic>;
        final descendants = json['descendants'] as List<dynamic>;
        final items = [
          ...ancestors.map((a) => timelineEntryFromJson(ref, profile, a)),
          ...descendants.map((d) => timelineEntryFromJson(ref, profile, d))
        ];
        _logger.finer(() =>
            'Got status $id with full context which returned ${ancestors.length} ancestors and ${descendants.length} descendants');
        return items;
      } else {
        return [timelineEntryFromJson(ref, profile, json)];
      }
    }));
  }));
  ref.read(postCommentLoadingStatusProvider(profile, id).notifier).end();

  return result.execErrorCast();
}

@riverpod
Future<Result<bool, ExecError>> deleteStatusEntryById(
    Ref ref, Profile profile, String id) async {
  _logger.finest(() => 'Deleting post/comment $id');
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final url = 'https://${profile.serverName}/api/v1/statuses/$id';
  final request = NetworkRequest(Uri.parse(url), headers: headers);
  final result =
      await ref.read(httpDeleteProvider(request).future).mapValue((_) => true);
  return result;
}

@riverpod
Future<Result<TimelineEntry, ExecError>> createNewStatus(
  Ref ref,
  Profile profile, {
  required String text,
  String spoilerText = '',
  String inReplyToId = '',
  List<String> mediaIds = const [],
  required Visibility visibility,
}) async {
  ref.cacheFor(const Duration(seconds: 10));
  _logger.finest(() =>
      'Creating status ${inReplyToId.isNotEmpty ? "In Reply to: " : ""} $inReplyToId, with media: $mediaIds');
  final url = Uri.parse('https://${profile.serverName}/api/v1/statuses');
  final body = {
    'status': text,
    if (spoilerText.isNotEmpty) 'spoiler_text': spoilerText,
    if (inReplyToId.isNotEmpty) 'in_reply_to_id': inReplyToId,
    if (mediaIds.isNotEmpty) 'media_ids': mediaIds,
    'visibility': visibility.toCreateStatusValue(inReplyToId.isNotEmpty),
    'friendica': {
      'title': '',
    },
  };
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final result = await ref
      .read(httpPostProvider(NetworkRequest(
    url,
    headers: headers,
    body: body,
  )).future)
      .transform((responseText) {
    final json = jsonDecode(responseText);
    return timelineEntryFromJson(ref, profile, json);
  }).mapError((error) {
    return ExecError(type: ErrorType.parsingError, message: error.toString());
  });

  return result;
}

@riverpod
Future<Result<TimelineEntry, ExecError>> editStatus(
  Ref ref,
  Profile profile, {
  required String id,
  required String text,
  String spoilerText = '',
  List<String> mediaIds = const [],
}) async {
  _logger.finest(() => 'Updating status $id');
  final url = Uri.parse('https://${profile.serverName}/api/v1/statuses/$id');
  final body = {
    'status': text,
    if (spoilerText.isNotEmpty) 'spoiler_text': spoilerText,
    if (mediaIds.isNotEmpty) 'media_ids': mediaIds,
    'friendica': {
      'title': '',
    },
  };
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final result = await ref
      .read(httpPutProvider(NetworkRequest(
    url,
    headers: headers,
    body: body,
  )).future)
      .transform((responseText) {
    final json = jsonDecode(responseText);
    return timelineEntryFromJson(ref, profile, json);
  }).mapError((error) {
    return ExecError(type: ErrorType.parsingError, message: error.toString());
  });

  return result;
}

@riverpod
Future<Result<TimelineEntry, ExecError>> resharePost(
    Ref ref, Profile profile, String id) async {
  _logger.finest(() => 'Reshare post $id');
  final url =
      Uri.parse('https://${profile.serverName}/api/v1/statuses/$id/reblog');
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final result = await ref.read(httpPostProvider(NetworkRequest(
    url,
    headers: headers,
  )).future);
  if (result.isFailure) {
    return result.errorCast();
  }

  if (result.isFailure) {
    return result.errorCast();
  }

  final responseText = result.value;

  return runCatching<TimelineEntry>(() {
    final json = jsonDecode(responseText);
    return Result.ok(timelineEntryFromJson(ref, profile, json));
  }).mapError((error) {
    return ExecError(type: ErrorType.parsingError, message: error.toString());
  });
}

@riverpod
Future<Result<TimelineEntry, ExecError>> unResharePost(
    Ref ref, Profile profile, String id) async {
  _logger.finest(() => 'Reshare post $id');
  final url =
      Uri.parse('https://${profile.serverName}/api/v1/statuses/$id/unreblog');
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final result = await ref
      .read(httpPostProvider(NetworkRequest(
    url,
    headers: headers,
  )).future)
      .transform((responseText) {
    final json = jsonDecode(responseText);
    return timelineEntryFromJson(ref, profile, json);
  }).mapError((error) {
    return ExecError(type: ErrorType.parsingError, message: error.toString());
  });

  return result;
}
