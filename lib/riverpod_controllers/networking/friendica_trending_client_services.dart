import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../models/auth/profile.dart';
import '../../models/connection.dart';
import '../../models/exec_error.dart';
import '../../models/hashtag.dart';
import '../../models/link_preview_data.dart';
import '../../models/networking/paging_data.dart';
import '../../models/timeline_entry.dart';
import '../../serializers/mastodon/connection_mastodon_extensions.dart';
import '../../serializers/mastodon/hashtag_mastodon_extensions.dart';
import '../../serializers/mastodon/link_preview_mastodon_extensions.dart';
import '../../serializers/mastodon/timeline_entry_mastodon_extensions.dart';
import 'friendica_client_services.dart';

part 'friendica_trending_client_services.g.dart';

@riverpod
Future<Result<List<HashtagWithTrending>, ExecError>> trendingHashtags(
  Ref ref,
  Profile profile, {
  bool local = false,
  PagingData page = const PagingData(),
}) async {
  final baseUrl = 'https://${profile.serverName}/api/v1/trends/tags';
  final url = '$baseUrl?${page.toQueryParameters()}&friendica_local=$local';
  final request = Uri.parse(url);
  final result = await ref
      .read(getApiListRequestProvider(profile, request).future)
      .transform((response) => response.data
          .map((json) => HashtagWithTrendingMastodonExtensions.fromJson(json))
          .toList());
  return result.execErrorCast();
}

@riverpod
Future<Result<List<TimelineEntry>, ExecError>> trendingStatuses(
  Ref ref,
  Profile profile, {
  PagingData page = const PagingData(),
}) async {
  final baseUrl = 'https://${profile.serverName}/api/v1/trends/statuses';
  final url = '$baseUrl?${page.toQueryParameters()}';
  final request = Uri.parse(url);
  final result = await ref
      .read(getApiListRequestProvider(profile, request).future)
      .transform((response) => response.data
          .map((json) => timelineEntryFromJson(ref, profile, json))
          .toList());
  return result.execErrorCast();
}

@riverpod
Future<Result<List<LinkPreviewData>, ExecError>> trendingLinks(
  Ref ref,
  Profile profile, {
  PagingData page = const PagingData(),
}) async {
  final baseUrl = 'https://${profile.serverName}/api/v1/trends/links';
  final url = '$baseUrl?${page.toQueryParameters()}';
  final request = Uri.parse(url);
  final result = await ref
      .read(getApiListRequestProvider(profile, request).future)
      .transform((response) => response.data
          .whereType<Map<String, dynamic>>()
          .map((json) => LinkPreviewMastodonExtensions.fromJson(json)!)
          .toList());
  return result.execErrorCast();
}

@riverpod
Future<Result<List<Connection>, ExecError>> suggestedConnectionsClient(
    Ref ref, Profile profile) async {
  final url = 'https://${profile.serverName}/api/v2/suggestions';
  final request = Uri.parse(url);
  final result = await ref
      .read(getApiListRequestProvider(profile, request).future)
      .transform((response) => response.data
          .map((json) => connectionFromJson(ref, profile, json['account']))
          .toList());
  return result.execErrorCast();
}
