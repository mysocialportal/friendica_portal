import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../models/auth/profile.dart';
import '../../models/direct_message.dart';
import '../../models/exec_error.dart';
import '../../models/networking/paging_data.dart';
import '../../serializers/friendica/direct_message_friendica_extensions.dart';
import 'friendica_client_services.dart';
import 'network_services.dart';
import 'network_status_services.dart';

part 'friendica_dms_client_services.g.dart';

final _logger = Logger('FriendicaDirectMessagesClient');

@riverpod
Future<Result<List<DirectMessage>, ExecError>> directMessages(
  Ref ref,
  Profile profile,
  PagingData page,
) async {
  ref.read(directMessageLoadingProvider(profile));
  Future.microtask(
    () async =>
        ref.read(directMessageLoadingProvider(profile).notifier).begin(),
  );

  final baseUrl = 'https://${profile.serverName}/api/direct_messages/all';
  final pagingQP = page.toQueryParameters(limitKeyword: 'count');
  final url = '$baseUrl?$pagingQP';
  final request = Uri.parse(url);
  _logger.finest(() => 'Getting direct messages with paging data $page');
  final result = (await ref
          .read(getApiListRequestProvider(profile, request).future)
          .andThenSuccessAsync((response) async => response.data
              .map((json) => DirectMessageFriendicaExtension.fromJson(json))
              .toList()))
      .execErrorCast();

  ref.read(directMessageLoadingProvider(profile).notifier).end();
  return result;
}

@riverpod
Future<Result<List<DirectMessage>, ExecError>> updateThread(
  Ref ref,
  Profile profile,
  String threadUri,
  PagingData page,
) async {
  ref.read(directMessageLoadingProvider(profile));
  Future.microtask(
    () async =>
        ref.read(directMessageLoadingProvider(profile).notifier).begin(),
  );

  final baseUrl =
      'https://${profile.serverName}/api/direct_messages/conversation';
  final pagingQP = page.toQueryParameters(limitKeyword: 'count');
  final url = '$baseUrl?$pagingQP&uri=$threadUri';
  final request = Uri.parse(url);
  _logger.finest(() => 'Getting direct messages with paging data $page');
  final result = (await ref
          .read(getApiListRequestProvider(profile, request).future)
          .andThenSuccessAsync((response) async => response.data
              .map((json) => DirectMessageFriendicaExtension.fromJson(json))
              .toList()))
      .execErrorCast();

  ref.read(directMessageLoadingProvider(profile).notifier).end();
  return result;
}

@riverpod
Future<Result<DirectMessage, ExecError>> markDirectMessageRead(
  Ref ref,
  Profile profile,
  DirectMessage message,
) async {
  ref.read(directMessageLoadingProvider(profile));
  Future.microtask(
    () async =>
        ref.read(directMessageLoadingProvider(profile).notifier).begin(),
  );
  final id = message.id;
  final url = Uri.parse(
      'https://${profile.serverName}/api/friendica/direct_messages_setseen?id=$id');
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(url, headers: headers);
  final result = await ref
      .read(httpPostProvider(request).future)
      .andThenSuccessAsync((jsonString) async {
    return message.copy(seen: true);
  });
  ref.read(directMessageLoadingProvider(profile).notifier).end();
  return result.execErrorCast();
}

@riverpod
Future<Result<DirectMessage, ExecError>> postDirectMessage(
  Ref ref,
  Profile profile,
  String? messageIdRepliedTo,
  String receivingUserId,
  String text,
) async {
  ref.read(directMessageLoadingProvider(profile));
  Future.microtask(
    () async =>
        ref.read(directMessageLoadingProvider(profile).notifier).begin(),
  );
  final url =
      Uri.parse('https://${profile.serverName}/api/direct_messages/new');
  final body = {
    'user_id': receivingUserId,
    'text': text,
    if (messageIdRepliedTo != null) 'replyto': messageIdRepliedTo,
  };
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(
    url,
    headers: headers,
    body: body,
  );

  final result = await ref
      .read(httpPostProvider(request).future)
      .andThenAsync<DirectMessage, ExecError>((jsonString) async {
    final json = jsonDecode(jsonString) as Map<String, dynamic>;
    if (json.containsKey('error')) {
      return buildErrorResult(
          type: ErrorType.serverError,
          message: "Error from server: ${json['error']}");
    }
    return Result.ok(
        DirectMessageFriendicaExtension.fromJson(jsonDecode(jsonString)));
  });

  ref.read(directMessageLoadingProvider(profile).notifier).end();
  return result.execErrorCast();
}
