// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fediverse_instance_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$instanceDataHash() => r'cd251c19eae415d2c9edf0b60298a6e2623aef6a';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [instanceData].
@ProviderFor(instanceData)
const instanceDataProvider = InstanceDataFamily();

/// See also [instanceData].
class InstanceDataFamily
    extends Family<AsyncValue<Result<InstanceInfo, ExecError>>> {
  /// See also [instanceData].
  const InstanceDataFamily();

  /// See also [instanceData].
  InstanceDataProvider call(
    Profile profile,
  ) {
    return InstanceDataProvider(
      profile,
    );
  }

  @override
  InstanceDataProvider getProviderOverride(
    covariant InstanceDataProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'instanceDataProvider';
}

/// See also [instanceData].
class InstanceDataProvider
    extends AutoDisposeFutureProvider<Result<InstanceInfo, ExecError>> {
  /// See also [instanceData].
  InstanceDataProvider(
    Profile profile,
  ) : this._internal(
          (ref) => instanceData(
            ref as InstanceDataRef,
            profile,
          ),
          from: instanceDataProvider,
          name: r'instanceDataProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$instanceDataHash,
          dependencies: InstanceDataFamily._dependencies,
          allTransitiveDependencies:
              InstanceDataFamily._allTransitiveDependencies,
          profile: profile,
        );

  InstanceDataProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<Result<InstanceInfo, ExecError>> Function(InstanceDataRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: InstanceDataProvider._internal(
        (ref) => create(ref as InstanceDataRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<InstanceInfo, ExecError>>
      createElement() {
    return _InstanceDataProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is InstanceDataProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin InstanceDataRef
    on AutoDisposeFutureProviderRef<Result<InstanceInfo, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _InstanceDataProviderElement
    extends AutoDisposeFutureProviderElement<Result<InstanceInfo, ExecError>>
    with InstanceDataRef {
  _InstanceDataProviderElement(super.provider);

  @override
  Profile get profile => (origin as InstanceDataProvider).profile;
}

String _$instanceDataV1Hash() => r'ca3dfae7d421d1c0b739d7e512279076af256b38';

/// See also [_instanceDataV1].
@ProviderFor(_instanceDataV1)
const _instanceDataV1Provider = _InstanceDataV1Family();

/// See also [_instanceDataV1].
class _InstanceDataV1Family
    extends Family<AsyncValue<Result<InstanceInfo, ExecError>>> {
  /// See also [_instanceDataV1].
  const _InstanceDataV1Family();

  /// See also [_instanceDataV1].
  _InstanceDataV1Provider call(
    Profile profile,
  ) {
    return _InstanceDataV1Provider(
      profile,
    );
  }

  @override
  _InstanceDataV1Provider getProviderOverride(
    covariant _InstanceDataV1Provider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'_instanceDataV1Provider';
}

/// See also [_instanceDataV1].
class _InstanceDataV1Provider
    extends AutoDisposeFutureProvider<Result<InstanceInfo, ExecError>> {
  /// See also [_instanceDataV1].
  _InstanceDataV1Provider(
    Profile profile,
  ) : this._internal(
          (ref) => _instanceDataV1(
            ref as _InstanceDataV1Ref,
            profile,
          ),
          from: _instanceDataV1Provider,
          name: r'_instanceDataV1Provider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$instanceDataV1Hash,
          dependencies: _InstanceDataV1Family._dependencies,
          allTransitiveDependencies:
              _InstanceDataV1Family._allTransitiveDependencies,
          profile: profile,
        );

  _InstanceDataV1Provider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<Result<InstanceInfo, ExecError>> Function(
            _InstanceDataV1Ref provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: _InstanceDataV1Provider._internal(
        (ref) => create(ref as _InstanceDataV1Ref),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<InstanceInfo, ExecError>>
      createElement() {
    return _InstanceDataV1ProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is _InstanceDataV1Provider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin _InstanceDataV1Ref
    on AutoDisposeFutureProviderRef<Result<InstanceInfo, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _InstanceDataV1ProviderElement
    extends AutoDisposeFutureProviderElement<Result<InstanceInfo, ExecError>>
    with _InstanceDataV1Ref {
  _InstanceDataV1ProviderElement(super.provider);

  @override
  Profile get profile => (origin as _InstanceDataV1Provider).profile;
}

String _$instanceDataV2Hash() => r'd3f9e57b9ba72cd18d4284ad0c109a2b7c2bb27f';

/// See also [_instanceDataV2].
@ProviderFor(_instanceDataV2)
const _instanceDataV2Provider = _InstanceDataV2Family();

/// See also [_instanceDataV2].
class _InstanceDataV2Family
    extends Family<AsyncValue<Result<InstanceInfo, ExecError>>> {
  /// See also [_instanceDataV2].
  const _InstanceDataV2Family();

  /// See also [_instanceDataV2].
  _InstanceDataV2Provider call(
    Profile profile,
  ) {
    return _InstanceDataV2Provider(
      profile,
    );
  }

  @override
  _InstanceDataV2Provider getProviderOverride(
    covariant _InstanceDataV2Provider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'_instanceDataV2Provider';
}

/// See also [_instanceDataV2].
class _InstanceDataV2Provider
    extends AutoDisposeFutureProvider<Result<InstanceInfo, ExecError>> {
  /// See also [_instanceDataV2].
  _InstanceDataV2Provider(
    Profile profile,
  ) : this._internal(
          (ref) => _instanceDataV2(
            ref as _InstanceDataV2Ref,
            profile,
          ),
          from: _instanceDataV2Provider,
          name: r'_instanceDataV2Provider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$instanceDataV2Hash,
          dependencies: _InstanceDataV2Family._dependencies,
          allTransitiveDependencies:
              _InstanceDataV2Family._allTransitiveDependencies,
          profile: profile,
        );

  _InstanceDataV2Provider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<Result<InstanceInfo, ExecError>> Function(
            _InstanceDataV2Ref provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: _InstanceDataV2Provider._internal(
        (ref) => create(ref as _InstanceDataV2Ref),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<InstanceInfo, ExecError>>
      createElement() {
    return _InstanceDataV2ProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is _InstanceDataV2Provider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin _InstanceDataV2Ref
    on AutoDisposeFutureProviderRef<Result<InstanceInfo, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _InstanceDataV2ProviderElement
    extends AutoDisposeFutureProviderElement<Result<InstanceInfo, ExecError>>
    with _InstanceDataV2Ref {
  _InstanceDataV2ProviderElement(super.provider);

  @override
  Profile get profile => (origin as _InstanceDataV2Provider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
