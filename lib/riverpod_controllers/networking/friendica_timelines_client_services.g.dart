// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_timelines_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$timelineHash() => r'8f994589000be1d6065bd9be0adffb55e27dc463';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [timeline].
@ProviderFor(timeline)
const timelineProvider = TimelineFamily();

/// See also [timeline].
class TimelineFamily
    extends Family<AsyncValue<Result<List<TimelineEntry>, ExecError>>> {
  /// See also [timeline].
  const TimelineFamily();

  /// See also [timeline].
  TimelineProvider call(
    Profile profile, {
    required TimelineIdentifiers type,
    required PagingData page,
  }) {
    return TimelineProvider(
      profile,
      type: type,
      page: page,
    );
  }

  @override
  TimelineProvider getProviderOverride(
    covariant TimelineProvider provider,
  ) {
    return call(
      provider.profile,
      type: provider.type,
      page: provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'timelineProvider';
}

/// See also [timeline].
class TimelineProvider
    extends AutoDisposeFutureProvider<Result<List<TimelineEntry>, ExecError>> {
  /// See also [timeline].
  TimelineProvider(
    Profile profile, {
    required TimelineIdentifiers type,
    required PagingData page,
  }) : this._internal(
          (ref) => timeline(
            ref as TimelineRef,
            profile,
            type: type,
            page: page,
          ),
          from: timelineProvider,
          name: r'timelineProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$timelineHash,
          dependencies: TimelineFamily._dependencies,
          allTransitiveDependencies: TimelineFamily._allTransitiveDependencies,
          profile: profile,
          type: type,
          page: page,
        );

  TimelineProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.type,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final TimelineIdentifiers type;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<List<TimelineEntry>, ExecError>> Function(
            TimelineRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: TimelineProvider._internal(
        (ref) => create(ref as TimelineRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        type: type,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<TimelineEntry>, ExecError>>
      createElement() {
    return _TimelineProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TimelineProvider &&
        other.profile == profile &&
        other.type == type &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, type.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin TimelineRef
    on AutoDisposeFutureProviderRef<Result<List<TimelineEntry>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `type` of this provider.
  TimelineIdentifiers get type;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _TimelineProviderElement extends AutoDisposeFutureProviderElement<
    Result<List<TimelineEntry>, ExecError>> with TimelineRef {
  _TimelineProviderElement(super.provider);

  @override
  Profile get profile => (origin as TimelineProvider).profile;
  @override
  TimelineIdentifiers get type => (origin as TimelineProvider).type;
  @override
  PagingData get page => (origin as TimelineProvider).page;
}

String _$userMediaTimelineClientHash() =>
    r'8f87005154b1c64d3f263530a5c67948e4f17d66';

/// See also [userMediaTimelineClient].
@ProviderFor(userMediaTimelineClient)
const userMediaTimelineClientProvider = UserMediaTimelineClientFamily();

/// See also [userMediaTimelineClient].
class UserMediaTimelineClientFamily extends Family<
    AsyncValue<Result<PagedResponse<List<TimelineEntry>>, ExecError>>> {
  /// See also [userMediaTimelineClient].
  const UserMediaTimelineClientFamily();

  /// See also [userMediaTimelineClient].
  UserMediaTimelineClientProvider call(
    Profile profile,
    String accountId, {
    required PagingData page,
  }) {
    return UserMediaTimelineClientProvider(
      profile,
      accountId,
      page: page,
    );
  }

  @override
  UserMediaTimelineClientProvider getProviderOverride(
    covariant UserMediaTimelineClientProvider provider,
  ) {
    return call(
      provider.profile,
      provider.accountId,
      page: provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'userMediaTimelineClientProvider';
}

/// See also [userMediaTimelineClient].
class UserMediaTimelineClientProvider extends AutoDisposeFutureProvider<
    Result<PagedResponse<List<TimelineEntry>>, ExecError>> {
  /// See also [userMediaTimelineClient].
  UserMediaTimelineClientProvider(
    Profile profile,
    String accountId, {
    required PagingData page,
  }) : this._internal(
          (ref) => userMediaTimelineClient(
            ref as UserMediaTimelineClientRef,
            profile,
            accountId,
            page: page,
          ),
          from: userMediaTimelineClientProvider,
          name: r'userMediaTimelineClientProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$userMediaTimelineClientHash,
          dependencies: UserMediaTimelineClientFamily._dependencies,
          allTransitiveDependencies:
              UserMediaTimelineClientFamily._allTransitiveDependencies,
          profile: profile,
          accountId: accountId,
          page: page,
        );

  UserMediaTimelineClientProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.accountId,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final String accountId;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<PagedResponse<List<TimelineEntry>>, ExecError>> Function(
            UserMediaTimelineClientRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: UserMediaTimelineClientProvider._internal(
        (ref) => create(ref as UserMediaTimelineClientRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        accountId: accountId,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
      Result<PagedResponse<List<TimelineEntry>>, ExecError>> createElement() {
    return _UserMediaTimelineClientProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is UserMediaTimelineClientProvider &&
        other.profile == profile &&
        other.accountId == accountId &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, accountId.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin UserMediaTimelineClientRef on AutoDisposeFutureProviderRef<
    Result<PagedResponse<List<TimelineEntry>>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `accountId` of this provider.
  String get accountId;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _UserMediaTimelineClientProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<PagedResponse<List<TimelineEntry>>, ExecError>>
    with UserMediaTimelineClientRef {
  _UserMediaTimelineClientProviderElement(super.provider);

  @override
  Profile get profile => (origin as UserMediaTimelineClientProvider).profile;
  @override
  String get accountId => (origin as UserMediaTimelineClientProvider).accountId;
  @override
  PagingData get page => (origin as UserMediaTimelineClientProvider).page;
}

String _$userPostsAndCommentsTimelineClientHash() =>
    r'086db168f64666fea09d7b6d2f20917aac6b1b53';

/// See also [userPostsAndCommentsTimelineClient].
@ProviderFor(userPostsAndCommentsTimelineClient)
const userPostsAndCommentsTimelineClientProvider =
    UserPostsAndCommentsTimelineClientFamily();

/// See also [userPostsAndCommentsTimelineClient].
class UserPostsAndCommentsTimelineClientFamily extends Family<
    AsyncValue<Result<PagedResponse<List<TimelineEntry>>, ExecError>>> {
  /// See also [userPostsAndCommentsTimelineClient].
  const UserPostsAndCommentsTimelineClientFamily();

  /// See also [userPostsAndCommentsTimelineClient].
  UserPostsAndCommentsTimelineClientProvider call(
    Profile profile,
    String accountId, {
    required PagingData page,
  }) {
    return UserPostsAndCommentsTimelineClientProvider(
      profile,
      accountId,
      page: page,
    );
  }

  @override
  UserPostsAndCommentsTimelineClientProvider getProviderOverride(
    covariant UserPostsAndCommentsTimelineClientProvider provider,
  ) {
    return call(
      provider.profile,
      provider.accountId,
      page: provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'userPostsAndCommentsTimelineClientProvider';
}

/// See also [userPostsAndCommentsTimelineClient].
class UserPostsAndCommentsTimelineClientProvider
    extends AutoDisposeFutureProvider<
        Result<PagedResponse<List<TimelineEntry>>, ExecError>> {
  /// See also [userPostsAndCommentsTimelineClient].
  UserPostsAndCommentsTimelineClientProvider(
    Profile profile,
    String accountId, {
    required PagingData page,
  }) : this._internal(
          (ref) => userPostsAndCommentsTimelineClient(
            ref as UserPostsAndCommentsTimelineClientRef,
            profile,
            accountId,
            page: page,
          ),
          from: userPostsAndCommentsTimelineClientProvider,
          name: r'userPostsAndCommentsTimelineClientProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$userPostsAndCommentsTimelineClientHash,
          dependencies: UserPostsAndCommentsTimelineClientFamily._dependencies,
          allTransitiveDependencies: UserPostsAndCommentsTimelineClientFamily
              ._allTransitiveDependencies,
          profile: profile,
          accountId: accountId,
          page: page,
        );

  UserPostsAndCommentsTimelineClientProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.accountId,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final String accountId;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<PagedResponse<List<TimelineEntry>>, ExecError>> Function(
            UserPostsAndCommentsTimelineClientRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: UserPostsAndCommentsTimelineClientProvider._internal(
        (ref) => create(ref as UserPostsAndCommentsTimelineClientRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        accountId: accountId,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
      Result<PagedResponse<List<TimelineEntry>>, ExecError>> createElement() {
    return _UserPostsAndCommentsTimelineClientProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is UserPostsAndCommentsTimelineClientProvider &&
        other.profile == profile &&
        other.accountId == accountId &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, accountId.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin UserPostsAndCommentsTimelineClientRef on AutoDisposeFutureProviderRef<
    Result<PagedResponse<List<TimelineEntry>>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `accountId` of this provider.
  String get accountId;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _UserPostsAndCommentsTimelineClientProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<PagedResponse<List<TimelineEntry>>, ExecError>>
    with UserPostsAndCommentsTimelineClientRef {
  _UserPostsAndCommentsTimelineClientProviderElement(super.provider);

  @override
  Profile get profile =>
      (origin as UserPostsAndCommentsTimelineClientProvider).profile;
  @override
  String get accountId =>
      (origin as UserPostsAndCommentsTimelineClientProvider).accountId;
  @override
  PagingData get page =>
      (origin as UserPostsAndCommentsTimelineClientProvider).page;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
