// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'link_preview_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$linkPreviewHash() => r'1b05ab037cc7337fe234227250b9e615519343b5';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [linkPreview].
@ProviderFor(linkPreview)
const linkPreviewProvider = LinkPreviewFamily();

/// See also [linkPreview].
class LinkPreviewFamily
    extends Family<AsyncValue<Result<LinkPreviewData, ExecError>>> {
  /// See also [linkPreview].
  const LinkPreviewFamily();

  /// See also [linkPreview].
  LinkPreviewProvider call(
    String url,
  ) {
    return LinkPreviewProvider(
      url,
    );
  }

  @override
  LinkPreviewProvider getProviderOverride(
    covariant LinkPreviewProvider provider,
  ) {
    return call(
      provider.url,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'linkPreviewProvider';
}

/// See also [linkPreview].
class LinkPreviewProvider
    extends AutoDisposeFutureProvider<Result<LinkPreviewData, ExecError>> {
  /// See also [linkPreview].
  LinkPreviewProvider(
    String url,
  ) : this._internal(
          (ref) => linkPreview(
            ref as LinkPreviewRef,
            url,
          ),
          from: linkPreviewProvider,
          name: r'linkPreviewProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$linkPreviewHash,
          dependencies: LinkPreviewFamily._dependencies,
          allTransitiveDependencies:
              LinkPreviewFamily._allTransitiveDependencies,
          url: url,
        );

  LinkPreviewProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.url,
  }) : super.internal();

  final String url;

  @override
  Override overrideWith(
    FutureOr<Result<LinkPreviewData, ExecError>> Function(
            LinkPreviewRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: LinkPreviewProvider._internal(
        (ref) => create(ref as LinkPreviewRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        url: url,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<LinkPreviewData, ExecError>>
      createElement() {
    return _LinkPreviewProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is LinkPreviewProvider && other.url == url;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, url.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin LinkPreviewRef
    on AutoDisposeFutureProviderRef<Result<LinkPreviewData, ExecError>> {
  /// The parameter `url` of this provider.
  String get url;
}

class _LinkPreviewProviderElement
    extends AutoDisposeFutureProviderElement<Result<LinkPreviewData, ExecError>>
    with LinkPreviewRef {
  _LinkPreviewProviderElement(super.provider);

  @override
  String get url => (origin as LinkPreviewProvider).url;
}

String _$openGraphDataHash() => r'20fdca2af6b2bf067d316dbb8a670abce3f4ec79';

/// See also [_openGraphData].
@ProviderFor(_openGraphData)
const _openGraphDataProvider = _OpenGraphDataFamily();

/// See also [_openGraphData].
class _OpenGraphDataFamily extends Family<
    AsyncValue<Result<List<MapEntry<String, String>>, ExecError>>> {
  /// See also [_openGraphData].
  const _OpenGraphDataFamily();

  /// See also [_openGraphData].
  _OpenGraphDataProvider call(
    String url,
  ) {
    return _OpenGraphDataProvider(
      url,
    );
  }

  @override
  _OpenGraphDataProvider getProviderOverride(
    covariant _OpenGraphDataProvider provider,
  ) {
    return call(
      provider.url,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'_openGraphDataProvider';
}

/// See also [_openGraphData].
class _OpenGraphDataProvider extends AutoDisposeFutureProvider<
    Result<List<MapEntry<String, String>>, ExecError>> {
  /// See also [_openGraphData].
  _OpenGraphDataProvider(
    String url,
  ) : this._internal(
          (ref) => _openGraphData(
            ref as _OpenGraphDataRef,
            url,
          ),
          from: _openGraphDataProvider,
          name: r'_openGraphDataProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$openGraphDataHash,
          dependencies: _OpenGraphDataFamily._dependencies,
          allTransitiveDependencies:
              _OpenGraphDataFamily._allTransitiveDependencies,
          url: url,
        );

  _OpenGraphDataProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.url,
  }) : super.internal();

  final String url;

  @override
  Override overrideWith(
    FutureOr<Result<List<MapEntry<String, String>>, ExecError>> Function(
            _OpenGraphDataRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: _OpenGraphDataProvider._internal(
        (ref) => create(ref as _OpenGraphDataRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        url: url,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
      Result<List<MapEntry<String, String>>, ExecError>> createElement() {
    return _OpenGraphDataProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is _OpenGraphDataProvider && other.url == url;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, url.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin _OpenGraphDataRef on AutoDisposeFutureProviderRef<
    Result<List<MapEntry<String, String>>, ExecError>> {
  /// The parameter `url` of this provider.
  String get url;
}

class _OpenGraphDataProviderElement extends AutoDisposeFutureProviderElement<
    Result<List<MapEntry<String, String>>, ExecError>> with _OpenGraphDataRef {
  _OpenGraphDataProviderElement(super.provider);

  @override
  String get url => (origin as _OpenGraphDataProvider).url;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
