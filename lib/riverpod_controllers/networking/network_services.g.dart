// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'network_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$userAgentHeaderHash() => r'bf595dbdbb2b524a050eced519769a59ad657510';

/// See also [userAgentHeader].
@ProviderFor(userAgentHeader)
final userAgentHeaderProvider =
    AutoDisposeProvider<Map<String, String>>.internal(
  userAgentHeader,
  name: r'userAgentHeaderProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$userAgentHeaderHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef UserAgentHeaderRef = AutoDisposeProviderRef<Map<String, String>>;
String _$httpGetRawBytesHash() => r'4da1dc26b044c4beda710f210fd5202f3a9e9f55';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [httpGetRawBytes].
@ProviderFor(httpGetRawBytes)
const httpGetRawBytesProvider = HttpGetRawBytesFamily();

/// See also [httpGetRawBytes].
class HttpGetRawBytesFamily
    extends Family<AsyncValue<Result<Uint8List, ExecError>>> {
  /// See also [httpGetRawBytes].
  const HttpGetRawBytesFamily();

  /// See also [httpGetRawBytes].
  HttpGetRawBytesProvider call(
    NetworkRequest request,
  ) {
    return HttpGetRawBytesProvider(
      request,
    );
  }

  @override
  HttpGetRawBytesProvider getProviderOverride(
    covariant HttpGetRawBytesProvider provider,
  ) {
    return call(
      provider.request,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'httpGetRawBytesProvider';
}

/// See also [httpGetRawBytes].
class HttpGetRawBytesProvider
    extends AutoDisposeFutureProvider<Result<Uint8List, ExecError>> {
  /// See also [httpGetRawBytes].
  HttpGetRawBytesProvider(
    NetworkRequest request,
  ) : this._internal(
          (ref) => httpGetRawBytes(
            ref as HttpGetRawBytesRef,
            request,
          ),
          from: httpGetRawBytesProvider,
          name: r'httpGetRawBytesProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$httpGetRawBytesHash,
          dependencies: HttpGetRawBytesFamily._dependencies,
          allTransitiveDependencies:
              HttpGetRawBytesFamily._allTransitiveDependencies,
          request: request,
        );

  HttpGetRawBytesProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.request,
  }) : super.internal();

  final NetworkRequest request;

  @override
  Override overrideWith(
    FutureOr<Result<Uint8List, ExecError>> Function(HttpGetRawBytesRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: HttpGetRawBytesProvider._internal(
        (ref) => create(ref as HttpGetRawBytesRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        request: request,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<Uint8List, ExecError>>
      createElement() {
    return _HttpGetRawBytesProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is HttpGetRawBytesProvider && other.request == request;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, request.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin HttpGetRawBytesRef
    on AutoDisposeFutureProviderRef<Result<Uint8List, ExecError>> {
  /// The parameter `request` of this provider.
  NetworkRequest get request;
}

class _HttpGetRawBytesProviderElement
    extends AutoDisposeFutureProviderElement<Result<Uint8List, ExecError>>
    with HttpGetRawBytesRef {
  _HttpGetRawBytesProviderElement(super.provider);

  @override
  NetworkRequest get request => (origin as HttpGetRawBytesProvider).request;
}

String _$httpGetHash() => r'187eba821340c5eee6ab48dd9144384a962784cc';

/// See also [httpGet].
@ProviderFor(httpGet)
const httpGetProvider = HttpGetFamily();

/// See also [httpGet].
class HttpGetFamily
    extends Family<AsyncValue<Result<PagedResponse<String>, ExecError>>> {
  /// See also [httpGet].
  const HttpGetFamily();

  /// See also [httpGet].
  HttpGetProvider call(
    NetworkRequest request,
  ) {
    return HttpGetProvider(
      request,
    );
  }

  @override
  HttpGetProvider getProviderOverride(
    covariant HttpGetProvider provider,
  ) {
    return call(
      provider.request,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'httpGetProvider';
}

/// See also [httpGet].
class HttpGetProvider extends AutoDisposeFutureProvider<
    Result<PagedResponse<String>, ExecError>> {
  /// See also [httpGet].
  HttpGetProvider(
    NetworkRequest request,
  ) : this._internal(
          (ref) => httpGet(
            ref as HttpGetRef,
            request,
          ),
          from: httpGetProvider,
          name: r'httpGetProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$httpGetHash,
          dependencies: HttpGetFamily._dependencies,
          allTransitiveDependencies: HttpGetFamily._allTransitiveDependencies,
          request: request,
        );

  HttpGetProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.request,
  }) : super.internal();

  final NetworkRequest request;

  @override
  Override overrideWith(
    FutureOr<Result<PagedResponse<String>, ExecError>> Function(
            HttpGetRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: HttpGetProvider._internal(
        (ref) => create(ref as HttpGetRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        request: request,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<PagedResponse<String>, ExecError>>
      createElement() {
    return _HttpGetProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is HttpGetProvider && other.request == request;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, request.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin HttpGetRef
    on AutoDisposeFutureProviderRef<Result<PagedResponse<String>, ExecError>> {
  /// The parameter `request` of this provider.
  NetworkRequest get request;
}

class _HttpGetProviderElement extends AutoDisposeFutureProviderElement<
    Result<PagedResponse<String>, ExecError>> with HttpGetRef {
  _HttpGetProviderElement(super.provider);

  @override
  NetworkRequest get request => (origin as HttpGetProvider).request;
}

String _$httpPostHash() => r'7602334df5b0a03ef49b2d5602377ae8997bfcb8';

/// See also [httpPost].
@ProviderFor(httpPost)
const httpPostProvider = HttpPostFamily();

/// See also [httpPost].
class HttpPostFamily extends Family<AsyncValue<Result<String, ExecError>>> {
  /// See also [httpPost].
  const HttpPostFamily();

  /// See also [httpPost].
  HttpPostProvider call(
    NetworkRequest request,
  ) {
    return HttpPostProvider(
      request,
    );
  }

  @override
  HttpPostProvider getProviderOverride(
    covariant HttpPostProvider provider,
  ) {
    return call(
      provider.request,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'httpPostProvider';
}

/// See also [httpPost].
class HttpPostProvider
    extends AutoDisposeFutureProvider<Result<String, ExecError>> {
  /// See also [httpPost].
  HttpPostProvider(
    NetworkRequest request,
  ) : this._internal(
          (ref) => httpPost(
            ref as HttpPostRef,
            request,
          ),
          from: httpPostProvider,
          name: r'httpPostProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$httpPostHash,
          dependencies: HttpPostFamily._dependencies,
          allTransitiveDependencies: HttpPostFamily._allTransitiveDependencies,
          request: request,
        );

  HttpPostProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.request,
  }) : super.internal();

  final NetworkRequest request;

  @override
  Override overrideWith(
    FutureOr<Result<String, ExecError>> Function(HttpPostRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: HttpPostProvider._internal(
        (ref) => create(ref as HttpPostRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        request: request,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<String, ExecError>> createElement() {
    return _HttpPostProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is HttpPostProvider && other.request == request;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, request.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin HttpPostRef on AutoDisposeFutureProviderRef<Result<String, ExecError>> {
  /// The parameter `request` of this provider.
  NetworkRequest get request;
}

class _HttpPostProviderElement
    extends AutoDisposeFutureProviderElement<Result<String, ExecError>>
    with HttpPostRef {
  _HttpPostProviderElement(super.provider);

  @override
  NetworkRequest get request => (origin as HttpPostProvider).request;
}

String _$httpPutHash() => r'2cc476a1c338340eba2eb0b76c281642f9e38b83';

/// See also [httpPut].
@ProviderFor(httpPut)
const httpPutProvider = HttpPutFamily();

/// See also [httpPut].
class HttpPutFamily extends Family<AsyncValue<Result<String, ExecError>>> {
  /// See also [httpPut].
  const HttpPutFamily();

  /// See also [httpPut].
  HttpPutProvider call(
    NetworkRequest request,
  ) {
    return HttpPutProvider(
      request,
    );
  }

  @override
  HttpPutProvider getProviderOverride(
    covariant HttpPutProvider provider,
  ) {
    return call(
      provider.request,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'httpPutProvider';
}

/// See also [httpPut].
class HttpPutProvider
    extends AutoDisposeFutureProvider<Result<String, ExecError>> {
  /// See also [httpPut].
  HttpPutProvider(
    NetworkRequest request,
  ) : this._internal(
          (ref) => httpPut(
            ref as HttpPutRef,
            request,
          ),
          from: httpPutProvider,
          name: r'httpPutProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$httpPutHash,
          dependencies: HttpPutFamily._dependencies,
          allTransitiveDependencies: HttpPutFamily._allTransitiveDependencies,
          request: request,
        );

  HttpPutProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.request,
  }) : super.internal();

  final NetworkRequest request;

  @override
  Override overrideWith(
    FutureOr<Result<String, ExecError>> Function(HttpPutRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: HttpPutProvider._internal(
        (ref) => create(ref as HttpPutRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        request: request,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<String, ExecError>> createElement() {
    return _HttpPutProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is HttpPutProvider && other.request == request;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, request.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin HttpPutRef on AutoDisposeFutureProviderRef<Result<String, ExecError>> {
  /// The parameter `request` of this provider.
  NetworkRequest get request;
}

class _HttpPutProviderElement
    extends AutoDisposeFutureProviderElement<Result<String, ExecError>>
    with HttpPutRef {
  _HttpPutProviderElement(super.provider);

  @override
  NetworkRequest get request => (origin as HttpPutProvider).request;
}

String _$httpDeleteHash() => r'78d2a3fb2aa1cb5e56509f99ec158f1258085c9d';

/// See also [httpDelete].
@ProviderFor(httpDelete)
const httpDeleteProvider = HttpDeleteFamily();

/// See also [httpDelete].
class HttpDeleteFamily extends Family<AsyncValue<Result<String, ExecError>>> {
  /// See also [httpDelete].
  const HttpDeleteFamily();

  /// See also [httpDelete].
  HttpDeleteProvider call(
    NetworkRequest request,
  ) {
    return HttpDeleteProvider(
      request,
    );
  }

  @override
  HttpDeleteProvider getProviderOverride(
    covariant HttpDeleteProvider provider,
  ) {
    return call(
      provider.request,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'httpDeleteProvider';
}

/// See also [httpDelete].
class HttpDeleteProvider
    extends AutoDisposeFutureProvider<Result<String, ExecError>> {
  /// See also [httpDelete].
  HttpDeleteProvider(
    NetworkRequest request,
  ) : this._internal(
          (ref) => httpDelete(
            ref as HttpDeleteRef,
            request,
          ),
          from: httpDeleteProvider,
          name: r'httpDeleteProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$httpDeleteHash,
          dependencies: HttpDeleteFamily._dependencies,
          allTransitiveDependencies:
              HttpDeleteFamily._allTransitiveDependencies,
          request: request,
        );

  HttpDeleteProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.request,
  }) : super.internal();

  final NetworkRequest request;

  @override
  Override overrideWith(
    FutureOr<Result<String, ExecError>> Function(HttpDeleteRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: HttpDeleteProvider._internal(
        (ref) => create(ref as HttpDeleteRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        request: request,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<String, ExecError>> createElement() {
    return _HttpDeleteProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is HttpDeleteProvider && other.request == request;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, request.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin HttpDeleteRef on AutoDisposeFutureProviderRef<Result<String, ExecError>> {
  /// The parameter `request` of this provider.
  NetworkRequest get request;
}

class _HttpDeleteProviderElement
    extends AutoDisposeFutureProviderElement<Result<String, ExecError>>
    with HttpDeleteRef {
  _HttpDeleteProviderElement(super.provider);

  @override
  NetworkRequest get request => (origin as HttpDeleteProvider).request;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
