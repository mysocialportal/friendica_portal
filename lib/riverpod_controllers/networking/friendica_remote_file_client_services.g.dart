// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_remote_file_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$remoteFileBytesHash() => r'eb88e1e1161f788a5312b5091d4c9d27b6407592';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [remoteFileBytes].
@ProviderFor(remoteFileBytes)
const remoteFileBytesProvider = RemoteFileBytesFamily();

/// See also [remoteFileBytes].
class RemoteFileBytesFamily
    extends Family<AsyncValue<Result<Uint8List, ExecError>>> {
  /// See also [remoteFileBytes].
  const RemoteFileBytesFamily();

  /// See also [remoteFileBytes].
  RemoteFileBytesProvider call(
    Profile profile,
    Uri url,
  ) {
    return RemoteFileBytesProvider(
      profile,
      url,
    );
  }

  @override
  RemoteFileBytesProvider getProviderOverride(
    covariant RemoteFileBytesProvider provider,
  ) {
    return call(
      provider.profile,
      provider.url,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'remoteFileBytesProvider';
}

/// See also [remoteFileBytes].
class RemoteFileBytesProvider
    extends AutoDisposeFutureProvider<Result<Uint8List, ExecError>> {
  /// See also [remoteFileBytes].
  RemoteFileBytesProvider(
    Profile profile,
    Uri url,
  ) : this._internal(
          (ref) => remoteFileBytes(
            ref as RemoteFileBytesRef,
            profile,
            url,
          ),
          from: remoteFileBytesProvider,
          name: r'remoteFileBytesProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$remoteFileBytesHash,
          dependencies: RemoteFileBytesFamily._dependencies,
          allTransitiveDependencies:
              RemoteFileBytesFamily._allTransitiveDependencies,
          profile: profile,
          url: url,
        );

  RemoteFileBytesProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.url,
  }) : super.internal();

  final Profile profile;
  final Uri url;

  @override
  Override overrideWith(
    FutureOr<Result<Uint8List, ExecError>> Function(RemoteFileBytesRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: RemoteFileBytesProvider._internal(
        (ref) => create(ref as RemoteFileBytesRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        url: url,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<Uint8List, ExecError>>
      createElement() {
    return _RemoteFileBytesProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is RemoteFileBytesProvider &&
        other.profile == profile &&
        other.url == url;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, url.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin RemoteFileBytesRef
    on AutoDisposeFutureProviderRef<Result<Uint8List, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `url` of this provider.
  Uri get url;
}

class _RemoteFileBytesProviderElement
    extends AutoDisposeFutureProviderElement<Result<Uint8List, ExecError>>
    with RemoteFileBytesRef {
  _RemoteFileBytesProviderElement(super.provider);

  @override
  Profile get profile => (origin as RemoteFileBytesProvider).profile;
  @override
  Uri get url => (origin as RemoteFileBytesProvider).url;
}

String _$uploadFileAsAttachmentHash() =>
    r'87f658e93336a9f477ed5575b6976caa2cdf492f';

/// See also [uploadFileAsAttachment].
@ProviderFor(uploadFileAsAttachment)
const uploadFileAsAttachmentProvider = UploadFileAsAttachmentFamily();

/// See also [uploadFileAsAttachment].
class UploadFileAsAttachmentFamily
    extends Family<AsyncValue<Result<ImageEntry, ExecError>>> {
  /// See also [uploadFileAsAttachment].
  const UploadFileAsAttachmentFamily();

  /// See also [uploadFileAsAttachment].
  UploadFileAsAttachmentProvider call(
    Profile profile, {
    required List<int> bytes,
    String description = '',
    String album = '',
    String fileName = '',
    required Visibility visibility,
  }) {
    return UploadFileAsAttachmentProvider(
      profile,
      bytes: bytes,
      description: description,
      album: album,
      fileName: fileName,
      visibility: visibility,
    );
  }

  @override
  UploadFileAsAttachmentProvider getProviderOverride(
    covariant UploadFileAsAttachmentProvider provider,
  ) {
    return call(
      provider.profile,
      bytes: provider.bytes,
      description: provider.description,
      album: provider.album,
      fileName: provider.fileName,
      visibility: provider.visibility,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'uploadFileAsAttachmentProvider';
}

/// See also [uploadFileAsAttachment].
class UploadFileAsAttachmentProvider
    extends AutoDisposeFutureProvider<Result<ImageEntry, ExecError>> {
  /// See also [uploadFileAsAttachment].
  UploadFileAsAttachmentProvider(
    Profile profile, {
    required List<int> bytes,
    String description = '',
    String album = '',
    String fileName = '',
    required Visibility visibility,
  }) : this._internal(
          (ref) => uploadFileAsAttachment(
            ref as UploadFileAsAttachmentRef,
            profile,
            bytes: bytes,
            description: description,
            album: album,
            fileName: fileName,
            visibility: visibility,
          ),
          from: uploadFileAsAttachmentProvider,
          name: r'uploadFileAsAttachmentProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$uploadFileAsAttachmentHash,
          dependencies: UploadFileAsAttachmentFamily._dependencies,
          allTransitiveDependencies:
              UploadFileAsAttachmentFamily._allTransitiveDependencies,
          profile: profile,
          bytes: bytes,
          description: description,
          album: album,
          fileName: fileName,
          visibility: visibility,
        );

  UploadFileAsAttachmentProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.bytes,
    required this.description,
    required this.album,
    required this.fileName,
    required this.visibility,
  }) : super.internal();

  final Profile profile;
  final List<int> bytes;
  final String description;
  final String album;
  final String fileName;
  final Visibility visibility;

  @override
  Override overrideWith(
    FutureOr<Result<ImageEntry, ExecError>> Function(
            UploadFileAsAttachmentRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: UploadFileAsAttachmentProvider._internal(
        (ref) => create(ref as UploadFileAsAttachmentRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        bytes: bytes,
        description: description,
        album: album,
        fileName: fileName,
        visibility: visibility,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<ImageEntry, ExecError>>
      createElement() {
    return _UploadFileAsAttachmentProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is UploadFileAsAttachmentProvider &&
        other.profile == profile &&
        other.bytes == bytes &&
        other.description == description &&
        other.album == album &&
        other.fileName == fileName &&
        other.visibility == visibility;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, bytes.hashCode);
    hash = _SystemHash.combine(hash, description.hashCode);
    hash = _SystemHash.combine(hash, album.hashCode);
    hash = _SystemHash.combine(hash, fileName.hashCode);
    hash = _SystemHash.combine(hash, visibility.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin UploadFileAsAttachmentRef
    on AutoDisposeFutureProviderRef<Result<ImageEntry, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `bytes` of this provider.
  List<int> get bytes;

  /// The parameter `description` of this provider.
  String get description;

  /// The parameter `album` of this provider.
  String get album;

  /// The parameter `fileName` of this provider.
  String get fileName;

  /// The parameter `visibility` of this provider.
  Visibility get visibility;
}

class _UploadFileAsAttachmentProviderElement
    extends AutoDisposeFutureProviderElement<Result<ImageEntry, ExecError>>
    with UploadFileAsAttachmentRef {
  _UploadFileAsAttachmentProviderElement(super.provider);

  @override
  Profile get profile => (origin as UploadFileAsAttachmentProvider).profile;
  @override
  List<int> get bytes => (origin as UploadFileAsAttachmentProvider).bytes;
  @override
  String get description =>
      (origin as UploadFileAsAttachmentProvider).description;
  @override
  String get album => (origin as UploadFileAsAttachmentProvider).album;
  @override
  String get fileName => (origin as UploadFileAsAttachmentProvider).fileName;
  @override
  Visibility get visibility =>
      (origin as UploadFileAsAttachmentProvider).visibility;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
