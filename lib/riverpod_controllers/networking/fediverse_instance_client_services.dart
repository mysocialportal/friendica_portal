import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../models/auth/profile.dart';
import '../../models/exec_error.dart';
import '../../models/instance_info.dart';
import '../../serializers/mastodon/instance_info_mastodon_extensions.dart';
import '../rp_provider_extension.dart';
import 'friendica_client_services.dart';

part 'fediverse_instance_client_services.g.dart';

final _logger = Logger('FriendicaInstanceDataClient');

@riverpod
Future<Result<InstanceInfo, ExecError>> instanceData(
  Ref ref,
  Profile profile,
) async {
  ref.cacheFor(const Duration(minutes: 10));
  _logger.finest(() => 'Getting ${profile.serverName} instance info');
  final v2Result = await ref.read(_instanceDataV2Provider(profile).future);
  if (v2Result.isSuccess) {
    return v2Result;
  }

  return ref.read(_instanceDataV1Provider(profile).future);
}

@riverpod
Future<Result<InstanceInfo, ExecError>> _instanceDataV1(
  Ref ref,
  Profile profile,
) async {
  _logger.finest(
      () => 'Getting ${profile.serverName} instance info via V1 endpoint');
  final url = Uri.parse('https://${profile.serverName}/api/v1/instance');
  return await ref
      .read(getApiRequestProvider(profile, url).future)
      .andThen((json) => fromInstanceV1Json(json))
      .execErrorCastAsync();
}

@riverpod
Future<Result<InstanceInfo, ExecError>> _instanceDataV2(
  Ref ref,
  Profile profile,
) async {
  _logger.finest(
      () => 'Getting ${profile.serverName} instance info via V2 endpoint');
  final url = Uri.parse('https://${profile.serverName}/api/v2/instance');
  return await ref
      .read(getApiRequestProvider(profile, url).future)
      .andThen((json) => fromInstanceV2Json(json))
      .execErrorCastAsync();
}
