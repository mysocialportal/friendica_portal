import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../models/auth/profile.dart';
import '../../models/exec_error.dart';
import '../../models/image_entry.dart';
import 'friendica_client_services.dart';
import 'network_services.dart';

part 'friendica_image_client_services.g.dart';

@riverpod
Future<Result<ImageEntry, ExecError>> editImageData(
  Ref ref,
  Profile profile,
  ImageEntry image,
) async {
  final uri =
      Uri.parse('https://${profile.serverName}/api/friendica/photo/update');
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final body = {
    'album': image.album,
    'desc': image.description,
    'photo_id': image.id,
  };
  final request = NetworkRequest(uri, headers: headers, body: body);

  final result =
      await ref.read(httpPostProvider(request).future).transform((_) => image);
  return result.execErrorCast();
}

@riverpod
Future<Result<ImageEntry, ExecError>> deleteImage(
  Ref ref,
  Profile profile,
  ImageEntry image,
) async {
  final uri = Uri.parse(
    'https://${profile.serverName}/api/friendica/photo/delete?photo_id=${image.id}',
  );
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(uri, headers: headers);

  final result =
      await ref.read(httpPostProvider(request).future).transform((_) => image);
  return result.execErrorCast();
}
