import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../models/auth/profile.dart';
import '../../models/exec_error.dart';
import '../../models/hashtag.dart';
import '../../serializers/mastodon/hashtag_mastodon_extensions.dart';
import '../hashtag_service.dart';
import 'friendica_client_services.dart';
import 'network_services.dart';

part 'friendica_tags_client_services.g.dart';

@riverpod
Future<Result<List<HashtagWithTrending>, ExecError>> followedTags(
    Ref ref, Profile profile) async {
  final url = 'https://${profile.serverName}/api/v1/followed_tags';
  final request = Uri.parse(url);
  final result = await ref
      .read(getApiListRequestProvider(profile, request).future)
      .transform((response) => response.data
          .map((json) => HashtagWithTrendingMastodonExtensions.fromJson(json))
          .toList());
  return result.execErrorCast();
}

@riverpod
Future<Result<HashtagWithTrending, ExecError>> followTag(
  Ref ref,
  Profile profile,
  String tagName,
) async {
  final url = 'https://${profile.serverName}/api/v1/tags/$tagName/follow';
  final headers = ref.watch(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(Uri.parse(url), headers: headers);
  final result = await ref
      .read(httpPostProvider(request).future)
      .transform((response) => jsonDecode(response))
      .transform((json) => HashtagWithTrendingMastodonExtensions.fromJson(json))
      .withResult(
    (hashtag) {
      ref
          .read(hashtagServiceProvider(profile).notifier)
          .add(hashtag.toHashtag());
      ref.invalidate(followedTagsMapProvider(profile));
    },
  );
  return result.execErrorCast();
}

@riverpod
Future<Result<HashtagWithTrending, ExecError>> unfollowTag(
  Ref ref,
  Profile profile,
  String tagName,
) async {
  final url = 'https://${profile.serverName}/api/v1/tags/$tagName/unfollow';
  final headers = ref.watch(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(Uri.parse(url), headers: headers);
  final result = await ref
      .read(httpPostProvider(request).future)
      .transform((response) => jsonDecode(response))
      .transform((json) => HashtagWithTrendingMastodonExtensions.fromJson(json))
      .withResult((hashtag) {
    ref.read(hashtagServiceProvider(profile).notifier).add(hashtag.toHashtag());
    ref.invalidate(followedTagsMapProvider(profile));
  });
  return result.execErrorCast();
}
