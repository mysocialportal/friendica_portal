// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$friendicaClientHeadersHash() =>
    r'dafcf4cedbf400da9142e44669901243fa3f7e69';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [friendicaClientHeaders].
@ProviderFor(friendicaClientHeaders)
const friendicaClientHeadersProvider = FriendicaClientHeadersFamily();

/// See also [friendicaClientHeaders].
class FriendicaClientHeadersFamily extends Family<Map<String, String>> {
  /// See also [friendicaClientHeaders].
  const FriendicaClientHeadersFamily();

  /// See also [friendicaClientHeaders].
  FriendicaClientHeadersProvider call(
    Profile profile, {
    bool withContentString = true,
    bool withAuthorization = true,
    bool withClientString = true,
  }) {
    return FriendicaClientHeadersProvider(
      profile,
      withContentString: withContentString,
      withAuthorization: withAuthorization,
      withClientString: withClientString,
    );
  }

  @override
  FriendicaClientHeadersProvider getProviderOverride(
    covariant FriendicaClientHeadersProvider provider,
  ) {
    return call(
      provider.profile,
      withContentString: provider.withContentString,
      withAuthorization: provider.withAuthorization,
      withClientString: provider.withClientString,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'friendicaClientHeadersProvider';
}

/// See also [friendicaClientHeaders].
class FriendicaClientHeadersProvider
    extends AutoDisposeProvider<Map<String, String>> {
  /// See also [friendicaClientHeaders].
  FriendicaClientHeadersProvider(
    Profile profile, {
    bool withContentString = true,
    bool withAuthorization = true,
    bool withClientString = true,
  }) : this._internal(
          (ref) => friendicaClientHeaders(
            ref as FriendicaClientHeadersRef,
            profile,
            withContentString: withContentString,
            withAuthorization: withAuthorization,
            withClientString: withClientString,
          ),
          from: friendicaClientHeadersProvider,
          name: r'friendicaClientHeadersProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$friendicaClientHeadersHash,
          dependencies: FriendicaClientHeadersFamily._dependencies,
          allTransitiveDependencies:
              FriendicaClientHeadersFamily._allTransitiveDependencies,
          profile: profile,
          withContentString: withContentString,
          withAuthorization: withAuthorization,
          withClientString: withClientString,
        );

  FriendicaClientHeadersProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.withContentString,
    required this.withAuthorization,
    required this.withClientString,
  }) : super.internal();

  final Profile profile;
  final bool withContentString;
  final bool withAuthorization;
  final bool withClientString;

  @override
  Override overrideWith(
    Map<String, String> Function(FriendicaClientHeadersRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FriendicaClientHeadersProvider._internal(
        (ref) => create(ref as FriendicaClientHeadersRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        withContentString: withContentString,
        withAuthorization: withAuthorization,
        withClientString: withClientString,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<Map<String, String>> createElement() {
    return _FriendicaClientHeadersProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FriendicaClientHeadersProvider &&
        other.profile == profile &&
        other.withContentString == withContentString &&
        other.withAuthorization == withAuthorization &&
        other.withClientString == withClientString;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, withContentString.hashCode);
    hash = _SystemHash.combine(hash, withAuthorization.hashCode);
    hash = _SystemHash.combine(hash, withClientString.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FriendicaClientHeadersRef on AutoDisposeProviderRef<Map<String, String>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `withContentString` of this provider.
  bool get withContentString;

  /// The parameter `withAuthorization` of this provider.
  bool get withAuthorization;

  /// The parameter `withClientString` of this provider.
  bool get withClientString;
}

class _FriendicaClientHeadersProviderElement
    extends AutoDisposeProviderElement<Map<String, String>>
    with FriendicaClientHeadersRef {
  _FriendicaClientHeadersProviderElement(super.provider);

  @override
  Profile get profile => (origin as FriendicaClientHeadersProvider).profile;
  @override
  bool get withContentString =>
      (origin as FriendicaClientHeadersProvider).withContentString;
  @override
  bool get withAuthorization =>
      (origin as FriendicaClientHeadersProvider).withAuthorization;
  @override
  bool get withClientString =>
      (origin as FriendicaClientHeadersProvider).withClientString;
}

String _$getApiRequestHash() => r'f569b889d9f48497fffb6dd874c6abd2fd6330b4';

/// See also [getApiRequest].
@ProviderFor(getApiRequest)
const getApiRequestProvider = GetApiRequestFamily();

/// See also [getApiRequest].
class GetApiRequestFamily
    extends Family<AsyncValue<Result<dynamic, ExecError>>> {
  /// See also [getApiRequest].
  const GetApiRequestFamily();

  /// See also [getApiRequest].
  GetApiRequestProvider call(
    Profile profile,
    Uri url, {
    Duration? timeout,
  }) {
    return GetApiRequestProvider(
      profile,
      url,
      timeout: timeout,
    );
  }

  @override
  GetApiRequestProvider getProviderOverride(
    covariant GetApiRequestProvider provider,
  ) {
    return call(
      provider.profile,
      provider.url,
      timeout: provider.timeout,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'getApiRequestProvider';
}

/// See also [getApiRequest].
class GetApiRequestProvider
    extends AutoDisposeFutureProvider<Result<dynamic, ExecError>> {
  /// See also [getApiRequest].
  GetApiRequestProvider(
    Profile profile,
    Uri url, {
    Duration? timeout,
  }) : this._internal(
          (ref) => getApiRequest(
            ref as GetApiRequestRef,
            profile,
            url,
            timeout: timeout,
          ),
          from: getApiRequestProvider,
          name: r'getApiRequestProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$getApiRequestHash,
          dependencies: GetApiRequestFamily._dependencies,
          allTransitiveDependencies:
              GetApiRequestFamily._allTransitiveDependencies,
          profile: profile,
          url: url,
          timeout: timeout,
        );

  GetApiRequestProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.url,
    required this.timeout,
  }) : super.internal();

  final Profile profile;
  final Uri url;
  final Duration? timeout;

  @override
  Override overrideWith(
    FutureOr<Result<dynamic, ExecError>> Function(GetApiRequestRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: GetApiRequestProvider._internal(
        (ref) => create(ref as GetApiRequestRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        url: url,
        timeout: timeout,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<dynamic, ExecError>> createElement() {
    return _GetApiRequestProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GetApiRequestProvider &&
        other.profile == profile &&
        other.url == url &&
        other.timeout == timeout;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, url.hashCode);
    hash = _SystemHash.combine(hash, timeout.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin GetApiRequestRef
    on AutoDisposeFutureProviderRef<Result<dynamic, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `url` of this provider.
  Uri get url;

  /// The parameter `timeout` of this provider.
  Duration? get timeout;
}

class _GetApiRequestProviderElement
    extends AutoDisposeFutureProviderElement<Result<dynamic, ExecError>>
    with GetApiRequestRef {
  _GetApiRequestProviderElement(super.provider);

  @override
  Profile get profile => (origin as GetApiRequestProvider).profile;
  @override
  Uri get url => (origin as GetApiRequestProvider).url;
  @override
  Duration? get timeout => (origin as GetApiRequestProvider).timeout;
}

String _$getApiPagedRequestHash() =>
    r'0997595092aee814a05c401744d3eb65658b78d1';

/// See also [getApiPagedRequest].
@ProviderFor(getApiPagedRequest)
const getApiPagedRequestProvider = GetApiPagedRequestFamily();

/// See also [getApiPagedRequest].
class GetApiPagedRequestFamily
    extends Family<AsyncValue<Result<PagedResponse<dynamic>, ExecError>>> {
  /// See also [getApiPagedRequest].
  const GetApiPagedRequestFamily();

  /// See also [getApiPagedRequest].
  GetApiPagedRequestProvider call(
    Profile profile,
    Uri url, {
    Duration? timeout,
  }) {
    return GetApiPagedRequestProvider(
      profile,
      url,
      timeout: timeout,
    );
  }

  @override
  GetApiPagedRequestProvider getProviderOverride(
    covariant GetApiPagedRequestProvider provider,
  ) {
    return call(
      provider.profile,
      provider.url,
      timeout: provider.timeout,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'getApiPagedRequestProvider';
}

/// See also [getApiPagedRequest].
class GetApiPagedRequestProvider extends AutoDisposeFutureProvider<
    Result<PagedResponse<dynamic>, ExecError>> {
  /// See also [getApiPagedRequest].
  GetApiPagedRequestProvider(
    Profile profile,
    Uri url, {
    Duration? timeout,
  }) : this._internal(
          (ref) => getApiPagedRequest(
            ref as GetApiPagedRequestRef,
            profile,
            url,
            timeout: timeout,
          ),
          from: getApiPagedRequestProvider,
          name: r'getApiPagedRequestProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$getApiPagedRequestHash,
          dependencies: GetApiPagedRequestFamily._dependencies,
          allTransitiveDependencies:
              GetApiPagedRequestFamily._allTransitiveDependencies,
          profile: profile,
          url: url,
          timeout: timeout,
        );

  GetApiPagedRequestProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.url,
    required this.timeout,
  }) : super.internal();

  final Profile profile;
  final Uri url;
  final Duration? timeout;

  @override
  Override overrideWith(
    FutureOr<Result<PagedResponse<dynamic>, ExecError>> Function(
            GetApiPagedRequestRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: GetApiPagedRequestProvider._internal(
        (ref) => create(ref as GetApiPagedRequestRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        url: url,
        timeout: timeout,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<PagedResponse<dynamic>, ExecError>>
      createElement() {
    return _GetApiPagedRequestProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GetApiPagedRequestProvider &&
        other.profile == profile &&
        other.url == url &&
        other.timeout == timeout;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, url.hashCode);
    hash = _SystemHash.combine(hash, timeout.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin GetApiPagedRequestRef
    on AutoDisposeFutureProviderRef<Result<PagedResponse<dynamic>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `url` of this provider.
  Uri get url;

  /// The parameter `timeout` of this provider.
  Duration? get timeout;
}

class _GetApiPagedRequestProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<PagedResponse<dynamic>, ExecError>> with GetApiPagedRequestRef {
  _GetApiPagedRequestProviderElement(super.provider);

  @override
  Profile get profile => (origin as GetApiPagedRequestProvider).profile;
  @override
  Uri get url => (origin as GetApiPagedRequestProvider).url;
  @override
  Duration? get timeout => (origin as GetApiPagedRequestProvider).timeout;
}

String _$getApiListRequestHash() => r'dced9ce278bb151aa55ad936cb35795e7973e7b7';

/// See also [getApiListRequest].
@ProviderFor(getApiListRequest)
const getApiListRequestProvider = GetApiListRequestFamily();

/// See also [getApiListRequest].
class GetApiListRequestFamily extends Family<
    AsyncValue<Result<PagedResponse<List<dynamic>>, ExecError>>> {
  /// See also [getApiListRequest].
  const GetApiListRequestFamily();

  /// See also [getApiListRequest].
  GetApiListRequestProvider call(
    Profile profile,
    Uri url, {
    Duration? timeout,
  }) {
    return GetApiListRequestProvider(
      profile,
      url,
      timeout: timeout,
    );
  }

  @override
  GetApiListRequestProvider getProviderOverride(
    covariant GetApiListRequestProvider provider,
  ) {
    return call(
      provider.profile,
      provider.url,
      timeout: provider.timeout,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'getApiListRequestProvider';
}

/// See also [getApiListRequest].
class GetApiListRequestProvider extends AutoDisposeFutureProvider<
    Result<PagedResponse<List<dynamic>>, ExecError>> {
  /// See also [getApiListRequest].
  GetApiListRequestProvider(
    Profile profile,
    Uri url, {
    Duration? timeout,
  }) : this._internal(
          (ref) => getApiListRequest(
            ref as GetApiListRequestRef,
            profile,
            url,
            timeout: timeout,
          ),
          from: getApiListRequestProvider,
          name: r'getApiListRequestProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$getApiListRequestHash,
          dependencies: GetApiListRequestFamily._dependencies,
          allTransitiveDependencies:
              GetApiListRequestFamily._allTransitiveDependencies,
          profile: profile,
          url: url,
          timeout: timeout,
        );

  GetApiListRequestProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.url,
    required this.timeout,
  }) : super.internal();

  final Profile profile;
  final Uri url;
  final Duration? timeout;

  @override
  Override overrideWith(
    FutureOr<Result<PagedResponse<List<dynamic>>, ExecError>> Function(
            GetApiListRequestRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: GetApiListRequestProvider._internal(
        (ref) => create(ref as GetApiListRequestRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        url: url,
        timeout: timeout,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
      Result<PagedResponse<List<dynamic>>, ExecError>> createElement() {
    return _GetApiListRequestProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GetApiListRequestProvider &&
        other.profile == profile &&
        other.url == url &&
        other.timeout == timeout;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, url.hashCode);
    hash = _SystemHash.combine(hash, timeout.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin GetApiListRequestRef on AutoDisposeFutureProviderRef<
    Result<PagedResponse<List<dynamic>>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `url` of this provider.
  Uri get url;

  /// The parameter `timeout` of this provider.
  Duration? get timeout;
}

class _GetApiListRequestProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<PagedResponse<List<dynamic>>, ExecError>>
    with GetApiListRequestRef {
  _GetApiListRequestProviderElement(super.provider);

  @override
  Profile get profile => (origin as GetApiListRequestProvider).profile;
  @override
  Uri get url => (origin as GetApiListRequestProvider).url;
  @override
  Duration? get timeout => (origin as GetApiListRequestProvider).timeout;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
