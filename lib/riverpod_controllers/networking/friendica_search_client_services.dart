import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../models/auth/profile.dart';
import '../../models/exec_error.dart';
import '../../models/networking/paged_response.dart';
import '../../models/networking/paging_data.dart';
import '../../models/search_results.dart';
import '../../models/search_types.dart';
import '../../serializers/mastodon/search_result_mastodon_extensions.dart';
import '../fediverse_server_validator_services.dart';
import 'friendica_client_services.dart';
import 'network_status_services.dart';

part 'friendica_search_client_services.g.dart';

final _logger = Logger('FriendicaSearchClient');

@riverpod
Future<Result<PagedResponse<SearchResults>, ExecError>> searchResults(
    Ref ref,
    Profile profile,
    SearchTypes type,
    String searchTerm,
    PagingData page) async {
  _logger.finest(() => 'Searching $type for  term: $searchTerm');

  if (type == SearchTypes.directLink) {
    final isFediverseResult =
        await ref.read(checkIfFromFediverseProvider(searchTerm).future);
    if (isFediverseResult.isFailure) {
      return isFediverseResult.errorCast();
    }

    if (!isFediverseResult.value) {
      return buildErrorResult(
        type: ErrorType.parsingError,
        message: 'URL appears to not be to a fediverse server: $searchTerm',
      );
    }
  }
  ref.read(searchLoadingStatusProvider(profile));
  Future.microtask(
    () async => ref.read(searchLoadingStatusProvider(profile).notifier).begin(),
  );

  var query = searchTerm.trim();
  // query sanitazation?
  if (query.startsWith('#')) {
    query = query.replaceFirst('#', '%23');
  }
  final url =
      'https://${profile.serverName}/api/v1/search?${page.toQueryParameters()}&${type.toQueryParameters()}&q=$query';
  final result = await ref.read(getApiPagedRequestProvider(
    profile,
    Uri.parse(url),
  ).future);

  ref.read(searchLoadingStatusProvider(profile).notifier).end();
  return result
      .andThenSuccess((response) =>
          response.map((json) => searchResultFromJson(ref, profile, json)))
      .execErrorCast();
}
