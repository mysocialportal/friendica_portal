import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../models/auth/profile.dart';
import '../../models/exec_error.dart';
import '../../models/image_entry.dart';
import '../../models/media_attachment_uploads/image_types_enum.dart';
import '../../models/visibility.dart';
import '../../serializers/friendica/image_entry_friendica_extensions.dart';
import '../../serializers/friendica/visibility_friendica_extensions.dart';
import 'friendica_client_services.dart';
import 'network_services.dart';

part 'friendica_remote_file_client_services.g.dart';

final _logger = Logger('FriendicaRemoteFileClient');

@riverpod
Future<Result<Uint8List, ExecError>> remoteFileBytes(
  Ref ref,
  Profile profile,
  Uri url,
) async {
  _logger.finest('GET: $url');
  try {
    final headers = ref.read(friendicaClientHeadersProvider(profile));
    final request = NetworkRequest(url, headers: headers);
    return ref.read(httpGetRawBytesProvider(request).future);
  } catch (e) {
    return Result.error(
        ExecError(type: ErrorType.localError, message: e.toString()));
  }
}

@riverpod
Future<Result<ImageEntry, ExecError>> uploadFileAsAttachment(
  Ref ref,
  Profile profile, {
  required List<int> bytes,
  String description = '',
  String album = '',
  String fileName = '',
  required Visibility visibility,
}) async {
  final postUri =
      Uri.parse('https://${profile.serverName}/api/friendica/photo/create');
  final headers = ref.read(friendicaClientHeadersProvider(
    profile,
    withContentString: false,
  ));
  final request = http.MultipartRequest('POST', postUri);
  request.headers.addAll(headers);
  request.fields['desc'] = description;
  request.fields['album'] = album;
  request.fields.addAll(visibility.toMapEntries());
  request.files.add(http.MultipartFile.fromBytes(
      'media',
      filename: fileName,
      contentType:
          MediaType.parse('image/${ImageTypes.fromExtension(fileName).name}'),
      bytes));
  final response = await request.send();
  final body = utf8.decode(await response.stream.toBytes());
  if (response.statusCode != 200) {
    return Result.error(
      ExecError(
        type: ErrorType.missingEndpoint,
        message: body,
      ),
    );
  }

  final imageDataJson = jsonDecode(body);
  final newImageData = ImageEntryFriendicaExtension.fromJson(imageDataJson);

  return Result.ok(newImageData);
}
