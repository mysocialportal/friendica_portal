import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../globals.dart';
import '../../models/auth/profile.dart';
import '../../models/exec_error.dart';
import '../../models/networking/paged_response.dart';
import '../../models/networking/paging_data.dart';
import '../../models/timeline_entry.dart';
import '../../models/timeline_identifiers.dart';
import '../../serializers/mastodon/timeline_entry_mastodon_extensions.dart';
import 'friendica_client_services.dart';
import 'network_status_services.dart';

part 'friendica_timelines_client_services.g.dart';

final _logger = Logger('FriendicaTimelineClient');

@riverpod
Future<Result<List<TimelineEntry>, ExecError>> timeline(
  Ref ref,
  Profile profile, {
  required TimelineIdentifiers type,
  required PagingData page,
}) async {
  ref.read(timelineLoadingStatusProvider(profile, type));
  Future.microtask(
    () async =>
        ref.read(timelineLoadingStatusProvider(profile, type).notifier).begin(),
  );

  final String timelinePath = _typeToTimelinePath(profile, type);
  final String timelineQPs = _typeToTimelineQueryParameters(type);
  final baseUrl = 'https://${profile.serverName}/api/v1/$timelinePath';
  final url =
      '$baseUrl?exclude_replies=true&${page.toQueryParameters()}&$timelineQPs';
  final request = Uri.parse(url);
  _logger.info(() => 'Getting ${type.toHumanKey()} with paging data $page');
  final result = await ref
      .read(getApiListRequestProvider(profile, request).future)
      .transformAsync(
        (postsJson) async =>
            await _timelineEntriesFromJson(ref, profile, postsJson.data),
      );
  ref.read(timelineLoadingStatusProvider(profile, type).notifier).end();
  return result.execErrorCast();
}

@riverpod
Future<Result<PagedResponse<List<TimelineEntry>>, ExecError>>
    userMediaTimelineClient(
  Ref ref,
  Profile profile,
  String accountId, {
  required PagingData page,
}) async {
  final type =
      TimelineIdentifiers(timeline: TimelineType.profile, auxData: accountId);
  ref.read(timelineLoadingStatusProvider(profile, type));
  Future.microtask(
    () async =>
        ref.read(timelineLoadingStatusProvider(profile, type).notifier).begin(),
  );

  final baseUrl =
      'https://${profile.serverName}/api/v1/accounts/$accountId/statuses';
  final url = '$baseUrl?only_media=true&${page.toQueryParameters()}';
  final request = Uri.parse(url);
  _logger.info(
      () => 'Getting ${type.toHumanKey()} media only with paging data $page');
  final result = await ref
      .read(getApiListRequestProvider(profile, request).future)
      .transformAsync((response) async {
    final entries = await _timelineEntriesFromJson(ref, profile, response.data);
    return response.map((_) => entries);
  });
  ref.read(timelineLoadingStatusProvider(profile, type).notifier).end();
  return result.execErrorCast();
}

@riverpod
Future<Result<PagedResponse<List<TimelineEntry>>, ExecError>>
    userPostsAndCommentsTimelineClient(
  Ref ref,
  Profile profile,
  String accountId, {
  required PagingData page,
}) async {
  final type =
      TimelineIdentifiers(timeline: TimelineType.profile, auxData: accountId);
  ref.read(timelineLoadingStatusProvider(profile, type));
  Future.microtask(
    () async =>
        ref.read(timelineLoadingStatusProvider(profile, type).notifier).begin(),
  );

  final baseUrl =
      'https://${profile.serverName}/api/v1/accounts/$accountId/statuses';
  final url = '$baseUrl?${page.toQueryParameters()}';
  final request = Uri.parse(url);
  _logger.info(() =>
      'Getting ${type.toHumanKey()} posts and comments only with paging data $page');
  final result = await ref
      .read(getApiListRequestProvider(profile, request).future)
      .transformAsync((response) async {
    final entries = await _timelineEntriesFromJson(ref, profile, response.data);
    return response.map((_) => entries);
  });
  ref.read(timelineLoadingStatusProvider(profile, type).notifier).end();
  return result.execErrorCast();
}

Future<List<TimelineEntry>> _timelineEntriesFromJson(
  Ref ref,
  Profile profile,
  List<dynamic> postsJson,
) async {
  final finalResult = <TimelineEntry>[];
  final st = Stopwatch()..start();
  for (final json in postsJson) {
    if (st.elapsedMilliseconds > maxProcessingMillis) {
      await Future.delayed(processingSleep, () => st.reset());
    }
    finalResult.add(timelineEntryFromJson(ref, profile, json));
  }

  return finalResult;
}

String _typeToTimelinePath(Profile profile, TimelineIdentifiers type) {
  switch (type.timeline) {
    case TimelineType.home:
      return 'timelines/home';
    case TimelineType.global:
      return 'timelines/public';
    case TimelineType.local:
      return 'timelines/public';
    case TimelineType.circle:
      return 'timelines/list/${type.auxData}';
    case TimelineType.profile:
      return '/accounts/${type.auxData}/statuses';
    case TimelineType.self:
      final myId = profile.userId;
      return '/accounts/$myId/statuses';
  }
}

String _typeToTimelineQueryParameters(TimelineIdentifiers type) {
  switch (type.timeline) {
    case TimelineType.home:
    case TimelineType.global:
    case TimelineType.profile:
    case TimelineType.circle:
    case TimelineType.self:
      return '';
    case TimelineType.local:
      return 'local=true';
  }
}
