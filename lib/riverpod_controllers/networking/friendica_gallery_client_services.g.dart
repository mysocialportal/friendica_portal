// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_gallery_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$galleryDataHash() => r'1481054ebd86106d25abe05d427759516b0bddb4';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [galleryData].
@ProviderFor(galleryData)
const galleryDataProvider = GalleryDataFamily();

/// See also [galleryData].
class GalleryDataFamily
    extends Family<AsyncValue<Result<List<GalleryData>, ExecError>>> {
  /// See also [galleryData].
  const GalleryDataFamily();

  /// See also [galleryData].
  GalleryDataProvider call(
    Profile profile,
  ) {
    return GalleryDataProvider(
      profile,
    );
  }

  @override
  GalleryDataProvider getProviderOverride(
    covariant GalleryDataProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'galleryDataProvider';
}

/// See also [galleryData].
class GalleryDataProvider
    extends AutoDisposeFutureProvider<Result<List<GalleryData>, ExecError>> {
  /// See also [galleryData].
  GalleryDataProvider(
    Profile profile,
  ) : this._internal(
          (ref) => galleryData(
            ref as GalleryDataRef,
            profile,
          ),
          from: galleryDataProvider,
          name: r'galleryDataProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$galleryDataHash,
          dependencies: GalleryDataFamily._dependencies,
          allTransitiveDependencies:
              GalleryDataFamily._allTransitiveDependencies,
          profile: profile,
        );

  GalleryDataProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    FutureOr<Result<List<GalleryData>, ExecError>> Function(
            GalleryDataRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: GalleryDataProvider._internal(
        (ref) => create(ref as GalleryDataRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<GalleryData>, ExecError>>
      createElement() {
    return _GalleryDataProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GalleryDataProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin GalleryDataRef
    on AutoDisposeFutureProviderRef<Result<List<GalleryData>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _GalleryDataProviderElement extends AutoDisposeFutureProviderElement<
    Result<List<GalleryData>, ExecError>> with GalleryDataRef {
  _GalleryDataProviderElement(super.provider);

  @override
  Profile get profile => (origin as GalleryDataProvider).profile;
}

String _$galleryImagesClientHash() =>
    r'7aa11185e9cd9fef7ba1e2f2a470a0a380d950fe';

/// See also [galleryImagesClient].
@ProviderFor(galleryImagesClient)
const galleryImagesClientProvider = GalleryImagesClientFamily();

/// See also [galleryImagesClient].
class GalleryImagesClientFamily
    extends Family<AsyncValue<Result<List<ImageEntry>, ExecError>>> {
  /// See also [galleryImagesClient].
  const GalleryImagesClientFamily();

  /// See also [galleryImagesClient].
  GalleryImagesClientProvider call(
    Profile profile,
    String galleryName,
    PagingData page,
  ) {
    return GalleryImagesClientProvider(
      profile,
      galleryName,
      page,
    );
  }

  @override
  GalleryImagesClientProvider getProviderOverride(
    covariant GalleryImagesClientProvider provider,
  ) {
    return call(
      provider.profile,
      provider.galleryName,
      provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'galleryImagesClientProvider';
}

/// See also [galleryImagesClient].
class GalleryImagesClientProvider
    extends AutoDisposeFutureProvider<Result<List<ImageEntry>, ExecError>> {
  /// See also [galleryImagesClient].
  GalleryImagesClientProvider(
    Profile profile,
    String galleryName,
    PagingData page,
  ) : this._internal(
          (ref) => galleryImagesClient(
            ref as GalleryImagesClientRef,
            profile,
            galleryName,
            page,
          ),
          from: galleryImagesClientProvider,
          name: r'galleryImagesClientProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$galleryImagesClientHash,
          dependencies: GalleryImagesClientFamily._dependencies,
          allTransitiveDependencies:
              GalleryImagesClientFamily._allTransitiveDependencies,
          profile: profile,
          galleryName: galleryName,
          page: page,
        );

  GalleryImagesClientProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.galleryName,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final String galleryName;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<List<ImageEntry>, ExecError>> Function(
            GalleryImagesClientRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: GalleryImagesClientProvider._internal(
        (ref) => create(ref as GalleryImagesClientRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        galleryName: galleryName,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<ImageEntry>, ExecError>>
      createElement() {
    return _GalleryImagesClientProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GalleryImagesClientProvider &&
        other.profile == profile &&
        other.galleryName == galleryName &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, galleryName.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin GalleryImagesClientRef
    on AutoDisposeFutureProviderRef<Result<List<ImageEntry>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `galleryName` of this provider.
  String get galleryName;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _GalleryImagesClientProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<List<ImageEntry>, ExecError>> with GalleryImagesClientRef {
  _GalleryImagesClientProviderElement(super.provider);

  @override
  Profile get profile => (origin as GalleryImagesClientProvider).profile;
  @override
  String get galleryName => (origin as GalleryImagesClientProvider).galleryName;
  @override
  PagingData get page => (origin as GalleryImagesClientProvider).page;
}

String _$renameGalleryHash() => r'56541f3327feadaa753add74bbcd4171a9b53924';

/// See also [renameGallery].
@ProviderFor(renameGallery)
const renameGalleryProvider = RenameGalleryFamily();

/// See also [renameGallery].
class RenameGalleryFamily extends Family<AsyncValue<Result<bool, ExecError>>> {
  /// See also [renameGallery].
  const RenameGalleryFamily();

  /// See also [renameGallery].
  RenameGalleryProvider call(
    Profile profile,
    String oldGalleryName,
    String newGalleryName,
  ) {
    return RenameGalleryProvider(
      profile,
      oldGalleryName,
      newGalleryName,
    );
  }

  @override
  RenameGalleryProvider getProviderOverride(
    covariant RenameGalleryProvider provider,
  ) {
    return call(
      provider.profile,
      provider.oldGalleryName,
      provider.newGalleryName,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'renameGalleryProvider';
}

/// See also [renameGallery].
class RenameGalleryProvider
    extends AutoDisposeFutureProvider<Result<bool, ExecError>> {
  /// See also [renameGallery].
  RenameGalleryProvider(
    Profile profile,
    String oldGalleryName,
    String newGalleryName,
  ) : this._internal(
          (ref) => renameGallery(
            ref as RenameGalleryRef,
            profile,
            oldGalleryName,
            newGalleryName,
          ),
          from: renameGalleryProvider,
          name: r'renameGalleryProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$renameGalleryHash,
          dependencies: RenameGalleryFamily._dependencies,
          allTransitiveDependencies:
              RenameGalleryFamily._allTransitiveDependencies,
          profile: profile,
          oldGalleryName: oldGalleryName,
          newGalleryName: newGalleryName,
        );

  RenameGalleryProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.oldGalleryName,
    required this.newGalleryName,
  }) : super.internal();

  final Profile profile;
  final String oldGalleryName;
  final String newGalleryName;

  @override
  Override overrideWith(
    FutureOr<Result<bool, ExecError>> Function(RenameGalleryRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: RenameGalleryProvider._internal(
        (ref) => create(ref as RenameGalleryRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        oldGalleryName: oldGalleryName,
        newGalleryName: newGalleryName,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<bool, ExecError>> createElement() {
    return _RenameGalleryProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is RenameGalleryProvider &&
        other.profile == profile &&
        other.oldGalleryName == oldGalleryName &&
        other.newGalleryName == newGalleryName;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, oldGalleryName.hashCode);
    hash = _SystemHash.combine(hash, newGalleryName.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin RenameGalleryRef
    on AutoDisposeFutureProviderRef<Result<bool, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `oldGalleryName` of this provider.
  String get oldGalleryName;

  /// The parameter `newGalleryName` of this provider.
  String get newGalleryName;
}

class _RenameGalleryProviderElement
    extends AutoDisposeFutureProviderElement<Result<bool, ExecError>>
    with RenameGalleryRef {
  _RenameGalleryProviderElement(super.provider);

  @override
  Profile get profile => (origin as RenameGalleryProvider).profile;
  @override
  String get oldGalleryName => (origin as RenameGalleryProvider).oldGalleryName;
  @override
  String get newGalleryName => (origin as RenameGalleryProvider).newGalleryName;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
