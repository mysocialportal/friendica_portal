// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_search_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$searchResultsHash() => r'a8a19c803d771b9da969d30cf69827f271b4f774';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [searchResults].
@ProviderFor(searchResults)
const searchResultsProvider = SearchResultsFamily();

/// See also [searchResults].
class SearchResultsFamily extends Family<
    AsyncValue<Result<PagedResponse<SearchResults>, ExecError>>> {
  /// See also [searchResults].
  const SearchResultsFamily();

  /// See also [searchResults].
  SearchResultsProvider call(
    Profile profile,
    SearchTypes type,
    String searchTerm,
    PagingData page,
  ) {
    return SearchResultsProvider(
      profile,
      type,
      searchTerm,
      page,
    );
  }

  @override
  SearchResultsProvider getProviderOverride(
    covariant SearchResultsProvider provider,
  ) {
    return call(
      provider.profile,
      provider.type,
      provider.searchTerm,
      provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'searchResultsProvider';
}

/// See also [searchResults].
class SearchResultsProvider extends AutoDisposeFutureProvider<
    Result<PagedResponse<SearchResults>, ExecError>> {
  /// See also [searchResults].
  SearchResultsProvider(
    Profile profile,
    SearchTypes type,
    String searchTerm,
    PagingData page,
  ) : this._internal(
          (ref) => searchResults(
            ref as SearchResultsRef,
            profile,
            type,
            searchTerm,
            page,
          ),
          from: searchResultsProvider,
          name: r'searchResultsProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$searchResultsHash,
          dependencies: SearchResultsFamily._dependencies,
          allTransitiveDependencies:
              SearchResultsFamily._allTransitiveDependencies,
          profile: profile,
          type: type,
          searchTerm: searchTerm,
          page: page,
        );

  SearchResultsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.type,
    required this.searchTerm,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final SearchTypes type;
  final String searchTerm;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<PagedResponse<SearchResults>, ExecError>> Function(
            SearchResultsRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: SearchResultsProvider._internal(
        (ref) => create(ref as SearchResultsRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        type: type,
        searchTerm: searchTerm,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
      Result<PagedResponse<SearchResults>, ExecError>> createElement() {
    return _SearchResultsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SearchResultsProvider &&
        other.profile == profile &&
        other.type == type &&
        other.searchTerm == searchTerm &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, type.hashCode);
    hash = _SystemHash.combine(hash, searchTerm.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin SearchResultsRef on AutoDisposeFutureProviderRef<
    Result<PagedResponse<SearchResults>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `type` of this provider.
  SearchTypes get type;

  /// The parameter `searchTerm` of this provider.
  String get searchTerm;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _SearchResultsProviderElement extends AutoDisposeFutureProviderElement<
    Result<PagedResponse<SearchResults>, ExecError>> with SearchResultsRef {
  _SearchResultsProviderElement(super.provider);

  @override
  Profile get profile => (origin as SearchResultsProvider).profile;
  @override
  SearchTypes get type => (origin as SearchResultsProvider).type;
  @override
  String get searchTerm => (origin as SearchResultsProvider).searchTerm;
  @override
  PagingData get page => (origin as SearchResultsProvider).page;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
