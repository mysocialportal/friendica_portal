import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../globals.dart';
import '../../models/auth/profile.dart';
import '../../models/connection.dart';
import '../../models/exec_error.dart';
import '../../serializers/mastodon/connection_mastodon_extensions.dart';
import 'friendica_client_services.dart';

part 'friendica_profile_client_services.g.dart';

final _logger = Logger('FriendicaProfileClient');

@riverpod
Future<Result<(Connection, Profile), ExecError>> myProfile(
  Ref ref,
  Profile profile,
) async {
  _logger.finest(() => 'Getting logged in user profile');
  final url = Uri.parse(
      'https://${profile.serverName}/api/v1/accounts/verify_credentials');
  return (await ref.read(
          getApiRequestProvider(profile, url, timeout: oauthTimeout).future))
      .transform((json) {
    final connection = connectionFromJson(
      ref,
      profile,
      json,
      defaultServerName: profile.serverName,
    ).copy(
      status: ConnectionStatus.you,
      network: 'friendica',
    );
    return (connection, profile);
  }).execErrorCast();
}
