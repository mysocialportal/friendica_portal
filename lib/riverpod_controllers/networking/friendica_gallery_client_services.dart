import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../models/auth/profile.dart';
import '../../models/exec_error.dart';
import '../../models/gallery_data.dart';
import '../../models/image_entry.dart';
import '../../models/networking/paging_data.dart';
import '../../serializers/friendica/gallery_data_friendica_extensions.dart';
import '../../serializers/friendica/image_entry_friendica_extensions.dart';
import 'friendica_client_services.dart';
import 'network_services.dart';

part 'friendica_gallery_client_services.g.dart';

final _logger = Logger('FriendicaGalleryClient');

// TODO Convert Albums to using paging for real
@riverpod
Future<Result<List<GalleryData>, ExecError>> galleryData(
  Ref ref,
  Profile profile,
) async {
  _logger.finest(() => 'Getting gallery data');
  final url = 'https://${profile.serverName}/api/friendica/photoalbums';
  final request = Uri.parse(url);
  final result = (await ref
          .read(getApiListRequestProvider(profile, request).future)
          .andThenSuccessAsync((albumsJson) async => albumsJson.data
              .map((json) => GalleryDataFriendicaExtensions.fromJson(json))
              .toList()))
      .execErrorCast();
  return result;
}

// TODO Convert Gallery Images to using paging for real once server side available
@riverpod
Future<Result<List<ImageEntry>, ExecError>> galleryImagesClient(
  Ref ref,
  Profile profile,
  String galleryName,
  PagingData page,
) async {
  _logger.finest(() => 'Getting gallery $galleryName data with page: $page');
  final baseUrl = 'https://${profile.serverName}/api/friendica/photoalbum?';
  final gallery = 'album=$galleryName&latest_first=true';
  final pageParams = page.toQueryParameters();
  final url = '$baseUrl$gallery&$pageParams';
  final request = Uri.parse(url);
  final result = (await ref
          .read(getApiListRequestProvider(profile, request).future)
          .andThenSuccessAsync((imagesJson) async => imagesJson.data
              .map((json) => ImageEntryFriendicaExtension.fromJson(json))
              .toList()))
      .execErrorCast();
  return result;
}

@riverpod
Future<Result<bool, ExecError>> renameGallery(
  Ref ref,
  Profile profile,
  String oldGalleryName,
  String newGalleryName,
) async {
  _logger.finest(() => 'Getting gallery data');
  final url = Uri.parse(
      'https://${profile.serverName}/api/friendica/photoalbum/update');
  final body = {
    'album': oldGalleryName,
    'album_new': newGalleryName,
  };
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(
    url,
    headers: headers,
    body: body,
  );
  final result =
      await ref.read(httpPostProvider(request).future).transform((_) => true);
  return result.execErrorCast();
}
