// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_image_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$editImageDataHash() => r'09dff447610d4b3b9cc42c191f2f0ff7120c3cc3';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [editImageData].
@ProviderFor(editImageData)
const editImageDataProvider = EditImageDataFamily();

/// See also [editImageData].
class EditImageDataFamily
    extends Family<AsyncValue<Result<ImageEntry, ExecError>>> {
  /// See also [editImageData].
  const EditImageDataFamily();

  /// See also [editImageData].
  EditImageDataProvider call(
    Profile profile,
    ImageEntry image,
  ) {
    return EditImageDataProvider(
      profile,
      image,
    );
  }

  @override
  EditImageDataProvider getProviderOverride(
    covariant EditImageDataProvider provider,
  ) {
    return call(
      provider.profile,
      provider.image,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'editImageDataProvider';
}

/// See also [editImageData].
class EditImageDataProvider
    extends AutoDisposeFutureProvider<Result<ImageEntry, ExecError>> {
  /// See also [editImageData].
  EditImageDataProvider(
    Profile profile,
    ImageEntry image,
  ) : this._internal(
          (ref) => editImageData(
            ref as EditImageDataRef,
            profile,
            image,
          ),
          from: editImageDataProvider,
          name: r'editImageDataProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$editImageDataHash,
          dependencies: EditImageDataFamily._dependencies,
          allTransitiveDependencies:
              EditImageDataFamily._allTransitiveDependencies,
          profile: profile,
          image: image,
        );

  EditImageDataProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.image,
  }) : super.internal();

  final Profile profile;
  final ImageEntry image;

  @override
  Override overrideWith(
    FutureOr<Result<ImageEntry, ExecError>> Function(EditImageDataRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: EditImageDataProvider._internal(
        (ref) => create(ref as EditImageDataRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        image: image,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<ImageEntry, ExecError>>
      createElement() {
    return _EditImageDataProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is EditImageDataProvider &&
        other.profile == profile &&
        other.image == image;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, image.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin EditImageDataRef
    on AutoDisposeFutureProviderRef<Result<ImageEntry, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `image` of this provider.
  ImageEntry get image;
}

class _EditImageDataProviderElement
    extends AutoDisposeFutureProviderElement<Result<ImageEntry, ExecError>>
    with EditImageDataRef {
  _EditImageDataProviderElement(super.provider);

  @override
  Profile get profile => (origin as EditImageDataProvider).profile;
  @override
  ImageEntry get image => (origin as EditImageDataProvider).image;
}

String _$deleteImageHash() => r'ace87bc6737b6b72a807e2d4a404911ee575c11f';

/// See also [deleteImage].
@ProviderFor(deleteImage)
const deleteImageProvider = DeleteImageFamily();

/// See also [deleteImage].
class DeleteImageFamily
    extends Family<AsyncValue<Result<ImageEntry, ExecError>>> {
  /// See also [deleteImage].
  const DeleteImageFamily();

  /// See also [deleteImage].
  DeleteImageProvider call(
    Profile profile,
    ImageEntry image,
  ) {
    return DeleteImageProvider(
      profile,
      image,
    );
  }

  @override
  DeleteImageProvider getProviderOverride(
    covariant DeleteImageProvider provider,
  ) {
    return call(
      provider.profile,
      provider.image,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'deleteImageProvider';
}

/// See also [deleteImage].
class DeleteImageProvider
    extends AutoDisposeFutureProvider<Result<ImageEntry, ExecError>> {
  /// See also [deleteImage].
  DeleteImageProvider(
    Profile profile,
    ImageEntry image,
  ) : this._internal(
          (ref) => deleteImage(
            ref as DeleteImageRef,
            profile,
            image,
          ),
          from: deleteImageProvider,
          name: r'deleteImageProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$deleteImageHash,
          dependencies: DeleteImageFamily._dependencies,
          allTransitiveDependencies:
              DeleteImageFamily._allTransitiveDependencies,
          profile: profile,
          image: image,
        );

  DeleteImageProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.image,
  }) : super.internal();

  final Profile profile;
  final ImageEntry image;

  @override
  Override overrideWith(
    FutureOr<Result<ImageEntry, ExecError>> Function(DeleteImageRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: DeleteImageProvider._internal(
        (ref) => create(ref as DeleteImageRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        image: image,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<ImageEntry, ExecError>>
      createElement() {
    return _DeleteImageProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is DeleteImageProvider &&
        other.profile == profile &&
        other.image == image;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, image.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin DeleteImageRef
    on AutoDisposeFutureProviderRef<Result<ImageEntry, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `image` of this provider.
  ImageEntry get image;
}

class _DeleteImageProviderElement
    extends AutoDisposeFutureProviderElement<Result<ImageEntry, ExecError>>
    with DeleteImageRef {
  _DeleteImageProviderElement(super.provider);

  @override
  Profile get profile => (origin as DeleteImageProvider).profile;
  @override
  ImageEntry get image => (origin as DeleteImageProvider).image;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
