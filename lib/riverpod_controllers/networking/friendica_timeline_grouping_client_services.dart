import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../models/auth/profile.dart';
import '../../models/connection.dart';
import '../../models/exec_error.dart';
import '../../models/networking/paged_response.dart';
import '../../models/networking/paging_data.dart';
import '../../models/timeline_grouping_list_data.dart';
import '../../serializers/mastodon/connection_mastodon_extensions.dart';
import '../../serializers/mastodon/timeline_grouping_list_data.dart';
import 'friendica_client_services.dart';
import 'network_services.dart';

part 'friendica_timeline_grouping_client_services.g.dart';

final _logger = Logger('FriendicaTimelineGroupingListClient');

@riverpod
Future<Result<List<TimelineGroupingListData>, ExecError>>
    timelineGroupingListDataClient(
  Ref ref,
  Profile profile,
) async {
  _logger.finest(() => 'Getting timeline grouping data (Mastodon List) data');
  final url = 'https://${profile.serverName}/api/v1/lists';
  final request = Uri.parse(url);
  return (await ref
          .read(getApiListRequestProvider(profile, request).future)
          .transform((listsJson) => listsJson.data
              .map((json) =>
                  TimelineGroupingListDataMastodonExtensions.fromJson(json))
              .toList()))
      .mapError((error) => error is ExecError
          ? error
          : ExecError(type: ErrorType.localError, message: error.toString()));
}

@riverpod
Future<Result<PagedResponse<List<Connection>>, ExecError>> circleMembers(
  Ref ref,
  Profile profile,
  TimelineGroupingListData circleData,
  PagingData page,
) async {
  // TODO See if need to be doing network status update directly
  _logger.finest(() =>
      'Getting members for circle (Mastodon List) of name ${circleData.name} with paging: $page');
  final baseUrl =
      'https://${profile.serverName}/api/v1/lists/${circleData.id}/accounts';
  final url = Uri.parse('$baseUrl?${page.toQueryParameters()}');
  final result =
      await ref.read(getApiPagedRequestProvider(profile, url).future);
  return result
      .andThenSuccess((response) => response.map((jsonArray) =>
          (jsonArray as List<dynamic>)
              .map((json) => connectionFromJson(ref, profile, json))
              .toList()))
      .execErrorCast();
}

@riverpod
Future<Result<TimelineGroupingListData, ExecError>> createCircle(
  Ref ref,
  Profile profile,
  String title,
) async {
  _logger.finest(() => 'Creating circle (Mastodon List) of name $title');
  final url = 'https://${profile.serverName}/api/v1/lists';
  final body = {
    'title': title,
  };
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(
    Uri.parse(url),
    headers: headers,
    body: body,
  );
  final result = await ref.read(httpPostProvider(request).future).transform(
      (data) => TimelineGroupingListDataMastodonExtensions.fromJson(
          jsonDecode(data)));
  return result.execErrorCast();
}

@riverpod
Future<Result<TimelineGroupingListData, ExecError>> renameCircle(
  Ref ref,
  Profile profile,
  String id,
  String title,
) async {
  _logger.finest(() => 'Renaming circle (Mastodon List) to name $title');
  final url = 'https://${profile.serverName}/api/v1/lists/$id';
  final body = {
    'title': title,
  };
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(
    Uri.parse(url),
    headers: headers,
    body: body,
  );
  final result =
      await ref.read(httpPutProvider(request).future).transform((data) {
    final json = jsonDecode(data);
    return TimelineGroupingListDataMastodonExtensions.fromJson(json);
  });
  return result.execErrorCast();
}

@riverpod
Future<Result<bool, ExecError>> deleteCircle(
  Ref ref,
  Profile profile,
  TimelineGroupingListData circleData,
) async {
  _logger.finest(
      () => 'Renaming circle (Mastodon List) to name ${circleData.name}');
  final url = 'https://${profile.serverName}/api/v1/lists/${circleData.id}';
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(Uri.parse(url), headers: headers);
  final result = await ref.read(httpDeleteProvider(request).future).mapValue(
        (_) => true,
      );
  return result.execErrorCast();
}

@riverpod
Future<Result<List<TimelineGroupingListData>, ExecError>>
    memberCirclesForConnection(
  Ref ref,
  Profile profile,
  String connectionId,
) async {
  _logger.finest(() =>
      'Getting circles (Mastodon Lists) containing connection: $connectionId');
  final url =
      'https://${profile.serverName}/api/v1/accounts/$connectionId/lists';
  final request = Uri.parse(url);
  return (await ref
          .read(getApiListRequestProvider(profile, request).future)
          .transform((listsJson) => listsJson.data
              .map((json) =>
                  TimelineGroupingListDataMastodonExtensions.fromJson(json))
              .toList()))
      .mapError((error) => error as ExecError);
}

@riverpod
Future<Result<bool, ExecError>> addConnectionToCircle(
  Ref ref,
  Profile profile,
  TimelineGroupingListData circle,
  Connection connection,
) async {
  _logger.finest(() => 'Adding connection to circle');
  final url =
      'https://${profile.serverName}/api/v1/lists/${circle.id}/accounts';
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final body = {
    'account_ids': [connection.id]
  };
  final request = NetworkRequest(Uri.parse(url), headers: headers, body: body);
  return (await ref.read(httpPostProvider(request).future))
      .mapValue((_) => true);
}

@riverpod
Future<Result<bool, ExecError>> removeConnectionFromCircle(
  Ref ref,
  Profile profile,
  TimelineGroupingListData circle,
  Connection connection,
) async {
  _logger.finest(() => 'Adding connection to circle');
  final url =
      'https://${profile.serverName}/api/v1/lists/${circle.id}/accounts';
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final body = {
    'account_ids': [connection.id]
  };
  final request = NetworkRequest(Uri.parse(url), headers: headers, body: body);
  return (await ref.read(httpDeleteProvider(request).future))
      .mapValue((_) => true);
}
