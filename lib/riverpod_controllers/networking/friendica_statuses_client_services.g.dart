// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_statuses_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$postOrCommentHash() => r'de83acaccd22bfa5c7a3372118df4e88936117fe';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [postOrComment].
@ProviderFor(postOrComment)
const postOrCommentProvider = PostOrCommentFamily();

/// See also [postOrComment].
class PostOrCommentFamily
    extends Family<AsyncValue<Result<List<TimelineEntry>, ExecError>>> {
  /// See also [postOrComment].
  const PostOrCommentFamily();

  /// See also [postOrComment].
  PostOrCommentProvider call(
    Profile profile,
    String id, {
    bool fullContext = false,
  }) {
    return PostOrCommentProvider(
      profile,
      id,
      fullContext: fullContext,
    );
  }

  @override
  PostOrCommentProvider getProviderOverride(
    covariant PostOrCommentProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
      fullContext: provider.fullContext,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'postOrCommentProvider';
}

/// See also [postOrComment].
class PostOrCommentProvider
    extends AutoDisposeFutureProvider<Result<List<TimelineEntry>, ExecError>> {
  /// See also [postOrComment].
  PostOrCommentProvider(
    Profile profile,
    String id, {
    bool fullContext = false,
  }) : this._internal(
          (ref) => postOrComment(
            ref as PostOrCommentRef,
            profile,
            id,
            fullContext: fullContext,
          ),
          from: postOrCommentProvider,
          name: r'postOrCommentProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$postOrCommentHash,
          dependencies: PostOrCommentFamily._dependencies,
          allTransitiveDependencies:
              PostOrCommentFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
          fullContext: fullContext,
        );

  PostOrCommentProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
    required this.fullContext,
  }) : super.internal();

  final Profile profile;
  final String id;
  final bool fullContext;

  @override
  Override overrideWith(
    FutureOr<Result<List<TimelineEntry>, ExecError>> Function(
            PostOrCommentRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: PostOrCommentProvider._internal(
        (ref) => create(ref as PostOrCommentRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
        fullContext: fullContext,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<TimelineEntry>, ExecError>>
      createElement() {
    return _PostOrCommentProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is PostOrCommentProvider &&
        other.profile == profile &&
        other.id == id &&
        other.fullContext == fullContext;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);
    hash = _SystemHash.combine(hash, fullContext.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin PostOrCommentRef
    on AutoDisposeFutureProviderRef<Result<List<TimelineEntry>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;

  /// The parameter `fullContext` of this provider.
  bool get fullContext;
}

class _PostOrCommentProviderElement extends AutoDisposeFutureProviderElement<
    Result<List<TimelineEntry>, ExecError>> with PostOrCommentRef {
  _PostOrCommentProviderElement(super.provider);

  @override
  Profile get profile => (origin as PostOrCommentProvider).profile;
  @override
  String get id => (origin as PostOrCommentProvider).id;
  @override
  bool get fullContext => (origin as PostOrCommentProvider).fullContext;
}

String _$deleteStatusEntryByIdHash() =>
    r'74d5d2d27895c8aa94817f28f82c3a13789af454';

/// See also [deleteStatusEntryById].
@ProviderFor(deleteStatusEntryById)
const deleteStatusEntryByIdProvider = DeleteStatusEntryByIdFamily();

/// See also [deleteStatusEntryById].
class DeleteStatusEntryByIdFamily
    extends Family<AsyncValue<Result<bool, ExecError>>> {
  /// See also [deleteStatusEntryById].
  const DeleteStatusEntryByIdFamily();

  /// See also [deleteStatusEntryById].
  DeleteStatusEntryByIdProvider call(
    Profile profile,
    String id,
  ) {
    return DeleteStatusEntryByIdProvider(
      profile,
      id,
    );
  }

  @override
  DeleteStatusEntryByIdProvider getProviderOverride(
    covariant DeleteStatusEntryByIdProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'deleteStatusEntryByIdProvider';
}

/// See also [deleteStatusEntryById].
class DeleteStatusEntryByIdProvider
    extends AutoDisposeFutureProvider<Result<bool, ExecError>> {
  /// See also [deleteStatusEntryById].
  DeleteStatusEntryByIdProvider(
    Profile profile,
    String id,
  ) : this._internal(
          (ref) => deleteStatusEntryById(
            ref as DeleteStatusEntryByIdRef,
            profile,
            id,
          ),
          from: deleteStatusEntryByIdProvider,
          name: r'deleteStatusEntryByIdProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$deleteStatusEntryByIdHash,
          dependencies: DeleteStatusEntryByIdFamily._dependencies,
          allTransitiveDependencies:
              DeleteStatusEntryByIdFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  DeleteStatusEntryByIdProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  Override overrideWith(
    FutureOr<Result<bool, ExecError>> Function(
            DeleteStatusEntryByIdRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: DeleteStatusEntryByIdProvider._internal(
        (ref) => create(ref as DeleteStatusEntryByIdRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<bool, ExecError>> createElement() {
    return _DeleteStatusEntryByIdProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is DeleteStatusEntryByIdProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin DeleteStatusEntryByIdRef
    on AutoDisposeFutureProviderRef<Result<bool, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _DeleteStatusEntryByIdProviderElement
    extends AutoDisposeFutureProviderElement<Result<bool, ExecError>>
    with DeleteStatusEntryByIdRef {
  _DeleteStatusEntryByIdProviderElement(super.provider);

  @override
  Profile get profile => (origin as DeleteStatusEntryByIdProvider).profile;
  @override
  String get id => (origin as DeleteStatusEntryByIdProvider).id;
}

String _$createNewStatusHash() => r'c140a6e3a2776edb93a0e389ef5115d3b2e85fb0';

/// See also [createNewStatus].
@ProviderFor(createNewStatus)
const createNewStatusProvider = CreateNewStatusFamily();

/// See also [createNewStatus].
class CreateNewStatusFamily
    extends Family<AsyncValue<Result<TimelineEntry, ExecError>>> {
  /// See also [createNewStatus].
  const CreateNewStatusFamily();

  /// See also [createNewStatus].
  CreateNewStatusProvider call(
    Profile profile, {
    required String text,
    String spoilerText = '',
    String inReplyToId = '',
    List<String> mediaIds = const [],
    required Visibility visibility,
  }) {
    return CreateNewStatusProvider(
      profile,
      text: text,
      spoilerText: spoilerText,
      inReplyToId: inReplyToId,
      mediaIds: mediaIds,
      visibility: visibility,
    );
  }

  @override
  CreateNewStatusProvider getProviderOverride(
    covariant CreateNewStatusProvider provider,
  ) {
    return call(
      provider.profile,
      text: provider.text,
      spoilerText: provider.spoilerText,
      inReplyToId: provider.inReplyToId,
      mediaIds: provider.mediaIds,
      visibility: provider.visibility,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'createNewStatusProvider';
}

/// See also [createNewStatus].
class CreateNewStatusProvider
    extends AutoDisposeFutureProvider<Result<TimelineEntry, ExecError>> {
  /// See also [createNewStatus].
  CreateNewStatusProvider(
    Profile profile, {
    required String text,
    String spoilerText = '',
    String inReplyToId = '',
    List<String> mediaIds = const [],
    required Visibility visibility,
  }) : this._internal(
          (ref) => createNewStatus(
            ref as CreateNewStatusRef,
            profile,
            text: text,
            spoilerText: spoilerText,
            inReplyToId: inReplyToId,
            mediaIds: mediaIds,
            visibility: visibility,
          ),
          from: createNewStatusProvider,
          name: r'createNewStatusProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$createNewStatusHash,
          dependencies: CreateNewStatusFamily._dependencies,
          allTransitiveDependencies:
              CreateNewStatusFamily._allTransitiveDependencies,
          profile: profile,
          text: text,
          spoilerText: spoilerText,
          inReplyToId: inReplyToId,
          mediaIds: mediaIds,
          visibility: visibility,
        );

  CreateNewStatusProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.text,
    required this.spoilerText,
    required this.inReplyToId,
    required this.mediaIds,
    required this.visibility,
  }) : super.internal();

  final Profile profile;
  final String text;
  final String spoilerText;
  final String inReplyToId;
  final List<String> mediaIds;
  final Visibility visibility;

  @override
  Override overrideWith(
    FutureOr<Result<TimelineEntry, ExecError>> Function(
            CreateNewStatusRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: CreateNewStatusProvider._internal(
        (ref) => create(ref as CreateNewStatusRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        text: text,
        spoilerText: spoilerText,
        inReplyToId: inReplyToId,
        mediaIds: mediaIds,
        visibility: visibility,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<TimelineEntry, ExecError>>
      createElement() {
    return _CreateNewStatusProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CreateNewStatusProvider &&
        other.profile == profile &&
        other.text == text &&
        other.spoilerText == spoilerText &&
        other.inReplyToId == inReplyToId &&
        other.mediaIds == mediaIds &&
        other.visibility == visibility;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, text.hashCode);
    hash = _SystemHash.combine(hash, spoilerText.hashCode);
    hash = _SystemHash.combine(hash, inReplyToId.hashCode);
    hash = _SystemHash.combine(hash, mediaIds.hashCode);
    hash = _SystemHash.combine(hash, visibility.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin CreateNewStatusRef
    on AutoDisposeFutureProviderRef<Result<TimelineEntry, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `text` of this provider.
  String get text;

  /// The parameter `spoilerText` of this provider.
  String get spoilerText;

  /// The parameter `inReplyToId` of this provider.
  String get inReplyToId;

  /// The parameter `mediaIds` of this provider.
  List<String> get mediaIds;

  /// The parameter `visibility` of this provider.
  Visibility get visibility;
}

class _CreateNewStatusProviderElement
    extends AutoDisposeFutureProviderElement<Result<TimelineEntry, ExecError>>
    with CreateNewStatusRef {
  _CreateNewStatusProviderElement(super.provider);

  @override
  Profile get profile => (origin as CreateNewStatusProvider).profile;
  @override
  String get text => (origin as CreateNewStatusProvider).text;
  @override
  String get spoilerText => (origin as CreateNewStatusProvider).spoilerText;
  @override
  String get inReplyToId => (origin as CreateNewStatusProvider).inReplyToId;
  @override
  List<String> get mediaIds => (origin as CreateNewStatusProvider).mediaIds;
  @override
  Visibility get visibility => (origin as CreateNewStatusProvider).visibility;
}

String _$editStatusHash() => r'cad5eeef14a26a7886c129230fa5e7f1c78da7e7';

/// See also [editStatus].
@ProviderFor(editStatus)
const editStatusProvider = EditStatusFamily();

/// See also [editStatus].
class EditStatusFamily
    extends Family<AsyncValue<Result<TimelineEntry, ExecError>>> {
  /// See also [editStatus].
  const EditStatusFamily();

  /// See also [editStatus].
  EditStatusProvider call(
    Profile profile, {
    required String id,
    required String text,
    String spoilerText = '',
    List<String> mediaIds = const [],
  }) {
    return EditStatusProvider(
      profile,
      id: id,
      text: text,
      spoilerText: spoilerText,
      mediaIds: mediaIds,
    );
  }

  @override
  EditStatusProvider getProviderOverride(
    covariant EditStatusProvider provider,
  ) {
    return call(
      provider.profile,
      id: provider.id,
      text: provider.text,
      spoilerText: provider.spoilerText,
      mediaIds: provider.mediaIds,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'editStatusProvider';
}

/// See also [editStatus].
class EditStatusProvider
    extends AutoDisposeFutureProvider<Result<TimelineEntry, ExecError>> {
  /// See also [editStatus].
  EditStatusProvider(
    Profile profile, {
    required String id,
    required String text,
    String spoilerText = '',
    List<String> mediaIds = const [],
  }) : this._internal(
          (ref) => editStatus(
            ref as EditStatusRef,
            profile,
            id: id,
            text: text,
            spoilerText: spoilerText,
            mediaIds: mediaIds,
          ),
          from: editStatusProvider,
          name: r'editStatusProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$editStatusHash,
          dependencies: EditStatusFamily._dependencies,
          allTransitiveDependencies:
              EditStatusFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
          text: text,
          spoilerText: spoilerText,
          mediaIds: mediaIds,
        );

  EditStatusProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
    required this.text,
    required this.spoilerText,
    required this.mediaIds,
  }) : super.internal();

  final Profile profile;
  final String id;
  final String text;
  final String spoilerText;
  final List<String> mediaIds;

  @override
  Override overrideWith(
    FutureOr<Result<TimelineEntry, ExecError>> Function(EditStatusRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: EditStatusProvider._internal(
        (ref) => create(ref as EditStatusRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
        text: text,
        spoilerText: spoilerText,
        mediaIds: mediaIds,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<TimelineEntry, ExecError>>
      createElement() {
    return _EditStatusProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is EditStatusProvider &&
        other.profile == profile &&
        other.id == id &&
        other.text == text &&
        other.spoilerText == spoilerText &&
        other.mediaIds == mediaIds;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);
    hash = _SystemHash.combine(hash, text.hashCode);
    hash = _SystemHash.combine(hash, spoilerText.hashCode);
    hash = _SystemHash.combine(hash, mediaIds.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin EditStatusRef
    on AutoDisposeFutureProviderRef<Result<TimelineEntry, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;

  /// The parameter `text` of this provider.
  String get text;

  /// The parameter `spoilerText` of this provider.
  String get spoilerText;

  /// The parameter `mediaIds` of this provider.
  List<String> get mediaIds;
}

class _EditStatusProviderElement
    extends AutoDisposeFutureProviderElement<Result<TimelineEntry, ExecError>>
    with EditStatusRef {
  _EditStatusProviderElement(super.provider);

  @override
  Profile get profile => (origin as EditStatusProvider).profile;
  @override
  String get id => (origin as EditStatusProvider).id;
  @override
  String get text => (origin as EditStatusProvider).text;
  @override
  String get spoilerText => (origin as EditStatusProvider).spoilerText;
  @override
  List<String> get mediaIds => (origin as EditStatusProvider).mediaIds;
}

String _$resharePostHash() => r'e1e39f2109c41f9054ddc161e2789fb963b2224d';

/// See also [resharePost].
@ProviderFor(resharePost)
const resharePostProvider = ResharePostFamily();

/// See also [resharePost].
class ResharePostFamily
    extends Family<AsyncValue<Result<TimelineEntry, ExecError>>> {
  /// See also [resharePost].
  const ResharePostFamily();

  /// See also [resharePost].
  ResharePostProvider call(
    Profile profile,
    String id,
  ) {
    return ResharePostProvider(
      profile,
      id,
    );
  }

  @override
  ResharePostProvider getProviderOverride(
    covariant ResharePostProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'resharePostProvider';
}

/// See also [resharePost].
class ResharePostProvider
    extends AutoDisposeFutureProvider<Result<TimelineEntry, ExecError>> {
  /// See also [resharePost].
  ResharePostProvider(
    Profile profile,
    String id,
  ) : this._internal(
          (ref) => resharePost(
            ref as ResharePostRef,
            profile,
            id,
          ),
          from: resharePostProvider,
          name: r'resharePostProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$resharePostHash,
          dependencies: ResharePostFamily._dependencies,
          allTransitiveDependencies:
              ResharePostFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  ResharePostProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  Override overrideWith(
    FutureOr<Result<TimelineEntry, ExecError>> Function(ResharePostRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ResharePostProvider._internal(
        (ref) => create(ref as ResharePostRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<TimelineEntry, ExecError>>
      createElement() {
    return _ResharePostProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ResharePostProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ResharePostRef
    on AutoDisposeFutureProviderRef<Result<TimelineEntry, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _ResharePostProviderElement
    extends AutoDisposeFutureProviderElement<Result<TimelineEntry, ExecError>>
    with ResharePostRef {
  _ResharePostProviderElement(super.provider);

  @override
  Profile get profile => (origin as ResharePostProvider).profile;
  @override
  String get id => (origin as ResharePostProvider).id;
}

String _$unResharePostHash() => r'6f8ea49b584ca01a2a6d87064fa682d3dc78311d';

/// See also [unResharePost].
@ProviderFor(unResharePost)
const unResharePostProvider = UnResharePostFamily();

/// See also [unResharePost].
class UnResharePostFamily
    extends Family<AsyncValue<Result<TimelineEntry, ExecError>>> {
  /// See also [unResharePost].
  const UnResharePostFamily();

  /// See also [unResharePost].
  UnResharePostProvider call(
    Profile profile,
    String id,
  ) {
    return UnResharePostProvider(
      profile,
      id,
    );
  }

  @override
  UnResharePostProvider getProviderOverride(
    covariant UnResharePostProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'unResharePostProvider';
}

/// See also [unResharePost].
class UnResharePostProvider
    extends AutoDisposeFutureProvider<Result<TimelineEntry, ExecError>> {
  /// See also [unResharePost].
  UnResharePostProvider(
    Profile profile,
    String id,
  ) : this._internal(
          (ref) => unResharePost(
            ref as UnResharePostRef,
            profile,
            id,
          ),
          from: unResharePostProvider,
          name: r'unResharePostProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$unResharePostHash,
          dependencies: UnResharePostFamily._dependencies,
          allTransitiveDependencies:
              UnResharePostFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  UnResharePostProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  Override overrideWith(
    FutureOr<Result<TimelineEntry, ExecError>> Function(
            UnResharePostRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: UnResharePostProvider._internal(
        (ref) => create(ref as UnResharePostRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<TimelineEntry, ExecError>>
      createElement() {
    return _UnResharePostProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is UnResharePostProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin UnResharePostRef
    on AutoDisposeFutureProviderRef<Result<TimelineEntry, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _UnResharePostProviderElement
    extends AutoDisposeFutureProviderElement<Result<TimelineEntry, ExecError>>
    with UnResharePostRef {
  _UnResharePostProviderElement(super.provider);

  @override
  Profile get profile => (origin as UnResharePostProvider).profile;
  @override
  String get id => (origin as UnResharePostProvider).id;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
