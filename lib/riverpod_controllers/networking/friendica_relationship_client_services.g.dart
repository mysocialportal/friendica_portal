// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_relationship_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$blocksClientHash() => r'e09cf6ec912d4da54b11cd8a1cd86fa34e98da63';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [blocksClient].
@ProviderFor(blocksClient)
const blocksClientProvider = BlocksClientFamily();

/// See also [blocksClient].
class BlocksClientFamily extends Family<
    AsyncValue<Result<PagedResponse<List<Connection>>, ExecError>>> {
  /// See also [blocksClient].
  const BlocksClientFamily();

  /// See also [blocksClient].
  BlocksClientProvider call(
    Profile profile,
    PagingData page,
  ) {
    return BlocksClientProvider(
      profile,
      page,
    );
  }

  @override
  BlocksClientProvider getProviderOverride(
    covariant BlocksClientProvider provider,
  ) {
    return call(
      provider.profile,
      provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'blocksClientProvider';
}

/// See also [blocksClient].
class BlocksClientProvider extends AutoDisposeFutureProvider<
    Result<PagedResponse<List<Connection>>, ExecError>> {
  /// See also [blocksClient].
  BlocksClientProvider(
    Profile profile,
    PagingData page,
  ) : this._internal(
          (ref) => blocksClient(
            ref as BlocksClientRef,
            profile,
            page,
          ),
          from: blocksClientProvider,
          name: r'blocksClientProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$blocksClientHash,
          dependencies: BlocksClientFamily._dependencies,
          allTransitiveDependencies:
              BlocksClientFamily._allTransitiveDependencies,
          profile: profile,
          page: page,
        );

  BlocksClientProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<PagedResponse<List<Connection>>, ExecError>> Function(
            BlocksClientRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: BlocksClientProvider._internal(
        (ref) => create(ref as BlocksClientRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
      Result<PagedResponse<List<Connection>>, ExecError>> createElement() {
    return _BlocksClientProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is BlocksClientProvider &&
        other.profile == profile &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin BlocksClientRef on AutoDisposeFutureProviderRef<
    Result<PagedResponse<List<Connection>>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _BlocksClientProviderElement extends AutoDisposeFutureProviderElement<
    Result<PagedResponse<List<Connection>>, ExecError>> with BlocksClientRef {
  _BlocksClientProviderElement(super.provider);

  @override
  Profile get profile => (origin as BlocksClientProvider).profile;
  @override
  PagingData get page => (origin as BlocksClientProvider).page;
}

String _$myFollowingClientHash() => r'a1b03e715e746ec7f70a4d424164d0332c03e89a';

/// See also [myFollowingClient].
@ProviderFor(myFollowingClient)
const myFollowingClientProvider = MyFollowingClientFamily();

/// See also [myFollowingClient].
class MyFollowingClientFamily extends Family<
    AsyncValue<Result<PagedResponse<List<Connection>>, ExecError>>> {
  /// See also [myFollowingClient].
  const MyFollowingClientFamily();

  /// See also [myFollowingClient].
  MyFollowingClientProvider call(
    Profile profile,
    PagingData page,
  ) {
    return MyFollowingClientProvider(
      profile,
      page,
    );
  }

  @override
  MyFollowingClientProvider getProviderOverride(
    covariant MyFollowingClientProvider provider,
  ) {
    return call(
      provider.profile,
      provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'myFollowingClientProvider';
}

/// See also [myFollowingClient].
class MyFollowingClientProvider extends AutoDisposeFutureProvider<
    Result<PagedResponse<List<Connection>>, ExecError>> {
  /// See also [myFollowingClient].
  MyFollowingClientProvider(
    Profile profile,
    PagingData page,
  ) : this._internal(
          (ref) => myFollowingClient(
            ref as MyFollowingClientRef,
            profile,
            page,
          ),
          from: myFollowingClientProvider,
          name: r'myFollowingClientProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$myFollowingClientHash,
          dependencies: MyFollowingClientFamily._dependencies,
          allTransitiveDependencies:
              MyFollowingClientFamily._allTransitiveDependencies,
          profile: profile,
          page: page,
        );

  MyFollowingClientProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<PagedResponse<List<Connection>>, ExecError>> Function(
            MyFollowingClientRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: MyFollowingClientProvider._internal(
        (ref) => create(ref as MyFollowingClientRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
      Result<PagedResponse<List<Connection>>, ExecError>> createElement() {
    return _MyFollowingClientProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is MyFollowingClientProvider &&
        other.profile == profile &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin MyFollowingClientRef on AutoDisposeFutureProviderRef<
    Result<PagedResponse<List<Connection>>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _MyFollowingClientProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<PagedResponse<List<Connection>>, ExecError>>
    with MyFollowingClientRef {
  _MyFollowingClientProviderElement(super.provider);

  @override
  Profile get profile => (origin as MyFollowingClientProvider).profile;
  @override
  PagingData get page => (origin as MyFollowingClientProvider).page;
}

String _$followRequestsClientHash() =>
    r'404a80317612567a4b8756a778a971f948ccd7c2';

/// See also [followRequestsClient].
@ProviderFor(followRequestsClient)
const followRequestsClientProvider = FollowRequestsClientFamily();

/// See also [followRequestsClient].
class FollowRequestsClientFamily extends Family<
    AsyncValue<Result<PagedResponse<List<FollowRequest>>, ExecError>>> {
  /// See also [followRequestsClient].
  const FollowRequestsClientFamily();

  /// See also [followRequestsClient].
  FollowRequestsClientProvider call(
    Profile profile,
    PagingData page,
  ) {
    return FollowRequestsClientProvider(
      profile,
      page,
    );
  }

  @override
  FollowRequestsClientProvider getProviderOverride(
    covariant FollowRequestsClientProvider provider,
  ) {
    return call(
      provider.profile,
      provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'followRequestsClientProvider';
}

/// See also [followRequestsClient].
class FollowRequestsClientProvider extends AutoDisposeFutureProvider<
    Result<PagedResponse<List<FollowRequest>>, ExecError>> {
  /// See also [followRequestsClient].
  FollowRequestsClientProvider(
    Profile profile,
    PagingData page,
  ) : this._internal(
          (ref) => followRequestsClient(
            ref as FollowRequestsClientRef,
            profile,
            page,
          ),
          from: followRequestsClientProvider,
          name: r'followRequestsClientProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$followRequestsClientHash,
          dependencies: FollowRequestsClientFamily._dependencies,
          allTransitiveDependencies:
              FollowRequestsClientFamily._allTransitiveDependencies,
          profile: profile,
          page: page,
        );

  FollowRequestsClientProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<PagedResponse<List<FollowRequest>>, ExecError>> Function(
            FollowRequestsClientRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FollowRequestsClientProvider._internal(
        (ref) => create(ref as FollowRequestsClientRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
      Result<PagedResponse<List<FollowRequest>>, ExecError>> createElement() {
    return _FollowRequestsClientProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FollowRequestsClientProvider &&
        other.profile == profile &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FollowRequestsClientRef on AutoDisposeFutureProviderRef<
    Result<PagedResponse<List<FollowRequest>>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _FollowRequestsClientProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<PagedResponse<List<FollowRequest>>, ExecError>>
    with FollowRequestsClientRef {
  _FollowRequestsClientProviderElement(super.provider);

  @override
  Profile get profile => (origin as FollowRequestsClientProvider).profile;
  @override
  PagingData get page => (origin as FollowRequestsClientProvider).page;
}

String _$getMyFollowersHash() => r'9fd38f58e5a464e468837eb9111cdd64301545a0';

/// See also [getMyFollowers].
@ProviderFor(getMyFollowers)
const getMyFollowersProvider = GetMyFollowersFamily();

/// See also [getMyFollowers].
class GetMyFollowersFamily extends Family<
    AsyncValue<Result<PagedResponse<List<Connection>>, ExecError>>> {
  /// See also [getMyFollowers].
  const GetMyFollowersFamily();

  /// See also [getMyFollowers].
  GetMyFollowersProvider call(
    Profile profile,
    PagingData page,
  ) {
    return GetMyFollowersProvider(
      profile,
      page,
    );
  }

  @override
  GetMyFollowersProvider getProviderOverride(
    covariant GetMyFollowersProvider provider,
  ) {
    return call(
      provider.profile,
      provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'getMyFollowersProvider';
}

/// See also [getMyFollowers].
class GetMyFollowersProvider extends AutoDisposeFutureProvider<
    Result<PagedResponse<List<Connection>>, ExecError>> {
  /// See also [getMyFollowers].
  GetMyFollowersProvider(
    Profile profile,
    PagingData page,
  ) : this._internal(
          (ref) => getMyFollowers(
            ref as GetMyFollowersRef,
            profile,
            page,
          ),
          from: getMyFollowersProvider,
          name: r'getMyFollowersProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$getMyFollowersHash,
          dependencies: GetMyFollowersFamily._dependencies,
          allTransitiveDependencies:
              GetMyFollowersFamily._allTransitiveDependencies,
          profile: profile,
          page: page,
        );

  GetMyFollowersProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<PagedResponse<List<Connection>>, ExecError>> Function(
            GetMyFollowersRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: GetMyFollowersProvider._internal(
        (ref) => create(ref as GetMyFollowersRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
      Result<PagedResponse<List<Connection>>, ExecError>> createElement() {
    return _GetMyFollowersProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GetMyFollowersProvider &&
        other.profile == profile &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin GetMyFollowersRef on AutoDisposeFutureProviderRef<
    Result<PagedResponse<List<Connection>>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _GetMyFollowersProviderElement extends AutoDisposeFutureProviderElement<
    Result<PagedResponse<List<Connection>>, ExecError>> with GetMyFollowersRef {
  _GetMyFollowersProviderElement(super.provider);

  @override
  Profile get profile => (origin as GetMyFollowersProvider).profile;
  @override
  PagingData get page => (origin as GetMyFollowersProvider).page;
}

String _$connectionWithStatusClientHash() =>
    r'687d060bab00296f5aa802713af4ea47989536b4';

/// See also [connectionWithStatusClient].
@ProviderFor(connectionWithStatusClient)
const connectionWithStatusClientProvider = ConnectionWithStatusClientFamily();

/// See also [connectionWithStatusClient].
class ConnectionWithStatusClientFamily
    extends Family<AsyncValue<Result<Connection, ExecError>>> {
  /// See also [connectionWithStatusClient].
  const ConnectionWithStatusClientFamily();

  /// See also [connectionWithStatusClient].
  ConnectionWithStatusClientProvider call(
    Profile profile,
    Connection connection,
  ) {
    return ConnectionWithStatusClientProvider(
      profile,
      connection,
    );
  }

  @override
  ConnectionWithStatusClientProvider getProviderOverride(
    covariant ConnectionWithStatusClientProvider provider,
  ) {
    return call(
      provider.profile,
      provider.connection,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'connectionWithStatusClientProvider';
}

/// See also [connectionWithStatusClient].
class ConnectionWithStatusClientProvider
    extends AutoDisposeFutureProvider<Result<Connection, ExecError>> {
  /// See also [connectionWithStatusClient].
  ConnectionWithStatusClientProvider(
    Profile profile,
    Connection connection,
  ) : this._internal(
          (ref) => connectionWithStatusClient(
            ref as ConnectionWithStatusClientRef,
            profile,
            connection,
          ),
          from: connectionWithStatusClientProvider,
          name: r'connectionWithStatusClientProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$connectionWithStatusClientHash,
          dependencies: ConnectionWithStatusClientFamily._dependencies,
          allTransitiveDependencies:
              ConnectionWithStatusClientFamily._allTransitiveDependencies,
          profile: profile,
          connection: connection,
        );

  ConnectionWithStatusClientProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.connection,
  }) : super.internal();

  final Profile profile;
  final Connection connection;

  @override
  Override overrideWith(
    FutureOr<Result<Connection, ExecError>> Function(
            ConnectionWithStatusClientRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ConnectionWithStatusClientProvider._internal(
        (ref) => create(ref as ConnectionWithStatusClientRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        connection: connection,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<Connection, ExecError>>
      createElement() {
    return _ConnectionWithStatusClientProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ConnectionWithStatusClientProvider &&
        other.profile == profile &&
        other.connection == connection;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, connection.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ConnectionWithStatusClientRef
    on AutoDisposeFutureProviderRef<Result<Connection, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `connection` of this provider.
  Connection get connection;
}

class _ConnectionWithStatusClientProviderElement
    extends AutoDisposeFutureProviderElement<Result<Connection, ExecError>>
    with ConnectionWithStatusClientRef {
  _ConnectionWithStatusClientProviderElement(super.provider);

  @override
  Profile get profile => (origin as ConnectionWithStatusClientProvider).profile;
  @override
  Connection get connection =>
      (origin as ConnectionWithStatusClientProvider).connection;
}

String _$connectionRequestsClientHash() =>
    r'0fdc2b229788469471ab6baa19e45f316be9cbbe';

/// See also [connectionRequestsClient].
@ProviderFor(connectionRequestsClient)
const connectionRequestsClientProvider = ConnectionRequestsClientFamily();

/// See also [connectionRequestsClient].
class ConnectionRequestsClientFamily extends Family<
    AsyncValue<Result<PagedResponse<List<Connection>>, ExecError>>> {
  /// See also [connectionRequestsClient].
  const ConnectionRequestsClientFamily();

  /// See also [connectionRequestsClient].
  ConnectionRequestsClientProvider call(
    Profile profile,
    PagingData page,
  ) {
    return ConnectionRequestsClientProvider(
      profile,
      page,
    );
  }

  @override
  ConnectionRequestsClientProvider getProviderOverride(
    covariant ConnectionRequestsClientProvider provider,
  ) {
    return call(
      provider.profile,
      provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'connectionRequestsClientProvider';
}

/// See also [connectionRequestsClient].
class ConnectionRequestsClientProvider extends AutoDisposeFutureProvider<
    Result<PagedResponse<List<Connection>>, ExecError>> {
  /// See also [connectionRequestsClient].
  ConnectionRequestsClientProvider(
    Profile profile,
    PagingData page,
  ) : this._internal(
          (ref) => connectionRequestsClient(
            ref as ConnectionRequestsClientRef,
            profile,
            page,
          ),
          from: connectionRequestsClientProvider,
          name: r'connectionRequestsClientProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$connectionRequestsClientHash,
          dependencies: ConnectionRequestsClientFamily._dependencies,
          allTransitiveDependencies:
              ConnectionRequestsClientFamily._allTransitiveDependencies,
          profile: profile,
          page: page,
        );

  ConnectionRequestsClientProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<PagedResponse<List<Connection>>, ExecError>> Function(
            ConnectionRequestsClientRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ConnectionRequestsClientProvider._internal(
        (ref) => create(ref as ConnectionRequestsClientRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<
      Result<PagedResponse<List<Connection>>, ExecError>> createElement() {
    return _ConnectionRequestsClientProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ConnectionRequestsClientProvider &&
        other.profile == profile &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ConnectionRequestsClientRef on AutoDisposeFutureProviderRef<
    Result<PagedResponse<List<Connection>>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _ConnectionRequestsClientProviderElement
    extends AutoDisposeFutureProviderElement<
        Result<PagedResponse<List<Connection>>, ExecError>>
    with ConnectionRequestsClientRef {
  _ConnectionRequestsClientProviderElement(super.provider);

  @override
  Profile get profile => (origin as ConnectionRequestsClientProvider).profile;
  @override
  PagingData get page => (origin as ConnectionRequestsClientProvider).page;
}

String _$adjudicateFollowHash() => r'd8489c80010722342110bcb8c5d43d90cbb42c28';

/// See also [adjudicateFollow].
@ProviderFor(adjudicateFollow)
const adjudicateFollowProvider = AdjudicateFollowFamily();

/// See also [adjudicateFollow].
class AdjudicateFollowFamily
    extends Family<AsyncValue<Result<Connection, ExecError>>> {
  /// See also [adjudicateFollow].
  const AdjudicateFollowFamily();

  /// See also [adjudicateFollow].
  AdjudicateFollowProvider call(
    Profile profile,
    Connection connection,
    FollowAdjudication adjudication,
  ) {
    return AdjudicateFollowProvider(
      profile,
      connection,
      adjudication,
    );
  }

  @override
  AdjudicateFollowProvider getProviderOverride(
    covariant AdjudicateFollowProvider provider,
  ) {
    return call(
      provider.profile,
      provider.connection,
      provider.adjudication,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'adjudicateFollowProvider';
}

/// See also [adjudicateFollow].
class AdjudicateFollowProvider
    extends AutoDisposeFutureProvider<Result<Connection, ExecError>> {
  /// See also [adjudicateFollow].
  AdjudicateFollowProvider(
    Profile profile,
    Connection connection,
    FollowAdjudication adjudication,
  ) : this._internal(
          (ref) => adjudicateFollow(
            ref as AdjudicateFollowRef,
            profile,
            connection,
            adjudication,
          ),
          from: adjudicateFollowProvider,
          name: r'adjudicateFollowProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$adjudicateFollowHash,
          dependencies: AdjudicateFollowFamily._dependencies,
          allTransitiveDependencies:
              AdjudicateFollowFamily._allTransitiveDependencies,
          profile: profile,
          connection: connection,
          adjudication: adjudication,
        );

  AdjudicateFollowProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.connection,
    required this.adjudication,
  }) : super.internal();

  final Profile profile;
  final Connection connection;
  final FollowAdjudication adjudication;

  @override
  Override overrideWith(
    FutureOr<Result<Connection, ExecError>> Function(
            AdjudicateFollowRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: AdjudicateFollowProvider._internal(
        (ref) => create(ref as AdjudicateFollowRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        connection: connection,
        adjudication: adjudication,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<Connection, ExecError>>
      createElement() {
    return _AdjudicateFollowProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is AdjudicateFollowProvider &&
        other.profile == profile &&
        other.connection == connection &&
        other.adjudication == adjudication;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, connection.hashCode);
    hash = _SystemHash.combine(hash, adjudication.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin AdjudicateFollowRef
    on AutoDisposeFutureProviderRef<Result<Connection, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `connection` of this provider.
  Connection get connection;

  /// The parameter `adjudication` of this provider.
  FollowAdjudication get adjudication;
}

class _AdjudicateFollowProviderElement
    extends AutoDisposeFutureProviderElement<Result<Connection, ExecError>>
    with AdjudicateFollowRef {
  _AdjudicateFollowProviderElement(super.provider);

  @override
  Profile get profile => (origin as AdjudicateFollowProvider).profile;
  @override
  Connection get connection => (origin as AdjudicateFollowProvider).connection;
  @override
  FollowAdjudication get adjudication =>
      (origin as AdjudicateFollowProvider).adjudication;
}

String _$blockConnectionHash() => r'b09298b7b9d2eba220a1e53ba62ef77de660c40e';

/// See also [blockConnection].
@ProviderFor(blockConnection)
const blockConnectionProvider = BlockConnectionFamily();

/// See also [blockConnection].
class BlockConnectionFamily
    extends Family<AsyncValue<Result<Connection, ExecError>>> {
  /// See also [blockConnection].
  const BlockConnectionFamily();

  /// See also [blockConnection].
  BlockConnectionProvider call(
    Profile profile,
    Connection connection,
  ) {
    return BlockConnectionProvider(
      profile,
      connection,
    );
  }

  @override
  BlockConnectionProvider getProviderOverride(
    covariant BlockConnectionProvider provider,
  ) {
    return call(
      provider.profile,
      provider.connection,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'blockConnectionProvider';
}

/// See also [blockConnection].
class BlockConnectionProvider
    extends AutoDisposeFutureProvider<Result<Connection, ExecError>> {
  /// See also [blockConnection].
  BlockConnectionProvider(
    Profile profile,
    Connection connection,
  ) : this._internal(
          (ref) => blockConnection(
            ref as BlockConnectionRef,
            profile,
            connection,
          ),
          from: blockConnectionProvider,
          name: r'blockConnectionProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$blockConnectionHash,
          dependencies: BlockConnectionFamily._dependencies,
          allTransitiveDependencies:
              BlockConnectionFamily._allTransitiveDependencies,
          profile: profile,
          connection: connection,
        );

  BlockConnectionProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.connection,
  }) : super.internal();

  final Profile profile;
  final Connection connection;

  @override
  Override overrideWith(
    FutureOr<Result<Connection, ExecError>> Function(
            BlockConnectionRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: BlockConnectionProvider._internal(
        (ref) => create(ref as BlockConnectionRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        connection: connection,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<Connection, ExecError>>
      createElement() {
    return _BlockConnectionProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is BlockConnectionProvider &&
        other.profile == profile &&
        other.connection == connection;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, connection.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin BlockConnectionRef
    on AutoDisposeFutureProviderRef<Result<Connection, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `connection` of this provider.
  Connection get connection;
}

class _BlockConnectionProviderElement
    extends AutoDisposeFutureProviderElement<Result<Connection, ExecError>>
    with BlockConnectionRef {
  _BlockConnectionProviderElement(super.provider);

  @override
  Profile get profile => (origin as BlockConnectionProvider).profile;
  @override
  Connection get connection => (origin as BlockConnectionProvider).connection;
}

String _$unblockConnectionHash() => r'25465e61c6ca54c2289a5d14fabe024dd26826a3';

/// See also [unblockConnection].
@ProviderFor(unblockConnection)
const unblockConnectionProvider = UnblockConnectionFamily();

/// See also [unblockConnection].
class UnblockConnectionFamily
    extends Family<AsyncValue<Result<Connection, ExecError>>> {
  /// See also [unblockConnection].
  const UnblockConnectionFamily();

  /// See also [unblockConnection].
  UnblockConnectionProvider call(
    Profile profile,
    Connection connection,
  ) {
    return UnblockConnectionProvider(
      profile,
      connection,
    );
  }

  @override
  UnblockConnectionProvider getProviderOverride(
    covariant UnblockConnectionProvider provider,
  ) {
    return call(
      provider.profile,
      provider.connection,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'unblockConnectionProvider';
}

/// See also [unblockConnection].
class UnblockConnectionProvider
    extends AutoDisposeFutureProvider<Result<Connection, ExecError>> {
  /// See also [unblockConnection].
  UnblockConnectionProvider(
    Profile profile,
    Connection connection,
  ) : this._internal(
          (ref) => unblockConnection(
            ref as UnblockConnectionRef,
            profile,
            connection,
          ),
          from: unblockConnectionProvider,
          name: r'unblockConnectionProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$unblockConnectionHash,
          dependencies: UnblockConnectionFamily._dependencies,
          allTransitiveDependencies:
              UnblockConnectionFamily._allTransitiveDependencies,
          profile: profile,
          connection: connection,
        );

  UnblockConnectionProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.connection,
  }) : super.internal();

  final Profile profile;
  final Connection connection;

  @override
  Override overrideWith(
    FutureOr<Result<Connection, ExecError>> Function(
            UnblockConnectionRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: UnblockConnectionProvider._internal(
        (ref) => create(ref as UnblockConnectionRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        connection: connection,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<Connection, ExecError>>
      createElement() {
    return _UnblockConnectionProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is UnblockConnectionProvider &&
        other.profile == profile &&
        other.connection == connection;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, connection.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin UnblockConnectionRef
    on AutoDisposeFutureProviderRef<Result<Connection, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `connection` of this provider.
  Connection get connection;
}

class _UnblockConnectionProviderElement
    extends AutoDisposeFutureProviderElement<Result<Connection, ExecError>>
    with UnblockConnectionRef {
  _UnblockConnectionProviderElement(super.provider);

  @override
  Profile get profile => (origin as UnblockConnectionProvider).profile;
  @override
  Connection get connection => (origin as UnblockConnectionProvider).connection;
}

String _$followConnectionHash() => r'4dd3d9a8c68e7f7a54b148dd85753bed3bd3c466';

/// See also [followConnection].
@ProviderFor(followConnection)
const followConnectionProvider = FollowConnectionFamily();

/// See also [followConnection].
class FollowConnectionFamily
    extends Family<AsyncValue<Result<Connection, ExecError>>> {
  /// See also [followConnection].
  const FollowConnectionFamily();

  /// See also [followConnection].
  FollowConnectionProvider call(
    Profile profile,
    Connection connection,
  ) {
    return FollowConnectionProvider(
      profile,
      connection,
    );
  }

  @override
  FollowConnectionProvider getProviderOverride(
    covariant FollowConnectionProvider provider,
  ) {
    return call(
      provider.profile,
      provider.connection,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'followConnectionProvider';
}

/// See also [followConnection].
class FollowConnectionProvider
    extends AutoDisposeFutureProvider<Result<Connection, ExecError>> {
  /// See also [followConnection].
  FollowConnectionProvider(
    Profile profile,
    Connection connection,
  ) : this._internal(
          (ref) => followConnection(
            ref as FollowConnectionRef,
            profile,
            connection,
          ),
          from: followConnectionProvider,
          name: r'followConnectionProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$followConnectionHash,
          dependencies: FollowConnectionFamily._dependencies,
          allTransitiveDependencies:
              FollowConnectionFamily._allTransitiveDependencies,
          profile: profile,
          connection: connection,
        );

  FollowConnectionProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.connection,
  }) : super.internal();

  final Profile profile;
  final Connection connection;

  @override
  Override overrideWith(
    FutureOr<Result<Connection, ExecError>> Function(
            FollowConnectionRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FollowConnectionProvider._internal(
        (ref) => create(ref as FollowConnectionRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        connection: connection,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<Connection, ExecError>>
      createElement() {
    return _FollowConnectionProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FollowConnectionProvider &&
        other.profile == profile &&
        other.connection == connection;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, connection.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FollowConnectionRef
    on AutoDisposeFutureProviderRef<Result<Connection, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `connection` of this provider.
  Connection get connection;
}

class _FollowConnectionProviderElement
    extends AutoDisposeFutureProviderElement<Result<Connection, ExecError>>
    with FollowConnectionRef {
  _FollowConnectionProviderElement(super.provider);

  @override
  Profile get profile => (origin as FollowConnectionProvider).profile;
  @override
  Connection get connection => (origin as FollowConnectionProvider).connection;
}

String _$unFollowConnectionHash() =>
    r'5c03485f2aa1d24916ff33a2c0ecead17ee1ab8a';

/// See also [unFollowConnection].
@ProviderFor(unFollowConnection)
const unFollowConnectionProvider = UnFollowConnectionFamily();

/// See also [unFollowConnection].
class UnFollowConnectionFamily
    extends Family<AsyncValue<Result<Connection, ExecError>>> {
  /// See also [unFollowConnection].
  const UnFollowConnectionFamily();

  /// See also [unFollowConnection].
  UnFollowConnectionProvider call(
    Profile profile,
    Connection connection,
  ) {
    return UnFollowConnectionProvider(
      profile,
      connection,
    );
  }

  @override
  UnFollowConnectionProvider getProviderOverride(
    covariant UnFollowConnectionProvider provider,
  ) {
    return call(
      provider.profile,
      provider.connection,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'unFollowConnectionProvider';
}

/// See also [unFollowConnection].
class UnFollowConnectionProvider
    extends AutoDisposeFutureProvider<Result<Connection, ExecError>> {
  /// See also [unFollowConnection].
  UnFollowConnectionProvider(
    Profile profile,
    Connection connection,
  ) : this._internal(
          (ref) => unFollowConnection(
            ref as UnFollowConnectionRef,
            profile,
            connection,
          ),
          from: unFollowConnectionProvider,
          name: r'unFollowConnectionProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$unFollowConnectionHash,
          dependencies: UnFollowConnectionFamily._dependencies,
          allTransitiveDependencies:
              UnFollowConnectionFamily._allTransitiveDependencies,
          profile: profile,
          connection: connection,
        );

  UnFollowConnectionProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.connection,
  }) : super.internal();

  final Profile profile;
  final Connection connection;

  @override
  Override overrideWith(
    FutureOr<Result<Connection, ExecError>> Function(
            UnFollowConnectionRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: UnFollowConnectionProvider._internal(
        (ref) => create(ref as UnFollowConnectionRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        connection: connection,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<Connection, ExecError>>
      createElement() {
    return _UnFollowConnectionProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is UnFollowConnectionProvider &&
        other.profile == profile &&
        other.connection == connection;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, connection.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin UnFollowConnectionRef
    on AutoDisposeFutureProviderRef<Result<Connection, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `connection` of this provider.
  Connection get connection;
}

class _UnFollowConnectionProviderElement
    extends AutoDisposeFutureProviderElement<Result<Connection, ExecError>>
    with UnFollowConnectionRef {
  _UnFollowConnectionProviderElement(super.provider);

  @override
  Profile get profile => (origin as UnFollowConnectionProvider).profile;
  @override
  Connection get connection =>
      (origin as UnFollowConnectionProvider).connection;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
