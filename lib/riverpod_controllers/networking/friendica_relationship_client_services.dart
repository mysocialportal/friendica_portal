import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:stack_trace/stack_trace.dart';

import '../../globals.dart';
import '../../models/auth/profile.dart';
import '../../models/connection.dart';
import '../../models/exec_error.dart';
import '../../models/follow_request.dart';
import '../../models/networking/paged_response.dart';
import '../../models/networking/paging_data.dart';
import '../../serializers/mastodon/connection_mastodon_extensions.dart';
import '../../serializers/mastodon/follow_request_mastodon_extensions.dart';
import 'friendica_client_services.dart';
import 'network_services.dart';
import 'network_status_services.dart';

part 'friendica_relationship_client_services.g.dart';

final _logger = Logger('RelationshipsClient');

@riverpod
Future<Result<PagedResponse<List<Connection>>, ExecError>> blocksClient(
  Ref ref,
  Profile profile,
  PagingData page,
) async {
  final url = 'https://${profile.serverName}/api/v1/blocks';
  final request = Uri.parse('$url?${page.toQueryParameters()}');
  _logger.finest(() => 'Getting blocks for $page');
  final result = await ref
      .read(getApiListRequestProvider(profile, request).future)
      .transformAsync((response) async {
    final blocks = <Connection>[];

    final st = Stopwatch()..start();
    for (final json in response.data) {
      if (st.elapsedMilliseconds > maxProcessingMillis) {
        await Future.delayed(processingSleep, () => st.reset());
      }
      blocks.add(
        connectionFromJson(ref, profile, json)
            .copy(status: ConnectionStatus.blocked),
      );
    }
    return PagedResponse(
      blocks,
      id: response.id,
      previous: response.previous,
      next: response.next,
    );
  });

  return result.execErrorCast();
}

@riverpod
Future<Result<PagedResponse<List<Connection>>, ExecError>> myFollowingClient(
  Ref ref,
  Profile profile,
  PagingData page,
) async {
  _logger.fine(() => 'Getting following with paging data $page');
  ref.read(connectionsLoadingProvider(profile));
  Future.microtask(
    () async => ref.read(connectionsLoadingProvider(profile).notifier).begin(),
  );

  final myId = profile.userId;
  final baseUrl = 'https://${profile.serverName}/api/v1/accounts/$myId';
  final url = Uri.parse('$baseUrl/following?${page.toQueryParameters()}');
  final result = await ref.read(getApiListRequestProvider(profile, url).future);
  ref.read(connectionsLoadingProvider(profile).notifier).end();
  return result
      .transform((response) => response.map((jsonArray) => jsonArray
          .map((json) => connectionFromJson(ref, profile, json))
          .toList()))
      .execErrorCast();
}

@riverpod
Future<Result<PagedResponse<List<FollowRequest>>, ExecError>>
    followRequestsClient(
  Ref ref,
  Profile profile,
  PagingData page,
) async {
  _logger.finest(() => 'Getting follow requests with paging data $page');
  ref.read(followRequestsLoadingProvider(profile));
  Future.microtask(
    () async =>
        ref.read(followRequestsLoadingProvider(profile).notifier).begin(),
  );

  final baseUrl = 'https://${profile.serverName}/api/v1/follow_requests';
  final url = Uri.parse('$baseUrl?${page.toQueryParameters()}');
  final result = await ref.read(getApiListRequestProvider(profile, url).future);
  ref.read(followRequestsLoadingProvider(profile).notifier).end();
  return result
      .transform((response) => response.map((jsonArray) => jsonArray
          .map((json) => followRequestFromJson(ref, profile, json))
          .toList()))
      .execErrorCast();
}

@riverpod
Future<Result<PagedResponse<List<Connection>>, ExecError>> getMyFollowers(
  Ref ref,
  Profile profile,
  PagingData page,
) async {
  _logger.finest(() => 'Getting followers data with page data $page');
  ref.read(connectionsLoadingProvider(profile));
  Future.microtask(
    () async => ref.read(connectionsLoadingProvider(profile).notifier).begin(),
  );

  final myId = profile.userId;
  final baseUrl = 'https://${profile.serverName}/api/v1/accounts/$myId';
  final url = Uri.parse('$baseUrl/followers?${page.toQueryParameters()}');
  final result = await ref.read(getApiListRequestProvider(profile, url).future);
  ref.read(connectionsLoadingProvider(profile).notifier).end();
  return result
      .transform((response) => response.map((jsonArray) => jsonArray
          .map((json) => connectionFromJson(ref, profile, json))
          .toList()))
      .execErrorCast();
}

@riverpod
Future<Result<Connection, ExecError>> connectionWithStatusClient(
  Ref ref,
  Profile profile,
  Connection connection,
) async {
  _logger.finest(() => 'Getting circle (Mastodon List) data');
  ref.read(connectionsLoadingProvider(profile));
  Future.microtask(
    () async => ref.read(connectionsLoadingProvider(profile).notifier).begin(),
  );

  final myId = profile.userId;
  final id = int.parse(connection.id);
  final connectionUpdateUrl =
      Uri.parse('https://${profile.serverName}/api/v1/accounts/$id');
  final updatedConnection = await ref
      .read(getApiRequestProvider(profile, connectionUpdateUrl).future)
      .fold(
          onSuccess: (json) => connectionFromJson(ref, profile, json),
          onError: (error) {
            _logger.severe('Error getting connection for $id', Trace.current());
            return connection;
          });
  final paging = '?min_id=${id - 1}&max_id=${id + 1}';
  final baseUrl = 'https://${profile.serverName}/api/v1/accounts/$myId';
  final following = await ref
      .read(getApiListRequestProvider(
        profile,
        Uri.parse('$baseUrl/following$paging'),
      ).future)
      .fold(
          onSuccess: (followings) => followings.data.isNotEmpty,
          onError: (error) {
            _logger.severe(
              'Error getting following list: $error',
              Trace.current(),
            );
            return false;
          });
  final follower = await ref
      .read(getApiListRequestProvider(
              profile, Uri.parse('$baseUrl/followers$paging'))
          .future)
      .fold(
          onSuccess: (followings) => followings.data.isNotEmpty,
          onError: (error) {
            _logger.severe(
              'Error getting follower list: $error',
              Trace.current(),
            );
            return false;
          });

  var status = ConnectionStatus.none;
  if (following && follower) {
    status = ConnectionStatus.mutual;
  } else if (following) {
    status = ConnectionStatus.youFollowThem;
  } else if (follower) {
    status = ConnectionStatus.theyFollowYou;
  }

  ref.read(connectionsLoadingProvider(profile).notifier).end();
  return Result.ok(updatedConnection.copy(status: status));
}

@riverpod
Future<Result<PagedResponse<List<Connection>>, ExecError>>
    connectionRequestsClient(
  Ref ref,
  Profile profile,
  PagingData page,
) async {
  _logger.finest(() => 'Getting connection requests with page data $page');
  ref.read(connectionsLoadingProvider(profile));
  Future.microtask(
    () async => ref.read(connectionsLoadingProvider(profile).notifier).begin(),
  );
  final baseUrl = 'https://${profile.serverName}/api/v1/follow_requests';
  final url = Uri.parse('$baseUrl?${page.toQueryParameters()}');
  final result = await ref.read(getApiListRequestProvider(profile, url).future);
  ref.read(connectionsLoadingProvider(profile).notifier).end();
  return result
      .transform((response) => response.map((jsonArray) => jsonArray
          .map((json) => connectionFromJson(ref, profile, json))
          .toList()))
      .execErrorCast();
}

enum FollowAdjudication {
  authorize,
  ignore,
  reject,
}

@riverpod
Future<Result<Connection, ExecError>> adjudicateFollow(
  Ref ref,
  Profile profile,
  Connection connection,
  FollowAdjudication adjudication,
) async {
  final id = connection.id;
  final url = Uri.parse(
      'https://${profile.serverName}/api/v1/follow_requests/$id/${adjudication.name}');
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(url, headers: headers);
  return await ref
      .read(httpPostProvider(request).future)
      .transform(
        (jsonString) => _updateFromFollowResult(connection, jsonString),
      )
      .mapError((error) => error is ExecError
          ? error
          : ExecError(type: ErrorType.localError, message: error.toString()));
}

@riverpod
Future<Result<Connection, ExecError>> blockConnection(
  Ref ref,
  Profile profile,
  Connection connection,
) async {
  final id = connection.id;
  final url =
      Uri.parse('https://${profile.serverName}/api/v1/accounts/$id/block');
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(url, headers: headers);
  return await ref
      .read(httpPostProvider(request).future)
      .transform(
        (_) => connection.copy(status: ConnectionStatus.blocked),
      )
      .execErrorCastAsync();
}

@riverpod
Future<Result<Connection, ExecError>> unblockConnection(
  Ref ref,
  Profile profile,
  Connection connection,
) async {
  final id = connection.id;
  final url =
      Uri.parse('https://${profile.serverName}/api/v1/accounts/$id/unblock');
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(url, headers: headers);
  return await ref
      .read(httpPostProvider(request).future)
      .andThenAsync((_) async => await ref
          .read(connectionWithStatusClientProvider(profile, connection).future))
      .execErrorCastAsync();
}

@riverpod
Future<Result<Connection, ExecError>> followConnection(
  Ref ref,
  Profile profile,
  Connection connection,
) async {
  final id = connection.id;
  final url =
      Uri.parse('https://${profile.serverName}/api/v1/accounts/$id/follow');
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(url, headers: headers);
  return await ref
      .read(httpPostProvider(request).future)
      .transform(
          (jsonString) => _updateFromFollowResult(connection, jsonString))
      .execErrorCastAsync();
}

@riverpod
Future<Result<Connection, ExecError>> unFollowConnection(
  Ref ref,
  Profile profile,
  Connection connection,
) async {
  final id = connection.id;
  final url =
      Uri.parse('https://${profile.serverName}/api/v1/accounts/$id/unfollow');
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(url, headers: headers);
  return await ref
      .read(httpPostProvider(request).future)
      .transform(
          (jsonString) => _updateFromFollowResult(connection, jsonString))
      .execErrorCastAsync();
}

Connection _updateFromFollowResult(Connection connection, String jsonString) {
  final json = jsonDecode(jsonString) as Map<String, dynamic>;
  final theyFollowYou = json['followed_by'] ?? 'false';
  final youFollowThem = json['following'] ?? 'false';
  late final ConnectionStatus newStatus;
  if (theyFollowYou && youFollowThem) {
    newStatus = ConnectionStatus.mutual;
  } else if (theyFollowYou) {
    newStatus = ConnectionStatus.theyFollowYou;
  } else if (youFollowThem) {
    newStatus = ConnectionStatus.youFollowThem;
  } else {
    newStatus = ConnectionStatus.none;
  }
  return connection.copy(status: newStatus);
}
