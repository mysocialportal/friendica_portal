// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_dms_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$directMessagesHash() => r'7ae1718f8f32b3e05ad1987ff1d7c4bc3d94d9db';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [directMessages].
@ProviderFor(directMessages)
const directMessagesProvider = DirectMessagesFamily();

/// See also [directMessages].
class DirectMessagesFamily
    extends Family<AsyncValue<Result<List<DirectMessage>, ExecError>>> {
  /// See also [directMessages].
  const DirectMessagesFamily();

  /// See also [directMessages].
  DirectMessagesProvider call(
    Profile profile,
    PagingData page,
  ) {
    return DirectMessagesProvider(
      profile,
      page,
    );
  }

  @override
  DirectMessagesProvider getProviderOverride(
    covariant DirectMessagesProvider provider,
  ) {
    return call(
      provider.profile,
      provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'directMessagesProvider';
}

/// See also [directMessages].
class DirectMessagesProvider
    extends AutoDisposeFutureProvider<Result<List<DirectMessage>, ExecError>> {
  /// See also [directMessages].
  DirectMessagesProvider(
    Profile profile,
    PagingData page,
  ) : this._internal(
          (ref) => directMessages(
            ref as DirectMessagesRef,
            profile,
            page,
          ),
          from: directMessagesProvider,
          name: r'directMessagesProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$directMessagesHash,
          dependencies: DirectMessagesFamily._dependencies,
          allTransitiveDependencies:
              DirectMessagesFamily._allTransitiveDependencies,
          profile: profile,
          page: page,
        );

  DirectMessagesProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<List<DirectMessage>, ExecError>> Function(
            DirectMessagesRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: DirectMessagesProvider._internal(
        (ref) => create(ref as DirectMessagesRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<DirectMessage>, ExecError>>
      createElement() {
    return _DirectMessagesProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is DirectMessagesProvider &&
        other.profile == profile &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin DirectMessagesRef
    on AutoDisposeFutureProviderRef<Result<List<DirectMessage>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _DirectMessagesProviderElement extends AutoDisposeFutureProviderElement<
    Result<List<DirectMessage>, ExecError>> with DirectMessagesRef {
  _DirectMessagesProviderElement(super.provider);

  @override
  Profile get profile => (origin as DirectMessagesProvider).profile;
  @override
  PagingData get page => (origin as DirectMessagesProvider).page;
}

String _$updateThreadHash() => r'425416c431e9f0c0f592c07eef2c3f3618c6fcfb';

/// See also [updateThread].
@ProviderFor(updateThread)
const updateThreadProvider = UpdateThreadFamily();

/// See also [updateThread].
class UpdateThreadFamily
    extends Family<AsyncValue<Result<List<DirectMessage>, ExecError>>> {
  /// See also [updateThread].
  const UpdateThreadFamily();

  /// See also [updateThread].
  UpdateThreadProvider call(
    Profile profile,
    String threadUri,
    PagingData page,
  ) {
    return UpdateThreadProvider(
      profile,
      threadUri,
      page,
    );
  }

  @override
  UpdateThreadProvider getProviderOverride(
    covariant UpdateThreadProvider provider,
  ) {
    return call(
      provider.profile,
      provider.threadUri,
      provider.page,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'updateThreadProvider';
}

/// See also [updateThread].
class UpdateThreadProvider
    extends AutoDisposeFutureProvider<Result<List<DirectMessage>, ExecError>> {
  /// See also [updateThread].
  UpdateThreadProvider(
    Profile profile,
    String threadUri,
    PagingData page,
  ) : this._internal(
          (ref) => updateThread(
            ref as UpdateThreadRef,
            profile,
            threadUri,
            page,
          ),
          from: updateThreadProvider,
          name: r'updateThreadProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$updateThreadHash,
          dependencies: UpdateThreadFamily._dependencies,
          allTransitiveDependencies:
              UpdateThreadFamily._allTransitiveDependencies,
          profile: profile,
          threadUri: threadUri,
          page: page,
        );

  UpdateThreadProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.threadUri,
    required this.page,
  }) : super.internal();

  final Profile profile;
  final String threadUri;
  final PagingData page;

  @override
  Override overrideWith(
    FutureOr<Result<List<DirectMessage>, ExecError>> Function(
            UpdateThreadRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: UpdateThreadProvider._internal(
        (ref) => create(ref as UpdateThreadRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        threadUri: threadUri,
        page: page,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<DirectMessage>, ExecError>>
      createElement() {
    return _UpdateThreadProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is UpdateThreadProvider &&
        other.profile == profile &&
        other.threadUri == threadUri &&
        other.page == page;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, threadUri.hashCode);
    hash = _SystemHash.combine(hash, page.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin UpdateThreadRef
    on AutoDisposeFutureProviderRef<Result<List<DirectMessage>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `threadUri` of this provider.
  String get threadUri;

  /// The parameter `page` of this provider.
  PagingData get page;
}

class _UpdateThreadProviderElement extends AutoDisposeFutureProviderElement<
    Result<List<DirectMessage>, ExecError>> with UpdateThreadRef {
  _UpdateThreadProviderElement(super.provider);

  @override
  Profile get profile => (origin as UpdateThreadProvider).profile;
  @override
  String get threadUri => (origin as UpdateThreadProvider).threadUri;
  @override
  PagingData get page => (origin as UpdateThreadProvider).page;
}

String _$markDirectMessageReadHash() =>
    r'62d62dd9e1cf638b0364cdeab742df00da685ccc';

/// See also [markDirectMessageRead].
@ProviderFor(markDirectMessageRead)
const markDirectMessageReadProvider = MarkDirectMessageReadFamily();

/// See also [markDirectMessageRead].
class MarkDirectMessageReadFamily
    extends Family<AsyncValue<Result<DirectMessage, ExecError>>> {
  /// See also [markDirectMessageRead].
  const MarkDirectMessageReadFamily();

  /// See also [markDirectMessageRead].
  MarkDirectMessageReadProvider call(
    Profile profile,
    DirectMessage message,
  ) {
    return MarkDirectMessageReadProvider(
      profile,
      message,
    );
  }

  @override
  MarkDirectMessageReadProvider getProviderOverride(
    covariant MarkDirectMessageReadProvider provider,
  ) {
    return call(
      provider.profile,
      provider.message,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'markDirectMessageReadProvider';
}

/// See also [markDirectMessageRead].
class MarkDirectMessageReadProvider
    extends AutoDisposeFutureProvider<Result<DirectMessage, ExecError>> {
  /// See also [markDirectMessageRead].
  MarkDirectMessageReadProvider(
    Profile profile,
    DirectMessage message,
  ) : this._internal(
          (ref) => markDirectMessageRead(
            ref as MarkDirectMessageReadRef,
            profile,
            message,
          ),
          from: markDirectMessageReadProvider,
          name: r'markDirectMessageReadProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$markDirectMessageReadHash,
          dependencies: MarkDirectMessageReadFamily._dependencies,
          allTransitiveDependencies:
              MarkDirectMessageReadFamily._allTransitiveDependencies,
          profile: profile,
          message: message,
        );

  MarkDirectMessageReadProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.message,
  }) : super.internal();

  final Profile profile;
  final DirectMessage message;

  @override
  Override overrideWith(
    FutureOr<Result<DirectMessage, ExecError>> Function(
            MarkDirectMessageReadRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: MarkDirectMessageReadProvider._internal(
        (ref) => create(ref as MarkDirectMessageReadRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        message: message,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<DirectMessage, ExecError>>
      createElement() {
    return _MarkDirectMessageReadProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is MarkDirectMessageReadProvider &&
        other.profile == profile &&
        other.message == message;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, message.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin MarkDirectMessageReadRef
    on AutoDisposeFutureProviderRef<Result<DirectMessage, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `message` of this provider.
  DirectMessage get message;
}

class _MarkDirectMessageReadProviderElement
    extends AutoDisposeFutureProviderElement<Result<DirectMessage, ExecError>>
    with MarkDirectMessageReadRef {
  _MarkDirectMessageReadProviderElement(super.provider);

  @override
  Profile get profile => (origin as MarkDirectMessageReadProvider).profile;
  @override
  DirectMessage get message =>
      (origin as MarkDirectMessageReadProvider).message;
}

String _$postDirectMessageHash() => r'866f5aa1a0b6b8eba36e1059ed8a3da9c623e47f';

/// See also [postDirectMessage].
@ProviderFor(postDirectMessage)
const postDirectMessageProvider = PostDirectMessageFamily();

/// See also [postDirectMessage].
class PostDirectMessageFamily
    extends Family<AsyncValue<Result<DirectMessage, ExecError>>> {
  /// See also [postDirectMessage].
  const PostDirectMessageFamily();

  /// See also [postDirectMessage].
  PostDirectMessageProvider call(
    Profile profile,
    String? messageIdRepliedTo,
    String receivingUserId,
    String text,
  ) {
    return PostDirectMessageProvider(
      profile,
      messageIdRepliedTo,
      receivingUserId,
      text,
    );
  }

  @override
  PostDirectMessageProvider getProviderOverride(
    covariant PostDirectMessageProvider provider,
  ) {
    return call(
      provider.profile,
      provider.messageIdRepliedTo,
      provider.receivingUserId,
      provider.text,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'postDirectMessageProvider';
}

/// See also [postDirectMessage].
class PostDirectMessageProvider
    extends AutoDisposeFutureProvider<Result<DirectMessage, ExecError>> {
  /// See also [postDirectMessage].
  PostDirectMessageProvider(
    Profile profile,
    String? messageIdRepliedTo,
    String receivingUserId,
    String text,
  ) : this._internal(
          (ref) => postDirectMessage(
            ref as PostDirectMessageRef,
            profile,
            messageIdRepliedTo,
            receivingUserId,
            text,
          ),
          from: postDirectMessageProvider,
          name: r'postDirectMessageProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$postDirectMessageHash,
          dependencies: PostDirectMessageFamily._dependencies,
          allTransitiveDependencies:
              PostDirectMessageFamily._allTransitiveDependencies,
          profile: profile,
          messageIdRepliedTo: messageIdRepliedTo,
          receivingUserId: receivingUserId,
          text: text,
        );

  PostDirectMessageProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.messageIdRepliedTo,
    required this.receivingUserId,
    required this.text,
  }) : super.internal();

  final Profile profile;
  final String? messageIdRepliedTo;
  final String receivingUserId;
  final String text;

  @override
  Override overrideWith(
    FutureOr<Result<DirectMessage, ExecError>> Function(
            PostDirectMessageRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: PostDirectMessageProvider._internal(
        (ref) => create(ref as PostDirectMessageRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        messageIdRepliedTo: messageIdRepliedTo,
        receivingUserId: receivingUserId,
        text: text,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<DirectMessage, ExecError>>
      createElement() {
    return _PostDirectMessageProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is PostDirectMessageProvider &&
        other.profile == profile &&
        other.messageIdRepliedTo == messageIdRepliedTo &&
        other.receivingUserId == receivingUserId &&
        other.text == text;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, messageIdRepliedTo.hashCode);
    hash = _SystemHash.combine(hash, receivingUserId.hashCode);
    hash = _SystemHash.combine(hash, text.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin PostDirectMessageRef
    on AutoDisposeFutureProviderRef<Result<DirectMessage, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `messageIdRepliedTo` of this provider.
  String? get messageIdRepliedTo;

  /// The parameter `receivingUserId` of this provider.
  String get receivingUserId;

  /// The parameter `text` of this provider.
  String get text;
}

class _PostDirectMessageProviderElement
    extends AutoDisposeFutureProviderElement<Result<DirectMessage, ExecError>>
    with PostDirectMessageRef {
  _PostDirectMessageProviderElement(super.provider);

  @override
  Profile get profile => (origin as PostDirectMessageProvider).profile;
  @override
  String? get messageIdRepliedTo =>
      (origin as PostDirectMessageProvider).messageIdRepliedTo;
  @override
  String get receivingUserId =>
      (origin as PostDirectMessageProvider).receivingUserId;
  @override
  String get text => (origin as PostDirectMessageProvider).text;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
