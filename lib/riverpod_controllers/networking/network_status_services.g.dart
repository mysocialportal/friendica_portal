// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'network_status_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$postCommentLoadingStatusHash() =>
    r'e141bbadf195cd67767aaab4aa670fcca5a13695';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$PostCommentLoadingStatus
    extends BuildlessAutoDisposeNotifier<bool> {
  late final Profile profile;
  late final String id;

  bool build(
    Profile profile,
    String id,
  );
}

/// See also [PostCommentLoadingStatus].
@ProviderFor(PostCommentLoadingStatus)
const postCommentLoadingStatusProvider = PostCommentLoadingStatusFamily();

/// See also [PostCommentLoadingStatus].
class PostCommentLoadingStatusFamily extends Family<bool> {
  /// See also [PostCommentLoadingStatus].
  const PostCommentLoadingStatusFamily();

  /// See also [PostCommentLoadingStatus].
  PostCommentLoadingStatusProvider call(
    Profile profile,
    String id,
  ) {
    return PostCommentLoadingStatusProvider(
      profile,
      id,
    );
  }

  @override
  PostCommentLoadingStatusProvider getProviderOverride(
    covariant PostCommentLoadingStatusProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'postCommentLoadingStatusProvider';
}

/// See also [PostCommentLoadingStatus].
class PostCommentLoadingStatusProvider
    extends AutoDisposeNotifierProviderImpl<PostCommentLoadingStatus, bool> {
  /// See also [PostCommentLoadingStatus].
  PostCommentLoadingStatusProvider(
    Profile profile,
    String id,
  ) : this._internal(
          () => PostCommentLoadingStatus()
            ..profile = profile
            ..id = id,
          from: postCommentLoadingStatusProvider,
          name: r'postCommentLoadingStatusProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$postCommentLoadingStatusHash,
          dependencies: PostCommentLoadingStatusFamily._dependencies,
          allTransitiveDependencies:
              PostCommentLoadingStatusFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  PostCommentLoadingStatusProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  bool runNotifierBuild(
    covariant PostCommentLoadingStatus notifier,
  ) {
    return notifier.build(
      profile,
      id,
    );
  }

  @override
  Override overrideWith(PostCommentLoadingStatus Function() create) {
    return ProviderOverride(
      origin: this,
      override: PostCommentLoadingStatusProvider._internal(
        () => create()
          ..profile = profile
          ..id = id,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<PostCommentLoadingStatus, bool>
      createElement() {
    return _PostCommentLoadingStatusProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is PostCommentLoadingStatusProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin PostCommentLoadingStatusRef on AutoDisposeNotifierProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _PostCommentLoadingStatusProviderElement
    extends AutoDisposeNotifierProviderElement<PostCommentLoadingStatus, bool>
    with PostCommentLoadingStatusRef {
  _PostCommentLoadingStatusProviderElement(super.provider);

  @override
  Profile get profile => (origin as PostCommentLoadingStatusProvider).profile;
  @override
  String get id => (origin as PostCommentLoadingStatusProvider).id;
}

String _$timelineLoadingStatusHash() =>
    r'9bb7b4a61cd61c12c38d6ef3a62a2bef7bf33691';

abstract class _$TimelineLoadingStatus
    extends BuildlessAutoDisposeNotifier<bool> {
  late final Profile profile;
  late final TimelineIdentifiers type;

  bool build(
    Profile profile,
    TimelineIdentifiers type,
  );
}

/// See also [TimelineLoadingStatus].
@ProviderFor(TimelineLoadingStatus)
const timelineLoadingStatusProvider = TimelineLoadingStatusFamily();

/// See also [TimelineLoadingStatus].
class TimelineLoadingStatusFamily extends Family<bool> {
  /// See also [TimelineLoadingStatus].
  const TimelineLoadingStatusFamily();

  /// See also [TimelineLoadingStatus].
  TimelineLoadingStatusProvider call(
    Profile profile,
    TimelineIdentifiers type,
  ) {
    return TimelineLoadingStatusProvider(
      profile,
      type,
    );
  }

  @override
  TimelineLoadingStatusProvider getProviderOverride(
    covariant TimelineLoadingStatusProvider provider,
  ) {
    return call(
      provider.profile,
      provider.type,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'timelineLoadingStatusProvider';
}

/// See also [TimelineLoadingStatus].
class TimelineLoadingStatusProvider
    extends AutoDisposeNotifierProviderImpl<TimelineLoadingStatus, bool> {
  /// See also [TimelineLoadingStatus].
  TimelineLoadingStatusProvider(
    Profile profile,
    TimelineIdentifiers type,
  ) : this._internal(
          () => TimelineLoadingStatus()
            ..profile = profile
            ..type = type,
          from: timelineLoadingStatusProvider,
          name: r'timelineLoadingStatusProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$timelineLoadingStatusHash,
          dependencies: TimelineLoadingStatusFamily._dependencies,
          allTransitiveDependencies:
              TimelineLoadingStatusFamily._allTransitiveDependencies,
          profile: profile,
          type: type,
        );

  TimelineLoadingStatusProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.type,
  }) : super.internal();

  final Profile profile;
  final TimelineIdentifiers type;

  @override
  bool runNotifierBuild(
    covariant TimelineLoadingStatus notifier,
  ) {
    return notifier.build(
      profile,
      type,
    );
  }

  @override
  Override overrideWith(TimelineLoadingStatus Function() create) {
    return ProviderOverride(
      origin: this,
      override: TimelineLoadingStatusProvider._internal(
        () => create()
          ..profile = profile
          ..type = type,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        type: type,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<TimelineLoadingStatus, bool>
      createElement() {
    return _TimelineLoadingStatusProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TimelineLoadingStatusProvider &&
        other.profile == profile &&
        other.type == type;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, type.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin TimelineLoadingStatusRef on AutoDisposeNotifierProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `type` of this provider.
  TimelineIdentifiers get type;
}

class _TimelineLoadingStatusProviderElement
    extends AutoDisposeNotifierProviderElement<TimelineLoadingStatus, bool>
    with TimelineLoadingStatusRef {
  _TimelineLoadingStatusProviderElement(super.provider);

  @override
  Profile get profile => (origin as TimelineLoadingStatusProvider).profile;
  @override
  TimelineIdentifiers get type =>
      (origin as TimelineLoadingStatusProvider).type;
}

String _$searchLoadingStatusHash() =>
    r'4768772e44f40c8b7f0a5862bd3f64b5e38d8ab8';

abstract class _$SearchLoadingStatus
    extends BuildlessAutoDisposeNotifier<bool> {
  late final Profile profile;

  bool build(
    Profile profile,
  );
}

/// See also [SearchLoadingStatus].
@ProviderFor(SearchLoadingStatus)
const searchLoadingStatusProvider = SearchLoadingStatusFamily();

/// See also [SearchLoadingStatus].
class SearchLoadingStatusFamily extends Family<bool> {
  /// See also [SearchLoadingStatus].
  const SearchLoadingStatusFamily();

  /// See also [SearchLoadingStatus].
  SearchLoadingStatusProvider call(
    Profile profile,
  ) {
    return SearchLoadingStatusProvider(
      profile,
    );
  }

  @override
  SearchLoadingStatusProvider getProviderOverride(
    covariant SearchLoadingStatusProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'searchLoadingStatusProvider';
}

/// See also [SearchLoadingStatus].
class SearchLoadingStatusProvider
    extends AutoDisposeNotifierProviderImpl<SearchLoadingStatus, bool> {
  /// See also [SearchLoadingStatus].
  SearchLoadingStatusProvider(
    Profile profile,
  ) : this._internal(
          () => SearchLoadingStatus()..profile = profile,
          from: searchLoadingStatusProvider,
          name: r'searchLoadingStatusProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$searchLoadingStatusHash,
          dependencies: SearchLoadingStatusFamily._dependencies,
          allTransitiveDependencies:
              SearchLoadingStatusFamily._allTransitiveDependencies,
          profile: profile,
        );

  SearchLoadingStatusProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  bool runNotifierBuild(
    covariant SearchLoadingStatus notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(SearchLoadingStatus Function() create) {
    return ProviderOverride(
      origin: this,
      override: SearchLoadingStatusProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<SearchLoadingStatus, bool>
      createElement() {
    return _SearchLoadingStatusProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SearchLoadingStatusProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin SearchLoadingStatusRef on AutoDisposeNotifierProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _SearchLoadingStatusProviderElement
    extends AutoDisposeNotifierProviderElement<SearchLoadingStatus, bool>
    with SearchLoadingStatusRef {
  _SearchLoadingStatusProviderElement(super.provider);

  @override
  Profile get profile => (origin as SearchLoadingStatusProvider).profile;
}

String _$directMessageLoadingHash() =>
    r'1d96249703fbcdabd55344c214866d9d370ea039';

abstract class _$DirectMessageLoading
    extends BuildlessAutoDisposeNotifier<bool> {
  late final Profile profile;

  bool build(
    Profile profile,
  );
}

/// See also [DirectMessageLoading].
@ProviderFor(DirectMessageLoading)
const directMessageLoadingProvider = DirectMessageLoadingFamily();

/// See also [DirectMessageLoading].
class DirectMessageLoadingFamily extends Family<bool> {
  /// See also [DirectMessageLoading].
  const DirectMessageLoadingFamily();

  /// See also [DirectMessageLoading].
  DirectMessageLoadingProvider call(
    Profile profile,
  ) {
    return DirectMessageLoadingProvider(
      profile,
    );
  }

  @override
  DirectMessageLoadingProvider getProviderOverride(
    covariant DirectMessageLoadingProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'directMessageLoadingProvider';
}

/// See also [DirectMessageLoading].
class DirectMessageLoadingProvider
    extends AutoDisposeNotifierProviderImpl<DirectMessageLoading, bool> {
  /// See also [DirectMessageLoading].
  DirectMessageLoadingProvider(
    Profile profile,
  ) : this._internal(
          () => DirectMessageLoading()..profile = profile,
          from: directMessageLoadingProvider,
          name: r'directMessageLoadingProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$directMessageLoadingHash,
          dependencies: DirectMessageLoadingFamily._dependencies,
          allTransitiveDependencies:
              DirectMessageLoadingFamily._allTransitiveDependencies,
          profile: profile,
        );

  DirectMessageLoadingProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  bool runNotifierBuild(
    covariant DirectMessageLoading notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(DirectMessageLoading Function() create) {
    return ProviderOverride(
      origin: this,
      override: DirectMessageLoadingProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<DirectMessageLoading, bool>
      createElement() {
    return _DirectMessageLoadingProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is DirectMessageLoadingProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin DirectMessageLoadingRef on AutoDisposeNotifierProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _DirectMessageLoadingProviderElement
    extends AutoDisposeNotifierProviderElement<DirectMessageLoading, bool>
    with DirectMessageLoadingRef {
  _DirectMessageLoadingProviderElement(super.provider);

  @override
  Profile get profile => (origin as DirectMessageLoadingProvider).profile;
}

String _$notificationsLoadingHash() =>
    r'e87138577153b534e92bc9b5b02b99f13c2afe06';

abstract class _$NotificationsLoading
    extends BuildlessAutoDisposeNotifier<bool> {
  late final Profile profile;

  bool build(
    Profile profile,
  );
}

/// See also [NotificationsLoading].
@ProviderFor(NotificationsLoading)
const notificationsLoadingProvider = NotificationsLoadingFamily();

/// See also [NotificationsLoading].
class NotificationsLoadingFamily extends Family<bool> {
  /// See also [NotificationsLoading].
  const NotificationsLoadingFamily();

  /// See also [NotificationsLoading].
  NotificationsLoadingProvider call(
    Profile profile,
  ) {
    return NotificationsLoadingProvider(
      profile,
    );
  }

  @override
  NotificationsLoadingProvider getProviderOverride(
    covariant NotificationsLoadingProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'notificationsLoadingProvider';
}

/// See also [NotificationsLoading].
class NotificationsLoadingProvider
    extends AutoDisposeNotifierProviderImpl<NotificationsLoading, bool> {
  /// See also [NotificationsLoading].
  NotificationsLoadingProvider(
    Profile profile,
  ) : this._internal(
          () => NotificationsLoading()..profile = profile,
          from: notificationsLoadingProvider,
          name: r'notificationsLoadingProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$notificationsLoadingHash,
          dependencies: NotificationsLoadingFamily._dependencies,
          allTransitiveDependencies:
              NotificationsLoadingFamily._allTransitiveDependencies,
          profile: profile,
        );

  NotificationsLoadingProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  bool runNotifierBuild(
    covariant NotificationsLoading notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(NotificationsLoading Function() create) {
    return ProviderOverride(
      origin: this,
      override: NotificationsLoadingProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<NotificationsLoading, bool>
      createElement() {
    return _NotificationsLoadingProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is NotificationsLoadingProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin NotificationsLoadingRef on AutoDisposeNotifierProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _NotificationsLoadingProviderElement
    extends AutoDisposeNotifierProviderElement<NotificationsLoading, bool>
    with NotificationsLoadingRef {
  _NotificationsLoadingProviderElement(super.provider);

  @override
  Profile get profile => (origin as NotificationsLoadingProvider).profile;
}

String _$connectionsLoadingHash() =>
    r'ecbd1eb44d2c5385a4a48481d44d00dfa9c5099e';

abstract class _$ConnectionsLoading extends BuildlessAutoDisposeNotifier<bool> {
  late final Profile profile;

  bool build(
    Profile profile,
  );
}

/// See also [ConnectionsLoading].
@ProviderFor(ConnectionsLoading)
const connectionsLoadingProvider = ConnectionsLoadingFamily();

/// See also [ConnectionsLoading].
class ConnectionsLoadingFamily extends Family<bool> {
  /// See also [ConnectionsLoading].
  const ConnectionsLoadingFamily();

  /// See also [ConnectionsLoading].
  ConnectionsLoadingProvider call(
    Profile profile,
  ) {
    return ConnectionsLoadingProvider(
      profile,
    );
  }

  @override
  ConnectionsLoadingProvider getProviderOverride(
    covariant ConnectionsLoadingProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'connectionsLoadingProvider';
}

/// See also [ConnectionsLoading].
class ConnectionsLoadingProvider
    extends AutoDisposeNotifierProviderImpl<ConnectionsLoading, bool> {
  /// See also [ConnectionsLoading].
  ConnectionsLoadingProvider(
    Profile profile,
  ) : this._internal(
          () => ConnectionsLoading()..profile = profile,
          from: connectionsLoadingProvider,
          name: r'connectionsLoadingProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$connectionsLoadingHash,
          dependencies: ConnectionsLoadingFamily._dependencies,
          allTransitiveDependencies:
              ConnectionsLoadingFamily._allTransitiveDependencies,
          profile: profile,
        );

  ConnectionsLoadingProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  bool runNotifierBuild(
    covariant ConnectionsLoading notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(ConnectionsLoading Function() create) {
    return ProviderOverride(
      origin: this,
      override: ConnectionsLoadingProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<ConnectionsLoading, bool> createElement() {
    return _ConnectionsLoadingProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ConnectionsLoadingProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ConnectionsLoadingRef on AutoDisposeNotifierProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _ConnectionsLoadingProviderElement
    extends AutoDisposeNotifierProviderElement<ConnectionsLoading, bool>
    with ConnectionsLoadingRef {
  _ConnectionsLoadingProviderElement(super.provider);

  @override
  Profile get profile => (origin as ConnectionsLoadingProvider).profile;
}

String _$followRequestsLoadingHash() =>
    r'd66bc183fad1e5ee37fcfc5e685fee37c5daa212';

abstract class _$FollowRequestsLoading
    extends BuildlessAutoDisposeNotifier<bool> {
  late final Profile profile;

  bool build(
    Profile profile,
  );
}

/// See also [FollowRequestsLoading].
@ProviderFor(FollowRequestsLoading)
const followRequestsLoadingProvider = FollowRequestsLoadingFamily();

/// See also [FollowRequestsLoading].
class FollowRequestsLoadingFamily extends Family<bool> {
  /// See also [FollowRequestsLoading].
  const FollowRequestsLoadingFamily();

  /// See also [FollowRequestsLoading].
  FollowRequestsLoadingProvider call(
    Profile profile,
  ) {
    return FollowRequestsLoadingProvider(
      profile,
    );
  }

  @override
  FollowRequestsLoadingProvider getProviderOverride(
    covariant FollowRequestsLoadingProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'followRequestsLoadingProvider';
}

/// See also [FollowRequestsLoading].
class FollowRequestsLoadingProvider
    extends AutoDisposeNotifierProviderImpl<FollowRequestsLoading, bool> {
  /// See also [FollowRequestsLoading].
  FollowRequestsLoadingProvider(
    Profile profile,
  ) : this._internal(
          () => FollowRequestsLoading()..profile = profile,
          from: followRequestsLoadingProvider,
          name: r'followRequestsLoadingProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$followRequestsLoadingHash,
          dependencies: FollowRequestsLoadingFamily._dependencies,
          allTransitiveDependencies:
              FollowRequestsLoadingFamily._allTransitiveDependencies,
          profile: profile,
        );

  FollowRequestsLoadingProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  bool runNotifierBuild(
    covariant FollowRequestsLoading notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(FollowRequestsLoading Function() create) {
    return ProviderOverride(
      origin: this,
      override: FollowRequestsLoadingProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<FollowRequestsLoading, bool>
      createElement() {
    return _FollowRequestsLoadingProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FollowRequestsLoadingProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FollowRequestsLoadingRef on AutoDisposeNotifierProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _FollowRequestsLoadingProviderElement
    extends AutoDisposeNotifierProviderElement<FollowRequestsLoading, bool>
    with FollowRequestsLoadingRef {
  _FollowRequestsLoadingProviderElement(super.provider);

  @override
  Profile get profile => (origin as FollowRequestsLoadingProvider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
