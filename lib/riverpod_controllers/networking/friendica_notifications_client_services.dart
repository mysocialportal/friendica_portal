import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../globals.dart';
import '../../models/auth/profile.dart';
import '../../models/exec_error.dart';
import '../../models/networking/paged_response.dart';
import '../../models/networking/paging_data.dart';
import '../../models/user_notification.dart';
import '../../serializers/mastodon/notification_mastodon_extension.dart';
import 'friendica_client_services.dart';
import 'network_services.dart';
import 'network_status_services.dart';

part 'friendica_notifications_client_services.g.dart';

final _logger = Logger('FriendicaNotificationsClient');

@riverpod
Future<Result<PagedResponse<List<UserNotification>>, ExecError>>
    notificationsClient(
  Ref ref,
  Profile profile,
  PagingData page,
  bool includeAll,
) async {
  ref.read(notificationsLoadingProvider(profile));
  Future.microtask(
    () async =>
        ref.read(notificationsLoadingProvider(profile).notifier).begin(),
  );
  final url =
      'https://${profile.serverName}/api/v1/notifications?include_all=$includeAll';
  final request = Uri.parse('$url&${page.toQueryParameters()}');
  _logger.finest(() => 'Getting new notifications');
  final result = await ref
      .read(getApiListRequestProvider(profile, request).future)
      .transformAsync((response) async {
    final notifications = <UserNotification>[];

    final st = Stopwatch()..start();
    for (final json in response.data) {
      if (st.elapsedMilliseconds > maxProcessingMillis) {
        await Future.delayed(processingSleep, () => st.reset());
      }
      notifications.add(userNotificationFromJson(ref, profile, json));
    }
    return PagedResponse(
      notifications,
      id: response.id,
      previous: response.previous,
      next: response.next,
    );
  });
  ref.read(notificationsLoadingProvider(profile).notifier).end();
  return result.execErrorCast();
}

@riverpod
Future<Result<bool, ExecError>> clearNotifications(
  Ref ref,
  Profile profile,
) async {
  _logger.finest(() => 'Clearing unread notification');
  final url = 'https://${profile.serverName}/api/v1/notifications/clear';
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(Uri.parse(url), headers: headers);
  return ref.read(httpPostProvider(request).future).mapValue((_) => true);
}

@riverpod
Future<Result<bool, ExecError>> clearNotification(
  Ref ref,
  Profile profile,
  UserNotification notification,
) async {
  _logger.fine(() => 'Clearing unread notification for $notification');
  final url =
      'https://${profile.serverName}/api/v1/notifications/${notification.id}/dismiss';
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(Uri.parse(url), headers: headers);
  return ref.read(httpPostProvider(request).future).mapValue((_) => true);
}
