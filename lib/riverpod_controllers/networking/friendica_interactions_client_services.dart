import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../models/auth/profile.dart';
import '../../models/connection.dart';
import '../../models/exec_error.dart';
import '../../models/timeline_entry.dart';
import '../../serializers/mastodon/connection_mastodon_extensions.dart';
import '../../serializers/mastodon/timeline_entry_mastodon_extensions.dart';
import 'friendica_client_services.dart';
import 'network_services.dart';

part 'friendica_interactions_client_services.g.dart';

final _logger = Logger('FriendicaInteractionsClient');

// TODO Convert getLikes to using paging for real
@riverpod
Future<Result<List<Connection>, ExecError>> likesClient(
  Ref ref,
  Profile profile,
  String id,
) async {
  _logger.finest(() => 'Getting favorites for status $id');
  final url = 'https://${profile.serverName}/api/v1/statuses/$id/favourited_by';
  final request = Uri.parse(url);
  final result = await ref
      .read(getApiListRequestProvider(profile, request).future)
      .transform((jsonArray) => jsonArray.data
          .map((p) => connectionFromJson(ref, profile, p))
          .toList())
      .execErrorCastAsync();
  return result;
}

// TODO Convert getReshares to using paging for real
@riverpod
Future<Result<List<Connection>, ExecError>> resharesClient(
  Ref ref,
  Profile profile,
  String id,
) async {
  final url = 'https://${profile.serverName}/api/v1/statuses/$id/reblogged_by';
  final request = Uri.parse(url);
  _logger.finest(() => 'Getting rebloggers for status $id');
  final result = await ref
      .read(getApiListRequestProvider(profile, request).future)
      .transform((jsonArray) => jsonArray.data
          .map((p) => connectionFromJson(ref, profile, p))
          .toList())
      .execErrorCastAsync();
  return result;
}

@riverpod
Future<Result<TimelineEntry, ExecError>> changeFavoriteStatus(
  Ref ref,
  Profile profile,
  String id,
  bool status,
) async {
  final action = status ? 'favourite' : 'unfavourite';
  final url =
      Uri.parse('https://${profile.serverName}/api/v1/statuses/$id/$action');
  final headers = ref.read(friendicaClientHeadersProvider(profile));
  final request = NetworkRequest(url, headers: headers);
  final result = await ref
      .read(httpPostProvider(request).future)
      .transform((data) => jsonDecode(data))
      .transform((json) => timelineEntryFromJson(ref, profile, json))
      .execErrorCastAsync();
  return result;
}
