// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friendica_interactions_client_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$likesClientHash() => r'41bb22a08207a21a5a39e40f9ffbe0eb0380afd9';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [likesClient].
@ProviderFor(likesClient)
const likesClientProvider = LikesClientFamily();

/// See also [likesClient].
class LikesClientFamily
    extends Family<AsyncValue<Result<List<Connection>, ExecError>>> {
  /// See also [likesClient].
  const LikesClientFamily();

  /// See also [likesClient].
  LikesClientProvider call(
    Profile profile,
    String id,
  ) {
    return LikesClientProvider(
      profile,
      id,
    );
  }

  @override
  LikesClientProvider getProviderOverride(
    covariant LikesClientProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'likesClientProvider';
}

/// See also [likesClient].
class LikesClientProvider
    extends AutoDisposeFutureProvider<Result<List<Connection>, ExecError>> {
  /// See also [likesClient].
  LikesClientProvider(
    Profile profile,
    String id,
  ) : this._internal(
          (ref) => likesClient(
            ref as LikesClientRef,
            profile,
            id,
          ),
          from: likesClientProvider,
          name: r'likesClientProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$likesClientHash,
          dependencies: LikesClientFamily._dependencies,
          allTransitiveDependencies:
              LikesClientFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  LikesClientProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  Override overrideWith(
    FutureOr<Result<List<Connection>, ExecError>> Function(
            LikesClientRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: LikesClientProvider._internal(
        (ref) => create(ref as LikesClientRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<Connection>, ExecError>>
      createElement() {
    return _LikesClientProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is LikesClientProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin LikesClientRef
    on AutoDisposeFutureProviderRef<Result<List<Connection>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _LikesClientProviderElement extends AutoDisposeFutureProviderElement<
    Result<List<Connection>, ExecError>> with LikesClientRef {
  _LikesClientProviderElement(super.provider);

  @override
  Profile get profile => (origin as LikesClientProvider).profile;
  @override
  String get id => (origin as LikesClientProvider).id;
}

String _$resharesClientHash() => r'65998fc7fe523be98cd1c8157f69a7d934e09da7';

/// See also [resharesClient].
@ProviderFor(resharesClient)
const resharesClientProvider = ResharesClientFamily();

/// See also [resharesClient].
class ResharesClientFamily
    extends Family<AsyncValue<Result<List<Connection>, ExecError>>> {
  /// See also [resharesClient].
  const ResharesClientFamily();

  /// See also [resharesClient].
  ResharesClientProvider call(
    Profile profile,
    String id,
  ) {
    return ResharesClientProvider(
      profile,
      id,
    );
  }

  @override
  ResharesClientProvider getProviderOverride(
    covariant ResharesClientProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'resharesClientProvider';
}

/// See also [resharesClient].
class ResharesClientProvider
    extends AutoDisposeFutureProvider<Result<List<Connection>, ExecError>> {
  /// See also [resharesClient].
  ResharesClientProvider(
    Profile profile,
    String id,
  ) : this._internal(
          (ref) => resharesClient(
            ref as ResharesClientRef,
            profile,
            id,
          ),
          from: resharesClientProvider,
          name: r'resharesClientProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$resharesClientHash,
          dependencies: ResharesClientFamily._dependencies,
          allTransitiveDependencies:
              ResharesClientFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  ResharesClientProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  Override overrideWith(
    FutureOr<Result<List<Connection>, ExecError>> Function(
            ResharesClientRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ResharesClientProvider._internal(
        (ref) => create(ref as ResharesClientRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<List<Connection>, ExecError>>
      createElement() {
    return _ResharesClientProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ResharesClientProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ResharesClientRef
    on AutoDisposeFutureProviderRef<Result<List<Connection>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _ResharesClientProviderElement extends AutoDisposeFutureProviderElement<
    Result<List<Connection>, ExecError>> with ResharesClientRef {
  _ResharesClientProviderElement(super.provider);

  @override
  Profile get profile => (origin as ResharesClientProvider).profile;
  @override
  String get id => (origin as ResharesClientProvider).id;
}

String _$changeFavoriteStatusHash() =>
    r'9f798c0e2c7bff22cbbec8b96539dc2811456a72';

/// See also [changeFavoriteStatus].
@ProviderFor(changeFavoriteStatus)
const changeFavoriteStatusProvider = ChangeFavoriteStatusFamily();

/// See also [changeFavoriteStatus].
class ChangeFavoriteStatusFamily
    extends Family<AsyncValue<Result<TimelineEntry, ExecError>>> {
  /// See also [changeFavoriteStatus].
  const ChangeFavoriteStatusFamily();

  /// See also [changeFavoriteStatus].
  ChangeFavoriteStatusProvider call(
    Profile profile,
    String id,
    bool status,
  ) {
    return ChangeFavoriteStatusProvider(
      profile,
      id,
      status,
    );
  }

  @override
  ChangeFavoriteStatusProvider getProviderOverride(
    covariant ChangeFavoriteStatusProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
      provider.status,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'changeFavoriteStatusProvider';
}

/// See also [changeFavoriteStatus].
class ChangeFavoriteStatusProvider
    extends AutoDisposeFutureProvider<Result<TimelineEntry, ExecError>> {
  /// See also [changeFavoriteStatus].
  ChangeFavoriteStatusProvider(
    Profile profile,
    String id,
    bool status,
  ) : this._internal(
          (ref) => changeFavoriteStatus(
            ref as ChangeFavoriteStatusRef,
            profile,
            id,
            status,
          ),
          from: changeFavoriteStatusProvider,
          name: r'changeFavoriteStatusProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$changeFavoriteStatusHash,
          dependencies: ChangeFavoriteStatusFamily._dependencies,
          allTransitiveDependencies:
              ChangeFavoriteStatusFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
          status: status,
        );

  ChangeFavoriteStatusProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
    required this.status,
  }) : super.internal();

  final Profile profile;
  final String id;
  final bool status;

  @override
  Override overrideWith(
    FutureOr<Result<TimelineEntry, ExecError>> Function(
            ChangeFavoriteStatusRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ChangeFavoriteStatusProvider._internal(
        (ref) => create(ref as ChangeFavoriteStatusRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
        status: status,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<TimelineEntry, ExecError>>
      createElement() {
    return _ChangeFavoriteStatusProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ChangeFavoriteStatusProvider &&
        other.profile == profile &&
        other.id == id &&
        other.status == status;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);
    hash = _SystemHash.combine(hash, status.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ChangeFavoriteStatusRef
    on AutoDisposeFutureProviderRef<Result<TimelineEntry, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;

  /// The parameter `status` of this provider.
  bool get status;
}

class _ChangeFavoriteStatusProviderElement
    extends AutoDisposeFutureProviderElement<Result<TimelineEntry, ExecError>>
    with ChangeFavoriteStatusRef {
  _ChangeFavoriteStatusProviderElement(super.provider);

  @override
  Profile get profile => (origin as ChangeFavoriteStatusProvider).profile;
  @override
  String get id => (origin as ChangeFavoriteStatusProvider).id;
  @override
  bool get status => (origin as ChangeFavoriteStatusProvider).status;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
