// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'blocks_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$blocksManagerHash() => r'ae13c1091ffe55564a03453b2c50a82829815da6';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$BlocksManager
    extends BuildlessAsyncNotifier<List<Connection>> {
  late final Profile profile;

  FutureOr<List<Connection>> build(
    Profile profile,
  );
}

/// See also [BlocksManager].
@ProviderFor(BlocksManager)
const blocksManagerProvider = BlocksManagerFamily();

/// See also [BlocksManager].
class BlocksManagerFamily extends Family<AsyncValue<List<Connection>>> {
  /// See also [BlocksManager].
  const BlocksManagerFamily();

  /// See also [BlocksManager].
  BlocksManagerProvider call(
    Profile profile,
  ) {
    return BlocksManagerProvider(
      profile,
    );
  }

  @override
  BlocksManagerProvider getProviderOverride(
    covariant BlocksManagerProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'blocksManagerProvider';
}

/// See also [BlocksManager].
class BlocksManagerProvider
    extends AsyncNotifierProviderImpl<BlocksManager, List<Connection>> {
  /// See also [BlocksManager].
  BlocksManagerProvider(
    Profile profile,
  ) : this._internal(
          () => BlocksManager()..profile = profile,
          from: blocksManagerProvider,
          name: r'blocksManagerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$blocksManagerHash,
          dependencies: BlocksManagerFamily._dependencies,
          allTransitiveDependencies:
              BlocksManagerFamily._allTransitiveDependencies,
          profile: profile,
        );

  BlocksManagerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  FutureOr<List<Connection>> runNotifierBuild(
    covariant BlocksManager notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(BlocksManager Function() create) {
    return ProviderOverride(
      origin: this,
      override: BlocksManagerProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AsyncNotifierProviderElement<BlocksManager, List<Connection>>
      createElement() {
    return _BlocksManagerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is BlocksManagerProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin BlocksManagerRef on AsyncNotifierProviderRef<List<Connection>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _BlocksManagerProviderElement
    extends AsyncNotifierProviderElement<BlocksManager, List<Connection>>
    with BlocksManagerRef {
  _BlocksManagerProviderElement(super.provider);

  @override
  Profile get profile => (origin as BlocksManagerProvider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
