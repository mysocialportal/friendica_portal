// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'autocomplete_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$postTreeHashtagsHash() => r'b966d062a16aef86277b1d347d99ab7bf6269c9a';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [postTreeHashtags].
@ProviderFor(postTreeHashtags)
const postTreeHashtagsProvider = PostTreeHashtagsFamily();

/// See also [postTreeHashtags].
class PostTreeHashtagsFamily extends Family<Result<List<String>, ExecError>> {
  /// See also [postTreeHashtags].
  const PostTreeHashtagsFamily();

  /// See also [postTreeHashtags].
  PostTreeHashtagsProvider call(
    Profile profile,
    String id,
  ) {
    return PostTreeHashtagsProvider(
      profile,
      id,
    );
  }

  @override
  PostTreeHashtagsProvider getProviderOverride(
    covariant PostTreeHashtagsProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'postTreeHashtagsProvider';
}

/// See also [postTreeHashtags].
class PostTreeHashtagsProvider
    extends AutoDisposeProvider<Result<List<String>, ExecError>> {
  /// See also [postTreeHashtags].
  PostTreeHashtagsProvider(
    Profile profile,
    String id,
  ) : this._internal(
          (ref) => postTreeHashtags(
            ref as PostTreeHashtagsRef,
            profile,
            id,
          ),
          from: postTreeHashtagsProvider,
          name: r'postTreeHashtagsProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$postTreeHashtagsHash,
          dependencies: PostTreeHashtagsFamily._dependencies,
          allTransitiveDependencies:
              PostTreeHashtagsFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  PostTreeHashtagsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  Override overrideWith(
    Result<List<String>, ExecError> Function(PostTreeHashtagsRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: PostTreeHashtagsProvider._internal(
        (ref) => create(ref as PostTreeHashtagsRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<Result<List<String>, ExecError>> createElement() {
    return _PostTreeHashtagsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is PostTreeHashtagsProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin PostTreeHashtagsRef
    on AutoDisposeProviderRef<Result<List<String>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _PostTreeHashtagsProviderElement
    extends AutoDisposeProviderElement<Result<List<String>, ExecError>>
    with PostTreeHashtagsRef {
  _PostTreeHashtagsProviderElement(super.provider);

  @override
  Profile get profile => (origin as PostTreeHashtagsProvider).profile;
  @override
  String get id => (origin as PostTreeHashtagsProvider).id;
}

String _$postTreeConnectionIdsHash() =>
    r'68b1d34a750e313db02b3a487deeba084242400b';

/// See also [postTreeConnectionIds].
@ProviderFor(postTreeConnectionIds)
const postTreeConnectionIdsProvider = PostTreeConnectionIdsFamily();

/// See also [postTreeConnectionIds].
class PostTreeConnectionIdsFamily
    extends Family<Result<List<String>, ExecError>> {
  /// See also [postTreeConnectionIds].
  const PostTreeConnectionIdsFamily();

  /// See also [postTreeConnectionIds].
  PostTreeConnectionIdsProvider call(
    Profile profile,
    String id,
  ) {
    return PostTreeConnectionIdsProvider(
      profile,
      id,
    );
  }

  @override
  PostTreeConnectionIdsProvider getProviderOverride(
    covariant PostTreeConnectionIdsProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'postTreeConnectionIdsProvider';
}

/// See also [postTreeConnectionIds].
class PostTreeConnectionIdsProvider
    extends AutoDisposeProvider<Result<List<String>, ExecError>> {
  /// See also [postTreeConnectionIds].
  PostTreeConnectionIdsProvider(
    Profile profile,
    String id,
  ) : this._internal(
          (ref) => postTreeConnectionIds(
            ref as PostTreeConnectionIdsRef,
            profile,
            id,
          ),
          from: postTreeConnectionIdsProvider,
          name: r'postTreeConnectionIdsProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$postTreeConnectionIdsHash,
          dependencies: PostTreeConnectionIdsFamily._dependencies,
          allTransitiveDependencies:
              PostTreeConnectionIdsFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  PostTreeConnectionIdsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  Override overrideWith(
    Result<List<String>, ExecError> Function(PostTreeConnectionIdsRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: PostTreeConnectionIdsProvider._internal(
        (ref) => create(ref as PostTreeConnectionIdsRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<Result<List<String>, ExecError>> createElement() {
    return _PostTreeConnectionIdsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is PostTreeConnectionIdsProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin PostTreeConnectionIdsRef
    on AutoDisposeProviderRef<Result<List<String>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _PostTreeConnectionIdsProviderElement
    extends AutoDisposeProviderElement<Result<List<String>, ExecError>>
    with PostTreeConnectionIdsRef {
  _PostTreeConnectionIdsProviderElement(super.provider);

  @override
  Profile get profile => (origin as PostTreeConnectionIdsProvider).profile;
  @override
  String get id => (origin as PostTreeConnectionIdsProvider).id;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
