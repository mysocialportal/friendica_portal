// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'circles_repo_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$circleDataHash() => r'e42c763d83d111aa659c369334fdb95be95b0581';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [circleData].
@ProviderFor(circleData)
const circleDataProvider = CircleDataFamily();

/// See also [circleData].
class CircleDataFamily
    extends Family<Result<TimelineGroupingListData, ExecError>> {
  /// See also [circleData].
  const CircleDataFamily();

  /// See also [circleData].
  CircleDataProvider call(
    Profile profile,
    String id,
  ) {
    return CircleDataProvider(
      profile,
      id,
    );
  }

  @override
  CircleDataProvider getProviderOverride(
    covariant CircleDataProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'circleDataProvider';
}

/// See also [circleData].
class CircleDataProvider
    extends AutoDisposeProvider<Result<TimelineGroupingListData, ExecError>> {
  /// See also [circleData].
  CircleDataProvider(
    Profile profile,
    String id,
  ) : this._internal(
          (ref) => circleData(
            ref as CircleDataRef,
            profile,
            id,
          ),
          from: circleDataProvider,
          name: r'circleDataProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$circleDataHash,
          dependencies: CircleDataFamily._dependencies,
          allTransitiveDependencies:
              CircleDataFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  CircleDataProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  Override overrideWith(
    Result<TimelineGroupingListData, ExecError> Function(CircleDataRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: CircleDataProvider._internal(
        (ref) => create(ref as CircleDataRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<Result<TimelineGroupingListData, ExecError>>
      createElement() {
    return _CircleDataProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CircleDataProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin CircleDataRef
    on AutoDisposeProviderRef<Result<TimelineGroupingListData, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _CircleDataProviderElement extends AutoDisposeProviderElement<
    Result<TimelineGroupingListData, ExecError>> with CircleDataRef {
  _CircleDataProviderElement(super.provider);

  @override
  Profile get profile => (origin as CircleDataProvider).profile;
  @override
  String get id => (origin as CircleDataProvider).id;
}

String _$timelineGroupingListHash() =>
    r'6e423f2dee187a7692eb4462655771e47d3283bd';

/// See also [timelineGroupingList].
@ProviderFor(timelineGroupingList)
const timelineGroupingListProvider = TimelineGroupingListFamily();

/// See also [timelineGroupingList].
class TimelineGroupingListFamily
    extends Family<List<TimelineGroupingListData>> {
  /// See also [timelineGroupingList].
  const TimelineGroupingListFamily();

  /// See also [timelineGroupingList].
  TimelineGroupingListProvider call(
    Profile profile,
    GroupingType type,
  ) {
    return TimelineGroupingListProvider(
      profile,
      type,
    );
  }

  @override
  TimelineGroupingListProvider getProviderOverride(
    covariant TimelineGroupingListProvider provider,
  ) {
    return call(
      provider.profile,
      provider.type,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'timelineGroupingListProvider';
}

/// See also [timelineGroupingList].
class TimelineGroupingListProvider
    extends AutoDisposeProvider<List<TimelineGroupingListData>> {
  /// See also [timelineGroupingList].
  TimelineGroupingListProvider(
    Profile profile,
    GroupingType type,
  ) : this._internal(
          (ref) => timelineGroupingList(
            ref as TimelineGroupingListRef,
            profile,
            type,
          ),
          from: timelineGroupingListProvider,
          name: r'timelineGroupingListProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$timelineGroupingListHash,
          dependencies: TimelineGroupingListFamily._dependencies,
          allTransitiveDependencies:
              TimelineGroupingListFamily._allTransitiveDependencies,
          profile: profile,
          type: type,
        );

  TimelineGroupingListProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.type,
  }) : super.internal();

  final Profile profile;
  final GroupingType type;

  @override
  Override overrideWith(
    List<TimelineGroupingListData> Function(TimelineGroupingListRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: TimelineGroupingListProvider._internal(
        (ref) => create(ref as TimelineGroupingListRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        type: type,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<List<TimelineGroupingListData>> createElement() {
    return _TimelineGroupingListProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TimelineGroupingListProvider &&
        other.profile == profile &&
        other.type == type;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, type.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin TimelineGroupingListRef
    on AutoDisposeProviderRef<List<TimelineGroupingListData>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `type` of this provider.
  GroupingType get type;
}

class _TimelineGroupingListProviderElement
    extends AutoDisposeProviderElement<List<TimelineGroupingListData>>
    with TimelineGroupingListRef {
  _TimelineGroupingListProviderElement(super.provider);

  @override
  Profile get profile => (origin as TimelineGroupingListProvider).profile;
  @override
  GroupingType get type => (origin as TimelineGroupingListProvider).type;
}

String _$circlesRepoHash() => r'82b95a626e70888ea4a4f0a2c1a736cd0bdcf9c9';

abstract class _$CirclesRepo extends BuildlessNotifier<ICirclesRepo> {
  late final Profile profile;

  ICirclesRepo build(
    Profile profile,
  );
}

/// See also [_CirclesRepo].
@ProviderFor(_CirclesRepo)
const _circlesRepoProvider = _CirclesRepoFamily();

/// See also [_CirclesRepo].
class _CirclesRepoFamily extends Family<ICirclesRepo> {
  /// See also [_CirclesRepo].
  const _CirclesRepoFamily();

  /// See also [_CirclesRepo].
  _CirclesRepoProvider call(
    Profile profile,
  ) {
    return _CirclesRepoProvider(
      profile,
    );
  }

  @override
  _CirclesRepoProvider getProviderOverride(
    covariant _CirclesRepoProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'_circlesRepoProvider';
}

/// See also [_CirclesRepo].
class _CirclesRepoProvider
    extends NotifierProviderImpl<_CirclesRepo, ICirclesRepo> {
  /// See also [_CirclesRepo].
  _CirclesRepoProvider(
    Profile profile,
  ) : this._internal(
          () => _CirclesRepo()..profile = profile,
          from: _circlesRepoProvider,
          name: r'_circlesRepoProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$circlesRepoHash,
          dependencies: _CirclesRepoFamily._dependencies,
          allTransitiveDependencies:
              _CirclesRepoFamily._allTransitiveDependencies,
          profile: profile,
        );

  _CirclesRepoProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  ICirclesRepo runNotifierBuild(
    covariant _CirclesRepo notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(_CirclesRepo Function() create) {
    return ProviderOverride(
      origin: this,
      override: _CirclesRepoProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<_CirclesRepo, ICirclesRepo> createElement() {
    return _CirclesRepoProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is _CirclesRepoProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin _CirclesRepoRef on NotifierProviderRef<ICirclesRepo> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _CirclesRepoProviderElement
    extends NotifierProviderElement<_CirclesRepo, ICirclesRepo>
    with _CirclesRepoRef {
  _CirclesRepoProviderElement(super.provider);

  @override
  Profile get profile => (origin as _CirclesRepoProvider).profile;
}

String _$circlesHash() => r'84741e2bec4232522eadd6f83046b4d147645985';

abstract class _$Circles
    extends BuildlessNotifier<List<TimelineGroupingListData>> {
  late final Profile profile;

  List<TimelineGroupingListData> build(
    Profile profile,
  );
}

/// See also [Circles].
@ProviderFor(Circles)
const circlesProvider = CirclesFamily();

/// See also [Circles].
class CirclesFamily extends Family<List<TimelineGroupingListData>> {
  /// See also [Circles].
  const CirclesFamily();

  /// See also [Circles].
  CirclesProvider call(
    Profile profile,
  ) {
    return CirclesProvider(
      profile,
    );
  }

  @override
  CirclesProvider getProviderOverride(
    covariant CirclesProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'circlesProvider';
}

/// See also [Circles].
class CirclesProvider
    extends NotifierProviderImpl<Circles, List<TimelineGroupingListData>> {
  /// See also [Circles].
  CirclesProvider(
    Profile profile,
  ) : this._internal(
          () => Circles()..profile = profile,
          from: circlesProvider,
          name: r'circlesProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$circlesHash,
          dependencies: CirclesFamily._dependencies,
          allTransitiveDependencies: CirclesFamily._allTransitiveDependencies,
          profile: profile,
        );

  CirclesProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  List<TimelineGroupingListData> runNotifierBuild(
    covariant Circles notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(Circles Function() create) {
    return ProviderOverride(
      origin: this,
      override: CirclesProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<Circles, List<TimelineGroupingListData>>
      createElement() {
    return _CirclesProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CirclesProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin CirclesRef on NotifierProviderRef<List<TimelineGroupingListData>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _CirclesProviderElement
    extends NotifierProviderElement<Circles, List<TimelineGroupingListData>>
    with CirclesRef {
  _CirclesProviderElement(super.provider);

  @override
  Profile get profile => (origin as CirclesProvider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
