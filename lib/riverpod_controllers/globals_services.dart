import 'dart:io';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../data/objectbox/objectbox_cache.dart';
import '../services/secrets_service.dart';

part 'globals_services.g.dart';

@Riverpod(keepAlive: true)
SharedPreferences sharedPreferences(Ref ref) => throw UnimplementedError();

@Riverpod(keepAlive: true)
Directory applicationSupportDirectory(Ref ref) => throw UnimplementedError();

@Riverpod(keepAlive: true)
SecretsService secretsService(Ref ref) => throw UnimplementedError();

@Riverpod(keepAlive: true)
String userAgent(Ref ref) => throw UnimplementedError();

@Riverpod(keepAlive: true)
ObjectBoxCache objectBoxCache(Ref ref) => throw UnimplementedError();
