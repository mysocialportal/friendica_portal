// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'instance_info_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$instanceInfoManagerHash() =>
    r'88f181cbbee4b4043bf0be55e8379fcc8ad2b8b3';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$InstanceInfoManager extends BuildlessNotifier<InstanceInfo> {
  late final Profile profile;

  InstanceInfo build(
    Profile profile,
  );
}

/// See also [InstanceInfoManager].
@ProviderFor(InstanceInfoManager)
const instanceInfoManagerProvider = InstanceInfoManagerFamily();

/// See also [InstanceInfoManager].
class InstanceInfoManagerFamily extends Family<InstanceInfo> {
  /// See also [InstanceInfoManager].
  const InstanceInfoManagerFamily();

  /// See also [InstanceInfoManager].
  InstanceInfoManagerProvider call(
    Profile profile,
  ) {
    return InstanceInfoManagerProvider(
      profile,
    );
  }

  @override
  InstanceInfoManagerProvider getProviderOverride(
    covariant InstanceInfoManagerProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'instanceInfoManagerProvider';
}

/// See also [InstanceInfoManager].
class InstanceInfoManagerProvider
    extends NotifierProviderImpl<InstanceInfoManager, InstanceInfo> {
  /// See also [InstanceInfoManager].
  InstanceInfoManagerProvider(
    Profile profile,
  ) : this._internal(
          () => InstanceInfoManager()..profile = profile,
          from: instanceInfoManagerProvider,
          name: r'instanceInfoManagerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$instanceInfoManagerHash,
          dependencies: InstanceInfoManagerFamily._dependencies,
          allTransitiveDependencies:
              InstanceInfoManagerFamily._allTransitiveDependencies,
          profile: profile,
        );

  InstanceInfoManagerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  InstanceInfo runNotifierBuild(
    covariant InstanceInfoManager notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(InstanceInfoManager Function() create) {
    return ProviderOverride(
      origin: this,
      override: InstanceInfoManagerProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<InstanceInfoManager, InstanceInfo> createElement() {
    return _InstanceInfoManagerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is InstanceInfoManagerProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin InstanceInfoManagerRef on NotifierProviderRef<InstanceInfo> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _InstanceInfoManagerProviderElement
    extends NotifierProviderElement<InstanceInfoManager, InstanceInfo>
    with InstanceInfoManagerRef {
  _InstanceInfoManagerProviderElement(super.provider);

  @override
  Profile get profile => (origin as InstanceInfoManagerProvider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
