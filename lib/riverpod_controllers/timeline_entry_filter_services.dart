import 'dart:convert';
import 'dart:io';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:path/path.dart' as p;
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:stack_trace/stack_trace.dart';

import '../models/auth/profile.dart';
import '../models/exec_error.dart';
import '../models/filters/timeline_entry_filter.dart';
import '../models/timeline_entry.dart';
import '../riverpod_controllers/globals_services.dart';
import '../riverpod_controllers/rp_provider_extension.dart';
import '../utils/filter_runner.dart';

part 'timeline_entry_filter_services.g.dart';

final _logger = Logger('TimelineEntryServiceFilterProvider');

@Riverpod(keepAlive: true)
class TimelineEntryFilters extends _$TimelineEntryFilters {
  late String filePath;

  @override
  Map<String, TimelineEntryFilter> build(Profile profile) {
    _logger.fine('Building filters for $profile');
    final appDir = ref.watch(applicationSupportDirectoryProvider);
    filePath = p.join(appDir.path, '${profile.id}_filters.json');
    final filtersFromDisk = _load();
    return filtersFromDisk;
  }

  void upsertFilter(TimelineEntryFilter filter) {
    state[filter.id] = filter;
    _save();
    ref.invalidateSelf();
  }

  void removeById(String id) {
    state.remove(id);
    _save();
    ref.invalidateSelf();
  }

  Map<String, TimelineEntryFilter> _load() {
    var filters = <String, TimelineEntryFilter>{};
    final file = File(filePath);
    if (!file.existsSync()) {
      return {};
    }

    try {
      final str = file.readAsStringSync();
      final json = jsonDecode(str) as List<dynamic>;
      final readFilters =
          json.map((j) => TimelineEntryFilter.fromJson(j)).toList();
      filters = {for (var f in readFilters) f.id: f};
    } catch (e) {
      _logger.severe(
        'Error parsing filters file $filePath: $e',
        Trace.current(),
      );
    }

    return filters;
  }

  void _save() {
    try {
      final json = state.values.map((f) => f.toJson()).toList();
      final str = jsonEncode(json);
      File(filePath).writeAsStringSync(str);
    } catch (e) {
      _logger.severe(
        'Error writing filters file $filePath: $e',
        Trace.current(),
      );
    }
  }
}

@riverpod
Result<TimelineEntryFilter, ExecError> filterById(
  Ref ref,
  Profile profile,
  String id,
) {
  ref.cacheFor(const Duration(minutes: 5));
  final filters = ref.watch(timelineEntryFiltersProvider(profile));
  if (!filters.containsKey(id)) {
    return buildErrorResult(
      type: ErrorType.notFound,
      message: 'No filter with id: $id',
    );
  }

  return Result.ok(filters[id]!);
}

final _checkerLogger = Logger('CheckTimelineEntry');

@riverpod
FilterResult checkTimelineEntry(
  Ref ref,
  Profile profile,
  TimelineEntry entry,
) {
  ref.cacheFor(const Duration(minutes: 5));
  final filters = ref.watch(timelineEntryFiltersProvider(profile));
  final result = runFilters(entry, filters.values.toList());

  _checkerLogger.finest(
      'Checked entry ${entry.id} for profile ${profile.id} and returning ${result.toActionString()}');
  return result;
}
