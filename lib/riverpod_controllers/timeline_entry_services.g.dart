// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timeline_entry_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$timelineEntriesHash() => r'06889ecf3efd6b1e91aeaadbab85325308ac8ce6';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [_timelineEntries].
@ProviderFor(_timelineEntries)
const _timelineEntriesProvider = _TimelineEntriesFamily();

/// See also [_timelineEntries].
class _TimelineEntriesFamily extends Family<Map<String, TimelineEntry>> {
  /// See also [_timelineEntries].
  const _TimelineEntriesFamily();

  /// See also [_timelineEntries].
  _TimelineEntriesProvider call(
    Profile profile,
  ) {
    return _TimelineEntriesProvider(
      profile,
    );
  }

  @override
  _TimelineEntriesProvider getProviderOverride(
    covariant _TimelineEntriesProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'_timelineEntriesProvider';
}

/// See also [_timelineEntries].
class _TimelineEntriesProvider extends Provider<Map<String, TimelineEntry>> {
  /// See also [_timelineEntries].
  _TimelineEntriesProvider(
    Profile profile,
  ) : this._internal(
          (ref) => _timelineEntries(
            ref as _TimelineEntriesRef,
            profile,
          ),
          from: _timelineEntriesProvider,
          name: r'_timelineEntriesProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$timelineEntriesHash,
          dependencies: _TimelineEntriesFamily._dependencies,
          allTransitiveDependencies:
              _TimelineEntriesFamily._allTransitiveDependencies,
          profile: profile,
        );

  _TimelineEntriesProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    Map<String, TimelineEntry> Function(_TimelineEntriesRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: _TimelineEntriesProvider._internal(
        (ref) => create(ref as _TimelineEntriesRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  ProviderElement<Map<String, TimelineEntry>> createElement() {
    return _TimelineEntriesProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is _TimelineEntriesProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin _TimelineEntriesRef on ProviderRef<Map<String, TimelineEntry>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _TimelineEntriesProviderElement
    extends ProviderElement<Map<String, TimelineEntry>>
    with _TimelineEntriesRef {
  _TimelineEntriesProviderElement(super.provider);

  @override
  Profile get profile => (origin as _TimelineEntriesProvider).profile;
}

String _$timelineEntryManagerHash() =>
    r'ba295f7e64118702af64a0f2f49c04e2cf1daee1';

abstract class _$TimelineEntryManager
    extends BuildlessNotifier<Result<TimelineEntry, ExecError>> {
  late final Profile profile;
  late final String id;

  Result<TimelineEntry, ExecError> build(
    Profile profile,
    String id,
  );
}

/// See also [TimelineEntryManager].
@ProviderFor(TimelineEntryManager)
const timelineEntryManagerProvider = TimelineEntryManagerFamily();

/// See also [TimelineEntryManager].
class TimelineEntryManagerFamily
    extends Family<Result<TimelineEntry, ExecError>> {
  /// See also [TimelineEntryManager].
  const TimelineEntryManagerFamily();

  /// See also [TimelineEntryManager].
  TimelineEntryManagerProvider call(
    Profile profile,
    String id,
  ) {
    return TimelineEntryManagerProvider(
      profile,
      id,
    );
  }

  @override
  TimelineEntryManagerProvider getProviderOverride(
    covariant TimelineEntryManagerProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'timelineEntryManagerProvider';
}

/// See also [TimelineEntryManager].
class TimelineEntryManagerProvider extends NotifierProviderImpl<
    TimelineEntryManager, Result<TimelineEntry, ExecError>> {
  /// See also [TimelineEntryManager].
  TimelineEntryManagerProvider(
    Profile profile,
    String id,
  ) : this._internal(
          () => TimelineEntryManager()
            ..profile = profile
            ..id = id,
          from: timelineEntryManagerProvider,
          name: r'timelineEntryManagerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$timelineEntryManagerHash,
          dependencies: TimelineEntryManagerFamily._dependencies,
          allTransitiveDependencies:
              TimelineEntryManagerFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  TimelineEntryManagerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  Result<TimelineEntry, ExecError> runNotifierBuild(
    covariant TimelineEntryManager notifier,
  ) {
    return notifier.build(
      profile,
      id,
    );
  }

  @override
  Override overrideWith(TimelineEntryManager Function() create) {
    return ProviderOverride(
      origin: this,
      override: TimelineEntryManagerProvider._internal(
        () => create()
          ..profile = profile
          ..id = id,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  NotifierProviderElement<TimelineEntryManager,
      Result<TimelineEntry, ExecError>> createElement() {
    return _TimelineEntryManagerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TimelineEntryManagerProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin TimelineEntryManagerRef
    on NotifierProviderRef<Result<TimelineEntry, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _TimelineEntryManagerProviderElement extends NotifierProviderElement<
    TimelineEntryManager,
    Result<TimelineEntry, ExecError>> with TimelineEntryManagerRef {
  _TimelineEntryManagerProviderElement(super.provider);

  @override
  Profile get profile => (origin as TimelineEntryManagerProvider).profile;
  @override
  String get id => (origin as TimelineEntryManagerProvider).id;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
