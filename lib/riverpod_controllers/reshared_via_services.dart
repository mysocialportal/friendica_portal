import 'package:flutter/foundation.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../models/auth/profile.dart';

part 'reshared_via_services.g.dart';

@Riverpod(keepAlive: true)
class ResharedVia extends _$ResharedVia {
  @override
  ResharedViaData build(Profile profile, String postId) {
    return ResharedViaData(postId: postId);
  }

  void upsertResharedVia(String resharerId) {
    state = state.withUpsertedResharer(resharerId);
  }

  void upsertRemovedSharer(String resharerId) {
    state = state.withRemovedResharer(resharerId);
  }
}

class ResharedViaData {
  final String postId;
  final Set<String> resharers;

  bool get hasResharedVia => resharers.isNotEmpty;

  const ResharedViaData({
    required this.postId,
    this.resharers = const {},
  });

  ResharedViaData withUpsertedResharer(String resharerId) => ResharedViaData(
        postId: postId,
        resharers: {resharerId, ...resharers},
      );

  ResharedViaData withRemovedResharer(String resharerId) => ResharedViaData(
        postId: postId,
        resharers: resharers..remove(resharerId),
      );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ResharedViaData &&
          runtimeType == other.runtimeType &&
          postId == other.postId &&
          setEquals(resharers, other.resharers);

  @override
  int get hashCode =>
      postId.hashCode ^ resharers.hashCode ^ Object.hashAll(resharers);
}
