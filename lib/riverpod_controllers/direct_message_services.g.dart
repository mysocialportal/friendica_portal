// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'direct_message_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$directMessageThreadIdsHash() =>
    r'ce42c0a00ab785c7685f8c135bada67bea2fcb14';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$DirectMessageThreadIds
    extends BuildlessNotifier<List<String>> {
  late final Profile profile;

  List<String> build(
    Profile profile,
  );
}

/// See also [DirectMessageThreadIds].
@ProviderFor(DirectMessageThreadIds)
const directMessageThreadIdsProvider = DirectMessageThreadIdsFamily();

/// See also [DirectMessageThreadIds].
class DirectMessageThreadIdsFamily extends Family<List<String>> {
  /// See also [DirectMessageThreadIds].
  const DirectMessageThreadIdsFamily();

  /// See also [DirectMessageThreadIds].
  DirectMessageThreadIdsProvider call(
    Profile profile,
  ) {
    return DirectMessageThreadIdsProvider(
      profile,
    );
  }

  @override
  DirectMessageThreadIdsProvider getProviderOverride(
    covariant DirectMessageThreadIdsProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'directMessageThreadIdsProvider';
}

/// See also [DirectMessageThreadIds].
class DirectMessageThreadIdsProvider
    extends NotifierProviderImpl<DirectMessageThreadIds, List<String>> {
  /// See also [DirectMessageThreadIds].
  DirectMessageThreadIdsProvider(
    Profile profile,
  ) : this._internal(
          () => DirectMessageThreadIds()..profile = profile,
          from: directMessageThreadIdsProvider,
          name: r'directMessageThreadIdsProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$directMessageThreadIdsHash,
          dependencies: DirectMessageThreadIdsFamily._dependencies,
          allTransitiveDependencies:
              DirectMessageThreadIdsFamily._allTransitiveDependencies,
          profile: profile,
        );

  DirectMessageThreadIdsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  List<String> runNotifierBuild(
    covariant DirectMessageThreadIds notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(DirectMessageThreadIds Function() create) {
    return ProviderOverride(
      origin: this,
      override: DirectMessageThreadIdsProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<DirectMessageThreadIds, List<String>>
      createElement() {
    return _DirectMessageThreadIdsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is DirectMessageThreadIdsProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin DirectMessageThreadIdsRef on NotifierProviderRef<List<String>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _DirectMessageThreadIdsProviderElement
    extends NotifierProviderElement<DirectMessageThreadIds, List<String>>
    with DirectMessageThreadIdsRef {
  _DirectMessageThreadIdsProviderElement(super.provider);

  @override
  Profile get profile => (origin as DirectMessageThreadIdsProvider).profile;
}

String _$directMessageThreadServiceHash() =>
    r'5ef67edc0bb13c3834ed998bd107af36762fde79';

abstract class _$DirectMessageThreadService
    extends BuildlessNotifier<DirectMessageThread> {
  late final Profile profile;
  late final String id;

  DirectMessageThread build(
    Profile profile,
    String id,
  );
}

/// See also [DirectMessageThreadService].
@ProviderFor(DirectMessageThreadService)
const directMessageThreadServiceProvider = DirectMessageThreadServiceFamily();

/// See also [DirectMessageThreadService].
class DirectMessageThreadServiceFamily extends Family<DirectMessageThread> {
  /// See also [DirectMessageThreadService].
  const DirectMessageThreadServiceFamily();

  /// See also [DirectMessageThreadService].
  DirectMessageThreadServiceProvider call(
    Profile profile,
    String id,
  ) {
    return DirectMessageThreadServiceProvider(
      profile,
      id,
    );
  }

  @override
  DirectMessageThreadServiceProvider getProviderOverride(
    covariant DirectMessageThreadServiceProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'directMessageThreadServiceProvider';
}

/// See also [DirectMessageThreadService].
class DirectMessageThreadServiceProvider extends NotifierProviderImpl<
    DirectMessageThreadService, DirectMessageThread> {
  /// See also [DirectMessageThreadService].
  DirectMessageThreadServiceProvider(
    Profile profile,
    String id,
  ) : this._internal(
          () => DirectMessageThreadService()
            ..profile = profile
            ..id = id,
          from: directMessageThreadServiceProvider,
          name: r'directMessageThreadServiceProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$directMessageThreadServiceHash,
          dependencies: DirectMessageThreadServiceFamily._dependencies,
          allTransitiveDependencies:
              DirectMessageThreadServiceFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  DirectMessageThreadServiceProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  DirectMessageThread runNotifierBuild(
    covariant DirectMessageThreadService notifier,
  ) {
    return notifier.build(
      profile,
      id,
    );
  }

  @override
  Override overrideWith(DirectMessageThreadService Function() create) {
    return ProviderOverride(
      origin: this,
      override: DirectMessageThreadServiceProvider._internal(
        () => create()
          ..profile = profile
          ..id = id,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  NotifierProviderElement<DirectMessageThreadService, DirectMessageThread>
      createElement() {
    return _DirectMessageThreadServiceProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is DirectMessageThreadServiceProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin DirectMessageThreadServiceRef
    on NotifierProviderRef<DirectMessageThread> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _DirectMessageThreadServiceProviderElement
    extends NotifierProviderElement<DirectMessageThreadService,
        DirectMessageThread> with DirectMessageThreadServiceRef {
  _DirectMessageThreadServiceProviderElement(super.provider);

  @override
  Profile get profile => (origin as DirectMessageThreadServiceProvider).profile;
  @override
  String get id => (origin as DirectMessageThreadServiceProvider).id;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
