// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'persistent_info_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$persistentInfoHash() => r'62bf2f76f88d746cf75f4b663d76302d4c3213fd';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$PersistentInfo extends BuildlessNotifier<DateTime> {
  late final Profile profile;

  DateTime build(
    Profile profile,
  );
}

/// See also [PersistentInfo].
@ProviderFor(PersistentInfo)
const persistentInfoProvider = PersistentInfoFamily();

/// See also [PersistentInfo].
class PersistentInfoFamily extends Family<DateTime> {
  /// See also [PersistentInfo].
  const PersistentInfoFamily();

  /// See also [PersistentInfo].
  PersistentInfoProvider call(
    Profile profile,
  ) {
    return PersistentInfoProvider(
      profile,
    );
  }

  @override
  PersistentInfoProvider getProviderOverride(
    covariant PersistentInfoProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'persistentInfoProvider';
}

/// See also [PersistentInfo].
class PersistentInfoProvider
    extends NotifierProviderImpl<PersistentInfo, DateTime> {
  /// See also [PersistentInfo].
  PersistentInfoProvider(
    Profile profile,
  ) : this._internal(
          () => PersistentInfo()..profile = profile,
          from: persistentInfoProvider,
          name: r'persistentInfoProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$persistentInfoHash,
          dependencies: PersistentInfoFamily._dependencies,
          allTransitiveDependencies:
              PersistentInfoFamily._allTransitiveDependencies,
          profile: profile,
        );

  PersistentInfoProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  DateTime runNotifierBuild(
    covariant PersistentInfo notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(PersistentInfo Function() create) {
    return ProviderOverride(
      origin: this,
      override: PersistentInfoProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<PersistentInfo, DateTime> createElement() {
    return _PersistentInfoProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is PersistentInfoProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin PersistentInfoRef on NotifierProviderRef<DateTime> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _PersistentInfoProviderElement
    extends NotifierProviderElement<PersistentInfo, DateTime>
    with PersistentInfoRef {
  _PersistentInfoProviderElement(super.provider);

  @override
  Profile get profile => (origin as PersistentInfoProvider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
