// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'follow_requests_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$followRequestListHash() => r'd2cdaab524c3d433ae0be75fa181afb13bcde74e';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [followRequestList].
@ProviderFor(followRequestList)
const followRequestListProvider = FollowRequestListFamily();

/// See also [followRequestList].
class FollowRequestListFamily extends Family<List<FollowRequest>> {
  /// See also [followRequestList].
  const FollowRequestListFamily();

  /// See also [followRequestList].
  FollowRequestListProvider call(
    Profile profile,
  ) {
    return FollowRequestListProvider(
      profile,
    );
  }

  @override
  FollowRequestListProvider getProviderOverride(
    covariant FollowRequestListProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'followRequestListProvider';
}

/// See also [followRequestList].
class FollowRequestListProvider
    extends AutoDisposeProvider<List<FollowRequest>> {
  /// See also [followRequestList].
  FollowRequestListProvider(
    Profile profile,
  ) : this._internal(
          (ref) => followRequestList(
            ref as FollowRequestListRef,
            profile,
          ),
          from: followRequestListProvider,
          name: r'followRequestListProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$followRequestListHash,
          dependencies: FollowRequestListFamily._dependencies,
          allTransitiveDependencies:
              FollowRequestListFamily._allTransitiveDependencies,
          profile: profile,
        );

  FollowRequestListProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    List<FollowRequest> Function(FollowRequestListRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FollowRequestListProvider._internal(
        (ref) => create(ref as FollowRequestListRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<List<FollowRequest>> createElement() {
    return _FollowRequestListProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FollowRequestListProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FollowRequestListRef on AutoDisposeProviderRef<List<FollowRequest>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _FollowRequestListProviderElement
    extends AutoDisposeProviderElement<List<FollowRequest>>
    with FollowRequestListRef {
  _FollowRequestListProviderElement(super.provider);

  @override
  Profile get profile => (origin as FollowRequestListProvider).profile;
}

String _$followRequestByIdHash() => r'175fe14003a334d35110c635f3cf2af7507c2855';

/// See also [followRequestById].
@ProviderFor(followRequestById)
const followRequestByIdProvider = FollowRequestByIdFamily();

/// See also [followRequestById].
class FollowRequestByIdFamily extends Family<Result<FollowRequest, ExecError>> {
  /// See also [followRequestById].
  const FollowRequestByIdFamily();

  /// See also [followRequestById].
  FollowRequestByIdProvider call(
    Profile profile,
    String id,
  ) {
    return FollowRequestByIdProvider(
      profile,
      id,
    );
  }

  @override
  FollowRequestByIdProvider getProviderOverride(
    covariant FollowRequestByIdProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'followRequestByIdProvider';
}

/// See also [followRequestById].
class FollowRequestByIdProvider
    extends AutoDisposeProvider<Result<FollowRequest, ExecError>> {
  /// See also [followRequestById].
  FollowRequestByIdProvider(
    Profile profile,
    String id,
  ) : this._internal(
          (ref) => followRequestById(
            ref as FollowRequestByIdRef,
            profile,
            id,
          ),
          from: followRequestByIdProvider,
          name: r'followRequestByIdProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$followRequestByIdHash,
          dependencies: FollowRequestByIdFamily._dependencies,
          allTransitiveDependencies:
              FollowRequestByIdFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  FollowRequestByIdProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  Override overrideWith(
    Result<FollowRequest, ExecError> Function(FollowRequestByIdRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FollowRequestByIdProvider._internal(
        (ref) => create(ref as FollowRequestByIdRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<Result<FollowRequest, ExecError>> createElement() {
    return _FollowRequestByIdProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FollowRequestByIdProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FollowRequestByIdRef
    on AutoDisposeProviderRef<Result<FollowRequest, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _FollowRequestByIdProviderElement
    extends AutoDisposeProviderElement<Result<FollowRequest, ExecError>>
    with FollowRequestByIdRef {
  _FollowRequestByIdProviderElement(super.provider);

  @override
  Profile get profile => (origin as FollowRequestByIdProvider).profile;
  @override
  String get id => (origin as FollowRequestByIdProvider).id;
}

String _$followRequestsHash() => r'a682d8c48283cfc3b57c2ce6a7e7e3a3568e050a';

abstract class _$FollowRequests
    extends BuildlessNotifier<Map<String, FollowRequest>> {
  late final Profile profile;

  Map<String, FollowRequest> build(
    Profile profile,
  );
}

/// See also [FollowRequests].
@ProviderFor(FollowRequests)
const followRequestsProvider = FollowRequestsFamily();

/// See also [FollowRequests].
class FollowRequestsFamily extends Family<Map<String, FollowRequest>> {
  /// See also [FollowRequests].
  const FollowRequestsFamily();

  /// See also [FollowRequests].
  FollowRequestsProvider call(
    Profile profile,
  ) {
    return FollowRequestsProvider(
      profile,
    );
  }

  @override
  FollowRequestsProvider getProviderOverride(
    covariant FollowRequestsProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'followRequestsProvider';
}

/// See also [FollowRequests].
class FollowRequestsProvider
    extends NotifierProviderImpl<FollowRequests, Map<String, FollowRequest>> {
  /// See also [FollowRequests].
  FollowRequestsProvider(
    Profile profile,
  ) : this._internal(
          () => FollowRequests()..profile = profile,
          from: followRequestsProvider,
          name: r'followRequestsProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$followRequestsHash,
          dependencies: FollowRequestsFamily._dependencies,
          allTransitiveDependencies:
              FollowRequestsFamily._allTransitiveDependencies,
          profile: profile,
        );

  FollowRequestsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Map<String, FollowRequest> runNotifierBuild(
    covariant FollowRequests notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(FollowRequests Function() create) {
    return ProviderOverride(
      origin: this,
      override: FollowRequestsProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<FollowRequests, Map<String, FollowRequest>>
      createElement() {
    return _FollowRequestsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FollowRequestsProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FollowRequestsRef on NotifierProviderRef<Map<String, FollowRequest>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _FollowRequestsProviderElement
    extends NotifierProviderElement<FollowRequests, Map<String, FollowRequest>>
    with FollowRequestsRef {
  _FollowRequestsProviderElement(super.provider);

  @override
  Profile get profile => (origin as FollowRequestsProvider).profile;
}

String _$followRequestRefresherHash() =>
    r'7f091a391b8bc1c2bb10704b24fbea98277082dd';

abstract class _$FollowRequestRefresher extends BuildlessNotifier<bool> {
  late final Profile profile;

  bool build(
    Profile profile,
  );
}

/// See also [FollowRequestRefresher].
@ProviderFor(FollowRequestRefresher)
const followRequestRefresherProvider = FollowRequestRefresherFamily();

/// See also [FollowRequestRefresher].
class FollowRequestRefresherFamily extends Family<bool> {
  /// See also [FollowRequestRefresher].
  const FollowRequestRefresherFamily();

  /// See also [FollowRequestRefresher].
  FollowRequestRefresherProvider call(
    Profile profile,
  ) {
    return FollowRequestRefresherProvider(
      profile,
    );
  }

  @override
  FollowRequestRefresherProvider getProviderOverride(
    covariant FollowRequestRefresherProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'followRequestRefresherProvider';
}

/// See also [FollowRequestRefresher].
class FollowRequestRefresherProvider
    extends NotifierProviderImpl<FollowRequestRefresher, bool> {
  /// See also [FollowRequestRefresher].
  FollowRequestRefresherProvider(
    Profile profile,
  ) : this._internal(
          () => FollowRequestRefresher()..profile = profile,
          from: followRequestRefresherProvider,
          name: r'followRequestRefresherProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$followRequestRefresherHash,
          dependencies: FollowRequestRefresherFamily._dependencies,
          allTransitiveDependencies:
              FollowRequestRefresherFamily._allTransitiveDependencies,
          profile: profile,
        );

  FollowRequestRefresherProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  bool runNotifierBuild(
    covariant FollowRequestRefresher notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(FollowRequestRefresher Function() create) {
    return ProviderOverride(
      origin: this,
      override: FollowRequestRefresherProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<FollowRequestRefresher, bool> createElement() {
    return _FollowRequestRefresherProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FollowRequestRefresherProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FollowRequestRefresherRef on NotifierProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _FollowRequestRefresherProviderElement
    extends NotifierProviderElement<FollowRequestRefresher, bool>
    with FollowRequestRefresherRef {
  _FollowRequestRefresherProviderElement(super.provider);

  @override
  Profile get profile => (origin as FollowRequestRefresherProvider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
