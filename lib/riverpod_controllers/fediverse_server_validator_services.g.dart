// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fediverse_server_validator_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$checkIfFromFediverseHash() =>
    r'b4611c26c92e1e552b1ad88e40a9bfa9131d2299';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [checkIfFromFediverse].
@ProviderFor(checkIfFromFediverse)
const checkIfFromFediverseProvider = CheckIfFromFediverseFamily();

/// See also [checkIfFromFediverse].
class CheckIfFromFediverseFamily
    extends Family<AsyncValue<Result<bool, ExecError>>> {
  /// See also [checkIfFromFediverse].
  const CheckIfFromFediverseFamily();

  /// See also [checkIfFromFediverse].
  CheckIfFromFediverseProvider call(
    String url,
  ) {
    return CheckIfFromFediverseProvider(
      url,
    );
  }

  @override
  CheckIfFromFediverseProvider getProviderOverride(
    covariant CheckIfFromFediverseProvider provider,
  ) {
    return call(
      provider.url,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'checkIfFromFediverseProvider';
}

/// See also [checkIfFromFediverse].
class CheckIfFromFediverseProvider
    extends AutoDisposeFutureProvider<Result<bool, ExecError>> {
  /// See also [checkIfFromFediverse].
  CheckIfFromFediverseProvider(
    String url,
  ) : this._internal(
          (ref) => checkIfFromFediverse(
            ref as CheckIfFromFediverseRef,
            url,
          ),
          from: checkIfFromFediverseProvider,
          name: r'checkIfFromFediverseProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$checkIfFromFediverseHash,
          dependencies: CheckIfFromFediverseFamily._dependencies,
          allTransitiveDependencies:
              CheckIfFromFediverseFamily._allTransitiveDependencies,
          url: url,
        );

  CheckIfFromFediverseProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.url,
  }) : super.internal();

  final String url;

  @override
  Override overrideWith(
    FutureOr<Result<bool, ExecError>> Function(CheckIfFromFediverseRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: CheckIfFromFediverseProvider._internal(
        (ref) => create(ref as CheckIfFromFediverseRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        url: url,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<bool, ExecError>> createElement() {
    return _CheckIfFromFediverseProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CheckIfFromFediverseProvider && other.url == url;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, url.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin CheckIfFromFediverseRef
    on AutoDisposeFutureProviderRef<Result<bool, ExecError>> {
  /// The parameter `url` of this provider.
  String get url;
}

class _CheckIfFromFediverseProviderElement
    extends AutoDisposeFutureProviderElement<Result<bool, ExecError>>
    with CheckIfFromFediverseRef {
  _CheckIfFromFediverseProviderElement(super.provider);

  @override
  String get url => (origin as CheckIfFromFediverseProvider).url;
}

String _$serverDataHash() => r'efc4bc985c7dc4639a6b2c0a8d5ac2546da0f2dd';

/// See also [serverData].
@ProviderFor(serverData)
const serverDataProvider = ServerDataFamily();

/// See also [serverData].
class ServerDataFamily
    extends Family<AsyncValue<Result<ServerData, ExecError>>> {
  /// See also [serverData].
  const ServerDataFamily();

  /// See also [serverData].
  ServerDataProvider call(
    String url,
  ) {
    return ServerDataProvider(
      url,
    );
  }

  @override
  ServerDataProvider getProviderOverride(
    covariant ServerDataProvider provider,
  ) {
    return call(
      provider.url,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'serverDataProvider';
}

/// See also [serverData].
class ServerDataProvider
    extends AutoDisposeFutureProvider<Result<ServerData, ExecError>> {
  /// See also [serverData].
  ServerDataProvider(
    String url,
  ) : this._internal(
          (ref) => serverData(
            ref as ServerDataRef,
            url,
          ),
          from: serverDataProvider,
          name: r'serverDataProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$serverDataHash,
          dependencies: ServerDataFamily._dependencies,
          allTransitiveDependencies:
              ServerDataFamily._allTransitiveDependencies,
          url: url,
        );

  ServerDataProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.url,
  }) : super.internal();

  final String url;

  @override
  Override overrideWith(
    FutureOr<Result<ServerData, ExecError>> Function(ServerDataRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ServerDataProvider._internal(
        (ref) => create(ref as ServerDataRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        url: url,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Result<ServerData, ExecError>>
      createElement() {
    return _ServerDataProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ServerDataProvider && other.url == url;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, url.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ServerDataRef
    on AutoDisposeFutureProviderRef<Result<ServerData, ExecError>> {
  /// The parameter `url` of this provider.
  String get url;
}

class _ServerDataProviderElement
    extends AutoDisposeFutureProviderElement<Result<ServerData, ExecError>>
    with ServerDataRef {
  _ServerDataProviderElement(super.provider);

  @override
  String get url => (origin as ServerDataProvider).url;
}

String _$serverDataFromServerHash() =>
    r'2284961f6fa7cf6146ee29a85675c33599add16e';

/// See also [serverDataFromServer].
@ProviderFor(serverDataFromServer)
const serverDataFromServerProvider = ServerDataFromServerFamily();

/// See also [serverDataFromServer].
class ServerDataFromServerFamily extends Family<AsyncValue<ServerData>> {
  /// See also [serverDataFromServer].
  const ServerDataFromServerFamily();

  /// See also [serverDataFromServer].
  ServerDataFromServerProvider call(
    String domainName,
  ) {
    return ServerDataFromServerProvider(
      domainName,
    );
  }

  @override
  ServerDataFromServerProvider getProviderOverride(
    covariant ServerDataFromServerProvider provider,
  ) {
    return call(
      provider.domainName,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'serverDataFromServerProvider';
}

/// See also [serverDataFromServer].
class ServerDataFromServerProvider
    extends AutoDisposeFutureProvider<ServerData> {
  /// See also [serverDataFromServer].
  ServerDataFromServerProvider(
    String domainName,
  ) : this._internal(
          (ref) => serverDataFromServer(
            ref as ServerDataFromServerRef,
            domainName,
          ),
          from: serverDataFromServerProvider,
          name: r'serverDataFromServerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$serverDataFromServerHash,
          dependencies: ServerDataFromServerFamily._dependencies,
          allTransitiveDependencies:
              ServerDataFromServerFamily._allTransitiveDependencies,
          domainName: domainName,
        );

  ServerDataFromServerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.domainName,
  }) : super.internal();

  final String domainName;

  @override
  Override overrideWith(
    FutureOr<ServerData> Function(ServerDataFromServerRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ServerDataFromServerProvider._internal(
        (ref) => create(ref as ServerDataFromServerRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        domainName: domainName,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<ServerData> createElement() {
    return _ServerDataFromServerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ServerDataFromServerProvider &&
        other.domainName == domainName;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, domainName.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ServerDataFromServerRef on AutoDisposeFutureProviderRef<ServerData> {
  /// The parameter `domainName` of this provider.
  String get domainName;
}

class _ServerDataFromServerProviderElement
    extends AutoDisposeFutureProviderElement<ServerData>
    with ServerDataFromServerRef {
  _ServerDataFromServerProviderElement(super.provider);

  @override
  String get domainName => (origin as ServerDataFromServerProvider).domainName;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
