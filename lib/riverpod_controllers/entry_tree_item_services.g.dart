// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'entry_tree_item_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$parentPostIdsHash() => r'ce39277c7ca99027e63e80330b8a4fdf408c40e3';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [_parentPostIds].
@ProviderFor(_parentPostIds)
const _parentPostIdsProvider = _ParentPostIdsFamily();

/// See also [_parentPostIds].
class _ParentPostIdsFamily extends Family<Map<String, String>> {
  /// See also [_parentPostIds].
  const _ParentPostIdsFamily();

  /// See also [_parentPostIds].
  _ParentPostIdsProvider call(
    Profile profile,
  ) {
    return _ParentPostIdsProvider(
      profile,
    );
  }

  @override
  _ParentPostIdsProvider getProviderOverride(
    covariant _ParentPostIdsProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'_parentPostIdsProvider';
}

/// See also [_parentPostIds].
class _ParentPostIdsProvider extends Provider<Map<String, String>> {
  /// See also [_parentPostIds].
  _ParentPostIdsProvider(
    Profile profile,
  ) : this._internal(
          (ref) => _parentPostIds(
            ref as _ParentPostIdsRef,
            profile,
          ),
          from: _parentPostIdsProvider,
          name: r'_parentPostIdsProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$parentPostIdsHash,
          dependencies: _ParentPostIdsFamily._dependencies,
          allTransitiveDependencies:
              _ParentPostIdsFamily._allTransitiveDependencies,
          profile: profile,
        );

  _ParentPostIdsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    Map<String, String> Function(_ParentPostIdsRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: _ParentPostIdsProvider._internal(
        (ref) => create(ref as _ParentPostIdsRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  ProviderElement<Map<String, String>> createElement() {
    return _ParentPostIdsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is _ParentPostIdsProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin _ParentPostIdsRef on ProviderRef<Map<String, String>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _ParentPostIdsProviderElement extends ProviderElement<Map<String, String>>
    with _ParentPostIdsRef {
  _ParentPostIdsProviderElement(super.provider);

  @override
  Profile get profile => (origin as _ParentPostIdsProvider).profile;
}

String _$postNodesHash() => r'f6d6c295a67fddb595d1f22b439a019f778980de';

/// See also [_postNodes].
@ProviderFor(_postNodes)
const _postNodesProvider = _PostNodesFamily();

/// See also [_postNodes].
class _PostNodesFamily extends Family<Map<String, _Node>> {
  /// See also [_postNodes].
  const _PostNodesFamily();

  /// See also [_postNodes].
  _PostNodesProvider call(
    Profile profile,
  ) {
    return _PostNodesProvider(
      profile,
    );
  }

  @override
  _PostNodesProvider getProviderOverride(
    covariant _PostNodesProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'_postNodesProvider';
}

/// See also [_postNodes].
class _PostNodesProvider extends Provider<Map<String, _Node>> {
  /// See also [_postNodes].
  _PostNodesProvider(
    Profile profile,
  ) : this._internal(
          (ref) => _postNodes(
            ref as _PostNodesRef,
            profile,
          ),
          from: _postNodesProvider,
          name: r'_postNodesProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$postNodesHash,
          dependencies: _PostNodesFamily._dependencies,
          allTransitiveDependencies:
              _PostNodesFamily._allTransitiveDependencies,
          profile: profile,
        );

  _PostNodesProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    Map<String, _Node> Function(_PostNodesRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: _PostNodesProvider._internal(
        (ref) => create(ref as _PostNodesRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  ProviderElement<Map<String, _Node>> createElement() {
    return _PostNodesProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is _PostNodesProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin _PostNodesRef on ProviderRef<Map<String, _Node>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _PostNodesProviderElement extends ProviderElement<Map<String, _Node>>
    with _PostNodesRef {
  _PostNodesProviderElement(super.provider);

  @override
  Profile get profile => (origin as _PostNodesProvider).profile;
}

String _$entryTreeItemsHash() => r'068c144eab0fee77da6912b22bcf9fd72db45239';

/// See also [_entryTreeItems].
@ProviderFor(_entryTreeItems)
const _entryTreeItemsProvider = _EntryTreeItemsFamily();

/// See also [_entryTreeItems].
class _EntryTreeItemsFamily extends Family<Map<String, EntryTreeItem>> {
  /// See also [_entryTreeItems].
  const _EntryTreeItemsFamily();

  /// See also [_entryTreeItems].
  _EntryTreeItemsProvider call(
    Profile profile,
  ) {
    return _EntryTreeItemsProvider(
      profile,
    );
  }

  @override
  _EntryTreeItemsProvider getProviderOverride(
    covariant _EntryTreeItemsProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'_entryTreeItemsProvider';
}

/// See also [_entryTreeItems].
class _EntryTreeItemsProvider extends Provider<Map<String, EntryTreeItem>> {
  /// See also [_entryTreeItems].
  _EntryTreeItemsProvider(
    Profile profile,
  ) : this._internal(
          (ref) => _entryTreeItems(
            ref as _EntryTreeItemsRef,
            profile,
          ),
          from: _entryTreeItemsProvider,
          name: r'_entryTreeItemsProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$entryTreeItemsHash,
          dependencies: _EntryTreeItemsFamily._dependencies,
          allTransitiveDependencies:
              _EntryTreeItemsFamily._allTransitiveDependencies,
          profile: profile,
        );

  _EntryTreeItemsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Override overrideWith(
    Map<String, EntryTreeItem> Function(_EntryTreeItemsRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: _EntryTreeItemsProvider._internal(
        (ref) => create(ref as _EntryTreeItemsRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  ProviderElement<Map<String, EntryTreeItem>> createElement() {
    return _EntryTreeItemsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is _EntryTreeItemsProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin _EntryTreeItemsRef on ProviderRef<Map<String, EntryTreeItem>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _EntryTreeItemsProviderElement
    extends ProviderElement<Map<String, EntryTreeItem>>
    with _EntryTreeItemsRef {
  _EntryTreeItemsProviderElement(super.provider);

  @override
  Profile get profile => (origin as _EntryTreeItemsProvider).profile;
}

String _$postTreeEntryByIdHash() => r'baa7bd0682250ab11aef2146419d0acca859b473';

/// See also [postTreeEntryById].
@ProviderFor(postTreeEntryById)
const postTreeEntryByIdProvider = PostTreeEntryByIdFamily();

/// See also [postTreeEntryById].
class PostTreeEntryByIdFamily extends Family<Result<EntryTreeItem, ExecError>> {
  /// See also [postTreeEntryById].
  const PostTreeEntryByIdFamily();

  /// See also [postTreeEntryById].
  PostTreeEntryByIdProvider call(
    Profile profile,
    String id,
  ) {
    return PostTreeEntryByIdProvider(
      profile,
      id,
    );
  }

  @override
  PostTreeEntryByIdProvider getProviderOverride(
    covariant PostTreeEntryByIdProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'postTreeEntryByIdProvider';
}

/// See also [postTreeEntryById].
class PostTreeEntryByIdProvider
    extends AutoDisposeProvider<Result<EntryTreeItem, ExecError>> {
  /// See also [postTreeEntryById].
  PostTreeEntryByIdProvider(
    Profile profile,
    String id,
  ) : this._internal(
          (ref) => postTreeEntryById(
            ref as PostTreeEntryByIdRef,
            profile,
            id,
          ),
          from: postTreeEntryByIdProvider,
          name: r'postTreeEntryByIdProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$postTreeEntryByIdHash,
          dependencies: PostTreeEntryByIdFamily._dependencies,
          allTransitiveDependencies:
              PostTreeEntryByIdFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  PostTreeEntryByIdProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  Override overrideWith(
    Result<EntryTreeItem, ExecError> Function(PostTreeEntryByIdRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: PostTreeEntryByIdProvider._internal(
        (ref) => create(ref as PostTreeEntryByIdRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<Result<EntryTreeItem, ExecError>> createElement() {
    return _PostTreeEntryByIdProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is PostTreeEntryByIdProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin PostTreeEntryByIdRef
    on AutoDisposeProviderRef<Result<EntryTreeItem, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _PostTreeEntryByIdProviderElement
    extends AutoDisposeProviderElement<Result<EntryTreeItem, ExecError>>
    with PostTreeEntryByIdRef {
  _PostTreeEntryByIdProviderElement(super.provider);

  @override
  Profile get profile => (origin as PostTreeEntryByIdProvider).profile;
  @override
  String get id => (origin as PostTreeEntryByIdProvider).id;
}

String _$entryTreeManagerHash() => r'4550b72602c1fdd6977608930c259fec2ef622ed';

abstract class _$EntryTreeManager
    extends BuildlessNotifier<Result<EntryTreeItem, ExecError>> {
  late final Profile profile;
  late final String id;

  Result<EntryTreeItem, ExecError> build(
    Profile profile,
    String id,
  );
}

/// See also [EntryTreeManager].
@ProviderFor(EntryTreeManager)
const entryTreeManagerProvider = EntryTreeManagerFamily();

/// See also [EntryTreeManager].
class EntryTreeManagerFamily extends Family<Result<EntryTreeItem, ExecError>> {
  /// See also [EntryTreeManager].
  const EntryTreeManagerFamily();

  /// See also [EntryTreeManager].
  EntryTreeManagerProvider call(
    Profile profile,
    String id,
  ) {
    return EntryTreeManagerProvider(
      profile,
      id,
    );
  }

  @override
  EntryTreeManagerProvider getProviderOverride(
    covariant EntryTreeManagerProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'entryTreeManagerProvider';
}

/// See also [EntryTreeManager].
class EntryTreeManagerProvider extends NotifierProviderImpl<EntryTreeManager,
    Result<EntryTreeItem, ExecError>> {
  /// See also [EntryTreeManager].
  EntryTreeManagerProvider(
    Profile profile,
    String id,
  ) : this._internal(
          () => EntryTreeManager()
            ..profile = profile
            ..id = id,
          from: entryTreeManagerProvider,
          name: r'entryTreeManagerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$entryTreeManagerHash,
          dependencies: EntryTreeManagerFamily._dependencies,
          allTransitiveDependencies:
              EntryTreeManagerFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  EntryTreeManagerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  Result<EntryTreeItem, ExecError> runNotifierBuild(
    covariant EntryTreeManager notifier,
  ) {
    return notifier.build(
      profile,
      id,
    );
  }

  @override
  Override overrideWith(EntryTreeManager Function() create) {
    return ProviderOverride(
      origin: this,
      override: EntryTreeManagerProvider._internal(
        () => create()
          ..profile = profile
          ..id = id,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  NotifierProviderElement<EntryTreeManager, Result<EntryTreeItem, ExecError>>
      createElement() {
    return _EntryTreeManagerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is EntryTreeManagerProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin EntryTreeManagerRef
    on NotifierProviderRef<Result<EntryTreeItem, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _EntryTreeManagerProviderElement extends NotifierProviderElement<
    EntryTreeManager,
    Result<EntryTreeItem, ExecError>> with EntryTreeManagerRef {
  _EntryTreeManagerProviderElement(super.provider);

  @override
  Profile get profile => (origin as EntryTreeManagerProvider).profile;
  @override
  String get id => (origin as EntryTreeManagerProvider).id;
}

String _$timelineUpdaterHash() => r'38d091cafcfa347f52bd3260d7ab72178bd2dc7b';

abstract class _$TimelineUpdater extends BuildlessAutoDisposeNotifier<bool> {
  late final Profile profile;

  bool build(
    Profile profile,
  );
}

/// See also [TimelineUpdater].
@ProviderFor(TimelineUpdater)
const timelineUpdaterProvider = TimelineUpdaterFamily();

/// See also [TimelineUpdater].
class TimelineUpdaterFamily extends Family<bool> {
  /// See also [TimelineUpdater].
  const TimelineUpdaterFamily();

  /// See also [TimelineUpdater].
  TimelineUpdaterProvider call(
    Profile profile,
  ) {
    return TimelineUpdaterProvider(
      profile,
    );
  }

  @override
  TimelineUpdaterProvider getProviderOverride(
    covariant TimelineUpdaterProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'timelineUpdaterProvider';
}

/// See also [TimelineUpdater].
class TimelineUpdaterProvider
    extends AutoDisposeNotifierProviderImpl<TimelineUpdater, bool> {
  /// See also [TimelineUpdater].
  TimelineUpdaterProvider(
    Profile profile,
  ) : this._internal(
          () => TimelineUpdater()..profile = profile,
          from: timelineUpdaterProvider,
          name: r'timelineUpdaterProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$timelineUpdaterHash,
          dependencies: TimelineUpdaterFamily._dependencies,
          allTransitiveDependencies:
              TimelineUpdaterFamily._allTransitiveDependencies,
          profile: profile,
        );

  TimelineUpdaterProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  bool runNotifierBuild(
    covariant TimelineUpdater notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(TimelineUpdater Function() create) {
    return ProviderOverride(
      origin: this,
      override: TimelineUpdaterProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<TimelineUpdater, bool> createElement() {
    return _TimelineUpdaterProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TimelineUpdaterProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin TimelineUpdaterRef on AutoDisposeNotifierProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _TimelineUpdaterProviderElement
    extends AutoDisposeNotifierProviderElement<TimelineUpdater, bool>
    with TimelineUpdaterRef {
  _TimelineUpdaterProviderElement(super.provider);

  @override
  Profile get profile => (origin as TimelineUpdaterProvider).profile;
}

String _$statusWriterHash() => r'8b8775b42a442b9ad7dd992ee3c67b1379ae0c1b';

abstract class _$StatusWriter extends BuildlessAutoDisposeNotifier<bool> {
  late final Profile profile;

  bool build(
    Profile profile,
  );
}

/// See also [StatusWriter].
@ProviderFor(StatusWriter)
const statusWriterProvider = StatusWriterFamily();

/// See also [StatusWriter].
class StatusWriterFamily extends Family<bool> {
  /// See also [StatusWriter].
  const StatusWriterFamily();

  /// See also [StatusWriter].
  StatusWriterProvider call(
    Profile profile,
  ) {
    return StatusWriterProvider(
      profile,
    );
  }

  @override
  StatusWriterProvider getProviderOverride(
    covariant StatusWriterProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'statusWriterProvider';
}

/// See also [StatusWriter].
class StatusWriterProvider
    extends AutoDisposeNotifierProviderImpl<StatusWriter, bool> {
  /// See also [StatusWriter].
  StatusWriterProvider(
    Profile profile,
  ) : this._internal(
          () => StatusWriter()..profile = profile,
          from: statusWriterProvider,
          name: r'statusWriterProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$statusWriterHash,
          dependencies: StatusWriterFamily._dependencies,
          allTransitiveDependencies:
              StatusWriterFamily._allTransitiveDependencies,
          profile: profile,
        );

  StatusWriterProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  bool runNotifierBuild(
    covariant StatusWriter notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(StatusWriter Function() create) {
    return ProviderOverride(
      origin: this,
      override: StatusWriterProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<StatusWriter, bool> createElement() {
    return _StatusWriterProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is StatusWriterProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin StatusWriterRef on AutoDisposeNotifierProviderRef<bool> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _StatusWriterProviderElement
    extends AutoDisposeNotifierProviderElement<StatusWriter, bool>
    with StatusWriterRef {
  _StatusWriterProviderElement(super.provider);

  @override
  Profile get profile => (origin as StatusWriterProvider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
