// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timeline_entry_filter_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$filterByIdHash() => r'ca624d942ca2403fb85a4e0cb9552b693de9021e';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [filterById].
@ProviderFor(filterById)
const filterByIdProvider = FilterByIdFamily();

/// See also [filterById].
class FilterByIdFamily extends Family<Result<TimelineEntryFilter, ExecError>> {
  /// See also [filterById].
  const FilterByIdFamily();

  /// See also [filterById].
  FilterByIdProvider call(
    Profile profile,
    String id,
  ) {
    return FilterByIdProvider(
      profile,
      id,
    );
  }

  @override
  FilterByIdProvider getProviderOverride(
    covariant FilterByIdProvider provider,
  ) {
    return call(
      provider.profile,
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'filterByIdProvider';
}

/// See also [filterById].
class FilterByIdProvider
    extends AutoDisposeProvider<Result<TimelineEntryFilter, ExecError>> {
  /// See also [filterById].
  FilterByIdProvider(
    Profile profile,
    String id,
  ) : this._internal(
          (ref) => filterById(
            ref as FilterByIdRef,
            profile,
            id,
          ),
          from: filterByIdProvider,
          name: r'filterByIdProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$filterByIdHash,
          dependencies: FilterByIdFamily._dependencies,
          allTransitiveDependencies:
              FilterByIdFamily._allTransitiveDependencies,
          profile: profile,
          id: id,
        );

  FilterByIdProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.id,
  }) : super.internal();

  final Profile profile;
  final String id;

  @override
  Override overrideWith(
    Result<TimelineEntryFilter, ExecError> Function(FilterByIdRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FilterByIdProvider._internal(
        (ref) => create(ref as FilterByIdRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<Result<TimelineEntryFilter, ExecError>>
      createElement() {
    return _FilterByIdProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FilterByIdProvider &&
        other.profile == profile &&
        other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FilterByIdRef
    on AutoDisposeProviderRef<Result<TimelineEntryFilter, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `id` of this provider.
  String get id;
}

class _FilterByIdProviderElement
    extends AutoDisposeProviderElement<Result<TimelineEntryFilter, ExecError>>
    with FilterByIdRef {
  _FilterByIdProviderElement(super.provider);

  @override
  Profile get profile => (origin as FilterByIdProvider).profile;
  @override
  String get id => (origin as FilterByIdProvider).id;
}

String _$checkTimelineEntryHash() =>
    r'e8c827759a1b988b063a5555c6b48edbbc96d00c';

/// See also [checkTimelineEntry].
@ProviderFor(checkTimelineEntry)
const checkTimelineEntryProvider = CheckTimelineEntryFamily();

/// See also [checkTimelineEntry].
class CheckTimelineEntryFamily extends Family<FilterResult> {
  /// See also [checkTimelineEntry].
  const CheckTimelineEntryFamily();

  /// See also [checkTimelineEntry].
  CheckTimelineEntryProvider call(
    Profile profile,
    TimelineEntry entry,
  ) {
    return CheckTimelineEntryProvider(
      profile,
      entry,
    );
  }

  @override
  CheckTimelineEntryProvider getProviderOverride(
    covariant CheckTimelineEntryProvider provider,
  ) {
    return call(
      provider.profile,
      provider.entry,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'checkTimelineEntryProvider';
}

/// See also [checkTimelineEntry].
class CheckTimelineEntryProvider extends AutoDisposeProvider<FilterResult> {
  /// See also [checkTimelineEntry].
  CheckTimelineEntryProvider(
    Profile profile,
    TimelineEntry entry,
  ) : this._internal(
          (ref) => checkTimelineEntry(
            ref as CheckTimelineEntryRef,
            profile,
            entry,
          ),
          from: checkTimelineEntryProvider,
          name: r'checkTimelineEntryProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$checkTimelineEntryHash,
          dependencies: CheckTimelineEntryFamily._dependencies,
          allTransitiveDependencies:
              CheckTimelineEntryFamily._allTransitiveDependencies,
          profile: profile,
          entry: entry,
        );

  CheckTimelineEntryProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.entry,
  }) : super.internal();

  final Profile profile;
  final TimelineEntry entry;

  @override
  Override overrideWith(
    FilterResult Function(CheckTimelineEntryRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: CheckTimelineEntryProvider._internal(
        (ref) => create(ref as CheckTimelineEntryRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        entry: entry,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<FilterResult> createElement() {
    return _CheckTimelineEntryProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CheckTimelineEntryProvider &&
        other.profile == profile &&
        other.entry == entry;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, entry.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin CheckTimelineEntryRef on AutoDisposeProviderRef<FilterResult> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `entry` of this provider.
  TimelineEntry get entry;
}

class _CheckTimelineEntryProviderElement
    extends AutoDisposeProviderElement<FilterResult>
    with CheckTimelineEntryRef {
  _CheckTimelineEntryProviderElement(super.provider);

  @override
  Profile get profile => (origin as CheckTimelineEntryProvider).profile;
  @override
  TimelineEntry get entry => (origin as CheckTimelineEntryProvider).entry;
}

String _$timelineEntryFiltersHash() =>
    r'cd4344f10166b5cabb7869c350e6d83f571cb20d';

abstract class _$TimelineEntryFilters
    extends BuildlessNotifier<Map<String, TimelineEntryFilter>> {
  late final Profile profile;

  Map<String, TimelineEntryFilter> build(
    Profile profile,
  );
}

/// See also [TimelineEntryFilters].
@ProviderFor(TimelineEntryFilters)
const timelineEntryFiltersProvider = TimelineEntryFiltersFamily();

/// See also [TimelineEntryFilters].
class TimelineEntryFiltersFamily
    extends Family<Map<String, TimelineEntryFilter>> {
  /// See also [TimelineEntryFilters].
  const TimelineEntryFiltersFamily();

  /// See also [TimelineEntryFilters].
  TimelineEntryFiltersProvider call(
    Profile profile,
  ) {
    return TimelineEntryFiltersProvider(
      profile,
    );
  }

  @override
  TimelineEntryFiltersProvider getProviderOverride(
    covariant TimelineEntryFiltersProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'timelineEntryFiltersProvider';
}

/// See also [TimelineEntryFilters].
class TimelineEntryFiltersProvider extends NotifierProviderImpl<
    TimelineEntryFilters, Map<String, TimelineEntryFilter>> {
  /// See also [TimelineEntryFilters].
  TimelineEntryFiltersProvider(
    Profile profile,
  ) : this._internal(
          () => TimelineEntryFilters()..profile = profile,
          from: timelineEntryFiltersProvider,
          name: r'timelineEntryFiltersProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$timelineEntryFiltersHash,
          dependencies: TimelineEntryFiltersFamily._dependencies,
          allTransitiveDependencies:
              TimelineEntryFiltersFamily._allTransitiveDependencies,
          profile: profile,
        );

  TimelineEntryFiltersProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  Map<String, TimelineEntryFilter> runNotifierBuild(
    covariant TimelineEntryFilters notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(TimelineEntryFilters Function() create) {
    return ProviderOverride(
      origin: this,
      override: TimelineEntryFiltersProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<TimelineEntryFilters,
      Map<String, TimelineEntryFilter>> createElement() {
    return _TimelineEntryFiltersProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TimelineEntryFiltersProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin TimelineEntryFiltersRef
    on NotifierProviderRef<Map<String, TimelineEntryFilter>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _TimelineEntryFiltersProviderElement extends NotifierProviderElement<
    TimelineEntryFilters,
    Map<String, TimelineEntryFilter>> with TimelineEntryFiltersRef {
  _TimelineEntryFiltersProviderElement(super.provider);

  @override
  Profile get profile => (origin as TimelineEntryFiltersProvider).profile;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
