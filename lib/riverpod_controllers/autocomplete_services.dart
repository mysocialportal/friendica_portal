import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../models/auth/profile.dart';
import '../models/exec_error.dart';
import '../utils/entry_tree_item_flattening.dart';
import 'entry_tree_item_services.dart';
import 'rp_provider_extension.dart';

part 'autocomplete_services.g.dart';

final _pthLogger = Logger('PostTreeHashtagsProvider');
const _cacheDuration = Duration(minutes: 30);

@riverpod
Result<List<String>, ExecError> postTreeHashtags(
    Ref ref, Profile profile, String id) {
  _pthLogger.info('Building for $id for $profile');
  ref.cacheFor(_cacheDuration);
  return ref
      .watch(postTreeEntryByIdProvider(profile, id))
      .transform(
        (entryTreeItem) => entryTreeItem
            .flatten(ref: ref, profile: profile)
            .map((e) => e.timelineEntry.tags)
            .expand((l) => l)
            .toSet()
            .toList()
          ..sort(),
      )
      .execErrorCast();
}

final _ptcLogger = Logger('PostTreeConnectionIdsProvider');

@riverpod
Result<List<String>, ExecError> postTreeConnectionIds(
    Ref ref, Profile profile, String id) {
  _ptcLogger.info('Building for $id for $profile');
  ref.cacheFor(_cacheDuration);
  return ref
      .watch(postTreeEntryByIdProvider(profile, id))
      .transform(
        (entryTreeItem) => entryTreeItem
            .flatten(ref: ref, profile: profile)
            .map((e) => [
                  e.timelineEntry.authorId,
                  e.timelineEntry.parentAuthorId,
                ])
            .expand((l) => l)
            .toSet()
            .toList(),
      )
      .execErrorCast();
}
