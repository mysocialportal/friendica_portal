// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timeline_services.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$timelineMaintainerHash() =>
    r'252fcde9865f1e66623ba475b5fdf0bd24cd6b6f';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$TimelineMaintainer
    extends BuildlessNotifier<List<TimelineIdentifiers>> {
  late final Profile profile;

  List<TimelineIdentifiers> build(
    Profile profile,
  );
}

/// See also [TimelineMaintainer].
@ProviderFor(TimelineMaintainer)
const timelineMaintainerProvider = TimelineMaintainerFamily();

/// See also [TimelineMaintainer].
class TimelineMaintainerFamily extends Family<List<TimelineIdentifiers>> {
  /// See also [TimelineMaintainer].
  const TimelineMaintainerFamily();

  /// See also [TimelineMaintainer].
  TimelineMaintainerProvider call(
    Profile profile,
  ) {
    return TimelineMaintainerProvider(
      profile,
    );
  }

  @override
  TimelineMaintainerProvider getProviderOverride(
    covariant TimelineMaintainerProvider provider,
  ) {
    return call(
      provider.profile,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'timelineMaintainerProvider';
}

/// See also [TimelineMaintainer].
class TimelineMaintainerProvider extends NotifierProviderImpl<
    TimelineMaintainer, List<TimelineIdentifiers>> {
  /// See also [TimelineMaintainer].
  TimelineMaintainerProvider(
    Profile profile,
  ) : this._internal(
          () => TimelineMaintainer()..profile = profile,
          from: timelineMaintainerProvider,
          name: r'timelineMaintainerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$timelineMaintainerHash,
          dependencies: TimelineMaintainerFamily._dependencies,
          allTransitiveDependencies:
              TimelineMaintainerFamily._allTransitiveDependencies,
          profile: profile,
        );

  TimelineMaintainerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
  }) : super.internal();

  final Profile profile;

  @override
  List<TimelineIdentifiers> runNotifierBuild(
    covariant TimelineMaintainer notifier,
  ) {
    return notifier.build(
      profile,
    );
  }

  @override
  Override overrideWith(TimelineMaintainer Function() create) {
    return ProviderOverride(
      origin: this,
      override: TimelineMaintainerProvider._internal(
        () => create()..profile = profile,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
      ),
    );
  }

  @override
  NotifierProviderElement<TimelineMaintainer, List<TimelineIdentifiers>>
      createElement() {
    return _TimelineMaintainerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TimelineMaintainerProvider && other.profile == profile;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin TimelineMaintainerRef on NotifierProviderRef<List<TimelineIdentifiers>> {
  /// The parameter `profile` of this provider.
  Profile get profile;
}

class _TimelineMaintainerProviderElement extends NotifierProviderElement<
    TimelineMaintainer, List<TimelineIdentifiers>> with TimelineMaintainerRef {
  _TimelineMaintainerProviderElement(super.provider);

  @override
  Profile get profile => (origin as TimelineMaintainerProvider).profile;
}

String _$timelineManagerHash() => r'9f806c36aeb207547fde5a352df1c1fb051cc9af';

abstract class _$TimelineManager extends BuildlessNotifier<Timeline> {
  late final Profile profile;
  late final TimelineIdentifiers timelineId;

  Timeline build(
    Profile profile,
    TimelineIdentifiers timelineId,
  );
}

/// See also [TimelineManager].
@ProviderFor(TimelineManager)
const timelineManagerProvider = TimelineManagerFamily();

/// See also [TimelineManager].
class TimelineManagerFamily extends Family<Timeline> {
  /// See also [TimelineManager].
  const TimelineManagerFamily();

  /// See also [TimelineManager].
  TimelineManagerProvider call(
    Profile profile,
    TimelineIdentifiers timelineId,
  ) {
    return TimelineManagerProvider(
      profile,
      timelineId,
    );
  }

  @override
  TimelineManagerProvider getProviderOverride(
    covariant TimelineManagerProvider provider,
  ) {
    return call(
      provider.profile,
      provider.timelineId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'timelineManagerProvider';
}

/// See also [TimelineManager].
class TimelineManagerProvider
    extends NotifierProviderImpl<TimelineManager, Timeline> {
  /// See also [TimelineManager].
  TimelineManagerProvider(
    Profile profile,
    TimelineIdentifiers timelineId,
  ) : this._internal(
          () => TimelineManager()
            ..profile = profile
            ..timelineId = timelineId,
          from: timelineManagerProvider,
          name: r'timelineManagerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$timelineManagerHash,
          dependencies: TimelineManagerFamily._dependencies,
          allTransitiveDependencies:
              TimelineManagerFamily._allTransitiveDependencies,
          profile: profile,
          timelineId: timelineId,
        );

  TimelineManagerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.timelineId,
  }) : super.internal();

  final Profile profile;
  final TimelineIdentifiers timelineId;

  @override
  Timeline runNotifierBuild(
    covariant TimelineManager notifier,
  ) {
    return notifier.build(
      profile,
      timelineId,
    );
  }

  @override
  Override overrideWith(TimelineManager Function() create) {
    return ProviderOverride(
      origin: this,
      override: TimelineManagerProvider._internal(
        () => create()
          ..profile = profile
          ..timelineId = timelineId,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        timelineId: timelineId,
      ),
    );
  }

  @override
  NotifierProviderElement<TimelineManager, Timeline> createElement() {
    return _TimelineManagerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TimelineManagerProvider &&
        other.profile == profile &&
        other.timelineId == timelineId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, timelineId.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin TimelineManagerRef on NotifierProviderRef<Timeline> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `timelineId` of this provider.
  TimelineIdentifiers get timelineId;
}

class _TimelineManagerProviderElement
    extends NotifierProviderElement<TimelineManager, Timeline>
    with TimelineManagerRef {
  _TimelineManagerProviderElement(super.provider);

  @override
  Profile get profile => (origin as TimelineManagerProvider).profile;
  @override
  TimelineIdentifiers get timelineId =>
      (origin as TimelineManagerProvider).timelineId;
}

String _$userMediaTimelineHash() => r'f13ba417b1d5550392f973af9e86c099efd60491';

abstract class _$UserMediaTimeline extends BuildlessAutoDisposeAsyncNotifier<
    Result<List<MediaAttachment>, ExecError>> {
  late final Profile profile;
  late final String accountId;

  FutureOr<Result<List<MediaAttachment>, ExecError>> build(
    Profile profile,
    String accountId,
  );
}

/// See also [UserMediaTimeline].
@ProviderFor(UserMediaTimeline)
const userMediaTimelineProvider = UserMediaTimelineFamily();

/// See also [UserMediaTimeline].
class UserMediaTimelineFamily
    extends Family<AsyncValue<Result<List<MediaAttachment>, ExecError>>> {
  /// See also [UserMediaTimeline].
  const UserMediaTimelineFamily();

  /// See also [UserMediaTimeline].
  UserMediaTimelineProvider call(
    Profile profile,
    String accountId,
  ) {
    return UserMediaTimelineProvider(
      profile,
      accountId,
    );
  }

  @override
  UserMediaTimelineProvider getProviderOverride(
    covariant UserMediaTimelineProvider provider,
  ) {
    return call(
      provider.profile,
      provider.accountId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'userMediaTimelineProvider';
}

/// See also [UserMediaTimeline].
class UserMediaTimelineProvider extends AutoDisposeAsyncNotifierProviderImpl<
    UserMediaTimeline, Result<List<MediaAttachment>, ExecError>> {
  /// See also [UserMediaTimeline].
  UserMediaTimelineProvider(
    Profile profile,
    String accountId,
  ) : this._internal(
          () => UserMediaTimeline()
            ..profile = profile
            ..accountId = accountId,
          from: userMediaTimelineProvider,
          name: r'userMediaTimelineProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$userMediaTimelineHash,
          dependencies: UserMediaTimelineFamily._dependencies,
          allTransitiveDependencies:
              UserMediaTimelineFamily._allTransitiveDependencies,
          profile: profile,
          accountId: accountId,
        );

  UserMediaTimelineProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.accountId,
  }) : super.internal();

  final Profile profile;
  final String accountId;

  @override
  FutureOr<Result<List<MediaAttachment>, ExecError>> runNotifierBuild(
    covariant UserMediaTimeline notifier,
  ) {
    return notifier.build(
      profile,
      accountId,
    );
  }

  @override
  Override overrideWith(UserMediaTimeline Function() create) {
    return ProviderOverride(
      origin: this,
      override: UserMediaTimelineProvider._internal(
        () => create()
          ..profile = profile
          ..accountId = accountId,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        accountId: accountId,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<UserMediaTimeline,
      Result<List<MediaAttachment>, ExecError>> createElement() {
    return _UserMediaTimelineProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is UserMediaTimelineProvider &&
        other.profile == profile &&
        other.accountId == accountId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, accountId.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin UserMediaTimelineRef on AutoDisposeAsyncNotifierProviderRef<
    Result<List<MediaAttachment>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `accountId` of this provider.
  String get accountId;
}

class _UserMediaTimelineProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<UserMediaTimeline,
        Result<List<MediaAttachment>, ExecError>> with UserMediaTimelineRef {
  _UserMediaTimelineProviderElement(super.provider);

  @override
  Profile get profile => (origin as UserMediaTimelineProvider).profile;
  @override
  String get accountId => (origin as UserMediaTimelineProvider).accountId;
}

String _$userPostsAndCommentsTimelineHash() =>
    r'8767f280545ae572e7a5134052863144d82c1e7e';

abstract class _$UserPostsAndCommentsTimeline
    extends BuildlessAutoDisposeAsyncNotifier<
        Result<List<TimelineEntry>, ExecError>> {
  late final Profile profile;
  late final String accountId;

  FutureOr<Result<List<TimelineEntry>, ExecError>> build(
    Profile profile,
    String accountId,
  );
}

/// See also [UserPostsAndCommentsTimeline].
@ProviderFor(UserPostsAndCommentsTimeline)
const userPostsAndCommentsTimelineProvider =
    UserPostsAndCommentsTimelineFamily();

/// See also [UserPostsAndCommentsTimeline].
class UserPostsAndCommentsTimelineFamily
    extends Family<AsyncValue<Result<List<TimelineEntry>, ExecError>>> {
  /// See also [UserPostsAndCommentsTimeline].
  const UserPostsAndCommentsTimelineFamily();

  /// See also [UserPostsAndCommentsTimeline].
  UserPostsAndCommentsTimelineProvider call(
    Profile profile,
    String accountId,
  ) {
    return UserPostsAndCommentsTimelineProvider(
      profile,
      accountId,
    );
  }

  @override
  UserPostsAndCommentsTimelineProvider getProviderOverride(
    covariant UserPostsAndCommentsTimelineProvider provider,
  ) {
    return call(
      provider.profile,
      provider.accountId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'userPostsAndCommentsTimelineProvider';
}

/// See also [UserPostsAndCommentsTimeline].
class UserPostsAndCommentsTimelineProvider
    extends AutoDisposeAsyncNotifierProviderImpl<UserPostsAndCommentsTimeline,
        Result<List<TimelineEntry>, ExecError>> {
  /// See also [UserPostsAndCommentsTimeline].
  UserPostsAndCommentsTimelineProvider(
    Profile profile,
    String accountId,
  ) : this._internal(
          () => UserPostsAndCommentsTimeline()
            ..profile = profile
            ..accountId = accountId,
          from: userPostsAndCommentsTimelineProvider,
          name: r'userPostsAndCommentsTimelineProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$userPostsAndCommentsTimelineHash,
          dependencies: UserPostsAndCommentsTimelineFamily._dependencies,
          allTransitiveDependencies:
              UserPostsAndCommentsTimelineFamily._allTransitiveDependencies,
          profile: profile,
          accountId: accountId,
        );

  UserPostsAndCommentsTimelineProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.profile,
    required this.accountId,
  }) : super.internal();

  final Profile profile;
  final String accountId;

  @override
  FutureOr<Result<List<TimelineEntry>, ExecError>> runNotifierBuild(
    covariant UserPostsAndCommentsTimeline notifier,
  ) {
    return notifier.build(
      profile,
      accountId,
    );
  }

  @override
  Override overrideWith(UserPostsAndCommentsTimeline Function() create) {
    return ProviderOverride(
      origin: this,
      override: UserPostsAndCommentsTimelineProvider._internal(
        () => create()
          ..profile = profile
          ..accountId = accountId,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        profile: profile,
        accountId: accountId,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<UserPostsAndCommentsTimeline,
      Result<List<TimelineEntry>, ExecError>> createElement() {
    return _UserPostsAndCommentsTimelineProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is UserPostsAndCommentsTimelineProvider &&
        other.profile == profile &&
        other.accountId == accountId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, profile.hashCode);
    hash = _SystemHash.combine(hash, accountId.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin UserPostsAndCommentsTimelineRef on AutoDisposeAsyncNotifierProviderRef<
    Result<List<TimelineEntry>, ExecError>> {
  /// The parameter `profile` of this provider.
  Profile get profile;

  /// The parameter `accountId` of this provider.
  String get accountId;
}

class _UserPostsAndCommentsTimelineProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<
        UserPostsAndCommentsTimeline, Result<List<TimelineEntry>, ExecError>>
    with UserPostsAndCommentsTimelineRef {
  _UserPostsAndCommentsTimelineProviderElement(super.provider);

  @override
  Profile get profile =>
      (origin as UserPostsAndCommentsTimelineProvider).profile;
  @override
  String get accountId =>
      (origin as UserPostsAndCommentsTimelineProvider).accountId;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
