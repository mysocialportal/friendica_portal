import 'package:riverpod_annotation/riverpod_annotation.dart';

import 'rp_provider_extension.dart';

part 'status_service.g.dart';

@riverpod
class StatusService extends _$StatusService {
  var _lastStatusTime = DateTime.now();

  DateTime get lastStatusTime => _lastStatusTime;

  @override
  String build() {
    ref.cacheFor(const Duration(minutes: 10));
    _lastStatusTime = DateTime.now();
    state = 'None';
    return state;
  }

  void setStatus(String status) {
    state = status;
    _lastStatusTime = DateTime.now();
  }
}
