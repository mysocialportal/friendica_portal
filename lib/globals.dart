import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

const deepLinkScheme = 'relatica';

const macOsGroupId = 'T69YZGT58U.relatica';

String randomId() => const Uuid().v4().toString();

final platformIsMobile = Platform.isIOS || Platform.isAndroid;

final platformHasCamera = platformIsMobile;

final platformIsDesktop = !platformIsMobile;

final useImagePicker = kIsWeb || platformIsMobile;

String appVersion = '';

const usePhpDebugging = false;

const maxViewPortalHeight = 750.0;
const maxViewPortalWidth = 750.0;

const maxProcessingMillis = 3;
const processingSleep = Duration(milliseconds: 1);
const apiCallTimeout = Duration(seconds: 90);
const oauthTimeout = Duration(seconds: 30);

Future<void> showInfoDialog(BuildContext context, String text) async {
  await showDialog<bool>(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Text(
          text,
          softWrap: true,
        ),
        actions: <Widget>[
          ElevatedButton(
            child: const Text('Dismiss'),
            onPressed: () {
              Navigator.pop(context, true); // showDialog() returns true
            },
          ),
        ],
      );
    },
  );
}

Future<bool?> showConfirmDialog(BuildContext context, String caption) {
  return showDialog<bool>(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Text(caption),
        actions: <Widget>[
          ElevatedButton(
            child: const Text('OK'),
            onPressed: () {
              Navigator.pop(context, true); // showDialog() returns true
            },
          ),
        ],
      );
    },
  );
}

Future<bool?> showYesNoDialog(BuildContext context, String caption) {
  return showDialog<bool>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Text(caption),
        actions: <Widget>[
          ElevatedButton(
            child: const Text('Yes'),
            onPressed: () {
              Navigator.pop(context, true); // showDialog() returns true
            },
          ),
          ElevatedButton(
            child: const Text('No'),
            onPressed: () {
              Navigator.pop(context, false); // showDialog() returns false
            },
          ),
        ],
      );
    },
  );
}

Future<String?> showChooseOptions(
  BuildContext context,
  String caption,
  List<String> options,
) {
  return showDialog<String?>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
          content: Text(caption),
          actions: options
              .map((o) => ElevatedButton(
                    child: Text(o),
                    onPressed: () {
                      Navigator.pop(context, o); // showDialog() returns true
                    },
                  ))
              .toList());
    },
  );
}
