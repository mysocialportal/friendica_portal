import 'package:result_monad/result_monad.dart';

import '../globals.dart';
import '../models/auth/profile.dart';
import '../routes.dart';

Uri generateTagUriFromProfile(Profile profile, String tag) {
  return Uri.parse('relatica://route${ScreenPaths.tagView}/$tag');
}

Result<String, bool> processUrlStringForTag(String url) {
  final uri = Uri.parse(url);
  if (uri.scheme == deepLinkScheme &&
      uri.host == 'route' &&
      uri.pathSegments.length == 2) {
    final route = '/${uri.pathSegments[0]}';
    final tag = uri.pathSegments[1];
    if (route == ScreenPaths.tagView) {
      return Result.ok(tag);
    }
  }

  return Result.error(false);
}

String generateTagSearchFromProfile(Profile profile, String tagName) {
  return Uri.https(
    profile.serverName,
    '/search',
    {'tag': tagName},
  ).toString();
}
