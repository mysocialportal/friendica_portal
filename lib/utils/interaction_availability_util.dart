import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/auth/profile.dart';
import '../models/timeline_entry.dart';
import '../models/timeline_network_info.dart';
import '../models/visibility.dart';
import '../riverpod_controllers/feature_checker_services.dart';
import '../riverpod_controllers/settings_services.dart';
import 'known_network_extensions.dart';

class InteractionCapabilityResult {
  final bool canDo;
  final String reason;

  const InteractionCapabilityResult(
      {required this.canDo, required this.reason});
}

extension InteractionAvailabilityExtension on TimelineEntry {
  InteractionCapabilityResult getCanComment(WidgetRef ref) {
    final nc = ref
        .read(networkCapabilitiesSettingProvider)
        .getCapabilities(networkInfo.network);

    if (!nc.comment) {
      return InteractionCapabilityResult(
        canDo: false,
        reason:
            "User disabled commenting on ${networkInfo.network.labelName} items. Go into settings to change.",
      );
    }

    return const InteractionCapabilityResult(
      canDo: true,
      reason: "Can comment on item",
    );
  }

  InteractionCapabilityResult getCanReact(WidgetRef ref) {
    final nc = ref
        .read(networkCapabilitiesSettingProvider)
        .getCapabilities(networkInfo.network);

    if (!nc.react) {
      return InteractionCapabilityResult(
        canDo: false,
        reason:
            "User disabled reacting on ${networkInfo.network.labelName} items. Go into settings to change.",
      );
    }

    return const InteractionCapabilityResult(
      canDo: true,
      reason: "Can react on item",
    );
  }

  InteractionCapabilityResult getIsReshareable(
      WidgetRef ref, Profile profile, bool isMine) {
    if (isMine) {
      return const InteractionCapabilityResult(
        canDo: false,
        reason: "Can't reshare your own post",
      );
    }

    if (networkInfo.network == KnownNetworks.bluesky) {
      final result = ref.read(
          featureCheckResultProvider(profile, RelaticaFeatures.blueskyReshare));

      if (result.isFailure) {
        return InteractionCapabilityResult(
          canDo: false,
          reason: result.error.message,
        );
      }
    }

    final nc = ref
        .read(networkCapabilitiesSettingProvider)
        .getCapabilities(networkInfo.network);

    if (!nc.reshare) {
      return InteractionCapabilityResult(
        canDo: false,
        reason:
            "User disabled resharing ${networkInfo.network.labelName} items. Go into settings to change.",
      );
    }

    if (visibility.type == VisibilityType.public ||
        visibility.type == VisibilityType.unlisted) {
      return const InteractionCapabilityResult(
        canDo: true,
        reason: "Can reshare item",
      );
    }

    return const InteractionCapabilityResult(
      canDo: false,
      reason: "Can't reshare private items",
    );
  }
}
