import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/auth/profile.dart';
import '../models/entry_tree_item.dart';
import '../models/flattened_tree_item.dart';
import '../models/timeline_entry.dart';
import '../riverpod_controllers/entry_tree_item_services.dart';
import '../riverpod_controllers/timeline_entry_services.dart';

extension FlatteningExtensions on EntryTreeItem {
  static const baseLevel = 0;

  List<FlattenedTreeItem> flatten(
      {int level = baseLevel,
      bool topLevelOnly = false,
      required Profile profile,
      required Ref ref}) {
    final items = <FlattenedTreeItem>[];

    final entryForItemResult =
        ref.read(timelineEntryManagerProvider(profile, id));
    if (entryForItemResult.isFailure) {
      return [];
    }
    final entryForItem = entryForItemResult.value;
    final myEntry = FlattenedTreeItem(
      timelineEntry: entryForItem,
      isMine: isMine,
      level: level,
    );

    items.add(myEntry);
    if (topLevelOnly) {
      return items;
    }

    final sortedChildren = children
        .map((id) {
          final treeResult = ref.read(entryTreeManagerProvider(profile, id));
          final entryResult =
              ref.read(timelineEntryManagerProvider(profile, id));
          if (treeResult.isFailure || entryResult.isFailure) {
            return null;
          }
          final tree = treeResult.value;
          final entry = entryResult.value;
          return _EntryTreeItemWithEntity(entry, tree);
        })
        .where((e) => e != null)
        .toList();
    for (final child in sortedChildren) {
      int childLevel = level + 1;
      if (child!.entry.authorId == entryForItem.authorId &&
          level != baseLevel) {
        childLevel = level;
      }

      final childItems =
          child.tree.flatten(level: childLevel, profile: profile, ref: ref);
      childItems.sort((c1, c2) {
        if (c2.level == c1.level) {
          return c1.timelineEntry.creationTimestamp
              .compareTo(c2.timelineEntry.creationTimestamp);
        }

        if (c1.level == childLevel) {
          return -1;
        }

        if (c2.level == childLevel) {
          return 1;
        }

        return 0;
      });

      items.addAll(childItems);
    }

    return items;
  }
}

class _EntryTreeItemWithEntity {
  final TimelineEntry entry;
  final EntryTreeItem tree;

  _EntryTreeItemWithEntity(this.entry, this.tree);
}
