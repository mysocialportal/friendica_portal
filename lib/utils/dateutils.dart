import 'package:result_monad/result_monad.dart';
import 'package:time_machine/time_machine_text_patterns.dart';

import '../models/exec_error.dart';

class OffsetDateTimeUtils {
  static final _offsetTimeParser =
      OffsetDateTimePattern.createWithInvariantCulture(
          'ddd MMM dd HH:mm:ss o<+HHmm> yyyy');

  static Result<int, ExecError> epochSecTimeFromFriendicaString(
      String dateString) {
    final offsetDateTime = _offsetTimeParser.parse(dateString);
    if (!offsetDateTime.success) {
      return Result.error(ExecError(
          type: ErrorType.parsingError,
          message: offsetDateTime.error.toString()));
    }

    return Result.ok(offsetDateTime.value.localDateTime
            .toDateTimeLocal()
            .millisecondsSinceEpoch ~/
        1000);
  }

  static Result<int, ExecError> epochSecTimeFromTimeZoneString(
      String dateString) {
    final offsetDateTime = OffsetDateTimePattern.extendedIso.parse(dateString);
    if (!offsetDateTime.success) {
      return Result.error(ExecError(
          type: ErrorType.parsingError,
          message: offsetDateTime.error.toString()));
    }

    return Result.ok(offsetDateTime.value.localDateTime
            .toDateTimeLocal()
            .millisecondsSinceEpoch ~/
        1000);
  }
}

class ElapsedDateUtils {
  static String elapsedTimeStringFromEpochSeconds(int epochSeconds) {
    return epochMilliSecondsToString(epochSeconds * 1000);
  }

  static String epochMilliSecondsToString(int epochMilliSeconds) {
    final epoch = DateTime.fromMillisecondsSinceEpoch(epochMilliSeconds);
    final elapsed = DateTime.now().difference(epoch);
    if (elapsed.inDays > 0) {
      return '${elapsed.inDays} days ago';
    }

    if (elapsed.inHours > 0) {
      return '${elapsed.inHours} hours ago';
    }

    if (elapsed.inMinutes > 0) {
      return '${elapsed.inMinutes} minutes ago';
    }

    return 'seconds ago';
  }

  static Duration elapsedTimeFromEpochSeconds(int epochSeconds) {
    return elapsedTimeFromEpochMilliseconds(epochSeconds * 1000);
  }

  static Duration elapsedTimeFromEpochMilliseconds(int epochMilliseconds) {
    final epoch = DateTime.fromMillisecondsSinceEpoch(epochMilliseconds);
    final elapsed = DateTime.now().difference(epoch);
    return elapsed;
  }
}

const _separator = '_';

extension DateTimeExtensions on DateTime {
  String toFileNameString() => '$year$month$day$_separator$hour$minute$second';
}

extension DurationExtensions on Duration {
  String get simpleLabel {
    final days = inHours / 24.0;
    if (days >= 1) {
      return days.round() == 1 ? '1 day' : '${days.round()} days';
    }

    final hours = inMinutes / 60.0;
    if (hours >= 1) {
      return hours.round() == 1 ? '1 hour' : '${hours.round()} hours';
    }

    final minutes = inSeconds / 60.0;
    if (minutes >= 1) {
      return minutes.round() == 1 ? '1 minute' : '${minutes.round()} minutes';
    }

    final seconds = inMilliseconds / 1000.0;
    if (seconds >= 1) {
      return seconds.round() == 1 ? '1 second' : '${seconds.round()} seconds';
    }

    if (inMilliseconds != 0) {
      return inMilliseconds == 1 ? '1 millisecond' : '$inSeconds milliseconds';
    }

    return inMicroseconds == 1
        ? '1 microsecond'
        : '$inMicroseconds microseconds';
  }
}
