import 'dart:math';

import 'package:flutter/cupertino.dart';

import '../globals.dart';

class ResponsiveSizesCalculator {
  final BuildContext context;

  const ResponsiveSizesCalculator(this.context);

  double get viewPortalWidth => min(_screenSize.width, maxViewPortalWidth);

  double get maxThumbnailHeight =>
      min(_screenSize.height * 0.5, maxViewPortalHeight);

  double get maxThumbnailWidth => min(
      _screenSize.width < 600
          ? _screenSize.width * 0.8
          : _screenSize.width * 0.5,
      maxViewPortalHeight);

  Size get _screenSize => MediaQuery.sizeOf(context);
}
