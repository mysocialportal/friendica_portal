import 'package:intl/intl.dart';

extension StringUtils on String {
  String truncate({int length = 32}) {
    if (this.length <= length) {
      return this;
    }

    return '${substring(0, length)}...';
  }

  String stripHyperlinks() => replaceAll(
      RegExp(
          r"(http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?"),
      "");
}

final decimalWithCommasFormat = NumberFormat("#,##0.###");
