import 'package:logging/logging.dart';

extension LogRecordExtensions on LogRecord {
  Map<String, dynamic> toJson() => {
        'logger': loggerName,
        'level': level.toString(),
        'time': time.toIso8601String(),
        'message': message,
        'stackTrace': stackTrace?.toString() ?? 'NONE',
      };
}
