import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import 'snackbar_builder.dart';

Future copyToClipboard({
  required BuildContext context,
  required String text,
  String message = 'Copied to clipboard',
}) async {
  await Clipboard.setData(ClipboardData(text: text));
  if (context.mounted) {
    buildSnackbar(context, message);
  }
}
