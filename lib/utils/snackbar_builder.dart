import 'package:flutter/material.dart';

Future<ScaffoldFeatureController<SnackBar, SnackBarClosedReason>?>
    buildSnackbar(BuildContext context, String message,
        {int durationSec = 3}) async {
  if (!context.mounted) {
    return null;
  }
  final snackBar = SnackBar(
    content: SelectableText(message),
    duration: Duration(seconds: durationSec),
    showCloseIcon: true,
  );
  return ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
