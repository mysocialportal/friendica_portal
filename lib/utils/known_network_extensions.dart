import '../models/timeline_network_info.dart';

extension KnownNetworkExtensions on KnownNetworks {
  String get labelName => switch (this) {
        KnownNetworks.activityPub => 'ActivityPub',
        KnownNetworks.bluesky => 'Bluesky',
        KnownNetworks.bookwyrm => 'BookWyrm',
        KnownNetworks.calckey => 'Calckey',
        KnownNetworks.diaspora => 'Diaspora',
        KnownNetworks.drupal => 'Drupal',
        KnownNetworks.firefish => 'Firefish',
        KnownNetworks.friendica => 'Friendica',
        KnownNetworks.funkwhale => 'Funkwhale',
        KnownNetworks.gnu_social => 'GNU Social',
        KnownNetworks.hometown => 'Hometown',
        KnownNetworks.hubzilla => 'Hubzilla',
        KnownNetworks.kbin => 'Kbin',
        KnownNetworks.lemmy => 'Lemmy',
        KnownNetworks.mastodon => 'Mastodon',
        KnownNetworks.nextcloud => 'Nextcloud',
        KnownNetworks.peertube => 'PeerTube',
        KnownNetworks.pixelfed => 'Pixelfed',
        KnownNetworks.pleroma => 'Pleroma',
        KnownNetworks.plume => 'Plume',
        KnownNetworks.red => 'Red',
        KnownNetworks.redmatrix => 'RedMatrix',
        KnownNetworks.socialhome => 'Socialhome',
        KnownNetworks.threads => 'Threads',
        KnownNetworks.wordpress => 'WordPress',
        KnownNetworks.unknown => 'Unknown',
      };

  String get forkAwesomeUnicode => switch (this) {
        KnownNetworks.activityPub => '\uf2f2',
        KnownNetworks.bluesky => '\uf111',
        KnownNetworks.bookwyrm => '\uf02d',
        KnownNetworks.calckey => '\uf1ec',
        KnownNetworks.diaspora => '\uf2e5',
        KnownNetworks.drupal => '\uf1a9',
        KnownNetworks.firefish => '\uf06d',
        KnownNetworks.friendica => '\uf2e6',
        KnownNetworks.funkwhale => '\uf339',
        KnownNetworks.gnu_social => '\uf2e7',
        KnownNetworks.hometown => '\uf2e1',
        KnownNetworks.hubzilla => '\uf2eb',
        KnownNetworks.kbin => '\uf058',
        KnownNetworks.lemmy => '\uf0c0',
        KnownNetworks.mastodon => '\uf2e1',
        KnownNetworks.nextcloud => '\uf307',
        KnownNetworks.peertube => '\uf2e4',
        KnownNetworks.pixelfed => '\uf314',
        KnownNetworks.pleroma => '\uf324',
        KnownNetworks.plume => '\uf356',
        KnownNetworks.red => '\uf2eb',
        KnownNetworks.redmatrix => '\uf2eb',
        KnownNetworks.socialhome => '\uf2ec',
        KnownNetworks.threads => '\uf16d',
        KnownNetworks.wordpress => '\uf19a',
        KnownNetworks.unknown => '\uf059',
      };

  String get dSocFontUnicode => switch (this) {
        KnownNetworks.activityPub => '\uEA02',
        KnownNetworks.bluesky => '\uEA06',
        KnownNetworks.bookwyrm => '\uEA07',
        KnownNetworks.calckey => '\uEA12',
        KnownNetworks.diaspora => '\uEA09',
        KnownNetworks.drupal => '\uEA0B',
        KnownNetworks.firefish => '\uEA12',
        KnownNetworks.friendica => '\uEA1A',
        KnownNetworks.funkwhale => '\uEA1B',
        KnownNetworks.gnu_social => '\uEA20',
        KnownNetworks.hometown => '\uEA25',
        KnownNetworks.hubzilla => '\uEA26',
        KnownNetworks.kbin => '\uEA29',
        KnownNetworks.lemmy => '\uEA2B',
        KnownNetworks.mastodon => '\uEA2D',
        KnownNetworks.nextcloud => '\uEA35',
        KnownNetworks.peertube => '\uEA3E',
        KnownNetworks.pixelfed => '\uEA40',
        KnownNetworks.pleroma => '\uEA41',
        KnownNetworks.plume => '\uEA42',
        KnownNetworks.red => '\uEA46',
        KnownNetworks.redmatrix => '\uEA46',
        KnownNetworks.socialhome => '\uEA4A',
        KnownNetworks.threads => '\uEA52',
        KnownNetworks.wordpress => '\uEA58',
        KnownNetworks.unknown => '\uEA10',
      };
}

extension TimelineNetworkInfoExtensions on TimelineNetworkInfo {
  String get labelName => network.labelName;

  String get forkAwesomeUnicode => network.forkAwesomeUnicode;

  String get dSocFontUnicode => network.dSocFontUnicode;
}
