import 'package:color_blindness/color_blindness.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

const _seedColor = Colors.indigo;
final _lightScheme = ColorScheme.fromSeed(
  seedColor: _seedColor,
  brightness: Brightness.light,
);

final _darkScheme = ColorScheme.fromSeed(
  seedColor: _seedColor,
  brightness: Brightness.dark,
);

ThemeData buildTheme({
  required Brightness brightness,
  ColorBlindnessType blindnessType = ColorBlindnessType.none,
}) {
  final baseScheme =
      brightness == Brightness.light ? _lightScheme : _darkScheme;
  late final ColorScheme scheme;

  if (!kReleaseMode && blindnessType != ColorBlindnessType.none) {
    scheme = colorBlindnessColorScheme(baseScheme, blindnessType);
  } else {
    scheme = baseScheme;
  }

  return ThemeData(
    colorScheme: scheme,
    brightness: brightness,
    useMaterial3: true,
  );
}
