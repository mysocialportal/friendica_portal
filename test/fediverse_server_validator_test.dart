// import 'package:flutter_test/flutter_test.dart';
// import 'package:relatica/services/fediverse_server_validator.dart';
//
// //TODO rewrite test to work with RP
// void main() {
//   test('Test against Diaspora Server', () async {
//     await testDomain('diasp.org', 'diaspora');
//   });
//
//   test('Test against Friendica Server', () async {
//     await testDomain('friendica.myportal.social', 'friendica');
//   });
//
//   test('Test against Mastodon Server', () async {
//     await testDomain('mastodon.social', 'mastodon');
//   });
//
//   test('Test against GNUSocial Server', () async {
//     await testDomain('gnusocial.net', 'gnusocial');
//   });
//
//   test('Test against MissKey Server', () async {
//     await testDomain('misskey.io', 'misskey');
//   });
//
//   test('Test against HubZilla Server', () async {
//     await testDomain('hub.hubzilla.de', 'redmatrix');
//   });
//
//   test('Test against PeerTube Server', () async {
//     await testDomain('tilvids.com', 'peertube');
//   });
//
//   test('Test against PixelFed Server', () async {
//     await testDomain('pixels.gsi.li', 'pixelfed');
//   });
//
//   test('Test against Funkwhale Server', () async {
//     await testDomain('open.audio', 'funkwhale');
//   });
//
//   test('Test against Akkoma Server', () async {
//     await testDomain('social.kernel.org', 'akkoma');
//   });
//
//   test('Test against Pleroma Server', () async {
//     await testDomain('stereophonic.space', 'pleroma');
//   });
//
//   test('Test against non-fediverse server', () async {
//     final result =
//         await FediverseServiceValidator.refreshServerData('myportal.social');
//     expect(result.isFediverse, equals(false));
//   });
// }
//
// Future<void> testDomain(String domain, String softwareName) async {
//   final result = await FediverseServiceValidator.refreshServerData(domain);
//   expect(result.isFediverse, equals(true));
//   expect(result.domainName, equals(domain));
//   expect(result.softwareName, equals(softwareName));
// }
