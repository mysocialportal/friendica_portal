# Relatica Beta Testing Program Notes

These are notes for the Relatica beta testers. Very import sections of this page
to check are:

* ["Things that work"](#things-that-work) to see what is working well
* ["Things that are broken"](#broken-and-hopefully-fixed-in-the-very-near-future) so
  you know how to avoid problems using the application in its current state,
  don't report duplicate bugs, etc.
* ["Cumbersome things"](#cumbersome-and-hopefully-improved-in-the-very-near-future)
  for the same reason as the above "broken" things.
* [The CHANGELOG](CHANGELOG.md) to see what has changed with each recent version
* [Install Instructions](install.md) to see how to install the application
* [Community Communications](README.md#community-and-support) to know how to
  get in touch with other members of the community, developers, etc.

## Introduction

At the present time it is possible to use Relatica as your daily driver for
functions that have already been implemented. I've been using it as my
primary Friendica interactions for over a month as of writing this in
January 2023. However, usable doesn't mean feature complete, even for the
subset of features I want to implement. Usable also doesn't mean bug free
experience. It can still be useful to people which is why I'm opening up
access to it right now. Having said that, this is an early beta program.
Since not everyone that is considering using the software at this point may
be familiar with that aspect of the software development process I
wanted to lay out some expectations before getting into the small details

### Expectations For Early Beta Testers

* Things that look like they are finished are not
* A lot of things look very not finished
* Download regular updates to get the latest and greatest features and fixes and feedback
* Check in on the CHANGELOG, Issue tracker, etc. for updates
* Some things that look finished may end up changing to improve overall user experience (the "Who
  moved the gas pedal?" problem)
* Don't suffer in silence with problems. The beta program is about getting the feedback to what
  works, what doesn't, etc.
  Provide feedback via the Matrix room, for interactive discussions, or the GitLab issue tracker for
  larger bug tracking.
* Feedback should be productive and specific while being as light a load on the beta tester as
  possible:
    * BAD: "This thing is crap!"
    * BAD: "It crashed!!!!"
    * Good: "When I click on a notification box it didn't seem to do anything until I clicked in a
      very specific area"
    * Good: "The app seemed to freeze up when I kept scrolling down my timeline for a long time (
      like half an hour)"
    * Good: "I'm finding X feature hard to use because Y is confusing"
    * Good: "I am trying to do X with Friendica but I don't see a way to do it. Could that be
      implemented in the app?"

## Application Status and Roadmap

### Things that work

* Logging in with username/password and OAuth. These are stored using the OS specific key vaults
* Having multiple accounts logged in at the same time and switching between them
* Writing public and private posts
    * For private posts have to select one of the Groups you have users setup for or all followers.
    * Typing @ brings up a list of all known fediverse accounts that the app has ever seen as you
      type (but not all that your server has seen)
    * Typing # brings up a list of all known hashtags that the app has ever seen as you type (but
      not all that your server has seen)
    * Very basic markdown (wrapping in stars for bold/italics, bulletted list)
    * Explicit link preview builder
    * Adding new images to posts
        * Image posting with easy means of adding ALT text
    * Attaching existing images to posts
* Editing posts and comments
* Resharing posts
* Direct (private) messaging
* Group creation, deletion, renaming
* Server-side searching tied into profiles, posts, hashtags
* Browsing timelines including from groups you've created. In this app "Home" is the same as the "
  Network" (grid) button in Frio and "Yours" is the equivalent of the Home button in the app.
* Browsing notifications and marking individual ones read or all read
* Browsing and editing image galleries and the images within it.
* Browsing your contacts or searching contacts that are known by your app
* Adjudicating follow requests
* Going to individual profiles to see their posts, follow/unfollow, add/remove from groups
* A "low bandwidth" mode that defers loading of images until you click on them, unless the app
  already has that image cached locally.
* Light/dark mode selection
* Opening posts, comments, and profiles in the "external" browser
* Copying posts, comments, image descriptions, direct messages, urls, etc.
* Playing videos within the app on iOS and Android.
* Refresh timelines, galleries, and posts but loading newer or older posts has
  specific buttons (this will probably change in the near future)
* Refresh notifications and contacts gets updates to that respective data (this may
  change in the near future)
* Show list of who liked and reshared posts/comments
* Detailed information about users, such as description fields
* Notifications browsing and dismissing
* Search through server using freeform text or "%" wild card for: accounts, statuses, hashtags
* Direct Load links to Fediverse posts and accounts within the app
* Responsive design (limited for now)
* Opening accounts, tags, etc within posts within the app itself through user's home server
* User configurable Server blocking
* Smarter hashtag and user account storage/searching

### Big things I want to have working in the near future:

* More timeline types like Comments, "By Activity", etc.
* Fix below identified issues
* Real paging across the board
    * Comments
* Offline caching of older content
* Dynamic search of locally known posts and hashtags.
* Make forum posts
* Internationalization
* Better screen reader interactions

### Further out things I want to have working:

* Profile editor
* Being able to ignore/unignore users
* Events

### Things I don't envision implementing in the foreseeable future:

* Account creation through the application
* Creating new forums through the application
* Site administration
* Adding videos or files to posts

## General behaviors/problems to be aware of:

### Broken and hopefully fixed in the very near future:

* Paging for some of the endpoints either isn't wired in yet or is not working as needed server
  side. That includes things like:
    * Friend requests
    * Comments on posts
    * Tags
    * Blocked user list

### Fixed in prior releases but may be issues on older Friendica servers:

* Sending direct messages only works if you are logged in with username/password due to limitations
  of the existing DM (Fixed in Friendica 2023.04)
  API. (Fixed in Friendica 2023.04)
* Resharing of Diaspora federated posts is currently broken server side. All other posts should be
  reshareable. (Fixed in Friendica 2023.04)
* Content Warnings/Spoiler Text on *posts* aren't federating over to Mastodon well so only use it on
  Comments for now (fixed on servers running Friendica 2023.04)
* ALT text on images should not have any quotation marks as it breaks when federating over to
  Diaspora for the time being (fixed on servers running Friendica 2023.04)
* Blocked/ignored user's content is still returned by the API (Fixed in Friendica 2023.04)

### Cumbersome and hopefully improved in the very near future:

* On Linux you will need to login to the key manager and unlock it before opening the app. Some
  Linux versions do this
  automatically while others do not.
* The "in fill" problem: Timelines fill only at the ends with at most 20 posts per call. So let's
  say you logged in at 09:00 and the initial pulls went from 07:00 to 09:00:
  ``` 
  07:00=====9:00
  ```

  While browsing you asked for older posts that pulled posts back to 06:00 and up to the time you
  asked for more, say 09:30 so the timeline looks like:
  ```
  06:00=========09:30
  ```
  Now at lunch time you come back and ask for newer posts. It will pull the last 20 posts from 12:00
  which say took you back to 11:00.
  The timeline looks contiguous but it really is:
  ```
  06:00=========09:30          11:00=====12:00
  ```
  There may be posts in the 09:30 to 11:00 gap or may not. Right now there is no way to fill
