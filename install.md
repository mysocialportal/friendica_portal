# Relatica Installation Instructions

**NOTE: Relatica is in an early beta status. Use at own risk.**
For more information about the current beta testing program
[see the program's notes page](beta-program.md).

# Latest Binaries:

* Android v0.15.0 Is available by invitation through Play Store beta (please see me for access)
  or [this self-installable ZIP file](https://mysocialportal-relatica.nyc3.cdn.digitaloceanspaces.com/v0.15.0/relatica_v0.15.0.apk.zip)
* iPhone/iPad v0.15.0: This is only available through TestFlight. Please contact me for access.
* [Windows (Intel) v0.15.0](https://mysocialportal-relatica.nyc3.cdn.digitaloceanspaces.com/v0.15.0/relatica_v0.15.0_win_x64.zip)
* macOS v0.15.0:This is only available through TestFlight. Please contact me for access.
* [Linux v0.15.0 (tested on Ubuntu 20 and 22)](https://mysocialportal-relatica.nyc3.cdn.digitaloceanspaces.com/v0.15.0/relatica_v0.15.0_linux_x64.zip)

## Mobile

### Android

1. Configure your phone to allow installing APKs through the browser.
   See [this LifeWire article](https://www.lifewire.com/install-apk-on-android-4177185) for
   instructions.
2. Download the latest Android version (see above for versions).
3. Unzip the archive and extract the apk file in your file browser
4. Click on the APK file and follow the prompts to allow the Installation

Note: If the application was previously installed it may complete with a "not installed".
Manually uninstall the app and try again.

### iPhone/iPad

1. Contact the development team for access to the TestFlight program.
2. Install the TestFlight application from the Apple Store
3. You will receive an invitation email that you must open on a phone or tablet
4. Open the TestFlight app to see the Relatica app ready for testing
5. Subsequent releases during the beta program will be pushed to TestFlight on each build.

## Desktop

### macOS

1. Download the latest version (see above)
2. Unzip the archive.
3. Copy to wherever you want to store the Application (Applications folder is the usual place)

Note: that the username/password are stored in macOS's keychain. You will need to provide a password
to this when you use the app for the first time. If you select "Always Allow" then you should only
be prompted for it if there is a major application change. If you selecte "Always" you will be
prompted again. The system does not work without the storage mechanism at this time.

### Linux

1. Install libmpv-dev, mpv, libsecret-1-0, and libjsoncpp25 libraries from your system's package
   manager.
2. Download the latest version (see above)
2. Unzip the archive.
3. Copy to wherever you want to store the applications
4. Run the application with `./relatica`

Note #1: This LD_LIBRARY_PATH issue is a temporary one while a build/deploy bug is being
worked out with one of the libraries that the application uses.

Note #2: At this time the library dependencies on libsecret and libjsoncpp are a bit finicky.
The build definitely works on Ubuntu 22 or Linux distributions built on Ubuntu 22. Testing
on other distros hasn't happened yet. Please report results back and we will try to provide
builds for other distros as they are needed.

Note #3: The app uses Linux's secret storage mechanism. Under testing under Cinnamon the
access to the storage is reset whenever the user logs out. You therefore will be prompted
for the password to the secret storage the first time you run the application after logging
in or rebooting.

### Windows

1. Download the latest version (see above)
2. Unzip the archive.
3. Copy to wherever you want to store the application
4. Open up the folder and double-click on the icon
5. If Windows defender puts up a warning screen hit "more info" to bring up the "Run anyway" button.
6. 
